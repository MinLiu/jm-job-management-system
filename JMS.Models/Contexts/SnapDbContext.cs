﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace JMS.Models
{
    public class SnapDbContext : IdentityDbContext<User>
    {
        public SnapDbContext() : base("name=DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Disable cascade delete by default for one-to-many relationships.
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            // Override default convertion for Decimals.
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention(28, 5)); //Set max decimal places to 5

            // Specify entities which will cascade delete.
            modelBuilder.Entity<User>().HasMany(q => q.Claims).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Logins).WithRequired().WillCascadeOnDelete();
            modelBuilder.Entity<User>().HasMany(q => q.Roles).WithRequired().WillCascadeOnDelete();

            modelBuilder.Entity<UserRoleOption>().HasRequired(q => q.IdentityRole).WithRequiredDependent();

            //Add Cascade Delete Roles Here

            /*             
                DropForeignKey("dbo.AspNetUsers", "CompanyID", "dbo.Companies");
                DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
                DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");             
             */

            base.OnModelCreating(modelBuilder);
        }

        public static SnapDbContext Create() {  return new SnapDbContext();  }

        //Company Entities
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<UserRoleOption> UserRoleOptions { get; set; }
        
        public virtual DbSet<ModuleType> ModuleTypes { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }

        //Clients Entities
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<ClientAddress> ClientAddresses { get; set; }
        public virtual DbSet<ClientAttachment> ClientAttachments { get; set; }
        public virtual DbSet<ClientContact> ClientContacts { get; set; }
        public virtual DbSet<ClientTagItem> ClientTagItems { get; set; }
        public virtual DbSet<ClientEvent> ClientEvents { get; set; }
        public virtual DbSet<ClientType> ClientTypes { get; set; }


        //Suppliers Entities
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<SupplierContact> SupplierContacts { get; set; }
        public virtual DbSet<SupplierType> SupplierTypes { get; set; }

        //Products Entities
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductPrice> ProductPrices { get; set; }
        public virtual DbSet<ProductCost> ProductCosts { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductKitItem> ProductKitItems { get; set; }
        public virtual DbSet<ProductAssociative> ProductAssociatives { get; set; }
        public virtual DbSet<ProductAlternative> ProductAlternatives { get; set; }
        public virtual DbSet<ProductLink> ProductLinks { get; set; }
        public virtual DbSet<Labour> Labours { get; set; }

        public virtual DbSet<Currency> Currencies { get; set; }
        

        //Settings & Others Entities
        public virtual DbSet<Fruitful.Email.EmailSetting> EmailSettings { get; set; }
        public virtual DbSet<ClientTag> ClientTags { get; set; }
        
        
        //Job Entities
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobStatusHistory> JobStatusHistories { get; set; }
        public virtual DbSet<JobStatus> JobStatuses { get; set; }
        public virtual DbSet<JobType> JobTypes { get; set; }
        public virtual DbSet<JobUrgencyLevel> JobUrgencyLevels { get; set; }
        public virtual DbSet<JobIssue> JobIssues { get; set; }
        public virtual DbSet<JobDesignStatus> JobDesignStatuses { get; set; }
        public virtual DbSet<JobWorkshopStatus> JobWorkshopStatuses { get; set; }
        public virtual DbSet<JobElectricalStatus> JobElectricalStatuses { get; set; }
        public virtual DbSet<JobSoftwareStatus> JobSoftwareStatuses { get; set; }



        // Department Entities
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<DepartmentEquipment> DepartmentEquipments { get; set; }
        public virtual DbSet<DepartmentPurchase> DepartmentPurchases { get; set; }

        // Manufacturer Entities
        public virtual DbSet<Manufacturer> Manufacturers { get; set; }

        // Equipment Entities
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<EquipStatus> EquipStatuses { get; set; }
        public virtual DbSet<EquipmentAsset> EquipmentAssets { get; set; }

        // Location Entities
        public virtual DbSet<Location> Locations { get; set; }

        // Project Code Entities
        public virtual DbSet<ProjectCode> ProjectCodes { get; set; }

        // Engineer Entities
        public virtual DbSet<Engineer> Engineers { get; set; }
        public virtual DbSet<EngineerJob> EngineerJobs { get; set; }
        public virtual DbSet<EngineerEquipment> EngineerEquipments { get; set; }

        // Requestor Entities
        public virtual DbSet<Requestor> Requestors { get; set; }
        public virtual DbSet<RequestorJob> RequestorJobs { get; set; }
        public virtual DbSet<RequestorPurchase> RequestorPurchases { get; set; }
        public virtual DbSet<RequestorEquipment> RequestorEquipments { get; set; }
        public virtual DbSet<OnBehalfOfPurchase> OnBehalfOfPurchase { get; set; }

        // Purchase Entities
        public virtual DbSet<Purchase> Purchases { get; set; }
        public virtual DbSet<PurchaseStatus> PurchaseStatuses { get; set; }
        public virtual DbSet<MaterialType> MaterialTypes { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethods { get; set; }

        // Statistics Entities
        public virtual DbSet<MonthlyJobsOnHold> MonthlyJobsOnHold { get; set; }
        public virtual DbSet<MonthlyJobsOpened> MonthlyJobsOpened { get; set; }


        public static List<User> GetCompanyUsers(User CurrentUser)
        {
            return new List<User>() { CurrentUser };
        }
    }
}

