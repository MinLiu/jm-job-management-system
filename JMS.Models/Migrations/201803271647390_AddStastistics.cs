namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStastistics : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MonthlyJobsOnHolds",
                c => new
                    {
                        Month = c.DateTime(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Month);
            
            CreateTable(
                "dbo.MonthlyJobsOpeneds",
                c => new
                    {
                        Month = c.DateTime(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Month);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MonthlyJobsOpeneds");
            DropTable("dbo.MonthlyJobsOnHolds");
        }
    }
}
