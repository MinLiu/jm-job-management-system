namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUrgencylevels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobUrgencyLevels", "HexColour", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.JobUrgencyLevels", "HexColour");
        }
    }
}
