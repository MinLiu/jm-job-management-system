namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientAddresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        County = c.String(),
                        Town = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        ClientTypeID = c.Guid(),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Website = c.String(),
                        Notes = c.String(),
                        SageAccountReference = c.String(),
                        Created = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ClientTypes", t => t.ClientTypeID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID)
                .Index(t => t.ClientTypeID);
            
            CreateTable(
                "dbo.ClientAttachments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Filename = c.String(),
                        FilePath = c.String(),
                        Format = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.ClientContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .Index(t => t.ClientID);
            
            CreateTable(
                "dbo.ClientEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        AssignedUserID = c.String(maxLength: 128),
                        CreatorID = c.String(maxLength: 128),
                        OpportunityID = c.Guid(),
                        ClientID = c.Guid(),
                        ClientContactID = c.Guid(),
                        Timestamp = c.DateTime(nullable: false),
                        Message = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AssignedUserID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientContacts", t => t.ClientContactID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorID)
                .Index(t => t.CompanyID)
                .Index(t => t.AssignedUserID)
                .Index(t => t.CreatorID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientContactID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Token = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Company_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.Company_ID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Company_ID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AccountNo = c.String(),
                        SignupDate = c.DateTime(nullable: false),
                        AccountClosed = c.Boolean(nullable: false),
                        DeleteData = c.Boolean(nullable: false),
                        Name = c.String(nullable: false),
                        Motto = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        Town = c.String(),
                        County = c.String(),
                        Postcode = c.String(),
                        Country = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        VatNumber = c.String(),
                        RegNumber = c.String(),
                        LogoImageURL = c.String(),
                        ScreenLogoImageURL = c.String(),
                        ThemeColor = c.String(),
                        DefaultVatRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                        DefaultCurrencyID = c.Int(nullable: false),
                        UseTeams = c.Boolean(nullable: false),
                        UseHierarchy = c.Boolean(nullable: false),
                        AccessSameLevelHierarchy = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.DefaultCurrencyID)
                .Index(t => t.DefaultCurrencyID);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 3),
                        Name = c.String(nullable: false, maxLength: 128),
                        Symbol = c.String(nullable: false, maxLength: 3),
                        UseInCommerce = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientTagItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ClientID = c.Guid(nullable: false),
                        ClientTagID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientID)
                .ForeignKey("dbo.ClientTags", t => t.ClientTagID)
                .Index(t => t.ClientID)
                .Index(t => t.ClientTagID);
            
            CreateTable(
                "dbo.ClientTags",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.ClientTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.DepartmentEquipments",
                c => new
                    {
                        DepartmentID = c.Guid(nullable: false),
                        EquipmentID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentID, t.EquipmentID })
                .ForeignKey("dbo.Departments", t => t.DepartmentID)
                .ForeignKey("dbo.Equipments", t => t.EquipmentID)
                .Index(t => t.DepartmentID)
                .Index(t => t.EquipmentID);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RefID = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        Code = c.String(nullable: false),
                        Contact = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RefID = c.Int(nullable: false),
                        EquipNo = c.String(nullable: false),
                        AssetNo = c.String(),
                        Title = c.String(),
                        LocationID = c.Guid(),
                        ManufacturerID = c.Guid(),
                        EquipType = c.String(),
                        DrawingNo = c.String(),
                        SerialNo = c.String(),
                        ProjectCode = c.String(),
                        PMNo = c.String(),
                        EquipStatusID = c.Int(nullable: false),
                        DOMorPurchase = c.DateTime(),
                        RiskAssessmentRef = c.String(),
                        SoftwareAndVersion = c.String(),
                        PCReference = c.String(),
                        Note = c.String(),
                        PCControlled = c.Boolean(nullable: false),
                        PCNetworked = c.Boolean(nullable: false),
                        AlarmInterlock = c.Boolean(nullable: false),
                        ExtractInterlock = c.Boolean(nullable: false),
                        ElectricalVisualInspectionDate = c.DateTime(),
                        ElectricalFullInspectionDate = c.DateTime(),
                        ElectricalTestNumber = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EquipStatus", t => t.EquipStatusID)
                .ForeignKey("dbo.Locations", t => t.LocationID)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerID)
                .Index(t => t.LocationID)
                .Index(t => t.ManufacturerID)
                .Index(t => t.EquipStatusID);
            
            CreateTable(
                "dbo.EngineerEquipments",
                c => new
                    {
                        EngineerID = c.Guid(nullable: false),
                        EquipmentID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EngineerID, t.EquipmentID })
                .ForeignKey("dbo.Engineers", t => t.EngineerID)
                .ForeignKey("dbo.Equipments", t => t.EquipmentID)
                .Index(t => t.EngineerID)
                .Index(t => t.EquipmentID);
            
            CreateTable(
                "dbo.Engineers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        ShortName = c.String(),
                        EngineerID = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EngineerJobs",
                c => new
                    {
                        EngineerID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EngineerID, t.JobID })
                .ForeignKey("dbo.Engineers", t => t.EngineerID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.EngineerID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Long(nullable: false),
                        Title = c.String(),
                        DepartmentID = c.Guid(),
                        LocationID = c.Guid(),
                        ProjectCode = c.String(),
                        DrawingNo = c.String(),
                        EquipmentNo = c.String(),
                        JobTypeID = c.Guid(),
                        UrgencyLevelID = c.Int(nullable: false),
                        ExternalProjectID = c.String(),
                        SubmittedDate = c.DateTime(),
                        RequiredByDate = c.DateTime(),
                        PlannedStartDate = c.DateTime(),
                        EstCompleteionDate = c.DateTime(),
                        PercentComplete = c.Int(nullable: false),
                        EstIssuesCompletionDate = c.DateTime(),
                        ClosedDate = c.DateTime(),
                        OnHarvest = c.Boolean(nullable: false),
                        StageConcept = c.Boolean(nullable: false),
                        StageLaunch = c.Boolean(nullable: false),
                        StagePrelimDesign = c.Boolean(nullable: false),
                        StageFinalDesign = c.Boolean(nullable: false),
                        StageConstruction = c.Boolean(nullable: false),
                        StageDelivery = c.Boolean(nullable: false),
                        StatusJob = c.String(),
                        StatusInfoRequired = c.Boolean(nullable: false),
                        StatusDesign = c.String(),
                        StatusElectrical = c.String(),
                        StatusSoftware = c.String(),
                        StatusWorkshop = c.String(),
                        StatusWorkshopEstHrs = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departments", t => t.DepartmentID)
                .ForeignKey("dbo.Locations", t => t.LocationID)
                .ForeignKey("dbo.JobUrgencyLevels", t => t.UrgencyLevelID)
                .ForeignKey("dbo.JobTypes", t => t.JobTypeID)
                .Index(t => t.DepartmentID)
                .Index(t => t.LocationID)
                .Index(t => t.JobTypeID)
                .Index(t => t.UrgencyLevelID);
            
            CreateTable(
                "dbo.JobIssues",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        EstCompleteDate = c.DateTime(nullable: false),
                        Details = c.String(),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Code = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RequestorJobs",
                c => new
                    {
                        RequestorID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RequestorID, t.JobID })
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .ForeignKey("dbo.Requestors", t => t.RequestorID)
                .Index(t => t.RequestorID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.Requestors",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RefID = c.Long(nullable: false),
                        FirstName = c.String(),
                        Surname = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RequestorEquipments",
                c => new
                    {
                        RequestorID = c.Guid(nullable: false),
                        EquipmentID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RequestorID, t.EquipmentID })
                .ForeignKey("dbo.Equipments", t => t.EquipmentID)
                .ForeignKey("dbo.Requestors", t => t.RequestorID)
                .Index(t => t.RequestorID)
                .Index(t => t.EquipmentID);
            
            CreateTable(
                "dbo.RequestorPurchases",
                c => new
                    {
                        RequestorID = c.Guid(nullable: false),
                        PurchaseID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RequestorID, t.PurchaseID })
                .ForeignKey("dbo.Purchases", t => t.PurchaseID)
                .ForeignKey("dbo.Requestors", t => t.RequestorID)
                .Index(t => t.RequestorID)
                .Index(t => t.PurchaseID);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Title = c.String(nullable: false),
                        PartNo = c.String(),
                        SupplierID = c.Guid(),
                        MaterialTypeID = c.Guid(),
                        MaterialSpec = c.String(),
                        QtyOrdered = c.Decimal(nullable: false, precision: 28, scale: 5),
                        QtyReceived = c.Decimal(nullable: false, precision: 28, scale: 5),
                        UnitPrice = c.Decimal(nullable: false, precision: 28, scale: 5),
                        CurrencyID = c.Int(nullable: false),
                        PurchaseTotal = c.Decimal(nullable: false, precision: 28, scale: 5),
                        PONo = c.String(),
                        PurchaseStatusID = c.Int(nullable: false),
                        DateOrdered = c.DateTime(),
                        DateReceived = c.DateTime(),
                        PaymentMethodID = c.Int(),
                        ProjectCode = c.String(),
                        WorkshopNo = c.String(),
                        DateDue = c.DateTime(),
                        LeadTimeApprx = c.String(),
                        SupplierRef = c.String(),
                        RelatedJobID = c.Guid(),
                        HistoricJobID = c.String(),
                        DepartmentID = c.Guid(),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.PaymentMethods", t => t.PaymentMethodID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .ForeignKey("dbo.MaterialTypes", t => t.MaterialTypeID)
                .Index(t => t.SupplierID)
                .Index(t => t.MaterialTypeID)
                .Index(t => t.CurrencyID)
                .Index(t => t.PaymentMethodID);
            
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RefID = c.Int(nullable: false),
                        SupplierTypeID = c.Guid(),
                        Name = c.String(nullable: false),
                        AccountDetail = c.String(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SupplierTypes", t => t.SupplierTypeID)
                .Index(t => t.SupplierTypeID);
            
            CreateTable(
                "dbo.SupplierContacts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SupplierID = c.Guid(nullable: false),
                        Name = c.String(),
                        PositionHeld = c.String(),
                        Telephone = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Suppliers", t => t.SupplierID)
                .Index(t => t.SupplierID);
            
            CreateTable(
                "dbo.SupplierTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobStatusHistories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JobID = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.JobUrgencyLevels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EquipStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        RefID = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmailSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                        Host = c.String(),
                        Port = c.Int(nullable: false),
                        EnableSSL = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ErrorLogs",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        CompanyID = c.String(),
                        CompanyName = c.String(),
                        UserID = c.String(),
                        UserName = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        TargetSite = c.String(),
                        SourceError = c.String(),
                        StackTrace = c.String(),
                        URL = c.String(),
                        GetValues = c.String(),
                        PostValues = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Labours",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        HourlyRate = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.MaterialTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ModuleTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        IndexURL = c.String(),
                        EditURL = c.String(),
                        HexColour = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductLinks",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        LinkedProductID = c.Guid(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.LinkedProductID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.LinkedProductID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        ProductCode = c.String(),
                        Description = c.String(),
                        DetailedDescription = c.String(),
                        Category = c.String(),
                        Unit = c.String(),
                        VendorCode = c.String(),
                        Manufacturer = c.String(),
                        ImageURL = c.String(),
                        AccountCode = c.String(),
                        IsKit = c.Boolean(nullable: false),
                        UseKitPrice = c.Boolean(nullable: false),
                        UsePriceBreaks = c.Boolean(nullable: false),
                        IsBought = c.Boolean(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        VAT = c.Decimal(nullable: false, precision: 28, scale: 5),
                        MinimumQty = c.Int(nullable: false),
                        MaximumQty = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .Index(t => t.CompanyID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductCosts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductKitItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        KitID = c.Guid(nullable: false),
                        ProductID = c.Guid(),
                        IsComment = c.Boolean(nullable: false),
                        Description = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 28, scale: 5),
                        SortPos = c.Int(nullable: false),
                        Product_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Products", t => t.KitID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Products", t => t.Product_ID)
                .Index(t => t.KitID)
                .Index(t => t.ProductID)
                .Index(t => t.Product_ID);
            
            CreateTable(
                "dbo.ProductPrices",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        CurrencyID = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 28, scale: 5),
                        BreakPoint = c.Decimal(nullable: false, precision: 28, scale: 5),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Currencies", t => t.CurrencyID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.CurrencyID);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompanyID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.PurchaseStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoleOptions",
                c => new
                    {
                        IdentityRoleID = c.String(nullable: false, maxLength: 128),
                        CompanyID = c.Guid(),
                        ViewQuotations = c.Boolean(nullable: false),
                        EditQuotations = c.Boolean(nullable: false),
                        DeleteQuotations = c.Boolean(nullable: false),
                        ExportQuotations = c.Boolean(nullable: false),
                        ViewJobs = c.Boolean(nullable: false),
                        EditJobs = c.Boolean(nullable: false),
                        DeleteJobs = c.Boolean(nullable: false),
                        ExportJobs = c.Boolean(nullable: false),
                        ViewInvoices = c.Boolean(nullable: false),
                        EditInvoices = c.Boolean(nullable: false),
                        DeleteInvoices = c.Boolean(nullable: false),
                        ExportInvoices = c.Boolean(nullable: false),
                        ViewPurchaseOrders = c.Boolean(nullable: false),
                        EditPurchaseOrders = c.Boolean(nullable: false),
                        DeletePurchaseOrders = c.Boolean(nullable: false),
                        ExportPurchaseOrders = c.Boolean(nullable: false),
                        ViewDeliveryNotes = c.Boolean(nullable: false),
                        EditDeliveryNotes = c.Boolean(nullable: false),
                        DeleteDeliveryNotes = c.Boolean(nullable: false),
                        ExportDeliveryNotes = c.Boolean(nullable: false),
                        ViewStockControl = c.Boolean(nullable: false),
                        EditStockControl = c.Boolean(nullable: false),
                        ExportStockControl = c.Boolean(nullable: false),
                        ViewCRM = c.Boolean(nullable: false),
                        EditCRM = c.Boolean(nullable: false),
                        DeleteCRM = c.Boolean(nullable: false),
                        ExportCRM = c.Boolean(nullable: false),
                        ViewProducts = c.Boolean(nullable: false),
                        EditProducts = c.Boolean(nullable: false),
                        DeleteProducts = c.Boolean(nullable: false),
                        ExportProducts = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdentityRoleID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.AspNetRoles", t => t.IdentityRoleID)
                .Index(t => t.IdentityRoleID)
                .Index(t => t.CompanyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoleOptions", "IdentityRoleID", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoleOptions", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ProductLinks", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductLinks", "LinkedProductID", "dbo.Products");
            DropForeignKey("dbo.ProductCategories", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ProductPrices", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPrices", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductKitItems", "Product_ID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductKitItems", "KitID", "dbo.Products");
            DropForeignKey("dbo.Products", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.ProductCosts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductCosts", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Products", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Purchases", "MaterialTypeID", "dbo.MaterialTypes");
            DropForeignKey("dbo.Labours", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.Labours", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Jobs", "JobTypeID", "dbo.JobTypes");
            DropForeignKey("dbo.Equipments", "ManufacturerID", "dbo.Manufacturers");
            DropForeignKey("dbo.Equipments", "LocationID", "dbo.Locations");
            DropForeignKey("dbo.Equipments", "EquipStatusID", "dbo.EquipStatus");
            DropForeignKey("dbo.EngineerEquipments", "EquipmentID", "dbo.Equipments");
            DropForeignKey("dbo.Jobs", "UrgencyLevelID", "dbo.JobUrgencyLevels");
            DropForeignKey("dbo.JobStatusHistories", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.RequestorPurchases", "RequestorID", "dbo.Requestors");
            DropForeignKey("dbo.Purchases", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "SupplierTypeID", "dbo.SupplierTypes");
            DropForeignKey("dbo.SupplierContacts", "SupplierID", "dbo.Suppliers");
            DropForeignKey("dbo.RequestorPurchases", "PurchaseID", "dbo.Purchases");
            DropForeignKey("dbo.Purchases", "PaymentMethodID", "dbo.PaymentMethods");
            DropForeignKey("dbo.Purchases", "CurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.RequestorJobs", "RequestorID", "dbo.Requestors");
            DropForeignKey("dbo.RequestorEquipments", "RequestorID", "dbo.Requestors");
            DropForeignKey("dbo.RequestorEquipments", "EquipmentID", "dbo.Equipments");
            DropForeignKey("dbo.RequestorJobs", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "LocationID", "dbo.Locations");
            DropForeignKey("dbo.JobIssues", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.EngineerJobs", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.EngineerJobs", "EngineerID", "dbo.Engineers");
            DropForeignKey("dbo.EngineerEquipments", "EngineerID", "dbo.Engineers");
            DropForeignKey("dbo.DepartmentEquipments", "EquipmentID", "dbo.Equipments");
            DropForeignKey("dbo.DepartmentEquipments", "DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.Clients", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTypes", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Clients", "ClientTypeID", "dbo.ClientTypes");
            DropForeignKey("dbo.ClientTagItems", "ClientTagID", "dbo.ClientTags");
            DropForeignKey("dbo.ClientTags", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.ClientTagItems", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "CreatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientEvents", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Companies", "DefaultCurrencyID", "dbo.Currencies");
            DropForeignKey("dbo.AspNetUsers", "Company_ID", "dbo.Companies");
            DropForeignKey("dbo.ClientEvents", "ClientContactID", "dbo.ClientContacts");
            DropForeignKey("dbo.ClientEvents", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientEvents", "AssignedUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientContacts", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientAttachments", "ClientID", "dbo.Clients");
            DropForeignKey("dbo.ClientAddresses", "ClientID", "dbo.Clients");
            DropIndex("dbo.AspNetUserRoleOptions", new[] { "CompanyID" });
            DropIndex("dbo.AspNetUserRoleOptions", new[] { "IdentityRoleID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ProductCategories", new[] { "CompanyID" });
            DropIndex("dbo.ProductPrices", new[] { "CurrencyID" });
            DropIndex("dbo.ProductPrices", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "Product_ID" });
            DropIndex("dbo.ProductKitItems", new[] { "ProductID" });
            DropIndex("dbo.ProductKitItems", new[] { "KitID" });
            DropIndex("dbo.ProductCosts", new[] { "CurrencyID" });
            DropIndex("dbo.ProductCosts", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "CurrencyID" });
            DropIndex("dbo.Products", new[] { "CompanyID" });
            DropIndex("dbo.ProductLinks", new[] { "LinkedProductID" });
            DropIndex("dbo.ProductLinks", new[] { "ProductID" });
            DropIndex("dbo.Labours", new[] { "CurrencyID" });
            DropIndex("dbo.Labours", new[] { "CompanyID" });
            DropIndex("dbo.JobStatusHistories", new[] { "JobID" });
            DropIndex("dbo.SupplierContacts", new[] { "SupplierID" });
            DropIndex("dbo.Suppliers", new[] { "SupplierTypeID" });
            DropIndex("dbo.Purchases", new[] { "PaymentMethodID" });
            DropIndex("dbo.Purchases", new[] { "CurrencyID" });
            DropIndex("dbo.Purchases", new[] { "MaterialTypeID" });
            DropIndex("dbo.Purchases", new[] { "SupplierID" });
            DropIndex("dbo.RequestorPurchases", new[] { "PurchaseID" });
            DropIndex("dbo.RequestorPurchases", new[] { "RequestorID" });
            DropIndex("dbo.RequestorEquipments", new[] { "EquipmentID" });
            DropIndex("dbo.RequestorEquipments", new[] { "RequestorID" });
            DropIndex("dbo.RequestorJobs", new[] { "JobID" });
            DropIndex("dbo.RequestorJobs", new[] { "RequestorID" });
            DropIndex("dbo.JobIssues", new[] { "JobID" });
            DropIndex("dbo.Jobs", new[] { "UrgencyLevelID" });
            DropIndex("dbo.Jobs", new[] { "JobTypeID" });
            DropIndex("dbo.Jobs", new[] { "LocationID" });
            DropIndex("dbo.Jobs", new[] { "DepartmentID" });
            DropIndex("dbo.EngineerJobs", new[] { "JobID" });
            DropIndex("dbo.EngineerJobs", new[] { "EngineerID" });
            DropIndex("dbo.EngineerEquipments", new[] { "EquipmentID" });
            DropIndex("dbo.EngineerEquipments", new[] { "EngineerID" });
            DropIndex("dbo.Equipments", new[] { "EquipStatusID" });
            DropIndex("dbo.Equipments", new[] { "ManufacturerID" });
            DropIndex("dbo.Equipments", new[] { "LocationID" });
            DropIndex("dbo.DepartmentEquipments", new[] { "EquipmentID" });
            DropIndex("dbo.DepartmentEquipments", new[] { "DepartmentID" });
            DropIndex("dbo.ClientTypes", new[] { "CompanyID" });
            DropIndex("dbo.ClientTags", new[] { "CompanyID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientTagID" });
            DropIndex("dbo.ClientTagItems", new[] { "ClientID" });
            DropIndex("dbo.Companies", new[] { "DefaultCurrencyID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Company_ID" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.ClientEvents", new[] { "ClientContactID" });
            DropIndex("dbo.ClientEvents", new[] { "ClientID" });
            DropIndex("dbo.ClientEvents", new[] { "CreatorID" });
            DropIndex("dbo.ClientEvents", new[] { "AssignedUserID" });
            DropIndex("dbo.ClientEvents", new[] { "CompanyID" });
            DropIndex("dbo.ClientContacts", new[] { "ClientID" });
            DropIndex("dbo.ClientAttachments", new[] { "ClientID" });
            DropIndex("dbo.Clients", new[] { "ClientTypeID" });
            DropIndex("dbo.Clients", new[] { "CompanyID" });
            DropIndex("dbo.ClientAddresses", new[] { "ClientID" });
            DropTable("dbo.AspNetUserRoleOptions");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PurchaseStatus");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.ProductPrices");
            DropTable("dbo.ProductKitItems");
            DropTable("dbo.ProductCosts");
            DropTable("dbo.Products");
            DropTable("dbo.ProductLinks");
            DropTable("dbo.ModuleTypes");
            DropTable("dbo.MaterialTypes");
            DropTable("dbo.Labours");
            DropTable("dbo.JobTypes");
            DropTable("dbo.JobStatus");
            DropTable("dbo.ErrorLogs");
            DropTable("dbo.EmailSettings");
            DropTable("dbo.Manufacturers");
            DropTable("dbo.EquipStatus");
            DropTable("dbo.JobUrgencyLevels");
            DropTable("dbo.JobStatusHistories");
            DropTable("dbo.SupplierTypes");
            DropTable("dbo.SupplierContacts");
            DropTable("dbo.Suppliers");
            DropTable("dbo.PaymentMethods");
            DropTable("dbo.Purchases");
            DropTable("dbo.RequestorPurchases");
            DropTable("dbo.RequestorEquipments");
            DropTable("dbo.Requestors");
            DropTable("dbo.RequestorJobs");
            DropTable("dbo.Locations");
            DropTable("dbo.JobIssues");
            DropTable("dbo.Jobs");
            DropTable("dbo.EngineerJobs");
            DropTable("dbo.Engineers");
            DropTable("dbo.EngineerEquipments");
            DropTable("dbo.Equipments");
            DropTable("dbo.Departments");
            DropTable("dbo.DepartmentEquipments");
            DropTable("dbo.ClientTypes");
            DropTable("dbo.ClientTags");
            DropTable("dbo.ClientTagItems");
            DropTable("dbo.Currencies");
            DropTable("dbo.Companies");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ClientEvents");
            DropTable("dbo.ClientContacts");
            DropTable("dbo.ClientAttachments");
            DropTable("dbo.Clients");
            DropTable("dbo.ClientAddresses");
        }
    }
}
