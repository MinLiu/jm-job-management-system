namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateEquipStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EquipStatus", "HexColour", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EquipStatus", "HexColour");
        }
    }
}
