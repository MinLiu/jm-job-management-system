namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePurchases2 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Purchases", "PurchaseStatusID");
            AddForeignKey("dbo.Purchases", "PurchaseStatusID", "dbo.PurchaseStatus", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Purchases", "PurchaseStatusID", "dbo.PurchaseStatus");
            DropIndex("dbo.Purchases", new[] { "PurchaseStatusID" });
        }
    }
}
