namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProjectCode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectCodes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Code = c.String(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Equipments", "ProjectCodeID", c => c.Guid());
            AddColumn("dbo.Jobs", "ProjectCodeID", c => c.Guid());
            AddColumn("dbo.Purchases", "ProjectCodeID", c => c.Guid());
            CreateIndex("dbo.Purchases", "ProjectCodeID");
            CreateIndex("dbo.Equipments", "ProjectCodeID");
            CreateIndex("dbo.Jobs", "ProjectCodeID");
            AddForeignKey("dbo.Jobs", "ProjectCodeID", "dbo.ProjectCodes", "ID");
            AddForeignKey("dbo.Equipments", "ProjectCodeID", "dbo.ProjectCodes", "ID");
            AddForeignKey("dbo.Purchases", "ProjectCodeID", "dbo.ProjectCodes", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Purchases", "ProjectCodeID", "dbo.ProjectCodes");
            DropForeignKey("dbo.Equipments", "ProjectCodeID", "dbo.ProjectCodes");
            DropForeignKey("dbo.Jobs", "ProjectCodeID", "dbo.ProjectCodes");
            DropIndex("dbo.Jobs", new[] { "ProjectCodeID" });
            DropIndex("dbo.Equipments", new[] { "ProjectCodeID" });
            DropIndex("dbo.Purchases", new[] { "ProjectCodeID" });
            DropColumn("dbo.Purchases", "ProjectCodeID");
            DropColumn("dbo.Jobs", "ProjectCodeID");
            DropColumn("dbo.Equipments", "ProjectCodeID");
            DropTable("dbo.ProjectCodes");
        }
    }
}
