namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJobs1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jobs", "HistoricJID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jobs", "HistoricJID");
        }
    }
}
