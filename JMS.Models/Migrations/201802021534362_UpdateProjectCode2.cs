namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProjectCode2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Purchases", "ProjectCode");
            DropColumn("dbo.Equipments", "ProjectCode");
            DropColumn("dbo.Jobs", "ProjectCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jobs", "ProjectCode", c => c.String());
            AddColumn("dbo.Equipments", "ProjectCode", c => c.String());
            AddColumn("dbo.Purchases", "ProjectCode", c => c.String());
        }
    }
}
