namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePurchases : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DepartmentPurchases",
                c => new
                    {
                        DepartmentID = c.Guid(nullable: false),
                        PurchaseID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentID, t.PurchaseID })
                .ForeignKey("dbo.Departments", t => t.DepartmentID)
                .ForeignKey("dbo.Purchases", t => t.PurchaseID)
                .Index(t => t.DepartmentID)
                .Index(t => t.PurchaseID);
            
            CreateTable(
                "dbo.OnBehalfOfPurchases",
                c => new
                    {
                        RequestorID = c.Guid(nullable: false),
                        PurchaseID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RequestorID, t.PurchaseID })
                .ForeignKey("dbo.Purchases", t => t.PurchaseID)
                .ForeignKey("dbo.Requestors", t => t.RequestorID)
                .Index(t => t.RequestorID)
                .Index(t => t.PurchaseID);
            
            AddColumn("dbo.Purchases", "RefID", c => c.Long(nullable: false));
            DropColumn("dbo.Purchases", "DepartmentID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Purchases", "DepartmentID", c => c.Guid());
            DropForeignKey("dbo.OnBehalfOfPurchases", "RequestorID", "dbo.Requestors");
            DropForeignKey("dbo.OnBehalfOfPurchases", "PurchaseID", "dbo.Purchases");
            DropForeignKey("dbo.DepartmentPurchases", "PurchaseID", "dbo.Purchases");
            DropForeignKey("dbo.DepartmentPurchases", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.OnBehalfOfPurchases", new[] { "PurchaseID" });
            DropIndex("dbo.OnBehalfOfPurchases", new[] { "RequestorID" });
            DropIndex("dbo.DepartmentPurchases", new[] { "PurchaseID" });
            DropIndex("dbo.DepartmentPurchases", new[] { "DepartmentID" });
            DropColumn("dbo.Purchases", "RefID");
            DropTable("dbo.OnBehalfOfPurchases");
            DropTable("dbo.DepartmentPurchases");
        }
    }
}
