namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePurchases3 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Purchases", "RelatedJobID");
            AddForeignKey("dbo.Purchases", "RelatedJobID", "dbo.Jobs", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Purchases", "RelatedJobID", "dbo.Jobs");
            DropIndex("dbo.Purchases", new[] { "RelatedJobID" });
        }
    }
}
