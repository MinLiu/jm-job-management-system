﻿namespace JMS.Models.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<JMS.Models.SnapDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(JMS.Models.SnapDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Currencies.AddOrUpdate(
                new Currency { ID = 1, Code = "GBP", Name = "British Pound", Symbol = "£", UseInCommerce = true },
                new Currency { ID = 2, Code = "EUR", Name = "Euro", Symbol = "€", UseInCommerce = true },
                new Currency { ID = 3, Code = "USD", Name = "United States Dollar", Symbol = "$", UseInCommerce = true }
                //new Currency { ID = 4, Code = "AUD", Name = "Australian Dollar", Symbol = "$" },
                //new Currency { ID = 5, Code = "CAD", Name = "Canadian Dollar", Symbol = "$" },
                //new Currency { ID = 6, Code = "CNY", Name = "Chinese Yuan", Symbol = "¥" },
                //new Currency { ID = 7, Code = "CZK", Name = "Czech Koruna", Symbol = "Kč" },
                //new Currency { ID = 8, Code = "DKK", Name = "Danish Krone", Symbol = "kr." },
                //new Currency { ID = 9, Code = "HKD", Name = "Hong Kong Dollar", Symbol = "$" },
                //new Currency { ID = 10, Code = "HUF", Name = "Hungarian Forint", Symbol = "Ft" },
                //new Currency { ID = 11, Code = "INR", Name = "Indian Rupee", Symbol = "₹" },
                //new Currency { ID = 12, Code = "ILS", Name = "Israeli Shekel", Symbol = "₪" },
                //new Currency { ID = 13, Code = "JPY", Name = "Japanese Yen", Symbol = "¥" },
                //new Currency { ID = 14, Code = "MYR", Name = "Malaysian ringgit", Symbol = "RM" },
                //new Currency { ID = 15, Code = "NZD", Name = "New Zealand Dollar", Symbol = "$" },
                //new Currency { ID = 16, Code = "NOK", Name = "Norwegian Krone", Symbol = "kr" },
                //new Currency { ID = 17, Code = "PLN", Name = "Polish Zloty", Symbol = "zł" },
                //new Currency { ID = 18, Code = "RUB", Name = "Russian Ruble", Symbol = "₽" },
                //new Currency { ID = 19, Code = "SAR", Name = "Saudi Riyal", Symbol = "ر.س" },
                //new Currency { ID = 20, Code = "SGD", Name = "Singapore Dollar", Symbol = "$" },
                //new Currency { ID = 21, Code = "ZAR", Name = "South African Rand", Symbol = "R" },
                //new Currency { ID = 22, Code = "KRW", Name = "South Korean Won", Symbol = "₩" },
                //new Currency { ID = 23, Code = "SEK", Name = "Swedish Krona", Symbol = "kr" },
                //new Currency { ID = 24, Code = "CHF", Name = "Swiss Franc", Symbol = "Fr" },
                //new Currency { ID = 25, Code = "TWD", Name = "Taiwan Dollar", Symbol = "$" },
                //new Currency { ID = 26, Code = "THB", Name = "Thai Baht", Symbol = "	฿" },
                //new Currency { ID = 27, Code = "TRY", Name = "Turkish Lira", Symbol = "₺" }
            );
            context.SaveChanges();

            
            //Add roles            
            context.Roles.AddOrUpdate(
                new IdentityRole { Id = "-1", Name = "Super Admin" },
                new IdentityRole { Id = "1", Name = "Admin" },
                new IdentityRole { Id = "2", Name = "User" },
                new IdentityRole { Id = "3", Name = "Viewer" }
            );

            context.UserRoleOptions.AddOrUpdate(
                new UserRoleOption { IdentityRoleID = "-1", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true },
                new UserRoleOption { IdentityRoleID = "1", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true },
                new UserRoleOption { IdentityRoleID = "2", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true },
                new UserRoleOption { IdentityRoleID = "3", CompanyID = null, ViewQuotations = true, DeleteQuotations = true, EditQuotations = true, DeleteCRM = true, DeleteDeliveryNotes = true, DeleteInvoices = true, DeleteJobs = true, DeleteProducts = true, DeletePurchaseOrders = true, EditCRM = true, EditDeliveryNotes = true, EditInvoices = true, EditJobs = true, EditProducts = true, EditPurchaseOrders = true, EditStockControl = true, ExportCRM = true, ExportDeliveryNotes = true, ExportInvoices = true, ExportJobs = true, ExportProducts = true, ExportPurchaseOrders = true, ExportQuotations = true, ExportStockControl = true, ViewCRM = true, ViewDeliveryNotes = true, ViewInvoices = true, ViewJobs = true, ViewProducts = true, ViewPurchaseOrders = true, ViewStockControl = true }
            );
            context.SaveChanges();



            //Add Company     
            /*
            if (context.Companies.Count() <= 0)
            {
                context.Companies.AddOrUpdate(
                    new Company
                    {
                        //ID = Guid.Parse("bff01aac-69fe-e611-bf05-e03f495631f0"),
                        Name = "Super User Company",
                        Email = "developer@fruitfulgroup.com",
                        SignupDate = DateTime.Now,
                        AccountClosed = false,
                        DefaultCurrencyID = 1,
                    }
                );
                context.SaveChanges();
            }

            var companyGiud = context.Companies.First().ID;


            context.SubscriptionPlans.AddOrUpdate(
                new SubscriptionPlan
                {
                    ID = companyGiud,
                    Name = "Developer Subscription Plan",
                    CompanyID = companyGiud,
                    NumberOfUsers = 9999,
                    StartDate = DateTime.Today.AddYears(-1),
                    EndDate = DateTime.Today.AddYears(10),
                    AccessCRM = true,
                    AccessDeliveryNotes = true,
                    AccessInvoices = true,
                    AccessProducts = true,
                    AccessQuotations = true,
                    AccessJobs = true,
                    Active = false,
                    Created = DateTime.Today.AddYears(-1),                    
                }
            );
            context.SaveChanges();
            */
            //Add Default Users Passwords : "Dev123!" 
            if (context.Users.Where(u => u.Id == "abe7ab8c-0063-48c2-9130-060ffc8169e1").LongCount() <= 0)
            {
                context.Users.Add(
                    new User
                    {
                        Id = "abe7ab8c-0063-48c2-9130-060ffc8169e1",
                        Email = "developer@fruitfulgroup.com",
                        UserName = "developer@fruitfulgroup.com",
                        PasswordHash = "AIh69MUSNlJKo1QGyT2tC1OSZK8NX3VM/x3gd6LLIRbhHO3WVNF+8FqVjv3XKnPN1A==",
                        SecurityStamp = "b75b58c7-4318-4cbf-99c7-8429856c9d0b",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        Roles = { new IdentityUserRole { UserId = "abe7ab8c-0063-48c2-9130-060ffc8169e1", RoleId = "-1" } }
                    }
                );
            }
           
            /*
            if (context.Users.Where(u => u.Id == "dbe0447f-9dd6-4e7f-8759-9bb37454ea2d").LongCount() <= 0)
            {
                context.Users.Add(
                    new User
                    {
                        Id = "dbe0447f-9dd6-4e7f-8759-9bb37454ea2d",
                        Email = "user@fruitfulgroup.com",
                        UserName = "user@fruitfulgroup.com",
                        PasswordHash = "AEWJ+U6SRhVCeB1cgPI7R+/8ioCgmiYXfhx7FNzVGud9XQTer7IfTLVPxH4LUdf11A==",
                        SecurityStamp = "915105c3-6f47-40b1-a88a-aeb74846cf5c",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = false,
                        AccessFailedCount = 0,
                        CompanyID = companyGiud,
                        Roles = { new IdentityUserRole { UserId = "dbe0447f-9dd6-4e7f-8759-9bb37454ea2d", RoleId = "1" } },
                        AccessCRM = true,
                        AccessDeliveryNotes = true,
                        AccessInvoices = true,
                        AccessProducts = true,
                        AccessQuotations = true,
                        AccessJobs = true,
                    }
                );
            }

            context.SaveChanges();

            */

            context.ModuleTypes.AddOrUpdate(
                new ModuleType { ID = ModuleTypeValues.PRODUCTS, Name = "Products", Code = "PDTS", IndexURL = "/Products/", EditURL = "/Products/Index/" },
                new ModuleType { ID = ModuleTypeValues.CLIENTS, Name = "Clients", Code = "CLTS", IndexURL = "/Clients/", EditURL = "/Clients/Index/" },
                new ModuleType { ID = ModuleTypeValues.SUPPLIERS, Name = "Suppliers", Code = "SPLR", IndexURL = "/Suppliers/", EditURL = "/Suppliers/Index/" },

                new ModuleType { ID = ModuleTypeValues.QUOTATIONS, Name = "Quotations", Code = "QS", IndexURL = "/Quotations/", EditURL = "/Quotations/Edit/" },
                new ModuleType { ID = ModuleTypeValues.JOBS, Name = "Jobs", Code = "JOBS", IndexURL = "/Jobs/", EditURL = "/Jobs/Edit/" },
                new ModuleType { ID = ModuleTypeValues.DELIVERY_NOTES, Name = "Delivery Notes", Code = "DEL", IndexURL = "/DeliveryNotes/", EditURL = "/DeliveryNotes/Edit/" },
                new ModuleType { ID = ModuleTypeValues.INVOICES, Name = "Invoices", Code = "INV", IndexURL = "/Invoices/", EditURL = "/Invoices/Edit/" },
                new ModuleType { ID = ModuleTypeValues.PURCHASE_ORDERS, Name = "Purchase Orders", Code = "POS", IndexURL = "/PurchaseOrders/", EditURL = "/PurchaseOrders/Edit/" },
                new ModuleType { ID = ModuleTypeValues.STOCK_CONTROL, Name = "Stock Control", Code = "SC", IndexURL = "/StockControl/", EditURL = "/StockControl/" }
            );

            context.JobStatuses.AddOrUpdate(
                new JobStatus { ID = JobStatusValues.Closed, Name = "Closed", Description = "Closed", HexColour = "#34495e", SortPos = 1 },
                new JobStatus { ID = JobStatusValues.ClosedCancelled, Name = "Closed - Cancelled", Description = "Closed - Cancelled", HexColour = "#2c5aa0", SortPos = 2 },
                new JobStatus { ID = JobStatusValues.Open, Name = "Open", Description = "Open", HexColour = "#3498db", SortPos = 3 },
                new JobStatus { ID = JobStatusValues.OpenOnHold, Name = "Open - On Hold", Description = "Open - On Hold", HexColour = "#1abc9c", SortPos = 4 }
            );

            context.JobDesignStatuses.AddOrUpdate(
                new JobDesignStatus { ID = JobDesignStatusValues.Awaiting, Name = "Awaiting", HexColour = "#34495e", SortPos = 1 },
                new JobDesignStatus { ID = JobDesignStatusValues.InProgress, Name = "In Progress", HexColour = "#3498db", SortPos = 2 },
                new JobDesignStatus { ID = JobDesignStatusValues.Fin, Name = "Fin", HexColour = "#2c5aa0", SortPos = 3 },
                new JobDesignStatus { ID = JobDesignStatusValues.NA, Name = "N/A", HexColour = "#1abc9c", SortPos = 4 }
            );

            context.JobWorkshopStatuses.AddOrUpdate(
                new JobWorkshopStatus { ID = JobWorkshopStatusValues.Awaiting, Name = "Awaiting", HexColour = "#34495e", SortPos = 1 },
                new JobWorkshopStatus { ID = JobWorkshopStatusValues.InProgress, Name = "In Progress", HexColour = "#3498db", SortPos = 2 },
                new JobWorkshopStatus { ID = JobWorkshopStatusValues.Fin, Name = "Fin", HexColour = "#2c5aa0", SortPos = 3 },
                new JobWorkshopStatus { ID = JobWorkshopStatusValues.NA, Name = "N/A", HexColour = "#1abc9c", SortPos = 4 }
            );

            context.JobElectricalStatuses.AddOrUpdate(
                new JobElectricalStatus { ID = JobElectricalStatusValues.Awaiting, Name = "Awaiting", HexColour = "#34495e", SortPos = 1 },
                new JobElectricalStatus { ID = JobElectricalStatusValues.InProgress, Name = "In Progress", HexColour = "#3498db", SortPos = 2 },
                new JobElectricalStatus { ID = JobElectricalStatusValues.Fin, Name = "Fin", HexColour = "#2c5aa0", SortPos = 3 },
                new JobElectricalStatus { ID = JobElectricalStatusValues.NA, Name = "N/A", HexColour = "#1abc9c", SortPos = 4 }
            );

            context.JobSoftwareStatuses.AddOrUpdate(
                new JobSoftwareStatus { ID = JobSoftwareStatusValues.Awaiting, Name = "Awaiting", HexColour = "#34495e", SortPos = 1 },
                new JobSoftwareStatus { ID = JobSoftwareStatusValues.InProgress, Name = "In Progress", HexColour = "#3498db", SortPos = 2 },
                new JobSoftwareStatus { ID = JobSoftwareStatusValues.Fin, Name = "Fin", HexColour = "#2c5aa0", SortPos = 3 },
                new JobSoftwareStatus { ID = JobSoftwareStatusValues.NA, Name = "N/A", HexColour = "#1abc9c", SortPos = 4 }
            );

            context.EquipStatuses.AddOrUpdate(
                new EquipStatus { ID = EquipStatusValues.Construction, Name = "Construction", HexColour = "#34495e", SortPos = 1 },
                new EquipStatus { ID = EquipStatusValues.Live, Name = "Live", HexColour = "#3498db", SortPos = 2 },
                new EquipStatus { ID = EquipStatusValues.Decommissioned, Name = "Decommissioned", HexColour = "#2c5aa0", SortPos = 3 }
            );

            context.PurchaseStatuses.AddOrUpdate(
                new PurchaseStatus { ID = PurchaseStatusValues.Enquiry, Name = "Enquiry", HexColour = "#34495e", SortPos = 1 },
                new PurchaseStatus { ID = PurchaseStatusValues.Quoted, Name = "Quoted", HexColour = "#3498db", SortPos = 2 },
                new PurchaseStatus { ID = PurchaseStatusValues.Ordered, Name = "Ordered", HexColour = "#2c5aa0", SortPos = 5 },
                new PurchaseStatus { ID = PurchaseStatusValues.ApprovedR, Name = "Approved R", HexColour = "#82b58e", SortPos = 3 },
                new PurchaseStatus { ID = PurchaseStatusValues.ApprovedP, Name = "Approved P", HexColour = "#3fa156", SortPos = 4 }
            );

            context.JobUrgencyLevels.AddOrUpdate(
                new JobUrgencyLevel { ID = JobUrgencyLevelValues.Safety, Name = "Safety", HexColour = "#1b9b57", SortPos = 1 },
                new JobUrgencyLevel { ID = JobUrgencyLevelValues.Low, Name = "Low", HexColour = "#b2b709", SortPos = 2 },
                new JobUrgencyLevel { ID = JobUrgencyLevelValues.Medium, Name = "Medium", HexColour = "#c66011", SortPos = 3 },
                new JobUrgencyLevel { ID = JobUrgencyLevelValues.High, Name = "High", HexColour = "#c61919", SortPos = 4 }
            );

            context.PaymentMethods.AddOrUpdate(
                new PaymentMethod { ID = PaymentMethodValues.Account, Name = "Account", SortPos = 1 },
                new PaymentMethod { ID = PaymentMethodValues.CreditCard, Name = "Credit Card", SortPos = 2 },
                new PaymentMethod { ID = PaymentMethodValues.FOC, Name = "FOC", SortPos = 3 },
                new PaymentMethod { ID = PaymentMethodValues.ProForma, Name = "Pro forma", SortPos = 4 },
                new PaymentMethod { ID = PaymentMethodValues.OTV, Name = "OTV", SortPos = 5 }
            );

            if (context.EmailSettings.Count() <= 0)
            {
                context.EmailSettings.AddOrUpdate(
                    new Fruitful.Email.EmailSetting
                    {
                        Email = "noreply@snap-suite.com",
                        Password = "SDb&4*2chh",
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSSL = true
                    }
                );
            }
            
            context.SaveChanges();
            /*
            context.SubscriptionPlans.AddOrUpdate(
                new SubscriptionPlan { ID = 1, Name = "Personal", Description = "SNAP Suite Personal with single user", DaysValid = 30, Price = 15, NumberOfUsers = 1 },
                new SubscriptionPlan { ID = 2, Name = "Professional 5", Description = "SNAP Suite Professional 5 with five users", DaysValid = 30, Price = 70, NumberOfUsers = 5 },
                new SubscriptionPlan { ID = 3, Name = "Professional 10", Description = "SNAP Suite Professional 10 with ten users", DaysValid = 30, Price = 130, NumberOfUsers = 10 }
            );
            */

        }
    }
}
