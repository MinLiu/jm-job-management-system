namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePurchaseStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseStatus", "HexColour", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseStatus", "HexColour");
        }
    }
}
