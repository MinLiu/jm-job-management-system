namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJobs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JobDesignStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobElectricalStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobSoftwareStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobWorkshopStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HexColour = c.String(),
                        SortPos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Jobs", "StatusDesignID", c => c.Int(nullable: false));
            AddColumn("dbo.Jobs", "StatusElectricalID", c => c.Int(nullable: false));
            AddColumn("dbo.Jobs", "StatusSoftwareID", c => c.Int(nullable: false));
            AddColumn("dbo.Jobs", "StatusWorkshopID", c => c.Int(nullable: false));
            AddColumn("dbo.Jobs", "TrackingStarted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Jobs", "TrackingDonw", c => c.Boolean(nullable: false));
            AddColumn("dbo.Jobs", "FileLocation", c => c.String());
            AlterColumn("dbo.Jobs", "StatusWorkshopEstHrs", c => c.Int());
            CreateIndex("dbo.Jobs", "StatusDesignID");
            CreateIndex("dbo.Jobs", "StatusElectricalID");
            CreateIndex("dbo.Jobs", "StatusSoftwareID");
            CreateIndex("dbo.Jobs", "StatusWorkshopID");
            AddForeignKey("dbo.Jobs", "StatusDesignID", "dbo.JobDesignStatus", "ID");
            AddForeignKey("dbo.Jobs", "StatusElectricalID", "dbo.JobElectricalStatus", "ID");
            AddForeignKey("dbo.Jobs", "StatusSoftwareID", "dbo.JobSoftwareStatus", "ID");
            AddForeignKey("dbo.Jobs", "StatusWorkshopID", "dbo.JobWorkshopStatus", "ID");
            DropColumn("dbo.Jobs", "StatusDesign");
            DropColumn("dbo.Jobs", "StatusElectrical");
            DropColumn("dbo.Jobs", "StatusSoftware");
            DropColumn("dbo.Jobs", "StatusWorkshop");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jobs", "StatusWorkshop", c => c.String());
            AddColumn("dbo.Jobs", "StatusSoftware", c => c.String());
            AddColumn("dbo.Jobs", "StatusElectrical", c => c.String());
            AddColumn("dbo.Jobs", "StatusDesign", c => c.String());
            DropForeignKey("dbo.Jobs", "StatusWorkshopID", "dbo.JobWorkshopStatus");
            DropForeignKey("dbo.Jobs", "StatusSoftwareID", "dbo.JobSoftwareStatus");
            DropForeignKey("dbo.Jobs", "StatusElectricalID", "dbo.JobElectricalStatus");
            DropForeignKey("dbo.Jobs", "StatusDesignID", "dbo.JobDesignStatus");
            DropIndex("dbo.Jobs", new[] { "StatusWorkshopID" });
            DropIndex("dbo.Jobs", new[] { "StatusSoftwareID" });
            DropIndex("dbo.Jobs", new[] { "StatusElectricalID" });
            DropIndex("dbo.Jobs", new[] { "StatusDesignID" });
            AlterColumn("dbo.Jobs", "StatusWorkshopEstHrs", c => c.String());
            DropColumn("dbo.Jobs", "FileLocation");
            DropColumn("dbo.Jobs", "TrackingDonw");
            DropColumn("dbo.Jobs", "TrackingStarted");
            DropColumn("dbo.Jobs", "StatusWorkshopID");
            DropColumn("dbo.Jobs", "StatusSoftwareID");
            DropColumn("dbo.Jobs", "StatusElectricalID");
            DropColumn("dbo.Jobs", "StatusDesignID");
            DropTable("dbo.JobWorkshopStatus");
            DropTable("dbo.JobSoftwareStatus");
            DropTable("dbo.JobElectricalStatus");
            DropTable("dbo.JobDesignStatus");
        }
    }
}
