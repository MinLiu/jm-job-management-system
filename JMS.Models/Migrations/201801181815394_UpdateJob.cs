namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJob : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Jobs", new[] { "UrgencyLevelID" });
            AddColumn("dbo.Jobs", "EquipmentNoID", c => c.Guid());
            AddColumn("dbo.Jobs", "StatusJobID", c => c.Int(nullable: false));
            AddColumn("dbo.Jobs", "TrackingDone", c => c.Boolean(nullable: false));
            AddColumn("dbo.Jobs", "Note", c => c.String());
            AlterColumn("dbo.Jobs", "UrgencyLevelID", c => c.Int());
            CreateIndex("dbo.Jobs", "EquipmentNoID");
            CreateIndex("dbo.Jobs", "UrgencyLevelID");
            CreateIndex("dbo.Jobs", "StatusJobID");
            AddForeignKey("dbo.Jobs", "EquipmentNoID", "dbo.Equipments", "ID");
            AddForeignKey("dbo.Jobs", "StatusJobID", "dbo.JobStatus", "ID");
            DropColumn("dbo.Jobs", "EquipmentNo");
            DropColumn("dbo.Jobs", "StatusJob");
            DropColumn("dbo.Jobs", "TrackingDonw");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jobs", "TrackingDonw", c => c.Boolean(nullable: false));
            AddColumn("dbo.Jobs", "StatusJob", c => c.String());
            AddColumn("dbo.Jobs", "EquipmentNo", c => c.String());
            DropForeignKey("dbo.Jobs", "StatusJobID", "dbo.JobStatus");
            DropForeignKey("dbo.Jobs", "EquipmentNoID", "dbo.Equipments");
            DropIndex("dbo.Jobs", new[] { "StatusJobID" });
            DropIndex("dbo.Jobs", new[] { "UrgencyLevelID" });
            DropIndex("dbo.Jobs", new[] { "EquipmentNoID" });
            AlterColumn("dbo.Jobs", "UrgencyLevelID", c => c.Int(nullable: false));
            DropColumn("dbo.Jobs", "Note");
            DropColumn("dbo.Jobs", "TrackingDone");
            DropColumn("dbo.Jobs", "StatusJobID");
            DropColumn("dbo.Jobs", "EquipmentNoID");
            CreateIndex("dbo.Jobs", "UrgencyLevelID");
        }
    }
}
