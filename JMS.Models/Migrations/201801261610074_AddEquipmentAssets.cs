namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEquipmentAssets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EquipmentAssets",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        EquipmentID = c.Guid(nullable: false),
                        PMNo = c.String(),
                        Title = c.String(),
                        ManufacturerID = c.Guid(),
                        EquipType = c.String(),
                        DrawingNo = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Equipments", t => t.EquipmentID)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerID)
                .Index(t => t.EquipmentID)
                .Index(t => t.ManufacturerID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EquipmentAssets", "ManufacturerID", "dbo.Manufacturers");
            DropForeignKey("dbo.EquipmentAssets", "EquipmentID", "dbo.Equipments");
            DropIndex("dbo.EquipmentAssets", new[] { "ManufacturerID" });
            DropIndex("dbo.EquipmentAssets", new[] { "EquipmentID" });
            DropTable("dbo.EquipmentAssets");
        }
    }
}
