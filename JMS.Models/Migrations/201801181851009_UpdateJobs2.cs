namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateJobs2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Jobs", "StatusWorkshopEstHrs", c => c.Decimal(precision: 28, scale: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Jobs", "StatusWorkshopEstHrs", c => c.Int());
        }
    }
}
