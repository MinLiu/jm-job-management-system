namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateEquipAsset : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EquipmentAssets", "SerialNo", c => c.String());
            DropColumn("dbo.EquipmentAssets", "EquipType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EquipmentAssets", "EquipType", c => c.String());
            DropColumn("dbo.EquipmentAssets", "SerialNo");
        }
    }
}
