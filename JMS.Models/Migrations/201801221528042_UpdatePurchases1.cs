namespace JMS.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePurchases1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Purchases", "CurrencyID", "dbo.Currencies");
            DropIndex("dbo.Purchases", new[] { "CurrencyID" });
            AddColumn("dbo.Purchases", "POCurrency", c => c.String());
            DropColumn("dbo.Purchases", "CurrencyID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Purchases", "CurrencyID", c => c.Int(nullable: false));
            DropColumn("dbo.Purchases", "POCurrency");
            CreateIndex("dbo.Purchases", "CurrencyID");
            AddForeignKey("dbo.Purchases", "CurrencyID", "dbo.Currencies", "ID");
        }
    }
}
