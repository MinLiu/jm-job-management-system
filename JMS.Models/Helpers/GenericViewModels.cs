﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    public class SelectItemViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class UserItemViewModel
    {
        public string ID { get; set; }
        public string Email { get; set; }
    }

    public enum TemplateType { Front, Body, Back }
    public enum TemplateFormat { Pdf, Docx }


    public class LineItemInfoViewModel
    {
        public string Description { get; set; }
        public string DescriptionShort { get; set; }
        public string Category { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
    }


    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}