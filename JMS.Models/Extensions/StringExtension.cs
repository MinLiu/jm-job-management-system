﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public static class StringExtensions
{
    public static Guid? ToNullableGuid(this String str)
    {
        bool flag = Guid.TryParse(str, out Guid guid);

        return flag == true ? guid : null as Guid?;
    }

    public static Guid ToGuid(this String str)
    {
        return Guid.Parse(str);
    }
}
