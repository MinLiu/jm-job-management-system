﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Client : IEntity
    {
        public Client() {  ID = Guid.NewGuid(); }


        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public Guid? ClientTypeID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string Notes { get; set; }
        public string SageAccountReference { get; set; }

        public DateTime Created { get; set; }

        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.
       
        [ForeignKey("CompanyID")]
        public virtual Company Company { get; set; }
        public virtual ClientType ClientType { get; set; }


        public virtual ICollection<ClientAddress> ClientAddresses { get; set; }
        public virtual ICollection<ClientAttachment> ClientAttachments { get; set; }
        public virtual ICollection<ClientContact> ClientContacts { get; set; }        
        public virtual ICollection<ClientTagItem> ClientTagItems { get; set; }
        public virtual ICollection<ClientEvent> ClientEvents { get; set; }
        
    }



    public class ClientViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string ClientTypeID { get; set; }
        public string ClientTypeName { get; set; }

        [Required(ErrorMessage = "* The Client Name field is required.")]
        [Display(Name = "Client Name")]
        public string Name { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string Notes { get; set; }
        public string SageAccountReference { get; set; }

        public string ClientTagsHtmlString { get; set; }

        public DateTime Created { get; set; }

        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
    }





    public class ClientMapper : ModelMapper<Client, ClientViewModel>
    {
        public override void MapToViewModel(Client model, ClientViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.ClientTypeID = (model.ClientType != null) ? model.ClientTypeID.ToString() : null;
            viewModel.ClientTypeName = (model.ClientType != null) ? model.ClientType.Name : "";
            viewModel.Name = model.Name;
            viewModel.Address1 = model.Address1;
            viewModel.Address2 = model.Address2;
            viewModel.Address3 = model.Address3;            
            viewModel.Town = model.Town;
            viewModel.County = model.County;
            viewModel.Postcode = model.Postcode;
            viewModel.Country = model.Country;
            viewModel.Email = model.Email;
            viewModel.Website = model.Website;
            viewModel.Telephone = model.Telephone;
            viewModel.Mobile = model.Mobile;
            viewModel.Notes = model.Notes;
            viewModel.SageAccountReference = model.SageAccountReference;
            viewModel.Created = model.Created;

            viewModel.Deleted = model.Deleted;
            viewModel.ClientTagsHtmlString = string.Join("&nbsp;", model.ClientTagItems.ToArray().Select(t => string.Format("<span class='label label-primary' style='background-color:{0}'>{1}</span>", t.ClientTag.HexColour, t.ClientTag.Name)));
            
        }

        public override void MapToModel(ClientViewModel viewModel, Client model)
        {
            // ID = viewModel.ID;
            //model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.ClientTypeID = (!string.IsNullOrEmpty(viewModel.ClientTypeID)) ? Guid.Parse(viewModel.ClientTypeID) as Guid? : null;
            model.Name = viewModel.Name;
            model.Address1 = viewModel.Address1;
            model.Address2 = viewModel.Address2;
            model.Address3 = viewModel.Address3;
            model.County = viewModel.County;
            model.Town = viewModel.Town;
            model.Postcode = viewModel.Postcode;
            model.Country = viewModel.Country;
            model.Email = viewModel.Email;
            model.Website = viewModel.Website;
            model.Telephone = viewModel.Telephone;
            model.Mobile = viewModel.Mobile;
            model.Notes = viewModel.Notes;
            model.SageAccountReference = viewModel.SageAccountReference;
            //Created = viewModel.Created;

            //Deleted = viewModel.Deleted;
            
        }

    }


}
