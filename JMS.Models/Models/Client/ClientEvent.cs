﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientEvent : IEntity
    {
        public ClientEvent() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public string AssignedUserID { get; set; }
        public string CreatorID { get; set; }

        public Guid? OpportunityID { get; set; }

        public Guid? ClientID { get; set; }
        public Guid? ClientContactID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Message { get; set; } 
        public bool Complete { get; set; }

        [NotMapped]
        public int Weight {
            get
            {
                if (Timestamp.Date == DateTime.Today && Complete == false)
                    return 2;
                else if (DateTime.Now > Timestamp && Complete == false)
                    return 1;
                else if (Timestamp.Date > DateTime.Today && Complete == false)
                    return 0;
                else
                    return -1;
            }
            private set { }
        }

        public virtual Company Company { get; set; }
        public virtual Client Client { get; set; }
        public virtual ClientContact ClientContact { get; set; }
        
        public virtual User AssignedUser { get; set; }
        public virtual User Creator { get; set; }

    }

    public class ContactItemViewModel 
    {
        public string ID { get; set; }
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Position { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class ClientEventViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string OpportunityID { get; set; }

        public UserItemViewModel AssignedUser { get; set; }
        public UserItemViewModel Creator { get; set; }

        public SelectItemViewModel Client { get; set; }
        public ContactItemViewModel Contact { get; set; }


        [Required(ErrorMessage = "* The Timestamp is required.")]       
        public DateTime Timestamp { get; set; }

        [Required(ErrorMessage = "* The Message is required.")]
        public string Message { get; set; }

        public bool Complete { get; set; }
        public bool Overdue { get; set; }
        public bool Due { get; set; }

    }



    public class ClientEventMapper : ModelMapper<ClientEvent, ClientEventViewModel>
    {
        public override void MapToViewModel(ClientEvent model, ClientEventViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.AssignedUser = (model.AssignedUser != null) ? new UserItemViewModel { ID = model.AssignedUser.Id, Email = model.AssignedUser.Email } : new UserItemViewModel { ID = null, Email = "" };
            viewModel.Creator = (model.Creator != null) ? new UserItemViewModel { ID = model.Creator.Id, Email = model.Creator.Email } : new UserItemViewModel { ID = null, Email = "" };
            viewModel.Client = (model.Client != null) ? new SelectItemViewModel { ID = model.Client.ID.ToString(), Name = model.Client.Name } : new SelectItemViewModel { ID = null, Name = "" };
            viewModel.Contact = (model.ClientContact != null) ? new ContactItemViewModel { ID = model.ClientContact.ID.ToString(), ClientID = model.ClientID.ToString(), Name = string.Format("{0} {1} {2}", model.ClientContact.Title, model.ClientContact.FirstName, model.ClientContact.LastName) } : new ContactItemViewModel { ID = null, ClientID = null, Name = "" };
            viewModel.Timestamp = model.Timestamp;
            viewModel.Message = model.Message;
            viewModel.Complete = model.Complete;
            viewModel.Overdue = (model.Timestamp < DateTime.Today);
            viewModel.Due = (model.Timestamp > DateTime.Today && model.Timestamp < DateTime.Today.AddDays(1.0));
            viewModel.OpportunityID = model.OpportunityID.HasValue ? model.OpportunityID.ToString() : null;
            
        }

        public override void MapToModel(ClientEventViewModel viewModel, ClientEvent model)
        {

            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);

            model.AssignedUserID = (viewModel.AssignedUser.ID == "") ? null : viewModel.AssignedUser.ID;
            model.CreatorID = (viewModel.Creator.ID == "") ? null : viewModel.Creator.ID;
            model.ClientID = (string.IsNullOrEmpty(viewModel.Client.ID)) ? null : Guid.Parse(viewModel.Client.ID) as Guid?;
            model.ClientContactID = (string.IsNullOrEmpty(viewModel.Contact.ID)) ? null : Guid.Parse(viewModel.Contact.ID) as Guid?;
            model.OpportunityID = (string.IsNullOrEmpty(viewModel.OpportunityID)) ? null : Guid.Parse(viewModel.OpportunityID) as Guid?;

            model.Timestamp = viewModel.Timestamp;
            model.Message = viewModel.Message;
            model.Complete = viewModel.Complete;
           
        }

    }

    public class OpportunityEventViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string OpportunityID { get; set; }

        public string EventAssignedUserID { get; set; }
        public string EventAssignedUserName { get; set; }

        public string EventCreatorID { get; set; }
        public string EventCreatorName { get; set; }

        public string ClientID { get; set; }
        public string ClientName { get; set; }

        public string ContactID { get; set; }
        public string ContactName { get; set; }


        [Required(ErrorMessage = "* The Timestamp is required.")]
        public DateTime Timestamp { get; set; }

        [Required(ErrorMessage = "* The Message is required.")]
        public string Message { get; set; }

        public bool Complete { get; set; }
        public bool Overdue { get; set; }
        public bool Due { get; set; }

    }

    public class OpportunityEventMapper : ModelMapper<ClientEvent, OpportunityEventViewModel>
    {
        public override void MapToModel(OpportunityEventViewModel viewModel, ClientEvent model)
        {
            model.AssignedUserID = viewModel.EventAssignedUserID;
            model.ClientContactID = string.IsNullOrWhiteSpace(viewModel.ContactID) ? null : Guid.Parse(viewModel.ContactID) as Guid?;
            model.ClientID = string.IsNullOrWhiteSpace(viewModel.ClientID) ? null : Guid.Parse(viewModel.ClientID) as Guid?;
            model.Complete = viewModel.Complete;
            model.Message = viewModel.Message;
            model.OpportunityID = Guid.Parse(viewModel.OpportunityID);
            model.Timestamp = viewModel.Timestamp;
        }
        public override void MapToViewModel(ClientEvent model, OpportunityEventViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.HasValue ? model.ClientID.ToString() : null;
            viewModel.ClientName = model.Client != null ? model.Client.Name : "";
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Complete = model.Complete;
            viewModel.ContactID = model.ClientContactID.HasValue ? model.ClientContactID.ToString() : null;
            viewModel.ContactName = model.ClientContact != null ? string.Format("{0} {1} {2}", model.ClientContact.Title, model.ClientContact.FirstName, model.ClientContact.LastName) : "";
            viewModel.EventAssignedUserID = model.AssignedUserID;
            viewModel.EventAssignedUserName = model.AssignedUser != null ? model.AssignedUser.Email : "";
            viewModel.EventCreatorID = model.CreatorID;
            viewModel.EventCreatorName = model.Creator != null ? model.Creator.Email : "";
            viewModel.Message = model.Message;
            viewModel.OpportunityID = model.OpportunityID.HasValue? model.OpportunityID.ToString() : null;
            viewModel.Timestamp = model.Timestamp;
            viewModel.Due = (model.Timestamp > DateTime.Today && model.Timestamp < DateTime.Today.AddDays(1.0));
            viewModel.OpportunityID = model.OpportunityID.HasValue ? model.OpportunityID.ToString() : null;
        }
    }

}
