﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientTypeRepository : EntityRespository<ClientType>
    {
        public ClientTypeRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientTypeRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(ClientType entity)
        {
            var set = _db.Set<ClientType>().Where(a => a.ID == entity.ID)
                                    .Include(a => a.Clients);

            _db.Set<ClientType>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
