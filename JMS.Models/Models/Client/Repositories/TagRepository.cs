﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientTagRepository : EntityRespository<ClientTag>
    {
        public ClientTagRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientTagRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(ClientTag entity)
        {
            _db.Set<ClientTagItem>().RemoveRange(_db.Set<ClientTagItem>().Where(x => x.ClientTagID == entity.ID));
            _db.Set<ClientTag>().RemoveRange(_db.Set<ClientTag>().Where(x => x.ID == entity.ID));
            _db.SaveChanges();
        }

    }

}
