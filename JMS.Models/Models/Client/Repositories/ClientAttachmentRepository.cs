﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientAttachmentRepository : EntityRespository<ClientAttachment>
    {
        public ClientAttachmentRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientAttachmentRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
