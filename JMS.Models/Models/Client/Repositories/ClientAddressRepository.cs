﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientAddressRepository : EntityRespository<ClientAddress>
    {
        public ClientAddressRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientAddressRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(ClientAddress entity)
        {
            var set = _db.Set<ClientAddress>().Where(a => a.ID == entity.ID);

            _db.Set<ClientAddress>().RemoveRange(set);
            _db.SaveChanges();
        }
    }

}
