﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientEventRepository : EntityRespository<ClientEvent>
    {
        public ClientEventRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientEventRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
