﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientContactRepository : EntityRespository<ClientContact>
    {
        public ClientContactRepository()
            : this(new SnapDbContext())
        {

        }

        public ClientContactRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(ClientContact entity)
        {
            var set = _db.Set<ClientContact>().Where(a => a.ID == entity.ID);

            _db.Set<ClientContact>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
