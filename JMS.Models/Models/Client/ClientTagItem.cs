﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientTagItem : IEntity
    {
        public ClientTagItem() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ClientID { get; set; }
        public Guid ClientTagID { get; set; }

        public virtual Client Client { get; set; }
        public virtual ClientTag ClientTag { get; set; }
    }



    public class ClientTagItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ClientID { get; set; }

        //[UIHint("ClientTagItemDropDown")]        
        public ClientTagViewModel ClientTag { get; set; }
    }





    public class ClientTagItemMapper : ModelMapper<ClientTagItem, ClientTagItemViewModel>
    {
        public override void MapToViewModel(ClientTagItem model, ClientTagItemViewModel viewModel)
        {   
            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.ToString();
            viewModel.ClientTag = new ClientTagViewModel { ID = model.ClientTag.ID.ToString(), Name = model.ClientTag.Name, HexColour = model.ClientTag.HexColour };           
        }

        public override void MapToModel(ClientTagItemViewModel viewModel, ClientTagItem model)
        {
            //model.ID = Guid.Parse(viewModel.ID);
            model.ClientID = Guid.Parse(viewModel.ClientID);
            model.ClientTagID = Guid.Parse(viewModel.ClientTag.ID);            
        }

    }
}
