﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientContact : IEntity
    {
        public ClientContact() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ClientID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool Deleted { get; set; }

        public virtual Client Client { get; set; }


        public string ToLongLabel()
        {
            if (string.IsNullOrWhiteSpace(Title))
                return string.Format("{0} {1}", FirstName, LastName);
            else
                return string.Format("{0} {1} {2}", Title, FirstName, LastName);
        }

        public string ToShortLabel()
        {
            if (string.IsNullOrWhiteSpace(Title))
                return string.Format("{0} {1}", (FirstName ?? "").Truncate(1), LastName);
            else
                return string.Format("{0} {1} {2}", Title, (FirstName ?? "").Truncate(1), LastName);
        }

    }



    public class ClientContactViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string Title { get; set; }

        [Required(ErrorMessage = "* The First Name field is required.")]
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Position { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool Deleted { get; set; }
    }



    public class ClientContactMapper : ModelMapper<ClientContact, ClientContactViewModel>
    {
        public override void MapToViewModel(ClientContact model, ClientContactViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.ToString();
            viewModel.ClientName = model.Client.Name;
            viewModel.Title = model.Title;
            viewModel.FirstName = model.FirstName;
            viewModel.LastName = model.LastName;
            viewModel.Position = model.Position;
            viewModel.Telephone = model.Telephone;
            viewModel.Mobile = model.Mobile;
            viewModel.Email = model.Email;
            viewModel.Deleted = model.Deleted;
        }

        public override void MapToModel(ClientContactViewModel viewModel, ClientContact model)
        {          
            //ID = viewModel.ID;
            model.ClientID = Guid.Parse(viewModel.ClientID);
            model.Title = viewModel.Title;
            model.FirstName = viewModel.FirstName;
            model.LastName = viewModel.LastName;
            model.Position = viewModel.Position;
            model.Telephone = viewModel.Telephone;
            model.Mobile = viewModel.Mobile;
            model.Email = viewModel.Email;
            //Deleted = viewModel.Deleted;
        }

    }
}
