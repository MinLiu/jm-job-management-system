﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientType : IEntity
    {
        public ClientType() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual Company Company { get; set; }

        //Foreign References
        public virtual ICollection<Client> Clients { get; set; }
    }




    public class ClientTypeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string Name { get; set; }
    }



    public class ClientTypeMapper : ModelMapper<ClientType, ClientTypeViewModel>
    {
        public override void MapToViewModel(ClientType model, ClientTypeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Name = model.Name;
        }

        public override void MapToModel(ClientTypeViewModel viewModel, ClientType model)
        {
            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
        }

    }
}
