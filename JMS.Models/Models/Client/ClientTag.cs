﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientTag : IEntity
    {
        public ClientTag() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public string HexColour { get; set; }

        public virtual Company Company { get; set; }
    }



    public class ClientTagViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        public string CompanyID { get; set; }

        [Required]
        public string Name { get; set; }
        
        public string HexColour { get; set; }
    }



    public class ClientTagMapper : ModelMapper<ClientTag, ClientTagViewModel>
    {
        public override void MapToViewModel(ClientTag model, ClientTagViewModel viewModel)
        {  
            viewModel. ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;      
        }

        public override void MapToModel(ClientTagViewModel viewModel, ClientTag model)
        {
            //model.ID = Guid.Parse(viewModel.ID);
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            model.HexColour = viewModel.HexColour;
        }

    }
}
