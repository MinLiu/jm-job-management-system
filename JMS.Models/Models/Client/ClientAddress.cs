﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientAddress : IEntity
    {
        public ClientAddress() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ClientID { get; set; }

        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string County { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }

        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.
        
        public virtual Client Client { get; set; }

    }





    public class ClientAddressViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ClientID { get; set; }
        public string ClientName { get; set; }

        [Required(ErrorMessage = "* The Name field is required.")]
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.

    }




    public class ClientAddressMapper : ModelMapper<ClientAddress, ClientAddressViewModel>
    {
        public override void MapToViewModel(ClientAddress model, ClientAddressViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.ToString();
            viewModel.ClientName = model.Client.Name;
            viewModel.Name = model.Name;
            viewModel.Address1 = model.Address1;
            viewModel.Address2 = model.Address2;
            viewModel.Address3 = model.Address3;
            viewModel.Town = model.Town;
            viewModel.County = model.County;
            viewModel.Postcode = model.Postcode;
            viewModel.Telephone = model.Telephone;
            viewModel.Mobile = model.Mobile;
            viewModel.Email = model.Email;
            viewModel.Deleted = model.Deleted;
        }

        public override void MapToModel(ClientAddressViewModel viewModel, ClientAddress model)
        {
            //ID = viewModel.ID;
            model.ClientID = Guid.Parse(viewModel.ClientID);
            model.Name = viewModel.Name;
            model.Address1 = viewModel.Address1;
            model.Address2 = viewModel.Address2;
            model.Address3 = viewModel.Address3;
            model.Town = viewModel.Town;
            model.County = viewModel.County;
            model.Postcode = viewModel.Postcode;
            model.Telephone = viewModel.Telephone;
            model.Mobile = viewModel.Mobile;
            model.Email = viewModel.Email;
            //Deleted = viewModel.Deleted;
        }

    }


}
