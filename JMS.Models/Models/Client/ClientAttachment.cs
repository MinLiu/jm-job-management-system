﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ClientAttachment : IEntity
    {
        public ClientAttachment() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ClientID { get; set; }

        public string Filename { get; set; }
        public string FilePath { get; set; }
        public string Format { get; set; }
        
        public virtual Client Client { get; set; }       
    }




    public class ClientAttachmentViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ClientID { get; set; }

        [Required(ErrorMessage = "* The name filed is required.")]
        public string Filename { get; set; }
        public string FilePath { get; set; }
        public string Format { get; set; }
    }




    public class ClientAttachmentMapper : ModelMapper<ClientAttachment, ClientAttachmentViewModel>
    {
        public override void MapToViewModel(ClientAttachment model, ClientAttachmentViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.ClientID = model.ClientID.ToString();

            viewModel.Filename = model.Filename;
            viewModel.FilePath = model.FilePath;
            viewModel.Format = model.Format;
        }

        public override void MapToModel(ClientAttachmentViewModel viewModel, ClientAttachment model)
        {
            //ID = viewModel.ID;
            model.ClientID = Guid.Parse(viewModel.ClientID);

            model.Filename = viewModel.Filename;
            model.FilePath = viewModel.FilePath;
            model.Format = viewModel.Format;
        }

    }

}
