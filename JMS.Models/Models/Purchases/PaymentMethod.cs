﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class PaymentMethod
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int SortPos { get; set; }
    }

    public class PaymentMethodViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public static class PaymentMethodValues
    {
        public static int Account = 1;
        public static int CreditCard = 2;
        public static int FOC = 3;
        public static int ProForma = 4;
        public static int OTV = 5;
    }

    public class PaymentMethodMapper : ModelMapper<PaymentMethod, PaymentMethodViewModel>
    {
        public override void MapToViewModel(PaymentMethod model, PaymentMethodViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
        }

        public override void MapToModel(PaymentMethodViewModel viewModel, PaymentMethod model)
        {
            throw new NotImplementedException();
        }
    }

}
