﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class MaterialType : IEntity
    {
        public MaterialType() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }

        //Foreign References
        public virtual ICollection<Purchase> Purchases { get; set; }
    }

    public class MaterialTypeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class MaterialTypeMapper : ModelMapper<MaterialType, MaterialTypeViewModel>
    {
        public override void MapToViewModel(MaterialType model, MaterialTypeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
        }

        public override void MapToModel(MaterialTypeViewModel viewModel, MaterialType model)
        {
            model.Name = viewModel.Name;
        }

    }
}
