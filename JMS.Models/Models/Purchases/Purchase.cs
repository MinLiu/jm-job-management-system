﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Purchase : IEntity
    {
        public Purchase() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        public long RefID { get; set; }
        [Required]
        public string Title { get; set; }
        public string PartNo { get; set; }
        public Guid? SupplierID { get; set; }
        public Guid? MaterialTypeID { get; set; }
        public string MaterialSpec { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived { get; set; }
        public decimal UnitPrice { get; set; }
        public string POCurrency { get; set; }
        public decimal PurchaseTotal { get; set; }
        public string PONo { get; set; }
        public int PurchaseStatusID { get; set; }
        public DateTime? DateOrdered { get; set; }
        public DateTime? DateReceived { get; set; }
        public int? PaymentMethodID { get; set; }
        public Guid? ProjectCodeID { get; set; }
        public string WorkshopNo { get; set; }
        public DateTime? DateDue { get; set; }
        public string LeadTimeApprx { get; set; }
        public string SupplierRef { get; set; }
        public Guid? RelatedJobID { get; set; }
        public string HistoricJobID { get; set; }
        public string Note { get; set; }
        
        //Foreign References
        public virtual ICollection<RequestorPurchase> RequestorPurchases { get; set; }
        public virtual ICollection<OnBehalfOfPurchase> OnBehalfOfPurchases { get; set; }
        public virtual ICollection<DepartmentPurchase> DepartmentPurchases { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }
        public virtual PurchaseStatus PurchaseStatus { get; set; }
        [ForeignKey("RelatedJobID")]
        public virtual Job RelatedJob { get; set; }
        [ForeignKey("ProjectCodeID")]
        public virtual ProjectCode ProjectCode { get; set; }
        public virtual MaterialType MaterialType { get; set; }
    }



    public class PurchaseGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string PartNo { get; set; }
        public string PONo { get; set; }
        public int PurchaseStatusID { get; set; }
        public PurchaseStatusViewModel Status { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string SupplierRef { get; set; }
        public string MaterialTypeID { get; set; }
        public string MaterialTypeName { get; set; }
        public decimal PurchaseTotal { get; set; }
        public string CurrencySymbol { get; set; }
        public DateTime? DateOrdered { get; set; }
        public DateTime? DateDue { get; set; }
        public decimal UnitPrice { get; set; }
    }

    public class PurchaseViewModel : IEntityViewModel, IValidatableObject
    {
        public string ID { get; set; }
        [Required(ErrorMessage = "The Description field is required.")]
        public string Title { get; set; }
        public string PartNo { get; set; }
        [Required(ErrorMessage ="The Supplier field is required.")]
        public string SupplierID { get; set; }
        public string MaterialTypeID { get; set; }
        public string MaterialSpec { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived { get; set; }
        public decimal UnitPrice { get; set; }
        public string POCurrency { get; set; }
        public decimal PurchaseTotal { get; set; }
        public string PONo { get; set; }
        public int PurchaseStatusID { get; set; }
        public DateTime? DateOrdered { get; set; }
        public DateTime? DateReceived { get; set; }
        public int? PaymentMethodID { get; set; }
        public string ProjectCodeID { get; set; }
        public string WorkshopNo { get; set; }
        public DateTime? DateDue { get; set; }
        public string LeadTimeApprx { get; set; }
        public string SupplierRef { get; set; }
        public string RelatedJobID { get; set; }
        public string HistoricJobID { get; set; }
        public string Note { get; set; }
        public List<string> RequestorIDs { get; set; }
        [Required(ErrorMessage = "The On Behalf of field is required.")]
        public List<string> OnBehalfOfIDs { get; set; }
        public List<string> DepartmentIDs { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            if (PurchaseStatusID == PurchaseStatusValues.Ordered && DepartmentIDs == null)
            {
                results.Add(new ValidationResult("Must have department when ordered", new[] { "DepartmentIDs" }));
            }
            return results;
        }
    }





    public class PurchaseGridMapper : ModelMapper<Purchase, PurchaseGridViewModel>
    {
        public override void MapToViewModel(Purchase model, PurchaseGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Title = model.Title;
            viewModel.PartNo = model.PartNo;
            viewModel.PONo = model.PONo;
            viewModel.QtyOrdered = model.QtyOrdered;
            viewModel.QtyReceived = model.QtyReceived;
            viewModel.SupplierID = model.SupplierID != null ? model.SupplierID.ToString() : "";
            viewModel.SupplierName = model.Supplier != null ? model.Supplier.Name : "";
            viewModel.SupplierRef = model.SupplierRef;
            viewModel.PurchaseTotal = model.PurchaseTotal;
            viewModel.PurchaseStatusID = model.PurchaseStatusID;
            viewModel.Status = new PurchaseStatusMapper().MapToViewModel(model.PurchaseStatus);
            viewModel.DateOrdered = model.DateOrdered;
            viewModel.DateDue = model.DateDue;
            viewModel.UnitPrice = model.UnitPrice;
            viewModel.MaterialTypeID = model.MaterialTypeID.ToString();
            viewModel.MaterialTypeName = model.MaterialType != null ? model.MaterialType.Name : "";
        }

        public override void MapToModel(PurchaseGridViewModel viewModel, Purchase model)
        {
            throw new NotImplementedException();
        }
    }

    public class PurchaseMapper : ModelMapper<Purchase, PurchaseViewModel>
    {
        public override void MapToModel(PurchaseViewModel viewModel, Purchase model)
        {
            model.Title = viewModel.Title;
            model.PartNo = viewModel.PartNo;
            model.SupplierID = viewModel.SupplierID.ToNullableGuid();
            model.MaterialTypeID = viewModel.MaterialTypeID.ToNullableGuid();
            model.MaterialSpec = viewModel.MaterialSpec;
            model.QtyOrdered = viewModel.QtyOrdered;
            model.QtyReceived = viewModel.QtyReceived;
            model.UnitPrice = viewModel.UnitPrice;
            model.POCurrency = viewModel.POCurrency;
            model.PurchaseTotal = viewModel.PurchaseTotal;
            model.PONo = viewModel.PONo;
            model.PurchaseStatusID = viewModel.PurchaseStatusID;
            model.DateOrdered = viewModel.DateOrdered;
            model.DateReceived = viewModel.DateReceived;
            model.PaymentMethodID = viewModel.PaymentMethodID;
            model.ProjectCodeID = viewModel.ProjectCodeID.ToNullableGuid();
            model.WorkshopNo = viewModel.WorkshopNo;
            model.DateDue = viewModel.DateDue;
            model.LeadTimeApprx = viewModel.LeadTimeApprx;
            model.SupplierRef = viewModel.SupplierRef;
            model.RelatedJobID = viewModel.RelatedJobID.ToNullableGuid();
            model.HistoricJobID = viewModel.HistoricJobID;
            model.Note = viewModel.Note;
        }
        public override void MapToViewModel(Purchase model, PurchaseViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Title = model.Title;
            viewModel.PartNo = model.PartNo;
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.MaterialTypeID = model.MaterialTypeID.ToString();
            viewModel.MaterialSpec = model.MaterialSpec;
            viewModel.QtyOrdered = model.QtyOrdered;
            viewModel.QtyReceived = model.QtyReceived;
            viewModel.UnitPrice = model.UnitPrice;
            viewModel.POCurrency = model.POCurrency;
            viewModel.PurchaseTotal = model.PurchaseTotal;
            viewModel.PONo = model.PONo;
            viewModel.PurchaseStatusID = model.PurchaseStatusID;
            viewModel.DateOrdered = model.DateOrdered;
            viewModel.DateReceived = model.DateReceived;
            viewModel.PaymentMethodID = model.PaymentMethodID;
            viewModel.ProjectCodeID = model.ProjectCodeID.ToString();
            viewModel.WorkshopNo = model.WorkshopNo;
            viewModel.DateDue = model.DateDue;
            viewModel.LeadTimeApprx = model.LeadTimeApprx;
            viewModel.SupplierRef = model.SupplierRef;
            viewModel.RelatedJobID = model.RelatedJobID.ToString();
            viewModel.HistoricJobID = model.HistoricJobID;
            viewModel.Note = model.Note;
            viewModel.RequestorIDs = model.RequestorPurchases.Select(x => x.RequestorID.ToString()).ToList();
            viewModel.OnBehalfOfIDs = model.OnBehalfOfPurchases.Select(x => x.RequestorID.ToString()).ToList();
            viewModel.DepartmentIDs = model.DepartmentPurchases.Select(x => x.DepartmentID.ToString()).ToList();
        }
    }
}
