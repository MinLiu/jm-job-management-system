﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class PurchaseStatus
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string HexColour { get; set; }
        public int SortPos { get; set; }
    }

    public class PurchaseStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string HexColour { get; set; }
    }

    public static class PurchaseStatusValues
    {
        public static int Enquiry = 1;
        public static int Quoted = 2;
        public static int Ordered = 3;
        public static int ApprovedR = 4;
        public static int ApprovedP = 5;
    }

    public class PurchaseStatusMapper : ModelMapper<PurchaseStatus, PurchaseStatusViewModel>
    {
        public override void MapToViewModel(PurchaseStatus model, PurchaseStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(PurchaseStatusViewModel viewModel, PurchaseStatus model)
        {
            throw new NotImplementedException();
        }
    }

}
