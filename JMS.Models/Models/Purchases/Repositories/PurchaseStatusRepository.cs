﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class PurchaseStatusRepository : EntityRespository<PurchaseStatus>
    {
        public PurchaseStatusRepository()
            : this(new SnapDbContext())
        {

        }

        public PurchaseStatusRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
