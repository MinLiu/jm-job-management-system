﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class MaterialRepository : EntityRespository<MaterialType>
    {
        public MaterialRepository()
            : this(new SnapDbContext())
        {

        }

        public MaterialRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
