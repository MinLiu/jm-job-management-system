﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class PurchaseRepository : EntityRespository<Purchase>
    {
        public PurchaseRepository()
            : this(new SnapDbContext())
        {

        }

        public PurchaseRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Purchase entity)
        {
            using (var db = new SnapDbContext())
            {
                db.RequestorPurchases.RemoveRange(db.RequestorPurchases.Where(x => x.PurchaseID == entity.ID));
                db.OnBehalfOfPurchase.RemoveRange(db.OnBehalfOfPurchase.Where(x => x.PurchaseID == entity.ID));
                db.DepartmentPurchases.RemoveRange(db.DepartmentPurchases.Where(x => x.PurchaseID == entity.ID));
                db.SaveChanges();
            }
            
            base.Delete(entity);
        }
    }

}
