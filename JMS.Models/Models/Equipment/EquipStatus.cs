﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EquipStatus
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int SortPos { get; set; }
        public string HexColour { get; set; }
    }

    public class EquipStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string HexColour { get; set; }
    }

    public static class EquipStatusValues
    {
        public static int Construction = 1;
        public static int Live = 2;
        public static int Decommissioned = 3;
    }

    public class EquipStatusMapper : ModelMapper<EquipStatus, EquipStatusViewModel>
    {
        public override void MapToViewModel(EquipStatus model, EquipStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(EquipStatusViewModel viewModel, EquipStatus model)
        {
            throw new NotImplementedException();
        }
    }

}
