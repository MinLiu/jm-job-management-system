﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EquipmentAsset : IEntity
    {
        public EquipmentAsset() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        public Guid EquipmentID { get; set; }
        public string PMNo { get; set; }
        public string Title { get; set; }
        public Guid? ManufacturerID { get; set; }
        public string DrawingNo { get; set; }
        public string SerialNo { get; set; }

        //Foreign References
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Equipment Equipment { get; set; }


    }



    public class EquipmentAssetViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string EquipmentID { get; set; }
        public string PMNo { get; set; }
        public string Title { get; set; }
        public string ManufacturerID { get; set; }
        public string DrawingNo { get; set; }
        public string SerialNo { get; set; }

    }

    public class EquipmentAssetGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string EquipmentID { get; set; }
        public string Title { get; set; }
        public SelectItemViewModel Manufacturer { get; set; }
        public string DrawingNo { get; set; }
        public string PMNo { get; set; }
        public string SerialNo { get; set; }

    }

    public class EquipmentAssetGridMapper : ModelMapper<EquipmentAsset, EquipmentAssetGridViewModel>
    {
        public override void MapToViewModel(EquipmentAsset model, EquipmentAssetGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.EquipmentID = model.EquipmentID.ToString();
            viewModel.Manufacturer = model.ManufacturerID != null ? new SelectItemViewModel() { ID = model.ManufacturerID.ToString(), Name = model.Manufacturer.Name } : new SelectItemViewModel() { ID = "", Name = "" };
            viewModel.DrawingNo = model.DrawingNo;
            viewModel.PMNo = model.PMNo;
            viewModel.Title = model.Title;
            viewModel.SerialNo = model.SerialNo;
        }

        public override void MapToModel(EquipmentAssetGridViewModel viewModel, EquipmentAsset model)
        {
            model.EquipmentID = viewModel.EquipmentID.ToGuid();
            model.ManufacturerID = !string.IsNullOrEmpty(viewModel.Manufacturer.ID) ? viewModel.Manufacturer.ID.ToNullableGuid() : null as Guid?;
            model.DrawingNo = viewModel.DrawingNo;
            model.PMNo = viewModel.PMNo;
            model.Title = viewModel.Title;
            model.SerialNo = viewModel.SerialNo;
        }
    }

}
