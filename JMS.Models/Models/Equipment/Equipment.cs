﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Equipment : IEntity
    {
        public Equipment() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        public int RefID { get; set; }
        [Required]
        public string EquipNo { get; set; }
        public string AssetNo { get; set; }
        public string Title { get; set; }
        public Guid? LocationID { get; set; }
        public Guid? ManufacturerID { get; set; }
        public string EquipType { get; set; }
        public string DrawingNo { get; set; }
        public string SerialNo { get; set; }
        public Guid? ProjectCodeID { get; set; }
        public string PMNo { get; set; }
        public int EquipStatusID { get; set; }
        public DateTime? DOMorPurchase { get; set; }
        public string RiskAssessmentRef { get; set; }
        public string SoftwareAndVersion { get; set; }
        public string PCReference { get; set; }
        public string Note { get; set; }
        public bool PCControlled { get; set; }
        public bool PCNetworked { get; set; }
        public bool AlarmInterlock { get; set; }
        public bool ExtractInterlock { get; set; }
        public DateTime? ElectricalVisualInspectionDate { get; set; }
        public DateTime? ElectricalFullInspectionDate { get; set; }
        public string ElectricalTestNumber { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.

        //Foreign References
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Location Location { get; set; }
        public virtual EquipStatus EquipStatus { get; set; }
        public virtual ICollection<RequestorEquipment> RequestorEquipments { get; set; }
        public virtual ICollection<EngineerEquipment> EngineerEquipments { get; set; }
        public virtual ICollection<DepartmentEquipment> DepartmentEquipments { get; set; }
        public virtual ICollection<EquipmentAsset> EquipmentAssets { get; set; }
        [ForeignKey("ProjectCodeID")]
        public virtual ProjectCode ProjectCode { get; set; }


    }



    public class EquipmentViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public int RefID { get; set; }
        [Required]
        public string EquipNo { get; set; }
        [Required]
        public string Title { get; set; }
        public string AssetNo { get; set; }
        public string LocationID { get; set; }
        public string ManufacturerID { get; set; }
        public string EquipType { get; set; }
        public string DrawingNo { get; set; }
        public string SerialNo { get; set; }
        public string ProjectCodeID { get; set; }
        public string PMNo { get; set; }
        [Required]
        public string EquipStatusID { get; set; }
        public DateTime? DOMorPurchase { get; set; }
        public string RiskAssessmentRef { get; set; }
        public string SoftwareAndVersion { get; set; }
        public string PCReference { get; set; }
        public string Note { get; set; }
        public bool PCControlled { get; set; }
        public bool PCNetworked { get; set; }
        public bool AlarmInterlock { get; set; }
        public bool ExtractInterlock { get; set; }
        public DateTime? ElectricalVisualInspectionDate { get; set; }
        public DateTime? ElectricalFullInspectionDate { get; set; }
        public string ElectricalTestNumber { get; set; }
        public bool Deleted { get; set; }
        public List<string> RequestorIDs { get; set; }
        public List<string> EngineerIDs { get; set; }
        public List<string> DepartmentIDs { get; set; }


    }

    public class EquipmentGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public int RefID { get; set; }
        public string EquipNo { get; set; }
        public string Title { get; set; }
        public string EquipType { get; set; }
        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public int EquipStatusID { get; set; }
        public EquipStatusViewModel EquipStatus { get; set; }
        public string ManufacturerID { get; set; }
        public string Manufacturer { get; set; }
    }




    public class EquipmentMapper : ModelMapper<Equipment, EquipmentViewModel>
    {
        public override void MapToViewModel(Equipment model, EquipmentViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.RefID = model.RefID;
            viewModel.Title = model.Title;
            viewModel.EquipNo = model.EquipNo;
            viewModel.AssetNo = model.AssetNo;
            viewModel.LocationID = model.LocationID.ToString();
            viewModel.EquipType = model.EquipType;
            viewModel.ManufacturerID = model.ManufacturerID.ToString();
            viewModel.SerialNo = model.SerialNo;
            viewModel.DrawingNo = model.DrawingNo;
            viewModel.PMNo = model.PMNo;
            viewModel.ProjectCodeID = model.ProjectCodeID.ToString();
            viewModel.DOMorPurchase = model.DOMorPurchase;
            viewModel.EquipStatusID = model.EquipStatusID.ToString();
            viewModel.PCControlled = model.PCControlled;
            viewModel.PCNetworked = model.PCNetworked;
            viewModel.AlarmInterlock = model.AlarmInterlock;
            viewModel.ExtractInterlock = model.ExtractInterlock;
            viewModel.RiskAssessmentRef = model.RiskAssessmentRef;
            viewModel.SoftwareAndVersion = model.SoftwareAndVersion;
            viewModel.PCReference = model.PCReference;
            viewModel.ElectricalVisualInspectionDate = model.ElectricalVisualInspectionDate;
            viewModel.ElectricalFullInspectionDate = model.ElectricalFullInspectionDate;
            viewModel.ElectricalTestNumber = model.ElectricalTestNumber;
            viewModel.Note = model.Note;
            viewModel.RequestorIDs = model.RequestorEquipments.Select(x => x.RequestorID.ToString()).ToList();
            viewModel.EngineerIDs = model.EngineerEquipments.Select(x => x.EngineerID.ToString()).ToList();
            viewModel.DepartmentIDs = model.DepartmentEquipments.Select(x => x.DepartmentID.ToString()).ToList();
        }

        public override void MapToModel(EquipmentViewModel viewModel, Equipment model)
        {
            model.RefID = viewModel.RefID;
            model.Title = viewModel.Title;
            model.EquipNo = viewModel.EquipNo;
            model.AssetNo = viewModel.AssetNo;
            model.LocationID = viewModel.LocationID.ToNullableGuid();
            model.EquipType = viewModel.EquipType;
            model.ManufacturerID = viewModel.ManufacturerID.ToNullableGuid();
            model.SerialNo = viewModel.SerialNo;
            model.DrawingNo = viewModel.DrawingNo;
            model.PMNo = viewModel.PMNo;
            model.ProjectCodeID = viewModel.ProjectCodeID.ToNullableGuid();
            model.DOMorPurchase = viewModel.DOMorPurchase;
            model.EquipStatusID = int.Parse(viewModel.EquipStatusID);
            model.PCControlled = viewModel.PCControlled;
            model.PCNetworked = viewModel.PCNetworked;
            model.AlarmInterlock = viewModel.AlarmInterlock;
            model.ExtractInterlock = viewModel.ExtractInterlock;
            model.RiskAssessmentRef = viewModel.RiskAssessmentRef;
            model.SoftwareAndVersion = viewModel.SoftwareAndVersion;
            model.PCReference = viewModel.PCReference;
            model.ElectricalVisualInspectionDate = viewModel.ElectricalVisualInspectionDate;
            model.ElectricalFullInspectionDate = viewModel.ElectricalFullInspectionDate;
            model.ElectricalTestNumber = viewModel.ElectricalTestNumber;
            model.Note = viewModel.Note;
        }
    }

    public class EquipmentGridMapper : ModelMapper<Equipment, EquipmentGridViewModel>
    {
        public override void MapToViewModel(Equipment model, EquipmentGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.RefID = model.RefID;
            viewModel.EquipNo = model.EquipNo;
            viewModel.Title = model.Title;
            viewModel.EquipType = model.EquipType;
            viewModel.LocationID = model.LocationID != null ? model.LocationID.ToString() : "";
            viewModel.LocationName = model.Location != null ? model.Location.Name : "";
            viewModel.DepartmentName = String.Join(", ", model.DepartmentEquipments.Select(x => "<a href='/Departments/Goto/" + x.Department.ID + "' target='_blank' title='Go to " + x.Department.Code + "'>" + x.Department.Code + "</a>"));
            viewModel.EquipStatusID = model.EquipStatusID;
            viewModel.EquipStatus = new EquipStatusMapper().MapToViewModel(model.EquipStatus);
            viewModel.ManufacturerID = model.ManufacturerID != null ? model.ManufacturerID.ToString() : "";
            viewModel.Manufacturer = model.Manufacturer != null ? model.Manufacturer.Name : "";
        }

        public override void MapToModel(EquipmentGridViewModel viewModel, Equipment model)
        {
            throw new NotImplementedException();
        }
    }

}
