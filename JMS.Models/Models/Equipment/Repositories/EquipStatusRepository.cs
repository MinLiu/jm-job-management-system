﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EquipStatusRepository : EntityRespository<EquipStatus>
    {
        public EquipStatusRepository()
            : this(new SnapDbContext())
        {

        }

        public EquipStatusRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
