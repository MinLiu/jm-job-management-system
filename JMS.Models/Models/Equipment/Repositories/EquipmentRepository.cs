﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EquipmentRepository : EntityRespository<Equipment>
    {
        public EquipmentRepository()
            : this(new SnapDbContext())
        {

        }

        public EquipmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
