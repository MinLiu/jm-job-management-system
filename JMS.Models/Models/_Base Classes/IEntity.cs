﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{

    public interface IEntity
    {
        //public IEntity() { ID = Guid.NewGuid(); }
        Guid ID { get; set; }
    }


    public interface IEntityViewModel
    {
        [StringLength(128)]
        string ID { get; set; }
    }



}
