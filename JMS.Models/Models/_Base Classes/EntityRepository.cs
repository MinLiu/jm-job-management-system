﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EntityRespository<T> : IDisposable where T : class
    {
        protected DbContext _db;

        public EntityRespository(DbContext db)
        {
            _db = db;
        }

        public virtual void Create(T entity)
        {
            _db.Set<T>().Add(entity);
            _db.SaveChanges();
        }

        public virtual void Create(IEnumerable<T> entities)
        {
            _db.Set<T>().AddRange(entities);
            _db.SaveChanges();
        }

        public virtual DbSet<T> Read()
        {
            return _db.Set<T>();
        }

        public virtual T Find(params object[] keyValues)
        {
            return _db.Set<T>().Find(keyValues);
        }

        public virtual void Update(T entity, string [] updatedColumns = null)
        {
            //_db.Configuration.AutoDetectChangesEnabled = false;

            if ((updatedColumns ?? new string [0]).Length > 0)
            {
                _db.Set<T>().Attach(entity);
                foreach (string col in updatedColumns)
                {
                    _db.Entry(entity).Property(col).IsModified = true;
                    
                }
                _db.SaveChanges();
            }
            else
            {
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }            
        }

        public virtual void Update(IEnumerable<T> entities, string[] updatedColumns = null)
        {
            //_db.Configuration.AutoDetectChangesEnabled = false;

            if ((updatedColumns ?? new string[0]).Length > 0)
            {
                foreach (var e in entities)
                {
                    _db.Set<T>().Attach(e);
                    foreach (string col in updatedColumns)
                    {
                        _db.Entry(e).Property(col).IsModified = true;
                    }
                }                
                
                _db.SaveChanges();
            }
            else
            {
                foreach (var e in entities)
                {
                    _db.Entry(e).State = EntityState.Modified;
                }
                _db.SaveChanges();
            }
        }

        public virtual void Delete(T entity)
        {
            _db.Entry(entity).State = EntityState.Deleted;
            _db.SaveChanges();
        }


        public virtual void Delete(IEnumerable<T> entities)
        {
            foreach (var e in entities)
            {
                _db.Entry(e).State = EntityState.Deleted;
            }
            _db.SaveChanges();
        }


        public virtual void Deattach(T entity)
        {
            _db.Entry(entity).State = EntityState.Detached;
        }

        public virtual void Deattach(IEnumerable<T> entities)
        {
            foreach (var e in entities)
            {
                _db.Entry(e).State = EntityState.Detached;
            }
        }

        public void Dispose()
        {
            if (_db != null)
                _db.Dispose();
        }
    }
}
