﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Requestor : IEntity
    {
        public Requestor() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        [Required]
        public long RefID { get; set; }
        [NotMapped]
        public string Name { get { return FirstName + " " + Surname; } private set { } }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        //Foreign References

        public virtual ICollection<RequestorJob> RequestorJobs { get; set; }
        public virtual ICollection<RequestorPurchase> RequestorPurchases { get; set; }
        public virtual ICollection<RequestorEquipment> RequestorEquipments { get; set; }
    }



    public class RequestorViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public long RefID { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }





    public class RequestorMapper : ModelMapper<Requestor, RequestorViewModel>
    {
        public override void MapToViewModel(Requestor model, RequestorViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.RefID = model.RefID;
            viewModel.Name = String.Format("{0} {1}", model.FirstName, model.Surname);
            viewModel.FirstName = model.FirstName;
            viewModel.Surname = model.Surname;
        }

        public override void MapToModel(RequestorViewModel viewModel, Requestor model)
        {
            model.RefID = viewModel.RefID;
            model.FirstName = viewModel.FirstName;
            model.Surname = viewModel.Surname;
        }

    }


}
