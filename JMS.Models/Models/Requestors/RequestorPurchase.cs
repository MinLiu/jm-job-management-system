﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{

    public class RequestorPurchase
    {

        [Key]
        [Column(Order = 1)]
        public Guid RequestorID { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid PurchaseID { get; set; }

        public virtual Purchase Purchase { get; set; }
        public virtual Requestor Requestor { get; set; }
    }

}
