﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{

    public class RequestorEquipment
    {

        [Key]
        [Column(Order = 1)]
        public Guid RequestorID { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid EquipmentID { get; set; }

        public virtual Equipment Equipment { get; set; }
        public virtual Requestor Requestor { get; set; }
    }

}
