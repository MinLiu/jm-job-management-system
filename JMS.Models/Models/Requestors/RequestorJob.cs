﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{

    public class RequestorJob
    {

        [Key]
        [Column(Order = 1)]
        public Guid RequestorID { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid JobID { get; set; }

        public virtual Job Job { get; set; }
        public virtual Requestor Requestor { get; set; }
    }

}
