﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class RequestorJobRepository : EntityRespository<RequestorJob>
    {
        public RequestorJobRepository()
            : this(new SnapDbContext())
        {

        }

        public RequestorJobRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
