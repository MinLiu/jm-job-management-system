﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class OnBehalfOfPurchaseRepository : EntityRespository<OnBehalfOfPurchase>
    {
        public OnBehalfOfPurchaseRepository()
            : this(new SnapDbContext())
        {

        }

        public OnBehalfOfPurchaseRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
