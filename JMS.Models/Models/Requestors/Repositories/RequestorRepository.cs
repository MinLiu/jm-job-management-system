﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class RequestorRepository : EntityRespository<Requestor>
    {
        public RequestorRepository()
            : this(new SnapDbContext())
        {

        }

        public RequestorRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
