﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class RequestorEquipmentRepository : EntityRespository<RequestorEquipment>
    {
        public RequestorEquipmentRepository()
            : this(new SnapDbContext())
        {

        }

        public RequestorEquipmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
