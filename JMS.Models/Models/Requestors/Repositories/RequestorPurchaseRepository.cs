﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class RequestorPurchaseRepository : EntityRespository<RequestorPurchase>
    {
        public RequestorPurchaseRepository()
            : this(new SnapDbContext())
        {

        }

        public RequestorPurchaseRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
