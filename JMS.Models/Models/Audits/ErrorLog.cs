﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ErrorLog : IEntity
    {
        
        //public ErrorLog() { ID = Guid.NewGuid(); }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public DateTime Timestamp { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string TargetSite { get; set; }
        public string SourceError { get; set; }
        public string StackTrace { get; set; }

        public string URL { get; set; }
        public string GetValues { get; set; }
        public string PostValues { get; set; }

    }


    public class ErrorLogViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public DateTime Timestamp { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string TargetSite { get; set; }
        public string SourceError { get; set; }
        public string StackTrace { get; set; }

        public string URL { get; set; }
        public string GetValues { get; set; }
        public string PostValues { get; set; }
    }

    public class ErrorLogMapper : ModelMapper<ErrorLog, ErrorLogViewModel>
    {

        public override void MapToViewModel(ErrorLog model, ErrorLogViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID;
            viewModel.CompanyName = model.CompanyName;
            viewModel.UserID = model.UserID;
            viewModel.UserName = model.UserName;
            viewModel.Timestamp = model.Timestamp;

            viewModel.Title = model.Title;
            viewModel.Description = model.Description;
            viewModel.TargetSite = model.TargetSite;
            viewModel.SourceError = model.SourceError;
            viewModel.StackTrace = model.StackTrace;
            viewModel.URL = model.URL;
            viewModel.GetValues = model.GetValues;
            viewModel.PostValues = model.PostValues;
        }


        public override void MapToModel(ErrorLogViewModel viewModel, ErrorLog model)
        {
            throw new NotImplementedException();
        }

    }

}
