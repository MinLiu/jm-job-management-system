﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ModuleType
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }        
        public string Name { get; set; }
        public string IndexURL { get; set; }
        public string EditURL { get; set; }
        public string HexColour { get; set; }

    }


    public static class ModuleTypeValues
    {
        public static int PRODUCTS = 1;
        public static int CLIENTS = 2;
        public static int SUPPLIERS = 3;

        public static int QUOTATIONS = 4;
        public static int JOBS = 5;
        public static int DELIVERY_NOTES = 6;
        public static int INVOICES = 7;
        public static int PURCHASE_ORDERS = 8;
        public static int STOCK_CONTROL = 9;
        
    }


    public class ModuleTypeViewModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string IndexURL { get; set; }
        public string EditURL { get; set; }
        public string HexColour { get; set; }
    }




    public class ModuleTypeMapper : ModelMapper<ModuleType, ModuleTypeViewModel>
    {
        public override void MapToViewModel(ModuleType model, ModuleTypeViewModel viewModel)
        {              
            viewModel.ID = model.ID;
            viewModel.Code = model.Code;
            viewModel.Name = model.Name;
            viewModel.IndexURL = model.IndexURL;
            viewModel.EditURL = model.EditURL;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(ModuleTypeViewModel viewModel, ModuleType model)
        {           
            //model.ID = viewModel.ID;
            model.Code = viewModel.Code;
            model.Name = viewModel.Name;
            model.IndexURL = viewModel.IndexURL;
            model.EditURL = viewModel.EditURL;
            model.HexColour = viewModel.HexColour;
        }

    }

}
