﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{

    public class Job : IEntity
    {
        public Job() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public long JobID { get; set; }
        public string Title { get; set; }
        public Guid? DepartmentID { get; set; }
        public Guid? LocationID { get; set; }
        public Guid? ProjectCodeID { get; set; }
        public string DrawingNo { get; set; }
        public Guid? EquipmentNoID { get; set; }
        public Guid? JobTypeID { get; set; }
        public int? UrgencyLevelID { get; set; }
        public string ExternalProjectID { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? RequiredByDate { get; set; }
        public DateTime? PlannedStartDate { get; set; }
        public DateTime? EstCompleteionDate { get; set; }
        public int PercentComplete { get; set; }
        public DateTime? EstIssuesCompletionDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public bool OnHarvest { get; set; }
        public bool StageConcept { get; set; }
        public bool StageLaunch { get; set; }
        public bool StagePrelimDesign { get; set; }
        public bool StageFinalDesign { get; set; }
        public bool StageConstruction { get; set; }
        public bool StageDelivery { get; set; }
        public int StatusJobID { get; set; }
        public bool StatusInfoRequired { get; set; }
        public int StatusDesignID { get; set; }
        public int StatusElectricalID { get; set; }
        public int StatusSoftwareID { get; set; }
        public int StatusWorkshopID { get; set; }
        public decimal? StatusWorkshopEstHrs { get; set; }
        public bool TrackingStarted { get; set; }
        public bool TrackingDone { get; set; }
        public string FileLocation { get; set; }
        public string HistoricJID { get; set; }
        public string Note { get; set; }

        public virtual Department Department { get; set; }
        public virtual Location Location { get; set; }
        [ForeignKey("ProjectCodeID")]
        public virtual ProjectCode ProjectCode { get; set; }
        [ForeignKey("EquipmentNoID")]
        public virtual Equipment Equipment { get; set; }
        public virtual JobUrgencyLevel UrgencyLevel { get; set; }
        public virtual ICollection<JobIssue> Issues { get; set; }
        public virtual ICollection<JobStatusHistory> StatusHistories { get; set; }
        public virtual ICollection<EngineerJob> EngineerJobs { get; set; }
        public virtual ICollection<RequestorJob> RequestorJobs { get; set; }
        [ForeignKey("StatusDesignID")]
        public virtual JobDesignStatus DesignStatus { get; set; }
        [ForeignKey("StatusWorkshopID")]
        public virtual JobWorkshopStatus WorkshopStatus { get; set; }
        [ForeignKey("StatusElectricalID")]
        public virtual JobElectricalStatus ElectricalStatus { get; set; }
        [ForeignKey("StatusSoftwareID")]
        public virtual JobSoftwareStatus SoftwareStatus { get; set; }
        [ForeignKey("StatusJobID")]
        public virtual JobStatus JobStatus { get; set; }
        public virtual JobType JobType { get; set; }
        public virtual ICollection<Purchase> JobPurchases { get; set; } 


    }




    public class JobViewModel : IEntityViewModel
    {
        //[Key]
        public string ID { get; set; }
        public long JobID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required(ErrorMessage = "The department field is required.")]
        public string DepartmentID { get; set; }
        public string LocationID { get; set; }
        public string ProjectCodeID { get; set; }
        public string DrawingNo { get; set; }
        public string EquipmentNoID { get; set; }
        [Required(ErrorMessage = "The job type field is required.")]
        public string JobTypeID { get; set; }
        public int? UrgencyLevelID { get; set; }
        public string ExternalProjectID { get; set; }
        public bool OnHarvest { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? StartedDate { get; set; }
        public DateTime? RequiredByDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public int EstimatedCompletion { get; set; }
        public DateTime? ClosedDate { get; set; }
        public bool StageConcept { get; set; }
        public bool StageLaunch { get; set; }
        public bool StagePrelimDesign { get; set; }
        public bool StageFinalDesign { get; set; }
        public bool StageConstruction { get; set; }
        public bool StageDelivery { get; set; }
        public int StatusJobID { get; set; }
        public bool StatusInfoRequired { get; set; }
        public int StatusDesignID { get; set; }
        public int StatusElectricalID { get; set; }
        public int StatusSoftwareID { get; set; }
        public int StatusWorkshopID { get; set; }
        public decimal? StatusWorkshopEstHrs { get; set; }
        public string Note { get; set; }
        public string HistoricJID { get; set; }
        public bool TrackingStarted { get; set; }
        public bool TrackingDone { get; set; }
        public string FileLocation { get; set; }
        public List<string> EngineerIDs { get; set; }
        [Required(ErrorMessage = "The requestor field is required.")]
        public List<string> RequestorIDs { get; set; }

    }


    public class JobGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public long JobID { get; set; }
        public string Title { get; set; }
        public string JobTypeID { get; set; }
        public string JobType { get; set; }
        public int? UrgencyLevelID { get; set; }
        public JobUrgencyLevelViewModel UrgencyLevel { get; set; }
        public string DepartmentID { get; set; }
        public string Department { get; set; }
        public string LocationID { get; set; }
        public string Location { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectCodeID { get; set; }
        public bool OnHarvest { get; set; }
        public bool TrackingStarted { get; set; }
        public bool TrackingDone { get; set; }
        public DateTime? ClosedDate { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? PlannedStartDate { get; set; }
        public DateTime? EstCompleteionDate { get; set; }
        public int PercentComplete { get; set; }
        public int StatusJobID { get; set; }
        public JobStatusViewModel JobStatus { get; set; }
        public decimal TotalSpend { get; set; }

    }


    public class JobMapper : ModelMapper<Job, JobViewModel>
    {
        public override void MapToViewModel(Job model, JobViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.JobID = model.JobID;
            viewModel.ClosedDate = model.ClosedDate;
            viewModel.DepartmentID = model.DepartmentID.ToString();
            viewModel.DrawingNo = model.DrawingNo;
            viewModel.EquipmentNoID = model.EquipmentNoID.ToString();
            viewModel.CompletionDate = model.EstCompleteionDate;
            viewModel.ExternalProjectID = model.ExternalProjectID;
            viewModel.JobTypeID = model.JobTypeID.ToString();
            viewModel.LocationID = model.LocationID.ToString();
            viewModel.OnHarvest = model.OnHarvest;
            viewModel.EstimatedCompletion = model.PercentComplete;
            viewModel.StartedDate = model.PlannedStartDate;
            viewModel.ProjectCodeID = model.ProjectCodeID.ToString();
            viewModel.RequiredByDate = model.RequiredByDate;
            viewModel.StageConcept = model.StageConcept;
            viewModel.StageConstruction = model.StageConstruction;
            viewModel.StageDelivery = model.StageDelivery;
            viewModel.StageFinalDesign = model.StageFinalDesign;
            viewModel.StageLaunch = model.StageLaunch;
            viewModel.StagePrelimDesign = model.StagePrelimDesign;
            viewModel.StatusDesignID = model.StatusDesignID;
            viewModel.StatusElectricalID = model.StatusElectricalID;
            viewModel.StatusInfoRequired = model.StatusInfoRequired;
            viewModel.StatusJobID = model.StatusJobID;
            viewModel.StatusSoftwareID = model.StatusSoftwareID;
            viewModel.StatusWorkshopID = model.StatusWorkshopID;
            viewModel.StatusWorkshopEstHrs = model.StatusWorkshopEstHrs;
            viewModel.SubmittedDate = model.SubmittedDate;
            viewModel.Title = model.Title;
            viewModel.UrgencyLevelID = model.UrgencyLevelID;
            viewModel.EngineerIDs = model.EngineerJobs.Select(x => x.Engineer.ID.ToString()).ToList();
            viewModel.RequestorIDs = model.RequestorJobs.Select(x => x.Requestor.ID.ToString()).ToList();
            viewModel.Note = model.Note;
            viewModel.HistoricJID = model.HistoricJID;
            viewModel.TrackingDone = model.TrackingDone;
            viewModel.TrackingStarted = model.TrackingStarted;
            viewModel.FileLocation = model.FileLocation;
        }

        public override void MapToModel(JobViewModel viewModel, Job model)
        {
            model.JobID = viewModel.JobID;
            model.ClosedDate = viewModel.ClosedDate;
            model.DepartmentID = viewModel.DepartmentID.ToNullableGuid();
            model.DrawingNo = viewModel.DrawingNo;
            model.EquipmentNoID = viewModel.EquipmentNoID.ToNullableGuid();
            model.EstCompleteionDate = viewModel.CompletionDate;
            model.ExternalProjectID = viewModel.ExternalProjectID;
            model.JobTypeID = viewModel.JobTypeID.ToNullableGuid();
            model.LocationID = viewModel.LocationID.ToNullableGuid();
            model.OnHarvest = viewModel.OnHarvest;
            model.PercentComplete = viewModel.EstimatedCompletion;
            model.PlannedStartDate = viewModel.StartedDate;
            model.ProjectCodeID = viewModel.ProjectCodeID.ToNullableGuid();
            model.RequiredByDate = viewModel.RequiredByDate;
            model.StageConcept = viewModel.StageConcept;
            model.StageConstruction = viewModel.StageConstruction;
            model.StageDelivery = viewModel.StageDelivery;
            model.StageFinalDesign = viewModel.StageFinalDesign;
            model.StageLaunch = viewModel.StageLaunch;
            model.StagePrelimDesign = viewModel.StagePrelimDesign;
            model.StatusDesignID = viewModel.StatusDesignID;
            model.StatusElectricalID = viewModel.StatusElectricalID;
            model.StatusInfoRequired = viewModel.StatusInfoRequired;
            model.StatusJobID = viewModel.StatusJobID;
            model.StatusSoftwareID = viewModel.StatusSoftwareID;
            model.StatusWorkshopID = viewModel.StatusWorkshopID;
            model.StatusWorkshopEstHrs = viewModel.StatusWorkshopEstHrs;
            model.SubmittedDate = viewModel.SubmittedDate;
            model.Title = viewModel.Title;
            model.UrgencyLevelID = viewModel.UrgencyLevelID;
            model.Note = viewModel.Note;
            model.HistoricJID = viewModel.HistoricJID;
            model.TrackingStarted = viewModel.TrackingStarted;
            model.TrackingDone = viewModel.TrackingDone;
            model.FileLocation = viewModel.FileLocation;
        }


        public void MapToModelHeader(JobViewModel viewModel, Job model)
        {

           

        }

    }


    public class JobGridMapper : ModelMapper<Job, JobGridViewModel>
    {
        public override void MapToViewModel(Job model, JobGridViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.JobID = model.JobID;
            viewModel.DepartmentID = model.Department != null ? model.Department.ID.ToString() : "";
            viewModel.Department = model.Department != null ? model.Department.Code : "";
            viewModel.LocationID = model.LocationID != null ? model.LocationID.ToString() : "";
            viewModel.Location = model.Location != null ? model.Location.Name : "";
            viewModel.ProjectCodeID = model.ProjectCodeID.ToString();
            viewModel.ProjectCode = model.ProjectCode != null ? model.ProjectCode.Code : "";
            viewModel.Title = model.Title;
            viewModel.JobTypeID = model.JobTypeID != null ? model.JobTypeID.ToString() : "";
            viewModel.JobType = model.JobType != null ? model.JobType.Name : "";
            viewModel.UrgencyLevelID = model.UrgencyLevelID;
            viewModel.UrgencyLevel = model.UrgencyLevel != null ? new JobUrgencyLevelMapper().MapToViewModel(model.UrgencyLevel) : new JobUrgencyLevelViewModel();
            viewModel.OnHarvest = model.OnHarvest;
            viewModel.TrackingStarted = model.TrackingStarted;
            viewModel.TrackingDone = model.TrackingDone;
            viewModel.ClosedDate = model.ClosedDate;
            viewModel.SubmittedDate = model.SubmittedDate;
            viewModel.PlannedStartDate = model.PlannedStartDate;
            viewModel.EstCompleteionDate = model.EstCompleteionDate;
            viewModel.PercentComplete = model.PercentComplete;
            viewModel.JobStatus = new JobStatusMapper().MapToViewModel(model.JobStatus);
            viewModel.StatusJobID = model.StatusJobID;
            viewModel.TotalSpend = model.JobPurchases.Where(x => x.PurchaseStatusID == PurchaseStatusValues.Ordered).Any() ? model.JobPurchases.Where(x => x.PurchaseStatusID == PurchaseStatusValues.Ordered).Sum(x => x.PurchaseTotal) : 0;
        }

        public override void MapToModel(JobGridViewModel viewModel, Job model)
        {
            throw new NotImplementedException();            
        }
    }
}
