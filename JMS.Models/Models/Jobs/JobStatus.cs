﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobStatus 
    {
        [Key]
        public int ID { get; set; }        
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
        public int SortPos { get; set; }

    }

    public static class JobStatusValues
    {
        public static int Closed = 1;
        public static int ClosedCancelled = 2;
        public static int Open = 3;
        public static int OpenOnHold = 4;
    }



    public class JobStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
    }




    public class JobStatusMapper : ModelMapper<JobStatus, JobStatusViewModel>
    {
        public override void MapToViewModel(JobStatus model, JobStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.Description = model.Description;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(JobStatusViewModel viewModel, JobStatus model)
        {
            model.Name = viewModel.Name;
            model.Description = viewModel.Description;
            model.HexColour = viewModel.HexColour;
        }

    }
        
}
