﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobIssue : IEntity
    {
        public JobIssue() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid JobID { get; set; }
        public DateTime EstCompleteDate { get; set; }
        public string Details { get; set; }  
        public bool Complete { get; set; }
       
        public virtual Job Job { get; set; }
    }


    public class JobIssueGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string JobID { get; set; }

        public DateTime EstCompleteDate { get; set; }
        [Required(ErrorMessage = "* The Details field is required.")]
        public string Details { get; set; }
        public bool Complete { get; set; }

    }

    public class JobIssueMapper : ModelMapper<JobIssue, JobIssueGridViewModel>
    {

        public override void MapToViewModel(JobIssue model, JobIssueGridViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.JobID = model.JobID.ToString();
            viewModel.EstCompleteDate = model.EstCompleteDate;
            viewModel.Details = model.Details;
            viewModel.Complete = model.Complete;
        }


        public override void MapToModel(JobIssueGridViewModel viewModel, JobIssue model)
        {
            model.JobID = Guid.Parse(viewModel.JobID);
            model.EstCompleteDate = viewModel.EstCompleteDate;
            model.Details = viewModel.Details;
            model.Complete = viewModel.Complete;
        }

    }

}
