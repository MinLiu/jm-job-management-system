﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobUrgencyLevel
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public int SortPos { get; set; }
        public string HexColour { get; set; }

    }

    public static class JobUrgencyLevelValues
    {
        public static int Safety = 1;
        public static int Low = 2;
        public static int Medium = 3;
        public static int High = 4;
    }

    public class JobUrgencyLevelViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string HexColour { get; set; }
    }

    public class JobUrgencyLevelMapper : ModelMapper<JobUrgencyLevel, JobUrgencyLevelViewModel>
    {
        public override void MapToViewModel(JobUrgencyLevel model, JobUrgencyLevelViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(JobUrgencyLevelViewModel viewModel, JobUrgencyLevel model)
        {
            model.Name = viewModel.Name;
        }
    }
}
