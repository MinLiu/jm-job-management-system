﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobElectricalStatus
    {
        [Key]
        public int ID { get; set; }        
        public string Name { get; set; }
        public string HexColour { get; set; }
        public int SortPos { get; set; }

    }

    public static class JobElectricalStatusValues
    {
        public static int Awaiting = 1;
        public static int InProgress = 2;
        public static int Fin = 3;
        public static int NA = 4;
    }



    public class JobElectricalStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
    }




    public class JobElectricalStatusMapper : ModelMapper<JobElectricalStatus, JobElectricalStatusViewModel>
    {
        public override void MapToViewModel(JobElectricalStatus model, JobElectricalStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(JobElectricalStatusViewModel viewModel, JobElectricalStatus model)
        {
            model.Name = viewModel.Name;
            model.HexColour = viewModel.HexColour;
        }

    }
        
}
