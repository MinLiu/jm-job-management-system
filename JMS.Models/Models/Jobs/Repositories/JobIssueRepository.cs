﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobIssueRepository : EntityRespository<JobIssue>
    {
        public JobIssueRepository()
            : this(new SnapDbContext())
        {

        }

        public JobIssueRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
