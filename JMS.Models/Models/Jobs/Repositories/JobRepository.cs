﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobRepository : EntityRespository<Job>
    {
        public JobRepository()
            : this(new SnapDbContext())
        {

        }

        public JobRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Job entity)
        {
            _db.Set<JobStatusHistory>().RemoveRange(_db.Set<JobStatusHistory>().Where(p => p.JobID == entity.ID));
            _db.SaveChanges();

            var set = _db.Set<Job>().Where(a => a.ID == entity.ID);

            _db.Set<Job>().RemoveRange(set);
            _db.SaveChanges();
            //base.Delete(entity);
        }

    }

}
