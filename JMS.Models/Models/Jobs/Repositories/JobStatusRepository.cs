﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobStatusRepository : EntityRespository<JobStatus>
    {
        public JobStatusRepository()
            : this(new SnapDbContext())
        {

        }

        public JobStatusRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
