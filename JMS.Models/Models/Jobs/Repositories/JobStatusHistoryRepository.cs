﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobStatusHistoryRepository : EntityRespository<JobStatusHistory>
    {
        public JobStatusHistoryRepository()
            : this(new SnapDbContext())
        {

        }

        public JobStatusHistoryRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
