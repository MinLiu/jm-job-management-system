﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobSoftwareStatus
    {
        [Key]
        public int ID { get; set; }        
        public string Name { get; set; }
        public string HexColour { get; set; }
        public int SortPos { get; set; }

    }

    public static class JobSoftwareStatusValues
    {
        public static int Awaiting = 1;
        public static int InProgress = 2;
        public static int Fin = 3;
        public static int NA = 4;
    }



    public class JobSoftwareStatusViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
    }




    public class JobSoftwareStatusMapper : ModelMapper<JobSoftwareStatus, JobSoftwareStatusViewModel>
    {
        public override void MapToViewModel(JobSoftwareStatus model, JobSoftwareStatusViewModel viewModel)
        {
            viewModel.ID = model.ID;
            viewModel.Name = model.Name;
            viewModel.HexColour = model.HexColour;
        }

        public override void MapToModel(JobSoftwareStatusViewModel viewModel, JobSoftwareStatus model)
        {
            model.Name = viewModel.Name;
            model.HexColour = viewModel.HexColour;
        }

    }
        
}
