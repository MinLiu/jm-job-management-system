﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobType : IEntity
    {
        public JobType() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }

        //Foreign References
        public virtual ICollection<Job> Jobs { get; set; }
    }

    public class JobTypeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class JobTypeMapper : ModelMapper<JobType, JobTypeViewModel>
    {
        public override void MapToViewModel(JobType model, JobTypeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
        }

        public override void MapToModel(JobTypeViewModel viewModel, JobType model)
        {
            model.Name = viewModel.Name;
        }

    }
}
