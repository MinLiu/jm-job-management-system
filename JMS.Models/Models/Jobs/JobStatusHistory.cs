﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class JobStatusHistory : IEntity
    {
        public JobStatusHistory() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid JobID { get; set; }

        public DateTime Timestamp { get; set; }
        public string Message { get; set; }        
       
        public virtual Job Job { get; set; }
    }


    public class JobStatusHistoryGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string JobID { get; set; }

        public DateTime Timestamp { get; set; }

        [Required(ErrorMessage = "* The Message field is required.")]
        public string Message { get; set; }

    }

    public class JobStatusHistoryMapper : ModelMapper<JobStatusHistory, JobStatusHistoryGridViewModel>
    {

        public override void MapToViewModel(JobStatusHistory model, JobStatusHistoryGridViewModel viewModel)
        {           
            viewModel.ID = model.ID.ToString();
            viewModel.JobID = model.JobID.ToString();
            viewModel.Timestamp = model.Timestamp;
            viewModel.Message = model.Message; 
        }


        public override void MapToModel(JobStatusHistoryGridViewModel viewModel, JobStatusHistory model)
        {
            model.JobID = Guid.Parse(viewModel.JobID);
            model.Timestamp = viewModel.Timestamp;
            model.Message = viewModel.Message;
        }

    }

}
