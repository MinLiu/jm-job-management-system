﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Supplier : IEntity
    {
        public Supplier() {  ID = Guid.NewGuid(); }
        
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public int RefID { get; set; }
        public Guid? SupplierTypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public string AccountDetail { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.
       

        public virtual SupplierType SupplierType { get; set; }
        public virtual ICollection<SupplierContact> SupplierContacts { get; set; }
        public virtual ICollection<Purchase> Purchases { get; set; }
    }



    public class SupplierViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public int RefID { get; set; }

        public string SupplierTypeID { get; set; }
        public string SupplierTypeName { get; set; }
        public string AccountDetail { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPositionHeld { get; set; }

        [Required(ErrorMessage = "* The Supplier Name field is required.")]
        [Display(Name = "Supplier Name")]
        public string Name { get; set; }

        public string SupplierTagsHtmlString { get; set; }

        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
    }





    public class SupplierMapper : ModelMapper<Supplier, SupplierViewModel>
    {
        public override void MapToViewModel(Supplier model, SupplierViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierTypeID = (model.SupplierType != null) ? model.SupplierTypeID.ToString() : null;
            viewModel.SupplierTypeName = (model.SupplierType != null) ? model.SupplierType.Name : "";
            viewModel.Name = model.Name;
            viewModel.Deleted = model.Deleted;
            var contact = model.SupplierContacts.FirstOrDefault();
            viewModel.ContactName = contact != null ? contact.Name : "";
            viewModel.ContactEmail = contact != null ? contact.Email : "";
            viewModel.ContactPhone = contact != null ? contact.Telephone : "";
            viewModel.ContactPositionHeld = contact != null ? contact.PositionHeld : "";
            viewModel.RefID = model.RefID;
            viewModel.AccountDetail = model.AccountDetail;
        }

        public override void MapToModel(SupplierViewModel viewModel, Supplier model)
        {
            // ID = viewModel.ID;
            //model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.SupplierTypeID = (!string.IsNullOrEmpty(viewModel.SupplierTypeID)) ? Guid.Parse(viewModel.SupplierTypeID) as Guid? : null;
            model.Name = viewModel.Name;
            model.RefID = viewModel.RefID;
            model.AccountDetail = viewModel.AccountDetail;
        }

    }


}
