﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class SupplierContact : IEntity
    {
        public SupplierContact() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid SupplierID { get; set; }
        public string Name { get; set; }
        public string PositionHeld { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }

        public virtual Supplier Supplier { get; set; }

    }

    public class SupplierContactItemViewModel
    {
        public string ID { get; set; }
        public string SupplierID { get; set; }
        public string Name { get; set; }
    }


    public class SupplierContactViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        [Required(ErrorMessage = "* The Name field is required.")]
        public string Name { get; set; }

        public string PositionHeld { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public bool Deleted { get; set; }
    }



    public class SupplierContactMapper : ModelMapper<SupplierContact, SupplierContactViewModel>
    {
        public override void MapToViewModel(SupplierContact model, SupplierContactViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.SupplierID = model.SupplierID.ToString();
            viewModel.SupplierName = model.Supplier.Name;
            viewModel.Name = model.Name;
            viewModel.PositionHeld = model.PositionHeld;
            viewModel.Telephone = model.Telephone;
            viewModel.Email = model.Email;
        }

        public override void MapToModel(SupplierContactViewModel viewModel, SupplierContact model)
        {          
            //ID = viewModel.ID;
            model.SupplierID = Guid.Parse(viewModel.SupplierID);
            model.PositionHeld = viewModel.PositionHeld;
            model.Telephone = viewModel.Telephone;
            model.Email = viewModel.Email;
            model.Name = viewModel.Name;
            //Deleted = viewModel.Deleted;
        }

    }
}
