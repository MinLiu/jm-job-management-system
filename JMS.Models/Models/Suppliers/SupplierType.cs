﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class SupplierType : IEntity
    {
        public SupplierType() { ID = Guid.NewGuid(); }

        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }

        //Foreign References
        public virtual ICollection<Supplier> Suppliers { get; set; }
    }




    public class SupplierTypeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }



    public class SupplierTypeMapper : ModelMapper<SupplierType, SupplierTypeViewModel>
    {
        public override void MapToViewModel(SupplierType model, SupplierTypeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
        }

        public override void MapToModel(SupplierTypeViewModel viewModel, SupplierType model)
        {
            //model.ID = viewModel.ID;
            model.Name = viewModel.Name;
        }

    }
}
