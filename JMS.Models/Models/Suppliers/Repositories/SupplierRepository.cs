﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class SupplierRepository : EntityRespository<Supplier>
    {
        public SupplierRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierRepository(SnapDbContext db)
            : base(db)
        {

        }

        public override void Delete(Supplier entity)
        {

            _db.Set<SupplierContact>().RemoveRange(_db.Set<SupplierContact>().Where(x => x.SupplierID == entity.ID));

            var set = _db.Set<Supplier>().Where(a => a.ID == entity.ID);
                                    //.Include(a => a.Quotations)
                                    //.Include(a => a.Jobs)
                                    //.Include(a => a.Invoices)
                                    //.Include(a => a.DeliveryNotes);
            _db.Set<Supplier>().RemoveRange(set);
            _db.SaveChanges();
        }


    }

}
