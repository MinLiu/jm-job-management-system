﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class SupplierTypeRepository : EntityRespository<SupplierType>
    {
        public SupplierTypeRepository()
            : this(new SnapDbContext())
        {

        }

        public SupplierTypeRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(SupplierType entity)
        {
            var set = _db.Set<SupplierType>().Where(a => a.ID == entity.ID)
                                    .Include(a => a.Suppliers);

            _db.Set<SupplierType>().RemoveRange(set);
            _db.SaveChanges();
        }

    }

}
