﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Product : IEntity
    {
        public Product() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public int CurrencyID { get; set; }
                
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string DetailedDescription { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
        public string ImageURL { get; set; }
        public string AccountCode { get; set; }


        public bool IsKit { get; set; }
        public bool UseKitPrice { get; set; }
        public bool UsePriceBreaks { get; set; }
        public bool IsBought { get; set; }

        public bool Deleted { get; set; }

        public decimal Price { get; set; }
        public decimal Cost { get; set; }        
        public decimal VAT { get; set; }

        public int MinimumQty { get; set; }
        public int MaximumQty { get; set; }

        public virtual Company Company { get; set; }
        public virtual Currency Currency { get; set; }
        //public virtual ProductCategory Category { get; set; }
        public virtual ICollection<ProductPrice> Prices { get; set; }
        public virtual ICollection<ProductCost> Costs { get; set; }
        public virtual ICollection<ProductKitItem> KitItems { get; set; }

        //public virtual ICollection<ProductAlternative> ProductAlternatives { get; set; }
        //public virtual ICollection<ProductAssociative> ProductAssociatives { get; set; }
        

        ////Foreign References
        //public virtual ICollection<QuotationItem> QuotationItems { get; set; }
        //public virtual ICollection<JobItem> JobItems { get; set; }
        //public virtual ICollection<InvoiceItem> InvoiceItems { get; set; }
        //public virtual ICollection<DeliveryNoteItem> DeliveryNoteItems { get; set; }
        //public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems { get; set; } 

    }


    public class ProductViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        
        public int CurrencyID { get; set; }
        public string Currency { get; set; }

        [Required(ErrorMessage = "* The Product Code field is required.")]
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        [Required(ErrorMessage = "* The Description field is required.")]
        public string Description { get; set; }
        public string DetailedDescription { get; set; }
        //public string DescriptionShort { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }

        [Display(Name = "Vendor Code")]
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
        public string ImageURL { get; set; }
        public string AccountCode { get; set; }


        public bool IsKit { get; set; }
        public bool UseKitPrice { get; set; }
        public bool UsePriceBreaks { get; set; }
        public bool IsBought { get; set; }

        public bool Deleted { get; set; }

        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public decimal VAT { get; set; }
    }


    public class ProductGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }        
        
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
        public string ImageURL { get; set; }
        public string AccountCode { get; set; }


        public bool IsKit { get; set; }
        public bool UseKitPrice { get; set; }
        public bool UsePriceBreaks { get; set; }
        public bool IsBought { get; set; }

        public bool Deleted { get; set; }

        public string Prices { get; set; }
        public string Costs { get; set; }

        public string MainSupplier { get; set; }
        public decimal? AgreedDiscount { get; set; }
    }



    public class ProductMapper : ModelMapper<Product, ProductViewModel>
    {

        public override void MapToViewModel(Product model, ProductViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.ProductCode = model.ProductCode;            
            viewModel.CurrencyID = model.CurrencyID;
            viewModel.Currency = model.Currency.Code;            
            //viewModel.DescriptionShort = (model.Description ?? "").Split('\n')[0];
            viewModel.Description = model.Description ?? "";
            viewModel.DetailedDescription = model.DetailedDescription ?? "";
            viewModel.Category = model.Category;
            viewModel.VendorCode = model.VendorCode;
            viewModel.Manufacturer = model.Manufacturer;
            viewModel.AccountCode = model.AccountCode;
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL;
            viewModel.Deleted = model.Deleted;
            viewModel.IsKit = model.IsKit;
            viewModel.UseKitPrice = model.UseKitPrice;
            viewModel.UsePriceBreaks = model.UsePriceBreaks;
            viewModel.IsBought = model.IsBought;

            viewModel.Price = model.Price;
            viewModel.Cost = model.Cost;
            viewModel.VAT = model.VAT;
            
        }


        public override void MapToModel(ProductViewModel viewModel, Product model)
        {            
            //ID = viewModel.ID,
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.ProductCode = viewModel.ProductCode;            
            model.CurrencyID = viewModel.CurrencyID;
            model.Description = viewModel.Description;
            model.DetailedDescription = viewModel.DetailedDescription;
            model.Category = viewModel.Category;
            model.Unit = viewModel.Unit;
            model.Manufacturer = viewModel.Manufacturer;
            model.VendorCode = viewModel.VendorCode;
            model.AccountCode = viewModel.AccountCode;
            model.ImageURL = viewModel.ImageURL;
            model.IsKit = viewModel.IsKit;
            model.UseKitPrice = (!viewModel.IsKit) ? false : viewModel.UseKitPrice;
            model.UsePriceBreaks = viewModel.UsePriceBreaks;
            model.IsBought = (viewModel.IsKit) ? false : viewModel.IsBought;
            model.Price = viewModel.Price;
            model.Cost = viewModel.Cost;
            model.VAT = viewModel.VAT;
        }

    }



    public class ProductGridMapper : ModelMapper<Product, ProductGridViewModel>
    {

        public override void MapToViewModel(Product model, ProductGridViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.ProductCode = model.ProductCode;            
            viewModel.Description = (model.Description ?? "").Split('\n')[0];
            viewModel.Category = model.Category ?? "";
            viewModel.VendorCode = model.VendorCode;
            viewModel.Manufacturer = model.Manufacturer;
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL ?? "/Product Images/NotFound.png";
            viewModel.AccountCode = model.AccountCode;
            viewModel.Deleted = model.Deleted;
            viewModel.IsKit = model.IsKit;
            viewModel.UseKitPrice = model.UseKitPrice;
            viewModel.UsePriceBreaks = model.UsePriceBreaks;
            viewModel.IsBought = model.IsBought;

            viewModel.Prices = "";
            viewModel.Costs = "";

            if (model.UsePriceBreaks)
            {
                decimal minP = 0.0m;
                decimal maxP = 0.0m;

                decimal minC = 0.0m;
                decimal maxC = 0.0m;

                foreach (var currency in model.Prices.Select(p => p.Currency).Distinct())
                {
                    minP = model.Prices.Where(p => p.CurrencyID == currency.ID).OrderBy(p => p.Price).Select(p => p.Price).DefaultIfEmpty(0m).First();
                    maxP = model.Prices.Where(p => p.CurrencyID == currency.ID).OrderByDescending(p => p.Price).Select(p => p.Price).DefaultIfEmpty(0m).First();

                    minC = model.Costs.Where(p => p.CurrencyID == currency.ID).OrderBy(p => p.Cost).Select(p => p.Cost).DefaultIfEmpty(0m).First();
                    maxC = model.Costs.Where(p => p.CurrencyID == currency.ID).OrderByDescending(p => p.Cost).Select(p => p.Cost).DefaultIfEmpty(0m).First();

                    if (minP == maxP) { viewModel.Prices += string.Format("<br />{0}", minP.ToString(currency.Symbol + "#,##0.#0")); }
                    else { viewModel.Prices += string.Format("<br />{0} ~ {1}", minP.ToString(currency.Symbol + "#,##0.#0"), maxP.ToString(currency.Symbol + "#,##0.#0")); }

                    if (minC == maxC) { viewModel.Costs += string.Format("<br />{0}", minC.ToString(currency.Symbol + "#,##0.#0")); }
                    else { viewModel.Costs += string.Format("<br />{0} ~ {1}", minC.ToString(currency.Symbol + "#,##0.#0"), maxC.ToString(currency.Symbol + "#,##0.#0")); }
                }

                if (viewModel.Prices.StartsWith("<br />")) viewModel.Prices = viewModel.Prices.Substring(6);
                if (viewModel.Costs.StartsWith("<br />")) viewModel.Costs = viewModel.Costs.Substring(6);

            }
            else if ((model.UseKitPrice && !model.UsePriceBreaks) || !model.IsKit)
            {
                viewModel.Prices += string.Format("{0}", model.Price.ToString(model.Currency.Symbol + "#,##0.#0"));
                viewModel.Costs += string.Format("{0}", model.Cost.ToString(model.Currency.Symbol + "#,##0.#0"));
            }

        }


        public override void MapToModel(ProductGridViewModel viewModel, Product model)
        {
            throw new NotImplementedException();
        }

    }

    #region StockOverviewViewModel

    public class StockOverviewViewModel : IEntityViewModel
    {
        public string ID { get; set; }

        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }
        public string ImageURL { get; set; }

        public decimal Cost { get; set; }

        public int MinimumQty { get; set; }
        public int MaximumQty { get; set; }
        public decimal Qty { get; set; }
    }

    public class StockOverviewMapper : ModelMapper<Product, StockOverviewViewModel>
    {
        public override void MapToViewModel(Product model, StockOverviewViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductCode = model.ProductCode;
            viewModel.Description = (model.Description ?? "").Split('\n')[0];
            viewModel.Category = model.Category;
            viewModel.VendorCode = model.VendorCode;
            viewModel.Manufacturer = model.Manufacturer;
            viewModel.Unit = model.Unit;
            viewModel.ImageURL = model.ImageURL;
            viewModel.MaximumQty = model.MaximumQty;
            viewModel.MinimumQty = model.MinimumQty;
        }

        public override void MapToModel(StockOverviewViewModel viewModel, Product model)
        {
            model.MinimumQty = viewModel.MinimumQty;
            model.MaximumQty = viewModel.MaximumQty;
        }
    }

    #endregion


}

