﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductLink : IEntity
    {
        public ProductLink() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public Guid LinkedProductID { get; set; }

        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        [ForeignKey("LinkedProductID")]
        public virtual Product LinkedProduct { get; set; }
    }


    public class ProductLinkViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public SelectItemViewModel LinkedProduct { get; set; }
    }





    public class ProductLinkMapper : ModelMapper<ProductLink, ProductLinkViewModel>
    {
        public override void MapToViewModel(ProductLink model, ProductLinkViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.LinkedProduct = (model.LinkedProduct != null) ? new SelectItemViewModel { ID = model.LinkedProduct.ID.ToString(), Name = string.Format("{0} - {1}", model.LinkedProduct.ProductCode, (model.LinkedProduct.Description ?? "").Split('\n')[0]) } : new SelectItemViewModel { };          
        }

        public override void MapToModel(ProductLinkViewModel viewModel, ProductLink model)
        {            
            //ID = viewModel.ID;
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.LinkedProductID = Guid.Parse(viewModel.LinkedProduct.ID);
        }
    }


}
