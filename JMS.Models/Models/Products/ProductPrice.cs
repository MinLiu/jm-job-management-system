﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductPrice : IEntity
    {
        public ProductPrice() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public int CurrencyID { get; set; }

        public decimal Price { get; set; }
        public decimal BreakPoint { get; set; }


        public virtual Product Product { get; set; }
        public virtual Currency Currency { get; set; }

    }





    public class ProductPriceViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }

        [Required]
        public SelectItemViewModel Currency { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public decimal BreakPoint { get; set; }
    }





    public class ProductPriceMapper : ModelMapper<ProductPrice, ProductPriceViewModel>
    {
        public override void MapToViewModel(ProductPrice model, ProductPriceViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Currency = new SelectItemViewModel { ID = model.Currency.ID.ToString(), Name = model.Currency.Code };
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Price = model.Price;
            viewModel.BreakPoint = model.BreakPoint;
        }

        public override void MapToModel(ProductPriceViewModel viewModel, ProductPrice model)
        {
            //model.ID = viewModel.ID;
            model.CurrencyID = int.Parse(viewModel.Currency.ID);
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.Price = viewModel.Price;
            model.BreakPoint = viewModel.BreakPoint;
        }

    }
}
