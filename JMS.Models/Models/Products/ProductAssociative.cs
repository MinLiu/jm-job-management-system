﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductAssociative : ProductLink
    {
                
    }


    public class ProductAssociativeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public SelectItemViewModel LinkedProduct { get; set; }
        public string ImageURL { get; set; }
    }
        
    
    public class ProductAssociativeMapper : ModelMapper<ProductAssociative, ProductAssociativeViewModel>
    {
        public override void MapToViewModel(ProductAssociative model, ProductAssociativeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.LinkedProduct = (model.LinkedProduct != null) ? new SelectItemViewModel { ID = model.LinkedProduct.ID.ToString(), Name = string.Format("{0} - {1}", model.LinkedProduct.ProductCode, (model.LinkedProduct.Description ?? "").Split('\n')[0]) } : new SelectItemViewModel { };
            viewModel.ImageURL = (model.LinkedProduct != null) ? model.LinkedProduct.ImageURL ?? "/Product Images/NotFound.png" : "/Product Images/NotFound.png";
        }

        public override void MapToModel(ProductAssociativeViewModel viewModel, ProductAssociative model)
        {
            //ID = viewModel.ID;
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.LinkedProductID = Guid.Parse(viewModel.LinkedProduct.ID);
        }
    }
    
}
