﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductAlternative : ProductLink
    {
                
    }


    public class ProductAlternativeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public SelectItemViewModel LinkedProduct { get; set; }
        public string ImageURL { get; set; }
    }

    
    public class ProductAlternativeMapper : ModelMapper<ProductAlternative, ProductAlternativeViewModel>
    {
        public override void MapToViewModel(ProductAlternative model, ProductAlternativeViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.LinkedProduct = (model.LinkedProduct != null) ? new SelectItemViewModel { ID = model.LinkedProduct.ID.ToString(), Name = string.Format("{0} - {1}", model.LinkedProduct.ProductCode, (model.LinkedProduct.Description ?? "").Split('\n')[0]) } : new SelectItemViewModel { };
            viewModel.ImageURL = (model.LinkedProduct != null) ? model.LinkedProduct.ImageURL ?? "/Product Images/NotFound.png" : "/Product Images/NotFound.png";
        }

        public override void MapToModel(ProductAlternativeViewModel viewModel, ProductAlternative model)
        {
            //ID = viewModel.ID;
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.LinkedProductID = Guid.Parse(viewModel.LinkedProduct.ID);
        }
    }
    
}
