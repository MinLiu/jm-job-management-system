﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductAssociativeRepository : EntityRespository<ProductAssociative>
    {
        public ProductAssociativeRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductAssociativeRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
