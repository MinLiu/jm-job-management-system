﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductPriceRepository : EntityRespository<ProductPrice>
    {
        public ProductPriceRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductPriceRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
