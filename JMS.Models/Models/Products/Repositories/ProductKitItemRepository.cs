﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductKitItemRepository : EntityRespository<ProductKitItem>
    {
        public ProductKitItemRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductKitItemRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
