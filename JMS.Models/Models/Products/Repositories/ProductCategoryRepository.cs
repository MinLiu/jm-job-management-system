﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductCategoryRepository : EntityRespository<ProductCategory>
    {
        public ProductCategoryRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductCategoryRepository(SnapDbContext db)
            : base(db)
        {

        }

        /*
        public override void Delete(ProductCategory entity)
        {
            var set = _db.Set<ProductCategory>().Where(a => a.ID == entity.ID)
                                    .Include(a => a.Products);

            _db.Set<ProductCategory>().RemoveRange(set);
            _db.SaveChanges();
        }
        */
    }

}
