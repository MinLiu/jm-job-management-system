﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductAlternativeRepository : EntityRespository<ProductAlternative>
    {
        public ProductAlternativeRepository()
            : this(new SnapDbContext())
        {

        }

        public ProductAlternativeRepository(SnapDbContext db)
            : base(db)
        {

        }

    }

}
