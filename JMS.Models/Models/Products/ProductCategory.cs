﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductCategory : IEntity
    {
        public ProductCategory() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual Company Company { get; set; }

        //Foreign References
        //public virtual ICollection<Product> Products { get; set; }        
    }




    public class ProductCategoryViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }
        public string Name { get; set; }
    }



    public class ProductCategoryMapper : ModelMapper<ProductCategory, ProductCategoryViewModel>
    {
        public override void MapToViewModel(ProductCategory model, ProductCategoryViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Name = model.Name;
        }

        public override void MapToModel(ProductCategoryViewModel viewModel, ProductCategory model)
        {
            //model.ID = viewModel.ID;
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
        }

    }
}
