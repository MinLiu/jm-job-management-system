﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Labour : IEntity
    {
        public Labour() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid CompanyID { get; set; }
        public int CurrencyID { get; set; }

        [Required]
        public string Description { get; set; }
        public decimal HourlyRate { get; set; }
        
        public virtual Company Company { get; set; }
        public virtual Currency Currency { get; set; }
    }


    public class LabourViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string CompanyID { get; set; }

        [Required]
        public CurrencyViewModel Currency { get; set; }

        [Required]
        public string Description { get; set; }        
        public decimal HourlyRate { get; set; }
    }

    

    public class LabourMapper : ModelMapper<Labour, LabourViewModel>
    {

        public override void MapToViewModel(Labour model, LabourViewModel viewModel)
        {

            viewModel.ID = model.ID.ToString();
            viewModel.CompanyID = model.CompanyID.ToString();
            viewModel.Currency = new CurrencyViewModel { ID = model.Currency.ID, Name = model.Currency.Code, Symbol = model.Currency.Symbol };
            viewModel.Description = model.Description ?? "";
            viewModel.HourlyRate = model.HourlyRate;
        }


        public override void MapToModel(LabourViewModel viewModel, Labour model)
        {            
            //ID = viewModel.ID,
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.CurrencyID = viewModel.Currency.ID;
            model.Description = viewModel.Description;
            model.HourlyRate = viewModel.HourlyRate;
        }

    }

    
}

