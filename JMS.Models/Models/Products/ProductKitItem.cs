﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductKitItem : IEntity
    {
        public ProductKitItem() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid KitID { get; set; }
        public Guid? ProductID { get; set; }
        public bool IsComment { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public int SortPos { get; set; }

        [ForeignKey("KitID")]
        public virtual Product Kit { get; set; }
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }
    }




    public class ProductKitItemViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string KitID { get; set; }
        public ProductKitSelectItemViewModel Product { get; set; }
        public decimal Quantity { get; set; }
        public int SortPos { get; set; }
        public string ImageURL { get; set; }
    }


    public class ProductKitSelectItemViewModel
    {
        public string ID { get; set; }
        public bool IsComment { get; set; }
        public string Description { get; set; }
    }




    public class ProductKitItemMapper : ModelMapper<ProductKitItem, ProductKitItemViewModel>
    {
        public override void MapToViewModel(ProductKitItem model, ProductKitItemViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.KitID = model.KitID.ToString();
            viewModel.Quantity = model.Quantity;
            viewModel.Product = (model.Product != null) ? new ProductKitSelectItemViewModel { ID = model.Product.ID.ToString(), Description = string.Format("{0} - {1}", model.Product.ProductCode, (model.Product.Description ?? "").Split('\n')[0]), IsComment = false } : new ProductKitSelectItemViewModel { Description = model.Description, IsComment = true };
            viewModel.SortPos = model.SortPos;
            viewModel.ImageURL = (model.Product != null) ? model.Product.ImageURL ?? "/Product Images/NotFound.png" : "/Product Images/NotFound.png";
          
        }

        public override void MapToModel(ProductKitItemViewModel viewModel, ProductKitItem model)
        {            
            //ID = viewModel.ID;
            model.KitID = Guid.Parse(viewModel.KitID);
            if(!string.IsNullOrEmpty(viewModel.Product.ID))
            {
                model.ProductID = Guid.Parse(viewModel.Product.ID) as Guid?;
                model.IsComment = false;
                model.Description = null;
            }
            else
            {
                model.ProductID = null;
                model.Description = viewModel.Product.Description;
                model.IsComment = true;
            }

            model.Quantity = viewModel.Quantity;
            model.SortPos = viewModel.SortPos;

        }

    }


}
