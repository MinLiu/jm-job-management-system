﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProductCost : IEntity
    {
        public ProductCost() { ID = Guid.NewGuid(); }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public int CurrencyID { get; set; }

        public decimal Cost { get; set; }
        public decimal BreakPoint { get; set; }


        public virtual Product Product { get; set; }
        public virtual Currency Currency { get; set; }
    }



    public class ProductCostViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string ProductID { get; set; }
        public SelectItemViewModel Currency { get; set; }

        public decimal Cost { get; set; }
        public decimal BreakPoint { get; set; }
    }



    public class ProductCostMapper : ModelMapper<ProductCost, ProductCostViewModel>
    {
        public override void MapToViewModel(ProductCost model, ProductCostViewModel viewModel)
        {
            viewModel.ID = model.ID.ToString();
            viewModel.Currency = new SelectItemViewModel { ID = model.Currency.ID.ToString(), Name = model.Currency.Code };
            viewModel.ProductID = model.ProductID.ToString();
            viewModel.Cost = model.Cost;
            viewModel.BreakPoint = model.BreakPoint;
        }

        public override void MapToModel(ProductCostViewModel viewModel, ProductCost model)
        {

            //model.ID = viewModel.ID;
            model.CurrencyID = int.Parse(viewModel.Currency.ID);
            model.ProductID = Guid.Parse(viewModel.ProductID);
            model.Cost = viewModel.Cost;
            model.BreakPoint = viewModel.BreakPoint;
        }

    }

}
