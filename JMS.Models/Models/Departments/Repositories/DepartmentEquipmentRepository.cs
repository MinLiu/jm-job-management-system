﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class DepartmentEquipmentRepository : EntityRespository<DepartmentEquipment>
    {
        public DepartmentEquipmentRepository()
            : this(new SnapDbContext())
        {

        }

        public DepartmentEquipmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
