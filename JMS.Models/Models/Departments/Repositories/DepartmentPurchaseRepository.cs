﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class DepartmentPurchaseRepository : EntityRespository<DepartmentPurchase>
    {
        public DepartmentPurchaseRepository()
            : this(new SnapDbContext())
        {

        }

        public DepartmentPurchaseRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
