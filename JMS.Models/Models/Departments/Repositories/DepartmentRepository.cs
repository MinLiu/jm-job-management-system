﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class DepartmentRepository : EntityRespository<Department>
    {
        public DepartmentRepository()
            : this(new SnapDbContext())
        {

        }

        public DepartmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
