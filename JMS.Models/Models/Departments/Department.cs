﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Department : IEntity
    {
        public Department() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        [Required]
        public long RefID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public string Contact { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.

        //Foreign References

        public virtual ICollection<DepartmentEquipment> DepartmentEquipments { get; set; }
        public virtual ICollection<DepartmentPurchase> DepartmentPurchases { get; set; }
    }



    public class DepartmentViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public long RefID { get; set; }
        public string Contact { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
    }





    public class DepartmentMapper : ModelMapper<Department, DepartmentViewModel>
    {
        public override void MapToViewModel(Department model, DepartmentViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Deleted = model.Deleted;
            viewModel.Contact = model.Contact;
            viewModel.Code = model.Code;
            viewModel.RefID = model.RefID;
        }

        public override void MapToModel(DepartmentViewModel viewModel, Department model)
        {
            // ID = viewModel.ID;
            //model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            model.Code = viewModel.Code;
            model.Contact = viewModel.Contact;
            model.RefID = viewModel.RefID;
        }

    }


}
