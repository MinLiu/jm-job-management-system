﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class MonthlyJobsOnHold
    {
        [Key]
        public DateTime Month { get; set; }
        public int Count { get; set; }
    }

}
