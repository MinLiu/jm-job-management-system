﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    [Table("AspNetUserRoleOptions")]
    public class UserRoleOption 
    {
        [Key]
        public string IdentityRoleID { get; set; }
        public Guid? CompanyID { get; set; }

        public bool ViewQuotations { get; set; }
        public bool EditQuotations { get; set; }
        public bool DeleteQuotations { get; set; }
        public bool ExportQuotations { get; set; }
        
        public bool ViewJobs { get; set; }
        public bool EditJobs { get; set; }
        public bool DeleteJobs { get; set; }
        public bool ExportJobs { get; set; }

        public bool ViewInvoices { get; set; }
        public bool EditInvoices { get; set; }
        public bool DeleteInvoices { get; set; }
        public bool ExportInvoices { get; set; }

        public bool ViewPurchaseOrders { get; set; }
        public bool EditPurchaseOrders { get; set; }
        public bool DeletePurchaseOrders { get; set; }
        public bool ExportPurchaseOrders { get; set; }

        public bool ViewDeliveryNotes { get; set; }
        public bool EditDeliveryNotes { get; set; }
        public bool DeleteDeliveryNotes { get; set; }
        public bool ExportDeliveryNotes { get; set; }

        public bool ViewStockControl { get; set; }
        public bool EditStockControl { get; set; }
        public bool ExportStockControl { get; set; }

        public bool ViewCRM { get; set; }
        public bool EditCRM { get; set; }
        public bool DeleteCRM { get; set; }
        public bool ExportCRM { get; set; }
        
        public bool ViewProducts { get; set; }
        public bool EditProducts { get; set; }
        public bool DeleteProducts { get; set; }
        public bool ExportProducts { get; set; }


        [ForeignKey("IdentityRoleID")]
        public virtual IdentityRole IdentityRole { get; set; }
        public virtual Company Company { get; set; }


        public void CorrectAccess()
        {
            if (!ViewQuotations && (EditQuotations || DeleteQuotations)) ViewQuotations = true;
            if (!ViewJobs && (EditJobs || DeleteJobs)) ViewJobs = true;
            if (!ViewInvoices && (EditInvoices || DeleteInvoices)) ViewInvoices = true;
            if (!ViewPurchaseOrders && (EditPurchaseOrders || DeletePurchaseOrders)) ViewPurchaseOrders = true;
            if (!ViewDeliveryNotes && (EditDeliveryNotes || DeleteDeliveryNotes)) ViewDeliveryNotes = true;
            if (!ViewStockControl && (EditStockControl)) ViewStockControl = true;

            if (!ViewCRM && (EditCRM || DeleteCRM)) ViewCRM = true;
            if (!ViewProducts && (EditProducts || DeleteProducts)) ViewProducts = true;


            if (!EditQuotations && DeleteQuotations) EditQuotations = true;
            if (!EditJobs && DeleteJobs) EditJobs = true;
            if (!EditInvoices && DeleteInvoices) EditInvoices = true;
            if (!EditPurchaseOrders && DeletePurchaseOrders) EditPurchaseOrders = true;
            if (!EditDeliveryNotes && DeleteDeliveryNotes) EditDeliveryNotes = true;

            if (!EditCRM && DeleteCRM) EditCRM = true;
            if (!EditProducts && DeleteProducts) EditProducts = true;
        }

    }




    public class UserRoleOptionViewModel
    {
        [Key]
        public string IdentityRoleID { get; set; }

        [Required]
        [Display(Name = "Role Name")]
        public string IdentityRoleName { get; set; }

        [Required]
        public string CompanyID { get; set; }

        public bool ViewQuotations { get; set; }
        public bool EditQuotations { get; set; }
        public bool DeleteQuotations { get; set; }
        public bool ExportQuotations { get; set; }

        public bool ViewJobs { get; set; }
        public bool EditJobs { get; set; }
        public bool DeleteJobs { get; set; }
        public bool ExportJobs { get; set; }

        public bool ViewInvoices { get; set; }
        public bool EditInvoices { get; set; }
        public bool DeleteInvoices { get; set; }
        public bool ExportInvoices { get; set; }

        public bool ViewPurchaseOrders { get; set; }
        public bool EditPurchaseOrders { get; set; }
        public bool DeletePurchaseOrders { get; set; }
        public bool ExportPurchaseOrders { get; set; }

        public bool ViewDeliveryNotes { get; set; }
        public bool EditDeliveryNotes { get; set; }
        public bool DeleteDeliveryNotes { get; set; }
        public bool ExportDeliveryNotes { get; set; }

        public bool ViewStockControl { get; set; }
        public bool EditStockControl { get; set; }
        public bool ExportStockControl { get; set; }

        public bool ViewCRM { get; set; }
        public bool EditCRM { get; set; }
        public bool DeleteCRM { get; set; }
        public bool ExportCRM { get; set; }

        public bool ViewProducts { get; set; }
        public bool EditProducts { get; set; }
        public bool DeleteProducts { get; set; }
        public bool ExportProducts { get; set; }
        
    }



    public class UserRoleOptionMapper : ModelMapper<UserRoleOption, UserRoleOptionViewModel>
    {
        public override void MapToViewModel(UserRoleOption model, UserRoleOptionViewModel viewModel)
        {
            viewModel.IdentityRoleID = model.IdentityRoleID;
            viewModel.IdentityRoleName = model.IdentityRole.Name;
            viewModel.CompanyID = (model.CompanyID != null) ? model.CompanyID.ToString() : "";

            viewModel.ViewQuotations = model.ViewQuotations;
            viewModel.EditQuotations = model.EditQuotations;
            viewModel.DeleteQuotations = model.DeleteQuotations;
            viewModel.ExportQuotations = model.ExportQuotations;

            viewModel.ViewJobs = model.ViewJobs;
            viewModel.EditJobs = model.EditJobs;
            viewModel.DeleteJobs = model.DeleteJobs;
            viewModel.ExportJobs = model.ExportJobs;

            viewModel.ViewInvoices = model.ViewInvoices;
            viewModel.EditInvoices = model.EditInvoices;
            viewModel.DeleteInvoices = model.DeleteInvoices;
            viewModel.ExportInvoices = model.ExportInvoices;

            viewModel.ViewPurchaseOrders = model.ViewPurchaseOrders;
            viewModel.EditPurchaseOrders = model.EditPurchaseOrders;
            viewModel.DeletePurchaseOrders = model.DeletePurchaseOrders;
            viewModel.ExportPurchaseOrders = model.ExportPurchaseOrders;

            viewModel.ViewDeliveryNotes = model.ViewDeliveryNotes;
            viewModel.EditDeliveryNotes = model.EditDeliveryNotes;
            viewModel.DeleteDeliveryNotes = model.DeleteDeliveryNotes;
            viewModel.ExportDeliveryNotes = model.ExportDeliveryNotes;

            viewModel.ViewStockControl = model.ViewStockControl;
            viewModel.EditStockControl = model.EditStockControl;
            viewModel.ExportStockControl = model.ExportStockControl;

            viewModel.ViewCRM = model.ViewCRM;
            viewModel.EditCRM = model.EditCRM;
            viewModel.DeleteCRM = model.DeleteCRM;
            viewModel.ExportCRM = model.ExportCRM;

            viewModel.ViewProducts = model.ViewProducts;
            viewModel.EditProducts = model.EditProducts;
            viewModel.DeleteProducts = model.DeleteProducts;
            viewModel.ExportProducts = model.ExportProducts;

        }

        public override void MapToModel(UserRoleOptionViewModel viewModel, UserRoleOption model)
        {
            //model.IdentityRoleID = viewModel.IdentityRoleID;
            //viewModel.IdentityRoleName = model.IdentityRole.Name;           

            model.ViewQuotations = viewModel.ViewQuotations;
            model.EditQuotations = viewModel.EditQuotations;
            model.DeleteQuotations = viewModel.DeleteQuotations;
            model.ExportQuotations = viewModel.ExportQuotations;

            model.ViewJobs = viewModel.ViewJobs;
            model.EditJobs = viewModel.EditJobs;
            model.DeleteJobs = viewModel.DeleteJobs;
            model.ExportJobs = viewModel.ExportJobs;

            model.ViewInvoices = viewModel.ViewInvoices;
            model.EditInvoices = viewModel.EditInvoices;
            model.DeleteInvoices = viewModel.DeleteInvoices;
            model.ExportInvoices = viewModel.ExportInvoices;

            model.ViewPurchaseOrders = viewModel.ViewPurchaseOrders;
            model.EditPurchaseOrders = viewModel.EditPurchaseOrders;
            model.DeletePurchaseOrders = viewModel.DeletePurchaseOrders;
            model.ExportPurchaseOrders = viewModel.ExportPurchaseOrders;

            model.ViewDeliveryNotes = viewModel.ViewDeliveryNotes;
            model.EditDeliveryNotes = viewModel.EditDeliveryNotes;
            model.DeleteDeliveryNotes = viewModel.DeleteDeliveryNotes;
            model.ExportDeliveryNotes = viewModel.ExportDeliveryNotes;

            model.ViewStockControl = viewModel.ViewStockControl;
            model.EditStockControl = viewModel.EditStockControl;
            model.ExportStockControl = viewModel.ExportStockControl;

            model.ViewCRM = viewModel.ViewCRM;
            model.EditCRM = viewModel.EditCRM;
            model.DeleteCRM = viewModel.DeleteCRM;
            model.ExportCRM = viewModel.ExportCRM;

            model.ViewProducts = viewModel.ViewProducts;
            model.EditProducts = viewModel.EditProducts;
            model.DeleteProducts = viewModel.DeleteProducts;
            model.ExportProducts = viewModel.ExportProducts;
        }

    }

}
