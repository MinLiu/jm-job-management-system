﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Currency
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(3)]
        public string Code { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(3)]
        public string Symbol { get; set; }

        public bool UseInCommerce { get; set; }

    }


    public class CurrencyViewModel
    {
        public int ID { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
        public bool UseInCommerce { get; set; }
    }




    public class CurrencyMapper : ModelMapper<Currency, CurrencyViewModel>
    {
        public override void MapToViewModel(Currency model, CurrencyViewModel viewModel)
        {              
            viewModel.ID = model.ID;
            viewModel.Symbol = model.Symbol;
            viewModel.Name = model.Name;
            viewModel.UseInCommerce = model.UseInCommerce;
        }

        public override void MapToModel(CurrencyViewModel viewModel, Currency model)
        {           
            //model.ID = viewModel.ID;
            model.Symbol = viewModel.Symbol;
            model.Name = viewModel.Name;
            model.UseInCommerce = viewModel.UseInCommerce;
        }

    }

}
