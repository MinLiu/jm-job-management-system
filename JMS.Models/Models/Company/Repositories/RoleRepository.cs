﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace JMS.Models
{
    public class RoleRepository : EntityRespository<IdentityRole>
    {
        public RoleRepository()
            : this(new SnapDbContext())
        {

        }

        public RoleRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(IEnumerable<IdentityRole> entities)
        {
            if (entities.Where(u => u.Id == "-1" || u.Id == "1" || u.Id == "2").Count() > 0) throw new Exception("Can not delete standard SNAP Suite roles.");
            base.Delete(entities);
        }


        public override void Delete(IdentityRole entity)
        {
            if (entity.Id == "-1" || entity.Id == "1" || entity.Id == "2") throw new Exception("Can not delete standard SNAP Suite roles.");
            base.Delete(entity);
        }
        
    }

}
