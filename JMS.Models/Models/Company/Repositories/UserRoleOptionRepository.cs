﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class UserRoleOptionRepository : EntityRespository<UserRoleOption>
    {
        public UserRoleOptionRepository()
            : this(new SnapDbContext())
        {

        }

        public UserRoleOptionRepository(SnapDbContext db)
            : base(db)
        {

        }
        

        public override void Create(IEnumerable<UserRoleOption> entities)
        {
            foreach (var e in entities)
                e.CorrectAccess();

            base.Create(entities);
        }

        public override void Create(UserRoleOption entity)
        {
            entity.CorrectAccess();
            base.Create(entity);
        }

        public override void Update(IEnumerable<UserRoleOption> entities, string[] updatedColumns = null)
        {
            foreach (var e in entities)
                e.CorrectAccess();

            base.Update(entities, updatedColumns);
        }

        public override void Update(UserRoleOption entity, string[] updatedColumns = null)
        {
            entity.CorrectAccess();
            base.Update(entity, updatedColumns);
        }


        public override void Delete(IEnumerable<UserRoleOption> entities)
        {
            if (entities.Where(u => u.CompanyID == null).Count() > 0) throw new Exception("Can not delete standard SNAP Suite roles.");
            base.Delete(entities);
        }


        public override void Delete(UserRoleOption entity)
        {
            if (entity.CompanyID == null) throw new Exception("Can not delete standard SNAP Suite roles.");
            base.Delete(entity);
        }

    }

}
