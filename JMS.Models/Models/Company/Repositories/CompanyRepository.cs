﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class CompanyRepository : EntityRespository<Company>
    {
        public CompanyRepository()
            : this(new SnapDbContext())
        {

        }

        public CompanyRepository(SnapDbContext db)
            : base(db)
        {

        }


        public override void Delete(Company entity)
        {
                       
            //Delete orders first because they are linked to the CRM entities.            

            _db.Set<ProductPrice>().RemoveRange(_db.Set<ProductPrice>().Where(x => x.Product.CompanyID == entity.ID));
            _db.Set<ProductCost>().RemoveRange(_db.Set<ProductCost>().Where(x => x.Product.CompanyID == entity.ID));           
            _db.Set<ProductKitItem>().RemoveRange(_db.Set<ProductKitItem>().Where(i => i.Product.CompanyID == entity.ID));
            _db.Set<ProductCategory>().RemoveRange(_db.Set<ProductCategory>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ProductAlternative>().RemoveRange(_db.Set<ProductAlternative>().Where(i => i.Product.CompanyID == entity.ID));
            _db.Set<ProductAssociative>().RemoveRange(_db.Set<ProductAssociative>().Where(i => i.Product.CompanyID == entity.ID));
            _db.Set<Product>().RemoveRange(_db.Set<Product>().Where(x => x.CompanyID == entity.ID));


            _db.Set<Client>().RemoveRange(_db.Set<Client>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ClientAddress>().RemoveRange(_db.Set<ClientAddress>().Where(x => x.Client.CompanyID == entity.ID));
            _db.Set<ClientAttachment>().RemoveRange(_db.Set<ClientAttachment>().Where(x => x.Client.CompanyID == entity.ID));            
            _db.Set<ClientContact>().RemoveRange(_db.Set<ClientContact>().Where(x => x.Client.CompanyID == entity.ID));            
            _db.Set<ClientTagItem>().RemoveRange(_db.Set<ClientTagItem>().Where(x => x.Client.CompanyID == entity.ID));
            _db.Set<ClientEvent>().RemoveRange(_db.Set<ClientEvent>().Where(x => x.Client.CompanyID == entity.ID));            
            _db.Set<ClientTag>().RemoveRange(_db.Set<ClientTag>().Where(x => x.CompanyID == entity.ID));
            _db.Set<ClientType>().RemoveRange(_db.Set<ClientType>().Where(x => x.CompanyID == entity.ID));

            //Always delete user stuff last.
            _db.Set<UserRoleOption>().RemoveRange(_db.Set<UserRoleOption>().Where(x => x.CompanyID == entity.ID));

            _db.SaveChanges();

            base.Delete(entity);
        }

    }

}
