﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Engineer : IEntity
    {
        public Engineer() {  ID = Guid.NewGuid(); Active = true; }
        
        [Key]
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int EngineerID { get; set; }
        public bool Active { get; set; }

        //Foreign References

        public virtual ICollection<EngineerJob> EngineerJobs { get; set; }
        public virtual ICollection<EngineerEquipment> EngineerEquipments { get; set; }
    }



    public class EngineerGridViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int EngineerID { get; set; }
        public bool Active { get; set; }
    }





    public class EngineerGridMapper : ModelMapper<Engineer, EngineerGridViewModel>
    {
        public override void MapToViewModel(Engineer model, EngineerGridViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.ShortName = model.ShortName;
            viewModel.EngineerID = model.EngineerID;
            viewModel.Active = model.Active;
        }

        public override void MapToModel(EngineerGridViewModel viewModel, Engineer model)
        {
            // ID = viewModel.ID;
            //model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Name = viewModel.Name;
            model.ShortName = viewModel.ShortName;
            model.EngineerID = viewModel.EngineerID;
            model.Active = viewModel.Active;
        }

    }


}
