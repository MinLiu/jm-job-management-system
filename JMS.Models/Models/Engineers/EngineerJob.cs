﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{

    public class EngineerJob
    {

        [Key]
        [Column(Order = 1)]
        public Guid EngineerID { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid JobID { get; set; }

        public virtual Job Job { get; set; }
        public virtual Engineer Engineer { get; set; }
    }

}
