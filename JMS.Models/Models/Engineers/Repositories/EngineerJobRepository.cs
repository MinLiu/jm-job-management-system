﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EngineerJobRepository : EntityRespository<EngineerJob>
    {
        public EngineerJobRepository()
            : this(new SnapDbContext())
        {

        }

        public EngineerJobRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
