﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class EngineerEquipmentRepository : EntityRespository<EngineerEquipment>
    {
        public EngineerEquipmentRepository()
            : this(new SnapDbContext())
        {

        }

        public EngineerEquipmentRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
