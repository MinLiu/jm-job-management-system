﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ManufacturerRepository : EntityRespository<Manufacturer>
    {
        public ManufacturerRepository()
            : this(new SnapDbContext())
        {

        }

        public ManufacturerRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
