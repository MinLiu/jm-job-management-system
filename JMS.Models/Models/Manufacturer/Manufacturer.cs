﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Manufacturer : IEntity
    {
        public Manufacturer() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int RefID{ get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.

        //Foreign References
    }



    public class ManufacturerViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int RefID { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
    }





    public class ManufacturerMapper : ModelMapper<Manufacturer, ManufacturerViewModel>
    {
        public override void MapToViewModel(Manufacturer model, ManufacturerViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Deleted = model.Deleted;
            viewModel.RefID = model.RefID;
        }

        public override void MapToModel(ManufacturerViewModel viewModel, Manufacturer model)
        {
            model.Name = viewModel.Name;
            model.RefID = viewModel.RefID;
        }

    }


}
