﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProjectCodeRepository : EntityRespository<ProjectCode>
    {
        public ProjectCodeRepository()
            : this(new SnapDbContext())
        {

        }

        public ProjectCodeRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
