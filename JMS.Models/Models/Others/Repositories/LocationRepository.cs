﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class LocationRepository : EntityRespository<Location>
    {
        public LocationRepository()
            : this(new SnapDbContext())
        {

        }

        public LocationRepository(SnapDbContext db)
            : base(db)
        {

        }
    }

}
