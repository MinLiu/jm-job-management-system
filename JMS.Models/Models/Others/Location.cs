﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class Location : IEntity
    {
        public Location() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.
    }



    public class LocationViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
    }





    public class LocationMapper : ModelMapper<Location, LocationViewModel>
    {
        public override void MapToViewModel(Location model, LocationViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.Name = model.Name;
            viewModel.Deleted = model.Deleted;
            viewModel.Code = model.Code;
        }

        public override void MapToModel(LocationViewModel viewModel, Location model)
        {
            model.Name = viewModel.Name;
            model.Code = viewModel.Code;
        }

    }


}
