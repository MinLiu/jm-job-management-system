﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JMS.Models
{
    public class ProjectCode : IEntity
    {
        public ProjectCode() {  ID = Guid.NewGuid(); }
        
        [Key]
        public Guid ID { get; set; }
        [Required]
        public string Code { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.
    }



    public class ProjectCodeViewModel : IEntityViewModel
    {
        public string ID { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }   //Marker used to place organisations in the recycle bin.        
    }





    public class ProjectCodeMapper : ModelMapper<ProjectCode, ProjectCodeViewModel>
    {
        public override void MapToViewModel(ProjectCode model, ProjectCodeViewModel viewModel)
        { 
            viewModel.ID = model.ID.ToString();
            viewModel.Deleted = model.Deleted;
            viewModel.Code = model.Code;
        }

        public override void MapToModel(ProjectCodeViewModel viewModel, ProjectCode model)
        {
            model.Code = viewModel.Code;
        }

    }


}
