﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Fruitful.Email
{
    public static class SendGridEmailer
    {
        public static bool SendEmail(MailMessage message)
        {
            try
            {
                var client = GetClient();
                SendEmail(client, message);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendEmail(SmtpClient client, MailMessage message)
        {
            try
            {
                client.Send(message);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static SmtpClient GetClient()
        {
            var host = "smtp.sendgrid.net";
            var port = 587;
            var userName = "tim.randall";
            var password = "SBhp2xb546";
            var enableSsl = true;

            var client = new SmtpClient(host, port)
            {
                Credentials = new NetworkCredential(userName, password),
                EnableSsl = enableSsl
            };

            return client;
        }
    }
}
