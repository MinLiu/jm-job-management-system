﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    public class UpdatePayRateViewModel
    {
        public int EmployeeID { get; set; }
        public decimal NewRate { get; set; }
    }
}