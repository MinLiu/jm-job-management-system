﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    

    public class CustomItemViewModel
    {
        public Guid ID { get; set; }
        public int OrderID { get; set; }

        [Required(ErrorMessage = "* The Product Code field is required.")]
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        [Required(ErrorMessage = "* The Description is required.")]
        public string Description { get; set; }
        public string Unit { get; set; }
        
        public string ImageURL { get; set; }
        public bool IsService { get; set; }

        [Required(ErrorMessage = "* The VAT field is required.")]
        public decimal VAT { get; set; }

        [Required(ErrorMessage = "* The Quantity field is required.")]
        public decimal Quantity { get; set; }

        [Required(ErrorMessage = "* The Price field is required.")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "* The Cost field is required.")]
        public decimal Cost { get; set; }
    }


}