﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    public class AddStockItemViewModel
    {

        public Guid ID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select a Product")]
        public int ProductID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select a Location")]
        public int LocationID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select an Aisle")]
        public int AisleID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select a Bay")]
        public int BayID { get; set; }

        [Range(0.0, 99999999999.0, ErrorMessage = "Enter a Quantity greater than zero.")]
        public decimal Quantity { get; set; }

        [Required]
        public string Reference { get; set; }
    }


    public class TransferStockItemViewModel
    {
        
        public int StockItemID { get; set; }
        public int StockItem_ProductID { get; set; }
        public string StockItem_ProductCode { get; set; }
        public string StockItem_ProductDescription { get; set; }
        public string StockItem_Location { get; set; }
        public string StockItem_Aisle { get; set; }
        public string StockItem_Bay { get; set; }
        public decimal StockItem_Quantity { get; set; }


        [Range(1, int.MaxValue, ErrorMessage = "Select a Location")]
        public int LocationID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select an Aisle")]
        public int AisleID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select a Bay")]
        public int BayID { get; set; }

        [Range(0.0, 99999999999.0, ErrorMessage = "Enter a Quantity greater than zero.")]
        public decimal Quantity { get; set; }

    }
}