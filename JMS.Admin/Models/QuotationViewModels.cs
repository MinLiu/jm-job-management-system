﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    
    public class QuotationPDFViewModel
    {
        //Used in PDF Generation
        public CompanyViewModel CompanyDetails { get; set; }
        public QuotationViewModel QuotationDetails { get; set; }
        public QuotationPreviewViewModel QuotationPreview { get; set; }
        public List<QuotationItemViewModel> QuotationItems { get; set; }
        public ClientAddressViewModel InvoiceAddress { get; set; }
        public ClientAddressViewModel DeliveryAddress { get; set; }

    }


    public class QuotationReviewViewModel
    {
        //Used to edit the display settings
        public int QuotationID { get; set; }
        public string CheckSum { get; set; }
        public string Reference { get; set; }
        public int Number { get; set; }
        public int Version { get; set; }

        public List<QuotationAttachmentViewModel> QuotationAttachments { get; set; }
    }

}