﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class CompaniesController : GridController<Company, CompanyViewModel>
    {

        public CompaniesController()
            : base(new CompanyRepository(), new CompanyMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {

            //User passed parameters on filter results
            var companies = _repo.Read().Where(
                    c => 
                    (
                        (c.ID.ToString().ToLower().Contains(filterText.ToLower())
                            || c.Name.ToLower().Contains(filterText.ToLower())
                            || c.Email.ToLower().Contains(filterText.ToLower())
                            || c.Telephone.ToLower().Contains(filterText.ToLower())
                        ) || filterText.Equals("") || filterText.Equals(null)
                    )
                );


            companies = ApplyQueryFiltersSort(request, companies, "SignupDate DESC");

            var companyList = companies.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(pdt => _mapper.MapToViewModel(pdt));
            DataSourceResult result = new DataSourceResult { Data = companyList, Total = companies.Count() };


            return Json(result);
            //return Json(productList.ToDataSourceResult(request));

        }


        public override JsonResult Create(DataSourceRequest request, CompanyViewModel viewModel)
        {
            throw new Exception("Can not create company directly from grid.");
        }

        public override JsonResult Update(DataSourceRequest request, CompanyViewModel viewModel)
        {
            throw new Exception("Can not update company directly from grid.");
        }

        public override JsonResult Destroy(DataSourceRequest request, CompanyViewModel viewModel)
        {
            throw new Exception("Can not delete company directly from grid.");
        }

        
    }

}