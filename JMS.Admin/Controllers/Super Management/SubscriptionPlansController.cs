﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SubscriptionPlansController : GridController<SubscriptionPlan, SubscriptionPlanViewModel>
    {

        public SubscriptionPlansController()
            : base(new SubscriptionPlanRepository(), new SubscriptionPlanMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request, string filterText, string cID = null, bool includeInactive = true)
        {            

            var plans = _repo.Read().Where(p => 
                                            (
                                                (p.Active == true || p.EndDate > DateTime.Now)
                                                || (includeInactive && p.Active == false)
                                            )
                                            && (p.CompanyID.ToString() == cID || cID == null)
                                            && 
                                            (
                                                (p.Company.Name.ToLower().Contains(filterText.ToLower())
                                                || p.Name.ToLower().Contains(filterText.ToLower())
                                                ) || filterText == "" || filterText == null
                                            )
            )
            .Include(p => p.Company);

            plans = ApplyQueryFiltersSort(request, plans, "Created DESC");

            var plansList = plans.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = plansList.ToList(), Total = plans.Count() };
            return Json(result);
        }



        public override JsonResult Read(DataSourceRequest request)
        {
            return Read(request, "", null, true);
        }

        public override JsonResult Create(DataSourceRequest request, SubscriptionPlanViewModel viewModel)
        {
            throw new Exception("Can not create subscription plan directly from grid.");
        }

        public override JsonResult Update(DataSourceRequest request, SubscriptionPlanViewModel viewModel)
        {
            throw new Exception("Can not update subscription plan directly from grid.");
        }

        public override JsonResult Destroy(DataSourceRequest request, SubscriptionPlanViewModel viewModel)
        {
            throw new Exception("Can not delete subscription plan directly from grid.");
        }


    }

}