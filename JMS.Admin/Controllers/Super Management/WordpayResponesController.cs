﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class WorldPayResponsesController : BaseController
    {

       private WorldPayResponseRepository _repo = new WorldPayResponseRepository();


       public ActionResult Index()
       {
           return View();
       }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read(DataSourceRequest request)
        {
            var responses = _repo.Read().OrderByDescending(x => x.Timestamp);
                        
            var responseList = responses.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList();
            DataSourceResult result = new DataSourceResult { Data = responseList.ToList(), Total = responses.Count() };
            return Json(result);
        }
        

    }

}