﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SubscriptionRequestsController : BaseController
    {

       private SubscriptionRequestRepository _repo = new SubscriptionRequestRepository();


       public ActionResult Index()
       {
           return View();
       }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read(DataSourceRequest request)
        {
            var requests = _repo.Read().OrderByDescending(x => x.Timestamp);
                        
            var requestList = requests.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList();
            DataSourceResult result = new DataSourceResult { Data = requestList.ToList(), Total = requests.Count() };
            return Json(result);
        }
        

    }

}