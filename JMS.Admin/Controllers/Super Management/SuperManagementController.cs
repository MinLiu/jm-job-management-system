﻿using System.Linq;
using System.Web.Mvc;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Threading.Tasks;
using System;
using System.IO;
using Fruitful.Email;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SuperManagementController : BaseController
    {
        private CompanyRepository repo = new CompanyRepository();
        private CompanyMapper mapper = new CompanyMapper();

        // GET: SuperManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Subscriptions()
        {
            return View();
        }


        // GET: CreateCompany
        public ActionResult CreateCompany()
        {
            Company model = new Company();
            return View("CreateCompany", new CompanyViewModel { });
        }

        // POST: /Create Company
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCompany(CompanyViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index", "SuperManagement");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = mapper.MapToModel(viewModel);
            model.ThemeColor = "#4883ba";
            model.SignupDate = DateTime.Now;
            repo.Create(model);

            return RedirectToAction("EditCompany", "SuperManagement", new { ID = model.ID });

        }

        // GET: _CompanyDetailsTab
        public ActionResult EditCompany(string ID)
        {
            CompanyViewModel viewModel = mapper.MapToViewModel(repo.Find(Guid.Parse(ID)));            
            return View(viewModel);                
        }


        // POST: /_CompanyDetailsTab
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCompany(CompanyViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index", "SuperManagement");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            
            var model = repo.Find(Guid.Parse(viewModel.ID));
            mapper.MapToModel(viewModel, model);
            repo.Update(model);
            ViewBag.SuccessMessage = "Business Details Saved.";

            return RedirectToAction("EditCompany", "SuperManagement", new { ID = model.ID });
        }


        public ActionResult DeleteCompany(string ID)
        {
            CompanyViewModel viewModel = mapper.MapToViewModel(repo.Find(Guid.Parse(ID)));
            return View(viewModel);
        }


        // POST: /_CompanyDetailsTab
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCompany(CompanyViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index", "SuperManagement");

            
            var model = repo.Find(Guid.Parse(viewModel.ID));
            repo.Delete(model);
            ViewBag.SuccessMessage = "Company has been deleted.";

            const string contentFolderRoot = "/Uploads/";
            var folderPath = string.Format("{0}{1}/", contentFolderRoot, viewModel.ID);
            var folderPhysicalPath = Server.MapPath(folderPath).Replace("SNAP Suite Admin", "SNAP Suite Service");

            try
            {
                System.IO.File.Delete(folderPhysicalPath);
            }
            catch { }

            return View("DeleteCompanyConfirm", viewModel);
        }


        public ActionResult SwitchCompany(string ID)
        {
            var repo = new UserRepository();
            var user = repo.Find(CurrentUser.Id);
            user.CompanyID = Guid.Parse(ID);
            repo.Update(user);

            return RedirectToAction("EditCompany", "SuperManagement", new { ID = ID });
        }


        public ActionResult SwitchBack()
        {
            var repo = new UserRepository();
            var user = repo.Find(CurrentUser.Id);
            user.CompanyID = Guid.Parse("eafeab66-4eff-e611-bf07-e03f495631f0");

            repo.Update(user);

            return RedirectToAction("Index", "SuperManagement");
        }


        // GET: _CompanyDetailsTab
        public ActionResult _CompanyUsersTab(CompanyViewModel model)
        {
            return PartialView("_CompanyUsersTab", model);
        }


        // READ: GridCompanyUsers
        public ActionResult CompanyUsers_Read([DataSourceRequest]DataSourceRequest request, string CompanyID)
        {
            var userRepo = new UserRepository();
            var roleRepo = new RoleRepository();

            var roles = roleRepo.Read().ToList();

            var users = userRepo.Read().Where(x => x.CompanyID.ToString() == CompanyID)
                                            .Include(x => x.Roles)
                                            .OrderBy(x => x.Email)
                                            .ToList()
                                            .Select(u => new UserViewModel
                                            {
                                                ID = u.Id,
                                                CompanyID = u.CompanyID.ToString(),
                                                Email = u.Email,
                                                FirstName = u.FirstName,
                                                LastName = u.LastName,
                                                PhoneNumber = u.PhoneNumber,
                                                RoleName = string.Join(",", roles.Where(r => u.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                                                Confirmed = u.EmailConfirmed,
                                                Modules = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}",
                                                                                        (u.AccessCRM) ? "CRM " : "",
                                                                                        (u.AccessProducts) ? "PDTS " : "",
                                                                                        (u.AccessQuotations) ? "Q " : "",
                                                                                        (u.AccessJobs) ? "JOBS " : "",
                                                                                        (u.AccessInvoices) ? "INV " : "",
                                                                                        (u.AccessDeliveryNotes) ? "DEL " : "",
                                                                                        (u.AccessPurchaseOrders) ? "PO " : "",
                                                                                        (u.AccessStockControl) ? "SC " : ""),
                                            });

            return Json(users.ToDataSourceResult(request));
        }

       

        public ActionResult _CompanyAddressesTab(CompanyViewModel model)
        {
            return PartialView("_CompanyAddressesTab", model);
        }


        public ActionResult _CompanySubscriptionTab(CompanyViewModel model)
        {
            return PartialView("_CompanySubscriptionTab", model);
        }


        #region Create User

        public ActionResult CreateUser(string ID)
        {
            return View(new NewUserViewModel {
                CompanyID = ID,
                AccessCRM = true,
                AccessProducts = true,
                AccessQuotations = true,
                AccessJobs = true,
                AccessInvoices = true,
                AccessDeliveryNotes = true,
                AccessPurchaseOrders = true,
                AccessStockControl = true,
            });
        }


        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateUser(NewUserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "SuperManagement");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Check to see if email is already taken
            if (UserManager.FindByEmail(model.Email) != null)
            {
                ViewBag.ErrorMessage = "The email address entered is already being used.";
                return View(model);
            }

            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                CompanyID = Guid.Parse(model.CompanyID), 
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                EmailConfirmed = true,
                AccessCRM = model.AccessCRM,
                AccessProducts = model.AccessProducts,
                AccessQuotations = model.AccessQuotations,
                AccessJobs = model.AccessJobs,
                AccessInvoices = model.AccessInvoices,
                AccessDeliveryNotes = model.AccessDeliveryNotes,
                AccessPurchaseOrders = model.AccessPurchaseOrders,
            };

            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                ViewBag.SuccessMessage = "New user account created.";
                UserManager.AddToRoleById(user.Id, model.RoleID);
                UserManager.Update(user);

                return RedirectToAction("EditCompany", "SuperManagement", new { ID = model.CompanyID });
            }
            else
            {
                ViewBag.ErrorMessage = result.Errors.First().ToString();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion


        #region Edit User


        // GET: User Edit
        public ActionResult EditUser(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "SuperManagement");

            try
            {
                var roleRepo = new RoleRepository();
                var roles = roleRepo.Read().ToList();

                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.Id.Equals(ID)).Include(u => u.Roles).First();

                //Create model then return it
                UserViewModel model = new UserViewModel
                {
                    ID = user.Id,
                    CompanyID = user.CompanyID.ToString(),
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    RoleID = string.Join(",", roles.Where(r => user.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Id)),
                    AccessCRM = user.AccessCRM,
                    AccessProducts = user.AccessProducts,
                    AccessQuotations = user.AccessQuotations,
                    AccessJobs = user.AccessJobs,
                    AccessInvoices = user.AccessInvoices,
                    AccessDeliveryNotes = user.AccessDeliveryNotes,
                    AccessPurchaseOrders = user.AccessPurchaseOrders,
                };

                return View(model);
            }
            catch
            {
                return RedirectToAction("Index", "SuperManagement");
            }
        }



        //
        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(UserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "SuperManagement");

            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            try
            {

                User user = UserManager.FindById(model.ID);

                //Check to see if the email address is already taken if the same email address is not input.
                if (user.Email != model.Email)
                {
                    if (UserManager.FindByEmail(model.Email) != null) //If a user already exsists with the user name then stop saving.
                    {
                        ViewBag.ErrorMessage = "Could not save changes. The Email address entered is already taken.";
                        return PartialView(model);
                    }
                }

                //Update the user
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;

                user.AccessCRM = model.AccessCRM;
                user.AccessProducts = model.AccessProducts;
                user.AccessQuotations = model.AccessQuotations;
                user.AccessJobs = model.AccessJobs;
                user.AccessInvoices = model.AccessInvoices;
                user.AccessDeliveryNotes = model.AccessDeliveryNotes;
                user.AccessPurchaseOrders = model.AccessPurchaseOrders;
                user.AccessStockControl = model.AccessStockControl;


                //Remove all the roles then add the selected role                
                UserManager.RemoveAllRoles(user.Id);
                UserManager.AddToRoleById(user.Id, model.RoleID);

                //Save Changes
                UserManager.Update(user);

                ViewBag.SuccessMessage = "User Account Details have successfully been Saved.";
            }
            catch
            {
                ViewBag.ErrorMessage = "Could not save changes. Unknown Exception.";
            }


            return PartialView(model);
        }

        #endregion


        #region Change Password & Confirm Account

        // GET: Change Password
        public ActionResult ChangeUserPassword(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "SuperManagement");

            try
            {
                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.Id.Equals(ID)).First();

                //Create model then return it
                ChangeUserPasswordViewModel model = new ChangeUserPasswordViewModel
                {
                    ID = user.Id,
                    CompanyID = user.CompanyID.ToString(),
                    Email = user.Email
                };

                return View(model);
            }
            catch { return RedirectToAction("Index", "SuperManagement"); }

        }


        // POST: Change Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeUserPassword(ChangeUserPasswordViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "SuperManagement");

            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            try
            {
                //Try Get User account
                var user = UserManager.FindById(model.ID);

                if (user == null)
                {
                    ViewBag.ErrorMessage = "User Account not found.";
                    return PartialView(model);
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                IdentityResult result = UserManager.ResetPassword(user.Id, code, model.Password);

                model.Password = "";
                model.ConfirmPassword = "";

                if (result.Succeeded)
                {
                    ViewBag.SuccessMessage = "Password was successfully reset.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Failed to reset password. " + result.Errors.First().ToString();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Failed to reset password. " + ex.ToString();
            }

            return PartialView(model);
        }


        public ActionResult ConfirmAccount(string ID)
        {
            User user = UserManager.Users.Where(x => x.Id.Equals(ID)).First();
            user.EmailConfirmed = true;
            UserManager.Update(user);

            return RedirectToAction("EditCompany", "SuperManagement", new { ID = user.CompanyID });
        }

        #endregion


        #region Email Settings

        public ActionResult EmailSettings()
        {
            using (var context = new SnapDbContext())
            {
                var settings = context.EmailSettings.First();
                return View(settings);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmailSettings(EmailSetting model)
        {
            using (var context = new SnapDbContext())
            {
                context.EmailSettings.Attach(model);
                context.Entry(model).State = EntityState.Modified;
                context.SaveChanges();

                ViewBag.SuccessMessage = "Email Settings Saved.";
                return View(model);
            }
        }

        #endregion


    }
}