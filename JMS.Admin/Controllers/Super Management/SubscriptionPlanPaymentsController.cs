﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SubscriptionPlanPaymentsController : GridController<SubscriptionPlanPayment, SubscriptionPlanPaymentViewModel>
    {

        public SubscriptionPlanPaymentsController()
            : base(new SubscriptionPlanPaymentRepository(), new SubscriptionPlanPaymentMapper())
        {

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read(DataSourceRequest request, string cID = null)
        {
            var payments = _repo.Read().Where(p => (p.CompanyID == new Guid(cID) || cID == null))
                                        .Include(p => p.Company)
                                        .OrderByDescending(p => p.Timestamp);

            //payments = ApplyQueryFiltersSort(request, payments, "ID DESC");

            var payList = payments.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = payList.ToList(), Total = payments.Count() };
            return Json(result);
        }



        public override JsonResult Read(DataSourceRequest request)
        {
            return Read(request, null);
        }




        public override JsonResult Create(DataSourceRequest request, SubscriptionPlanPaymentViewModel viewModel)
        {
            throw new Exception("Can not create subscription payment directly from grid.");
        }

        public override JsonResult Update(DataSourceRequest request, SubscriptionPlanPaymentViewModel viewModel)
        {
            throw new Exception("Can not update subscription payment directly from grid.");
        }

        public override JsonResult Destroy(DataSourceRequest request, SubscriptionPlanPaymentViewModel viewModel)
        {
            throw new Exception("Can not delete subscription payment directly from grid.");
        }



    }

}