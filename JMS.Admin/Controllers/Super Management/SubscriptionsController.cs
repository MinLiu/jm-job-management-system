﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using JMS.Models;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SubscriptionsController : BaseController
    {

        SubscriptionPlanRepository repo = new SubscriptionPlanRepository();
        SubscriptionPlanMapper mapper = new SubscriptionPlanMapper();
        
        public ActionResult Index()
        {
            return View();

        }

        public ActionResult New(string ID)
        {

            var company = new CompanyRepository().Find(Guid.Parse(ID));

            var viewModel = new SubscriptionPlanViewModel
            {
                CompanyID = company.ID.ToString(),
                CompanyName = company.Name,
            };

            return View(viewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(SubscriptionPlanViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }


            var model = mapper.MapToModel(viewModel);
            model.CompanyID = Guid.Parse(viewModel.CompanyID);
            model.Created = DateTime.Now;
            repo.Create(model);
            return RedirectToAction("Edit", new { ID = model.ID });
        }




        public ActionResult Edit(string ID){
                       
            var model = repo.Find(Guid.Parse(ID));
            var viewModel = mapper.MapToViewModel(model);

            return View(viewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubscriptionPlanViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }


            var model = repo.Find(Guid.Parse(viewModel.ID));
            mapper.MapToModel(viewModel, model);
            repo.Update(model);
            ViewBag.SuccessMessage = "Saved.";

            return View(viewModel);
        }

    }
}