﻿using JMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JMS.Controllers
{
    public class DropdownsController : BaseController
    {
      
        public JsonResult DropDownUsers()
        {
            //Get the teams where the user is in     
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersWithNotAssigned()
        {
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Add(new UserItemViewModel { ID = "-1", Email = "Not Assigned" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownEventUsers()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Where(u => u.Id != CurrentUser.Id)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersSearch()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Where(u => u.Id != CurrentUser.Id)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            //users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersAll()
        {
            //Get the teams where the user is in  
            var userRepo = new UserRepository();

            var users = userRepo.Read().Where(u => u.CompanyID == CurrentUser.CompanyID)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DropDownCurrencies()
        {
            using (var context = new SnapDbContext())
            {
                var currencies = context.Currencies.Where(c => c.UseInCommerce == true)
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Code
                    }).ToList();
                return Json(currencies, JsonRequestBehavior.AllowGet);
            }
        }


    }



}