﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using System.Web.Routing;
using Kendo.Mvc.UI;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class BaseController : Controller
    {
        public UserManager UserManager;
        public User CurrentUser;  //This is the current user logged in.

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            try
            {
                UserManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();

                var userId = User.Identity.GetUserId().ToString();
                var userRepo = new UserRepository();
                CurrentUser = userRepo.Read().Where(u => u.Id == userId).Include(u => u.Company).First();
               
                //Check Login Misuse by looking at the cookies
                if (!User.IsInRole("Super Admin"))
                {
                    ControllerContext.HttpContext.GetOwinContext().Authentication.SignOut();
                    Response.Redirect("/");
                }
                
            }
            catch {
                Response.Redirect("/");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
            }

            base.Dispose(disposing);
        }


        


    }
}