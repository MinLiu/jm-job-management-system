﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;


namespace JMS.Controllers
{
    public class GridController<TModel, TViewModel> : BaseController where TModel : class, IEntity, new()
                                                                 where TViewModel : class, IEntityViewModel, new()
    {
        protected readonly EntityRespository<TModel> _repo;
        protected readonly ModelMapper<TModel, TViewModel> _mapper;

        public GridController(EntityRespository<TModel> repo, ModelMapper<TModel, TViewModel> mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read();
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Create([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Map the view model to the model.
                var model = _mapper.MapToModel(viewModel);

                // Add the new entity to the database.
                _repo.Create(model);

                // Update the ID of the view model to match the ID of the newly-created entity.
                viewModel.ID = model.ID.ToString();
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Update([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Find the existing entity.
                var model = _repo.Find(viewModel.ID);

                // Map the view model to the model and update the database.
                _mapper.MapToModel(viewModel, model);
                _repo.Update(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Destroy([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
                var model = new TModel { ID = Guid.Parse(viewModel.ID) };
                _repo.Delete(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        public IQueryable<TModel> ApplyQueryFiltersSort([DataSourceRequest]DataSourceRequest request, IQueryable<TModel> query, string defaultSortColumn = "ID")
        {
            //Filtering
            /*
            if (request.Filters.Any())
            {
                foreach (FilterDescriptor x in request.Filters)
                {
                    switch (x.Operator)
                    {
                        case FilterOperator.Contains: query = query.Where(string.Format("{0} LIKE '%@{0}%'", x.Member), x.Value); break;
                        case FilterOperator.DoesNotContain: query = query.Where(string.Format("{0} NOT LIKE '%@{0}%'", x.Member), x.Value); break;
                        case FilterOperator.IsEqualTo: query = query.Where(string.Format("{0}=@{0}", x.Member), x.Value); break;
                        case FilterOperator.IsNotEqualTo: query = query.Where(string.Format("{0}!=@{0}", x.Member), x.Value); break;
                        case FilterOperator.StartsWith: query = query.Where(string.Format("{0} LIKE '@{0}%'", x.Member), x.Value); break;
                        case FilterOperator.EndsWith: query = query.Where(string.Format("{0} LIKE '%@{0}'", x.Member), x.Value); break;

                        case FilterOperator.IsGreaterThan: query = query.Where(string.Format("{0}>@{0}", x.Member), x.Value); break;
                        case FilterOperator.IsGreaterThanOrEqualTo: query = query.Where(string.Format("{0}>=@{0}", x.Member), x.Value); break;
                        case FilterOperator.IsLessThan: query = query.Where(string.Format("{0}<@{0}", x.Member), x.Value); break;
                        case FilterOperator.IsLessThanOrEqualTo: query = query.Where(string.Format("{0}<=@{0}", x.Member), x.Value); break;

                        default: query = query = query.Where(string.Format("{0} LIKE %@{0}%", x.Member), x.Value); break;
                    }
                }
            }
             */

            //Sorting
            if (request.Sorts.Any())
            {
                foreach (var x in request.Sorts)
                {
                    query = query.OrderBy(x.Member + ((x.SortDirection == ListSortDirection.Ascending) ? " ASC" : " DESC")) as IQueryable<TModel>;
                }
            }
            else
            {
                query = query.OrderBy(defaultSortColumn);
            }

            return query;
        }

        protected override void Dispose(bool disposing)
        {
            if (_repo != null)
                _repo.Dispose();

            base.Dispose(disposing);
        }
    }
}