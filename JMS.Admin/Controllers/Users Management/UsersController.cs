﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class UsersController : BaseController
    {

        #region User Management List View

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        // READ: GridUsers
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, string filter)
        {

            var userRepo = new UserRepository();
            var roleRepo = new RoleRepository();

            var roles = roleRepo.Read().ToList();

            var users = userRepo.Read().Where(x => x.CompanyID == CurrentUser.CompanyID 
                                                && ( x.Email.ToLower().Contains(filter) || filter == "")
                                                && (x.FirstName.ToLower().Contains(filter) || filter == "")
                                                && (x.LastName.ToLower().Contains(filter) || filter == "")
                                            )
                                            .Include(x => x.Roles)
                                            .OrderBy(x => x.Email)
                                            .ToList()
                                            .Select( u => new UserViewModel
                                                        {
                                                            ID = u.Id,
                                                            Email = u.Email,
                                                            FirstName = u.FirstName,
                                                            LastName = u.LastName,
                                                            PhoneNumber = u.PhoneNumber,
                                                            RoleName = string.Join(",", roles.Where(r => u.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Name)),
                                                            Confirmed = u.EmailConfirmed,
                                                            Modules = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}",
                                                                                                    (u.AccessCRM) ? "CRM " : "",
                                                                                                    (u.AccessProducts) ? "PDTS " : "",
                                                                                                    (u.AccessQuotations) ? "Q " : "",
                                                                                                    (u.AccessJobs) ? "JOBS " : "",
                                                                                                    (u.AccessInvoices) ? "INV " : "",
                                                                                                    (u.AccessDeliveryNotes) ? "DEL " : "",
                                                                                                    (u.AccessPurchaseOrders) ? "PO " : "",
                                                                                                    (u.AccessStockControl) ? "SC " : ""),
                                            });

            return Json(users.ToDataSourceResult(request));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest]DataSourceRequest request, UserViewModel viewModel)
        {
            var repo = new UserRepository();
            var model = repo.Read().Where(u => u.Id == viewModel.ID).First();
            repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest]DataSourceRequest request, UserViewModel viewModel)
        {
            var repo = new UserRepository();
            var model = repo.Read().Where(u => u.Id == viewModel.ID).First();
            repo.Delete(model);

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }



        #endregion


        #region Create User

        public ActionResult Create()
        {
            return View(new NewUserViewModel { });
        }


        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(NewUserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "UserManagement");

            var repo = new UserRepository();

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Check to see if email is already taken
            if (UserManager.FindByEmail(model.Email) != null)
            {
                ViewBag.ErrorMessage = "The email address entered is already being used.";
                return View(model);
            }

            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                CompanyID = CurrentUser.CompanyID,  //Set the company ID as the same 1, the our company id 
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                EmailConfirmed = true,
                AccessCRM = true,
                AccessProducts = true,
                AccessQuotations = true,
                AccessJobs = true,
                AccessInvoices = true,
                AccessDeliveryNotes = true,
                AccessPurchaseOrders = true,
                AccessStockControl = true,
            };

            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                ViewBag.SuccessMessage = "New user account created.";
                UserManager.AddToRoleById(user.Id, model.RoleID);
                UserManager.Update(user);

                return RedirectToAction("Index", "Users");
            }
            else
            {
                ViewBag.ErrorMessage = result.Errors.First().ToString();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #endregion


        #region Edit User


        // GET: User Edit
        public ActionResult Edit(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "Users");

            try
            {
                var roleRepo = new RoleRepository();
                var roles = roleRepo.Read().ToList();

                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.Id.Equals(ID)).Include(u => u.Roles).First();

                //Create model then return it
                UserViewModel model = new UserViewModel
                {
                    ID = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    RoleID = string.Join(",", roles.Where(r => user.Roles.Select(i => i.RoleId).Contains(r.Id)).OrderBy(r => r.Id).Select(r => r.Id)),
                    AccessCRM = user.AccessCRM,
                    AccessProducts = user.AccessProducts,
                    AccessQuotations = user.AccessQuotations,
                    AccessJobs = user.AccessJobs,
                    AccessInvoices = user.AccessInvoices,
                    AccessDeliveryNotes = user.AccessDeliveryNotes,
                    AccessPurchaseOrders = user.AccessPurchaseOrders,
                    AccessStockControl = user.AccessStockControl
                };

                return View("Edit", model);
            }
            catch
            {
                return RedirectToAction("Index", "Users");
            }
        }



        //
        // POST: /User Edit /Save Changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "Users");

            if (!ModelState.IsValid)
            {
                return PartialView("Edit", model);
            }
                        
            try
            {

                User user = UserManager.FindById(model.ID);

                //Check to see if the email address is already taken if the same email address is not input.
                if (user.Email != model.Email)
                {
                    if (UserManager.FindByEmail(model.Email) != null) //If a user already exsists with the user name then stop saving.
                    {
                        ViewBag.ErrorMessage = "Could not save changes. The Email address entered is already taken.";
                        return View("Edit", model);
                    }
                }

                //Update the user
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;

                //Module Access set elsewhere
                user.AccessCRM = model.AccessCRM;
                user.AccessProducts = model.AccessProducts;
                user.AccessQuotations = model.AccessQuotations;
                user.AccessJobs = model.AccessJobs;
                user.AccessInvoices = model.AccessInvoices;
                user.AccessDeliveryNotes = model.AccessDeliveryNotes;
                user.AccessPurchaseOrders = model.AccessPurchaseOrders;
                user.AccessStockControl = model.AccessStockControl;

                //Remove all the roles then add the selected role                
                UserManager.RemoveAllRoles(user.Id);
                UserManager.AddToRoleById(user.Id, model.RoleID);

                //Save Changes
                UserManager.Update(user);
                
                ViewBag.SuccessMessage = "User Account Details have successfully been Saved.";
            }
            catch
            {
                ViewBag.ErrorMessage = "Could not save changes. Unknown Exception.";
            }


            return PartialView("Edit", model);
        }

        #endregion


        #region Change Password

        // GET: Change Password
        public ActionResult ChangePassword(string ID)
        {
            if (ID == null) return RedirectToAction("Index", "Users");

            try
            {
                //Get the users with the same CompanyID as the user currently logged in and the passed ID. 
                //By checking the CompanyID, we prevent usrs from access users outside there company.
                User user = UserManager.Users.Where(x => x.Id.Equals(ID)).First();

                //Create model then return it
                ChangeUserPasswordViewModel model = new ChangeUserPasswordViewModel
                {
                    ID = user.Id,
                    Email = user.Email
                };

                return View(model);
            }
            catch { return RedirectToAction("Index", "Users"); }

        }


        // POST: Change Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangeUserPasswordViewModel model)
        {
            if (model == null) return RedirectToAction("Index", "Users");

            if (!ModelState.IsValid)
            {
                return PartialView("ChangePassword", model);
            }

            try
            {
                //Try Get User account
                var user = UserManager.FindById(model.ID);

                if (user == null)
                {
                    ViewBag.ErrorMessage = "User Account not found.";
                    return View("ChangePassword", model);
                }

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                IdentityResult result = UserManager.ResetPassword(user.Id, code, model.Password);

                model.Password = "";
                model.ConfirmPassword = "";

                if (result.Succeeded)
                {
                    ViewBag.SuccessMessage = "Password was successfully reset.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Failed to reset password. " + result.Errors.First().ToString();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Failed to reset password. " + ex.ToString();
            }

            return PartialView(model);
        }


        #endregion



        #region Helpers


        public JsonResult DropDownRoles(string companyID)
        {
            using (var context = new SnapDbContext())
            {
                
                var roles = context.UserRoleOptions
                    .Where(r => r.CompanyID == null || r.CompanyID.ToString() == companyID)
                    .OrderBy(r => r.IdentityRole.Name).ToArray()
                    .Select(r => new RoleViewModel { ID = r.IdentityRole.Id, Name = r.IdentityRole.Name })
                    .ToList();

                return Json(roles, JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion
    }
}