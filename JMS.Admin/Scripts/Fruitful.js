﻿
function KendoGridDatabound(e) {

    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() == 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();
        var msg;
        if (options.pageable.messages != undefined) {
            msg = options.pageable.messages.empty;
        }
        if (!msg) msg = "No Records Found"; // Default message

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row'><td colspan='" + colCount + "'><h1 style='color:lightgray; text-align:center; margin:40px;'>" + msg + "</h1></td></tr>");

        grid.pager.element.hide();
    }
    else {

        var pages = grid.dataSource.total() / grid.dataSource.pageSize();


        if (grid.dataSource.pageSize() == null) {
            //if the grid doesn't have a page size (paging not set) then do nothing
            
        }
        else if (grid.dataSource.page() > 1 && grid.dataSource.data().length <= 0) {
            //The grid page happens to not 1 when the total returned is zero
            //then rest back to page one then do another read.
            grid.dataSource.page(1);
            grid.dataSource.read();
        }
        else if (grid.dataSource.total() > 15) {
            //The total items is lager then 15 then show pager anyway.
            grid.pager.element.show();
        }
        else if (pages < 1) {
            //If the page number is less 1 then hide pager.
            grid.pager.element.hide();
        }
        else {
            grid.pager.element.show();
        }
    }

}


function KendoGridSave(e) {
    var grid = e.sender;
    grid.refresh();
}




$(document).ready(function () {
    SetKendoNumericTextBoxFocus();
});

$(document).ajaxSuccess(function () {
    SetKendoNumericTextBoxFocus();
});

function SetKendoNumericTextBoxFocus() {
    $("input[data-role='numerictextbox']").focus(function () {
        if ($(this).data("kendoNumericTextBox").value() == 0) {
            $(this).data("kendoNumericTextBox").value("");
        }
    });

    $("input[data-role='numerictextbox']").focusout(function () {
        if ($(this).data("kendoNumericTextBox").value() == null) {
            $(this).data("kendoNumericTextBox").value(0);
        }
    });
}