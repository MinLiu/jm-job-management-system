namespace WordToPdfDemo
{
    partial class demoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(demoForm));
            this.label1 = new System.Windows.Forms.Label();
            this.btnWordToPdf = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.addFooterCheckBox = new System.Windows.Forms.CheckBox();
            this.addHeaderCheckBox = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bottomSpacingTextBox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.topSpacingTextBox = new System.Windows.Forms.TextBox();
            this.yLocationTextBox = new System.Windows.Forms.TextBox();
            this.xLocationTextBox = new System.Windows.Forms.TextBox();
            this.contentHeightTextBox = new System.Windows.Forms.TextBox();
            this.contentWidthTextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.urlPanel = new System.Windows.Forms.Panel();
            this.selectFileButton = new System.Windows.Forms.Button();
            this.wordFilePathTextBox = new System.Windows.Forms.TextBox();
            this.enterUrlLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1.SuspendLayout();
            this.urlPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(688, 39);
            this.label1.TabIndex = 88;
            this.label1.Text = "Experiment various options of the Word to PDF Converter. You can set Word content" +
    " position and scaling in PDF page, add header and footer in resulted PDF.";
            // 
            // btnWordToPdf
            // 
            this.btnWordToPdf.BackColor = System.Drawing.SystemColors.Control;
            this.btnWordToPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWordToPdf.FlatAppearance.BorderColor = System.Drawing.Color.LightBlue;
            this.btnWordToPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWordToPdf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnWordToPdf.Image = ((System.Drawing.Image)(resources.GetObject("btnWordToPdf.Image")));
            this.btnWordToPdf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWordToPdf.Location = new System.Drawing.Point(225, 20);
            this.btnWordToPdf.Name = "btnWordToPdf";
            this.btnWordToPdf.Size = new System.Drawing.Size(191, 59);
            this.btnWordToPdf.TabIndex = 84;
            this.btnWordToPdf.Text = "Convert Word to PDF";
            this.btnWordToPdf.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnWordToPdf.UseVisualStyleBackColor = false;
            this.btnWordToPdf.Click += new System.EventHandler(this.btnWordToPdf_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.addFooterCheckBox);
            this.panel1.Controls.Add(this.addHeaderCheckBox);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.bottomSpacingTextBox);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.topSpacingTextBox);
            this.panel1.Controls.Add(this.yLocationTextBox);
            this.panel1.Controls.Add(this.xLocationTextBox);
            this.panel1.Controls.Add(this.contentHeightTextBox);
            this.panel1.Controls.Add(this.contentWidthTextBox);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Location = new System.Drawing.Point(5, 129);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(685, 179);
            this.panel1.TabIndex = 91;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(3, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 15);
            this.label3.TabIndex = 179;
            this.label3.Text = "Header and Footer";
            // 
            // addFooterCheckBox
            // 
            this.addFooterCheckBox.AutoSize = true;
            this.addFooterCheckBox.Location = new System.Drawing.Point(145, 147);
            this.addFooterCheckBox.Name = "addFooterCheckBox";
            this.addFooterCheckBox.Size = new System.Drawing.Size(85, 19);
            this.addFooterCheckBox.TabIndex = 177;
            this.addFooterCheckBox.Text = "Add Footer";
            this.addFooterCheckBox.UseVisualStyleBackColor = true;
            // 
            // addHeaderCheckBox
            // 
            this.addHeaderCheckBox.AutoSize = true;
            this.addHeaderCheckBox.Location = new System.Drawing.Point(30, 147);
            this.addHeaderCheckBox.Name = "addHeaderCheckBox";
            this.addHeaderCheckBox.Size = new System.Drawing.Size(91, 19);
            this.addHeaderCheckBox.TabIndex = 178;
            this.addHeaderCheckBox.Text = "Add Header";
            this.addHeaderCheckBox.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label31.Location = new System.Drawing.Point(551, 42);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 15);
            this.label31.TabIndex = 159;
            this.label31.Text = "pt";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label23.Location = new System.Drawing.Point(417, 42);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 15);
            this.label23.TabIndex = 158;
            this.label23.Text = "pt";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label14.Location = new System.Drawing.Point(328, 77);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 15);
            this.label14.TabIndex = 163;
            this.label14.Text = "pt";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label24.Location = new System.Drawing.Point(285, 42);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 15);
            this.label24.TabIndex = 162;
            this.label24.Text = "pt";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label12.Location = new System.Drawing.Point(196, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 15);
            this.label12.TabIndex = 161;
            this.label12.Text = "pt";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label25.Location = new System.Drawing.Point(183, 42);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 15);
            this.label25.TabIndex = 160;
            this.label25.Text = "pt";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label30.Location = new System.Drawing.Point(459, 42);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 15);
            this.label30.TabIndex = 156;
            this.label30.Text = "Height:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label26.Location = new System.Drawing.Point(330, 42);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 15);
            this.label26.TabIndex = 157;
            this.label26.Text = "Width:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label8.Location = new System.Drawing.Point(230, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 15);
            this.label8.TabIndex = 155;
            this.label8.Text = "Bottom:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label27.Location = new System.Drawing.Point(222, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 15);
            this.label27.TabIndex = 154;
            this.label27.Text = "Y:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label7.Location = new System.Drawing.Point(119, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 152;
            this.label7.Text = "Top:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label28.Location = new System.Drawing.Point(119, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(18, 15);
            this.label28.TabIndex = 153;
            this.label28.Text = "X:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label6.Location = new System.Drawing.Point(28, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 15);
            this.label6.TabIndex = 145;
            this.label6.Text = "Spacing:";
            // 
            // bottomSpacingTextBox
            // 
            this.bottomSpacingTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.bottomSpacingTextBox.Location = new System.Drawing.Point(285, 74);
            this.bottomSpacingTextBox.Name = "bottomSpacingTextBox";
            this.bottomSpacingTextBox.Size = new System.Drawing.Size(34, 21);
            this.bottomSpacingTextBox.TabIndex = 147;
            this.bottomSpacingTextBox.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label29.Location = new System.Drawing.Point(28, 42);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 15);
            this.label29.TabIndex = 144;
            this.label29.Text = "Destination:";
            // 
            // topSpacingTextBox
            // 
            this.topSpacingTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.topSpacingTextBox.Location = new System.Drawing.Point(156, 74);
            this.topSpacingTextBox.Name = "topSpacingTextBox";
            this.topSpacingTextBox.Size = new System.Drawing.Size(34, 21);
            this.topSpacingTextBox.TabIndex = 149;
            this.topSpacingTextBox.Text = "0";
            // 
            // yLocationTextBox
            // 
            this.yLocationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.yLocationTextBox.Location = new System.Drawing.Point(245, 39);
            this.yLocationTextBox.Name = "yLocationTextBox";
            this.yLocationTextBox.Size = new System.Drawing.Size(34, 21);
            this.yLocationTextBox.TabIndex = 146;
            this.yLocationTextBox.Text = "0";
            // 
            // xLocationTextBox
            // 
            this.xLocationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.xLocationTextBox.Location = new System.Drawing.Point(142, 39);
            this.xLocationTextBox.Name = "xLocationTextBox";
            this.xLocationTextBox.Size = new System.Drawing.Size(34, 21);
            this.xLocationTextBox.TabIndex = 148;
            this.xLocationTextBox.Text = "0";
            // 
            // contentHeightTextBox
            // 
            this.contentHeightTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.contentHeightTextBox.Location = new System.Drawing.Point(511, 39);
            this.contentHeightTextBox.Name = "contentHeightTextBox";
            this.contentHeightTextBox.Size = new System.Drawing.Size(34, 21);
            this.contentHeightTextBox.TabIndex = 150;
            // 
            // contentWidthTextBox
            // 
            this.contentWidthTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.contentWidthTextBox.Location = new System.Drawing.Point(377, 39);
            this.contentWidthTextBox.Name = "contentWidthTextBox";
            this.contentWidthTextBox.Size = new System.Drawing.Size(34, 21);
            this.contentWidthTextBox.TabIndex = 151;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label20.Location = new System.Drawing.Point(3, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(146, 15);
            this.label20.TabIndex = 133;
            this.label20.Text = "Word Content Options";
            // 
            // urlPanel
            // 
            this.urlPanel.Controls.Add(this.selectFileButton);
            this.urlPanel.Controls.Add(this.wordFilePathTextBox);
            this.urlPanel.Controls.Add(this.enterUrlLabel);
            this.urlPanel.Location = new System.Drawing.Point(5, 50);
            this.urlPanel.Name = "urlPanel";
            this.urlPanel.Size = new System.Drawing.Size(685, 73);
            this.urlPanel.TabIndex = 144;
            // 
            // selectFileButton
            // 
            this.selectFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.selectFileButton.Location = new System.Drawing.Point(601, 35);
            this.selectFileButton.Name = "selectFileButton";
            this.selectFileButton.Size = new System.Drawing.Size(66, 26);
            this.selectFileButton.TabIndex = 44;
            this.selectFileButton.Text = "Browse";
            this.selectFileButton.UseVisualStyleBackColor = true;
            this.selectFileButton.Click += new System.EventHandler(this.selectFileButton_Click);
            // 
            // wordFilePathTextBox
            // 
            this.wordFilePathTextBox.Location = new System.Drawing.Point(9, 38);
            this.wordFilePathTextBox.Name = "wordFilePathTextBox";
            this.wordFilePathTextBox.Size = new System.Drawing.Size(586, 21);
            this.wordFilePathTextBox.TabIndex = 43;
            // 
            // enterUrlLabel
            // 
            this.enterUrlLabel.AutoSize = true;
            this.enterUrlLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.enterUrlLabel.Location = new System.Drawing.Point(6, 13);
            this.enterUrlLabel.Name = "enterUrlLabel";
            this.enterUrlLabel.Size = new System.Drawing.Size(205, 15);
            this.enterUrlLabel.TabIndex = 42;
            this.enterUrlLabel.Text = "The Word Document to Convert";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnWordToPdf);
            this.groupBox1.Location = new System.Drawing.Point(5, 314);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(685, 92);
            this.groupBox1.TabIndex = 90;
            this.groupBox1.TabStop = false;
            // 
            // demoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(693, 415);
            this.Controls.Add(this.urlPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "demoForm";
            this.Text = "EVO Word to PDF Converter Demo";
            this.Load += new System.EventHandler(this.demoForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.urlPanel.ResumeLayout(false);
            this.urlPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnWordToPdf;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox bottomSpacingTextBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox topSpacingTextBox;
        private System.Windows.Forms.TextBox yLocationTextBox;
        private System.Windows.Forms.TextBox xLocationTextBox;
        private System.Windows.Forms.TextBox contentHeightTextBox;
        private System.Windows.Forms.TextBox contentWidthTextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel urlPanel;
        private System.Windows.Forms.Button selectFileButton;
        private System.Windows.Forms.TextBox wordFilePathTextBox;
        private System.Windows.Forms.Label enterUrlLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox addFooterCheckBox;
        private System.Windows.Forms.CheckBox addHeaderCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

