using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EvoWordToPdf;

namespace WordToPdfDemo
{
    public partial class demoForm : Form
    {
        public demoForm()
        {
            InitializeComponent();
        }

        private void btnWordToPdf_Click(object sender, EventArgs e)
        {
            // Create a Word to PDF converter object with default settings
            WordToPdfConverter wordToPdfConverter = new WordToPdfConverter();

            // Set license key received after purchase to use the converter in licensed mode
            // Leave it not set to use the converter in demo mode
            wordToPdfConverter.LicenseKey = "0lxNXUtPXU1dS1NNXU5MU0xPU0RERERdTQ==";

            // Word Content Destination and Spacing Options

            // Set Word content destination in PDF page
            if (xLocationTextBox.Text.Length > 0)
                wordToPdfConverter.PdfDocumentOptions.X = float.Parse(xLocationTextBox.Text);
            if (yLocationTextBox.Text.Length > 0)
                wordToPdfConverter.PdfDocumentOptions.Y = float.Parse(yLocationTextBox.Text);
            if (contentWidthTextBox.Text.Length > 0)
                wordToPdfConverter.PdfDocumentOptions.Width = float.Parse(contentWidthTextBox.Text);
            if (contentHeightTextBox.Text.Length > 0)
                wordToPdfConverter.PdfDocumentOptions.Height = float.Parse(contentHeightTextBox.Text);

            // Set Word content top and bottom spacing or leave them not set to have no spacing for the Word content
            wordToPdfConverter.PdfDocumentOptions.TopSpacing = float.Parse(topSpacingTextBox.Text);
            wordToPdfConverter.PdfDocumentOptions.BottomSpacing = float.Parse(bottomSpacingTextBox.Text);
            
            // Add Header

            // Enable header in the generated PDF document
            wordToPdfConverter.PdfDocumentOptions.ShowHeader = addHeaderCheckBox.Checked;

            // Draw header elements
            if (wordToPdfConverter.PdfDocumentOptions.ShowHeader)
                DrawHeader(wordToPdfConverter, true);

            // Add Footer

            // Enable footer in the generated PDF document
            wordToPdfConverter.PdfDocumentOptions.ShowFooter = addFooterCheckBox.Checked;

            // Draw footer elements
            if (wordToPdfConverter.PdfDocumentOptions.ShowFooter)
                DrawFooter(wordToPdfConverter, true, true);

            Cursor = Cursors.WaitCursor;

            string outPdfFile = @"DemoAppFiles\Output\WordToPdf.pdf";
            try
            {
                string wordFile = wordFilePathTextBox.Text;

                // Convert the Word document to a PDF document
                byte[] outPdfBuffer = wordToPdfConverter.ConvertWordFile(wordFile);

                // Write the memory buffer in a PDF file
                System.IO.File.WriteAllBytes(outPdfFile, outPdfBuffer);
            }
            catch (Exception ex)
            {
                // The Word to PDF conversion failed
                MessageBox.Show(String.Format("Word to PDF Error. {0}", ex.Message));
                return;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

            // Open the created PDF document in default PDF viewer
            try
            {
                System.Diagnostics.Process.Start(outPdfFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Cannot open created PDF file '{0}'. {1}", outPdfFile, ex.Message));
            }
        }

        /// <summary>
        /// Draw the header elements
        /// </summary>
        /// <param name="wordToPdfConverter">The Word to PDF Converter object</param>
        /// <param name="drawHeaderLine">A flag indicating if a line should be drawn at the bottom of the header</param>
        private void DrawHeader(WordToPdfConverter WordToPdfConverter, bool drawHeaderLine)
        {
            string headerImagePath = System.IO.Path.Combine(Application.StartupPath,
                        @"DemoAppFiles\Input\Images\logo.jpg");

            // Set the header height in points
            WordToPdfConverter.PdfHeaderOptions.HeaderHeight = 60;

            // Set header background color
            WordToPdfConverter.PdfHeaderOptions.HeaderBackColor = Color.WhiteSmoke;

            // Set logo
            ImageElement headerImage = new ImageElement(5, 5, 100, 50, headerImagePath);
            WordToPdfConverter.PdfHeaderOptions.AddElement(headerImage);

            // Set header text
            TextElement headerText = new TextElement(0, 5, "EVO Word to PDF Converter ",
                new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point));
            // Align the text at the right of the footer
            headerText.TextAlign = HorizontalTextAlign.Right;
            // Set text color
            headerText.ForeColor = Color.Navy;
            // Embed the text element font in PDF
            headerText.EmbedSysFont = true;
            // Add the text element to header
            WordToPdfConverter.PdfHeaderOptions.AddElement(headerText);
        }

        /// <summary>
        /// Draw the footer elements
        /// </summary>
        /// <param name="wordToPdfConverter">The Word to PDF Converter object</param>
        /// <param name="addPageNumbers">A flag indicating if the page numbering is present in footer</param>
        /// <param name="drawFooterLine">A flag indicating if a line should be drawn at the top of the footer</param>
        private void DrawFooter(WordToPdfConverter WordToPdfConverter, bool addPageNumbers, bool drawFooterLine)
        {
            string footerImagePath = System.IO.Path.Combine(Application.StartupPath,
                        @"DemoAppFiles\Input\Images\logo.jpg");

            // Set the footer height in points
            WordToPdfConverter.PdfFooterOptions.FooterHeight = 60;

            // Set footer background color
            WordToPdfConverter.PdfFooterOptions.FooterBackColor = Color.WhiteSmoke;

            // Set logo
            ImageElement headerImage = new ImageElement(5, 5, 100, 50, footerImagePath);
            WordToPdfConverter.PdfFooterOptions.AddElement(headerImage);

            // Add page numbering
            if (addPageNumbers)
            {
                // Create a text element with page numbering place holders &p; and & P;
                TextElement footerText = new TextElement(0, 30, "Page &p; of &P;  ",
                    new System.Drawing.Font(new System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point));

                // Align the text at the right of the footer
                footerText.TextAlign = HorizontalTextAlign.Right;

                // Set page numbering text color
                footerText.ForeColor = Color.Navy;

                // Embed the text element font in PDF
                footerText.EmbedSysFont = true;

                // Add the text element to footer
                WordToPdfConverter.PdfFooterOptions.AddElement(footerText);
            }
        }

        private void demoForm_Load(object sender, EventArgs e)
        {
            wordFilePathTextBox.Text = System.IO.Path.Combine(Application.StartupPath, @"DemoAppFiles\Input\Word\Demo.docx");
        }

        private void selectFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Docx files (*.docx)|*.docx|Doc files (*.doc)|*.doc|All files (*.*)|*.*";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                wordFilePathTextBox.Text = fd.FileName;
            }
        }        
    }
}