﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JMS.Models
{
    public class StatisticsGridViewModel
    {
        public string Title { get; set; }
        public decimal Month0 { get; set; }
        public decimal Month1 { get; set; }
        public decimal Month2 { get; set; }
        public decimal Month3 { get; set; }
        public decimal Month4 { get; set; }
        public decimal Month5 { get; set; }
        public decimal Month6 { get; set; }
        public decimal Month7 { get; set; }
        public decimal Month8 { get; set; }
        public decimal Month9 { get; set; }
        public decimal Month10 { get; set; }
        public decimal Month11 { get; set; }
    }

    public class EngineerPurchaseStatisticsViewModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string CategoryLabel { get; set; }
        public int Count { get; set; }
        public decimal TotalValue { get; set; }
    }

    public class EngineerJobStatisticsViewModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string CategoryLabel { get; set; }
        public int CountNewJobs { get; set; }
        public int CountCompletedJobs { get; set; }
        public int CountJobsOnHold { get; set; }
        public int CountJobsOpened { get; set; }
        public int CountJobsTotal { get; set; }
    }

}

