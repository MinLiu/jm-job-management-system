﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace JMS.Models
{
    
    public class JobGanttViewModel : IGanttTask
    {
        public string JobID { get; set; }
        public string RefNumber { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public decimal PercentComplete { get; set; }
        public bool Summary { get; set; }
        public bool Expanded { get; set; }
        public int OrderId { get; set; }
        public JobStatus Status { get; set; }
    }

}