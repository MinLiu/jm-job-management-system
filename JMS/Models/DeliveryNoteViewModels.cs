﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JMS.Models
{
    
    public class DeliveryNotePDFViewModel
    {
        //Used in PDF Generation
        public CompanyViewModel CompanyDetails { get; set; }
        public ClientAddressViewModel DeliveryAddress { get; set; }
        public string TermsConditions { get; set; }

        public string DefaultFooterImageURL { get; set; }
    }


    public class DeliveryNoteReviewViewModel
    {
        //Used to edit the display settings
        public string DeliveryNoteID { get; set; }
        public string CheckSum { get; set; }
        public string Reference { get; set; }
        public int Number { get; set; }
        public int StatusID { get; set; }
        public string StatusHexColour { get; set; }
        public string StatusDescription { get; set; }
        public bool HasAgreement { get; set; }

    }


    public class DeliveryNoteAcceptDeclineViewModel
    {
        [Required]
        public string DeliveryNoteID { get; set; }
        public string CheckSum { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusDescription { get; set; }
        public string StatusHexColour { get; set; }

        public bool Accept { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Message { get; set; }

        public string PoNumber { get; set; }
    }


    public class DeliveryNoteEmailViewModel
    {
        public string DeliveryNoteID { get; set; }
        public string ClientID { get; set; }
        public string CheckSum { get; set; }
        public string Bcc { get; set; }

        [Required]
        [Display(Name = "To Email Addresses")]
        public string ToEmails { get; set; }

        [EmailAddress]
        [Display(Name = "From Email Address")]
        public string FromEmail { get; set; }

        public string Title { get; set; }
        public string Message { get; set; }
        public bool IncludePDF { get; set; }
        public bool SendCopyPDF { get; set; }

        public SelectList Attachments { get; set; }
        public IEnumerable<string> SelectedAttachments { get; set; }
    }


    public class DeliveryNotesDashboardViewModel
    {
        public IEnumerable<DashboardChartValueViewModel> Stats { get; set; }
    }

}