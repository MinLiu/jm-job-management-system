﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    

    public class CustomItemViewModel
    {
        public string ID { get; set; }
        public string OrderID { get; set; }
        public string SectionID { get; set; }

        //[Required(ErrorMessage = "* The Product Code field is required.")]
        //[Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        [Required(ErrorMessage = "* The Description is required.")]
        public string Description { get; set; }
        public string Category { get; set; }
        public string VendorCode { get; set; }
        public string Manufacturer { get; set; }

        public string Unit { get; set; }
        
        public string ImageURL { get; set; }

        //[Required(ErrorMessage = "* The VAT field is required.")]
        public decimal VAT { get; set; }

        [Required(ErrorMessage = "* The Quantity field is required.")]
        public decimal Quantity { get; set; }

        [Required(ErrorMessage = "* The Price field is required.")]
        public decimal Price { get; set; }

        //[Required(ErrorMessage = "* The Cost field is required.")]
        public decimal Cost { get; set; }
    }

    

    public class DashboardChartValueViewModel
    {
        public string Name { get; set; }
        public string HexColour { get; set; }
        public int Count { get; set; }
        public int SortPos { get; set; }
    }


    public class TemplateUploadViewModel
    {
        [Required]
        public string Name { get; set; }
        public string FilePath { get; set; }
        public string Extension { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public int Stage { get; set; }

    }


    public class SubmitAgreementViewModel
    {
        public string ID { get; set; }
        public string PoNumber { get; set; }
        
        [Required]
        public string Title { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        
        [Required]
        public bool Accept { get; set; }

        public string DeviceInformation { get; set; }
        public string IP_Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

}