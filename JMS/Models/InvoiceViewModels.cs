﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JMS.Models
{
    
    public class InvoicePDFViewModel
    {
        //Used in PDF Generation
        public CompanyViewModel CompanyDetails { get; set; }
        public ClientAddressViewModel InvoiceAddress { get; set; }
        public ClientAddressViewModel DeliveryAddress { get; set; }
        public string TermsConditions { get; set; }

        public string DefaultFooterImageURL { get; set; }
    }


    public class InvoiceReviewViewModel
    {
        //Used to edit the display settings
        public string InvoiceID { get; set; }
        public string CheckSum { get; set; }
        public string Reference { get; set; }
        public int Number { get; set; }

    }


    public class InvoiceAcceptDeclineViewModel
    {
        [Required]
        public string InvoiceID { get; set; }
        public string CheckSum { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusDescription { get; set; }
        public string StatusHexColour { get; set; }

        public bool Accept { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Message { get; set; }

        public string PoNumber { get; set; }
    }

    public class InvoiceEmailViewModel
    {
        public string InvoiceID { get; set; }
        public string ClientID { get; set; }
        public string CheckSum { get; set; }

        [Required]
        [Display(Name = "To Email Addresses")]
        public string ToEmails { get; set; }

        [EmailAddress]
        [Display(Name = "From Email Address")]
        public string FromEmail { get; set; }

        public string Bcc { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IncludePDF { get; set; }
        public bool SendCopyPDF { get; set; }

        public SelectList Attachments { get; set; }
        public IEnumerable<string> SelectedAttachments { get; set; }
    }


    public class InvoiceSageExportViewModel
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public string AccountRef { get; set; }
        public string NominalRef { get; set; }
        public string DepartmentCode { get; set; }
        public DateTime Date { get; set; }
        public string Reference { get; set; }
        public string Details { get; set; }
        public decimal NetAmount { get; set; }
        public string TaxCode { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public string ExtraRef { get; set; }
        public string UserName { get; set; }
        public string ProjectRef { get; set; }
        public string CostCodeRef { get; set; }
        public string Currency { get; set; }
        public int CurrencyID { get; set; }
    }


    public class InvoiceXeroExportViewModel
    {
        public string ID { get; set; }
        public string ContactName { get; set; }

        public string POAddressLine1 { get; set; }
        public string POAddressLine2 { get; set; }
        public string POAddressLine3 { get; set; }
        public string POCity { get; set; }
        public string POPostalCode { get; set; }
        public string POCountry { get; set; }

        public string InvoiceNumber { get; set; }
        public string Reference { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }

        
        public string InventoryItemCode { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitAmount { get; set; }
        public decimal Discount { get; set; }

        public string AccountCode { get; set; }
        public string TaxType { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }

        public string Currency { get; set; }
        public string BrandingTheme { get; set; }
        public string Sent { get; set; }
        public string Status { get; set; }

        public int CurrencyID { get; set; }
    }


    public class InvoiceKashFlowExportViewModel
    {
        public string ID { get; set; }
        public int InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerReference { get; set; }

        public string LineDescription { get; set; }
        public decimal LineQuantity { get; set; }        
        public decimal LineRate { get; set; }
        public string LineChargeType { get; set; }

        public decimal LineVatAmount { get; set; }
        public decimal LineVatRate { get; set; }

        public string CurrencyCode { get; set; }
    }


    public class InvoicesDashboardViewModel
    {
        public IEnumerable<DashboardChartValueViewModel> Stats { get; set; }
    }

}