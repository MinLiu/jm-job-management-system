﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JMS.Models
{
    public class ReportDepartmentGridViewModel
    {
        public string DepartmentID { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class ReportSupplierGridViewModel
    {
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string SupplierType { get; set; }
        public string SupplierAccountDetail { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class ReportJobGridViewModel
    {
        public string JobID { get; set; }
        public long JobNo { get; set; }
        public string JobTitle { get; set; }
        public string JobType { get; set; }
        public decimal TotalCost { get; set; }
    }

}

