﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JMS.Models
{
    public class ContactUsViewModel
    {
        [Required]
        public string Category { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }


        [Required]
        public string Message { get; set; }


        public string Username { get; set; }
    }
}