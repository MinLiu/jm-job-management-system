﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using JMS.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Diagnostics;

namespace SnapSuite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error()
        {
#if !DEBUG
            var ex = Server.GetLastError();

            if (ex == null) return;

            StackTrace st = new StackTrace(ex, true);
            StackFrame frame = st.GetFrame(0);
            string fileName = frame.GetFileName();
            string methodName = frame.GetMethod().Name;
            int line = frame.GetFileLineNumber();
             
            string url ="";  
            string get = "";
            string post = "";
            string companyID = "";
            string companyName = "";
            string userID = "";
            string username = "";


            try { url = HttpContext.Current.Request.Url.AbsoluteUri; }
            catch{}
            
            try{
                foreach(var key in  HttpContext.Current.Request.Form.AllKeys) {
                    post += string.Format("{0} : {1}\n", key, HttpContext.Current.Request.Form[key]);
                }
            }
            catch{}

            try{
                foreach(var key in  HttpContext.Current.Request.QueryString.AllKeys) {
                    get += string.Format("{0} : {1}\n", key, HttpContext.Current.Request.QueryString[key]);
                }
            }
            catch{}

            try {                
                userID = User.Identity.GetUserId().ToString();
                var userRepo = new UserRepository();
                var currentUser = userRepo.Read().Where(u => u.Id == userID).First();

                companyID = "";
                companyName = "";
                username = currentUser.Email;
            }
            catch{}


            var repo = new EntityRespository<ErrorLog>(new SnapDbContext());

            
            repo.Create(new ErrorLog {
                Timestamp = DateTime.Now,
                CompanyID = companyID,
                CompanyName = companyName,
                UserID = userID,
                UserName = username,

                Title =  ex.Message,
                Description = string.Format("{0} : {1}", ex.GetType().ToString(), ex.Message),
                TargetSite = (ex.TargetSite != null) ? ex.TargetSite.ToString() : "",
                SourceError = ex.Source,
                StackTrace = ex.StackTrace,
                URL = url,
                GetValues = get,
                PostValues = post
            });
#endif

        }
    }
}
