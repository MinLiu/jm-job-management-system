﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class StatisticsController : BaseController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read()
        {
            using (var context = new SnapDbContext())
            {
                //var newJobs = context.Jobs.GroupBy(x => new { Year = x.SubmittedDate.Value.Year, Month = x.SubmittedDate.Value.Month })
                //                       .Where(x => x.Key.Year >= DateTime.Now.Year - 1)
                //                       .OrderByDescending(x => x.Key.Year)
                //                       .ThenByDescending(x => x.Key.Month)
                //                       .Select(x => new StatisticsViewModel
                //                       {
                //                           Title = "New Jobs",
                //                           Year = x.Key.Year,
                //                           Month = x.Key.Month,
                //                           CategoryLabel = x.Key.Year + " - " + x.Key.Month,
                //                           Count = x.Count()
                //                       })
                //                       .Take(12)
                //                       .OrderBy(x => x.Year)
                //                       .ThenBy(x => x.Month)
                //                       .ToList();

                //var closedJobs = context.Jobs.GroupBy(x => new { Year = x.ClosedDate.Value.Year, Month = x.ClosedDate.Value.Month })
                //                       .Where(x => x.Key.Year >= DateTime.Now.Year - 1)
                //                       .OrderByDescending(x => x.Key.Year)
                //                       .ThenByDescending(x => x.Key.Month)
                //                       .Select(x => new StatisticsViewModel
                //                       {
                //                           Title = "Jobs Completed",
                //                           Year = x.Key.Year,
                //                           Month = x.Key.Month,
                //                           CategoryLabel = x.Key.Year + " - " + x.Key.Month,
                //                           Count = -x.Count()
                //                       })
                //                       .Take(12)
                //                       .OrderBy(x => x.Year)
                //                       .ThenBy(x => x.Month)
                //                       .ToList();

                //var jobsOnHold = context.MonthlyJobsOnHold
                //                        .OrderByDescending(x => x.Month)
                //                        .Take(11)
                //                        .ToList()
                //                        .Select(x => new StatisticsViewModel
                //                        {
                //                            Title = "current jobs on Hold",
                //                            CategoryLabel = x.Month.ToString("yyyy - MM"),
                //                            Count = x.Count
                //                        })
                //                        .ToList();

                //var jobsOpened = context.MonthlyJobsOpened
                //                        .OrderByDescending(x => x.Month)
                //                        .Take(11)
                //                        .ToList()
                //                        .Select(x => new StatisticsViewModel
                //                        {
                //                            Title = "Current open jobs(not on Hold)",
                //                            CategoryLabel = x.Month.ToString("yyyy - MM"),
                //                            Count = x.Count
                //                        })
                //                        .ToList();

                //var list = new List<StatisticsViewModel>();
                //list.AddRange(newJobs);
                //list.AddRange(closedJobs);
                //list.AddRange(jobsOnHold);
                //list.AddRange(jobsOpened);

                var result = new List<EngineerJobStatisticsViewModel>();
                for (var i = 11; i >= 0; i--)
                {
                    DateTime yearMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-i);
                    var newJobsInMonth = context.Jobs.GroupBy(x => new { Year = x.SubmittedDate.Value.Year, Month = x.SubmittedDate.Value.Month }).Where(g => g.Key.Year == yearMonth.Year && g.Key.Month == yearMonth.Month).FirstOrDefault();
                    var completedJobsInMonth = context.Jobs.GroupBy(x => new { Year = x.ClosedDate.Value.Year, Month = x.ClosedDate.Value.Month }).Where(g => g.Key.Year == yearMonth.Year && g.Key.Month == yearMonth.Month).FirstOrDefault();
                    var jobsOnHoldInMonth = context.MonthlyJobsOnHold.Where(x => x.Month == yearMonth).FirstOrDefault();
                    var jobsOpenedInMonth = context.MonthlyJobsOpened.Where(x => x.Month == yearMonth).FirstOrDefault();
                    if (i == 0)
                    {
                        // Current Month
                        jobsOnHoldInMonth = new MonthlyJobsOnHold { Count = context.Jobs.Where(x => x.StatusJobID == JobStatusValues.OpenOnHold).Count() };
                        jobsOpenedInMonth = new MonthlyJobsOpened{ Count = context.Jobs.Where(x => x.StatusJobID == JobStatusValues.Open).Count() };
                    }

                    result.Add(new EngineerJobStatisticsViewModel
                    {
                        Year = yearMonth.Year,
                        Month = yearMonth.Month,
                        CategoryLabel = yearMonth.ToString("MMM"),
                        CountNewJobs = newJobsInMonth != null ? newJobsInMonth.Count() : 0,
                        CountCompletedJobs = completedJobsInMonth != null ? -completedJobsInMonth.Count() : 0,
                        CountJobsOnHold = jobsOnHoldInMonth != null ? jobsOnHoldInMonth.Count : 0,
                        CountJobsOpened = jobsOpenedInMonth != null ? jobsOpenedInMonth.Count : 0,
                        CountJobsTotal = (jobsOnHoldInMonth != null ? jobsOnHoldInMonth.Count : 0) + (jobsOpenedInMonth != null ? jobsOpenedInMonth.Count : 0)
                    });
                }

                return Json(result);
            }
        }

        public ActionResult ReadPurchases()
        {
            using (var context = new SnapDbContext())
            {
                //var newPurchases = context.Purchases.GroupBy(x => new { Year = x.DateOrdered.Value.Year, Month = x.DateOrdered.Value.Month })
                //                       .Where(x => x.Key.Year >= DateTime.Now.Year - 1)
                //                       .OrderByDescending(x => x.Key.Year)
                //                       .ThenByDescending(x => x.Key.Month)
                //                       .Select(x => new PurchaseStatisticsViewModel
                //                       {
                //                           Title = "Purchases Order Placed",
                //                           Year = x.Key.Year,
                //                           Month = x.Key.Month,
                //                           CategoryLabel = x.Key.Year + " - " + x.Key.Month,
                //                           Count = x.Count(),
                //                           TotalValue = x.Count() > 0 ? x.Sum(s => s.PurchaseTotal) : 0
                //                       })
                //                       .Take(12)
                //                       .OrderBy(x => x.Year)
                //                       .ThenBy(x => x.Month)
                //                       .ToList();

                var result = new List<EngineerPurchaseStatisticsViewModel>();

                for (var i = 11; i >= 0; i--)
                {
                    DateTime yearMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-i);
                    var newPurchasesInMonth = context.Purchases.GroupBy(x => new { Year = x.DateOrdered.Value.Year, Month = x.DateOrdered.Value.Month }).Where(g => g.Key.Year == yearMonth.Year && g.Key.Month == yearMonth.Month).FirstOrDefault();

                    result.Add(new EngineerPurchaseStatisticsViewModel
                    {
                        Year = yearMonth.Year,
                        Month = yearMonth.Month,
                        CategoryLabel = yearMonth.ToString("MMM"),
                        Count = newPurchasesInMonth != null ? newPurchasesInMonth.Count() : 0,
                        TotalValue = newPurchasesInMonth != null ? newPurchasesInMonth.Sum(x => x.PurchaseTotal) : 0
                    });
                }

                return Json(result);
            }
        }
    }
    
}