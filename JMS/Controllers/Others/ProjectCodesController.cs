﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize]
    public class ProjectCodesController : GridController<ProjectCode, ProjectCodeViewModel>
    {

        public ProjectCodesController()
            : base(new ProjectCodeRepository(), new ProjectCodeMapper())
        {

        }

        public override JsonResult Create(DataSourceRequest request, ProjectCodeViewModel viewModel)
        {
            return base.Create(request, viewModel);
        }
                

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _repo.Read()
                            .OrderBy(o => o.Code)
                            .ToList()
                            .Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Update(DataSourceRequest request, ProjectCodeViewModel viewModel)
        {
            return base.Update(request, viewModel);
        }

        public override JsonResult Destroy(DataSourceRequest request, ProjectCodeViewModel viewModel)
        {
            return base.Destroy(request, viewModel);
        }

    }

}