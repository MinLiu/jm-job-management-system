﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class UserModulesController : BaseController
    {

        UserRepository _repo = new UserRepository();

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Read([DataSourceRequest]DataSourceRequest request,
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false, 
            bool PurchaseOrders = false, 
            bool StockControl = false
        )
        {

            var users = _repo.Read()
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.FirstName)
                .ThenBy(u => u.Email)
                .ToList()
                .Select(u => new UserViewModel
                {
                    ID = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName
                });

            return Json(users.ToDataSourceResult(request));
        }


        public ActionResult _AddUser(string userId, 
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false,
            bool PurchaseOrders = false,
            bool StockControl = false)
        {

            var user = _repo.Find(userId);

            _repo.Update(user);
            return Json("Success");
        }

        public ActionResult _RemoveUser(string userId,
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false,
            bool PurchaseOrders = false,
            bool StockControl = false)
        {
            var user = _repo.Find(userId);

            _repo.Update(user);
            return Json("Success");
        }



        public JsonResult DropDownUsers(
            bool CRM = false,
            bool Products = false,
            bool Quotations = false,
            bool Jobs = false,
            bool Invoices = false,
            bool DeliveryNotes = false,
            bool PurchaseOrders = false,
            bool StockControl = false
        )
        {

            var users = _repo.Read()
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.FirstName)
                .ThenBy(u => u.Email)
                .ToList()
                .Select(u => new SelectItemViewModel
                {
                    ID = u.Id,
                    Name = u.Email
                });

            return Json(users, JsonRequestBehavior.AllowGet);
        }


    }
}