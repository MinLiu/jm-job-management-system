﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin,Admin")]
    public class UserRolesController : BaseController
    {

        //public UserRoleOptionMapper _mapper = new UserRoleOptionMapper();
        //public RoleRepository roleRepo = new RoleRepository();
        //public UserRoleOptionRepository optionRepo = new UserRoleOptionRepository();


        //// GET: Users
        //public ActionResult Index()
        //{
        //    return View();
        //}


        //public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        //{

        //    var roles = optionRepo.Read()
        //                     .Include(x => x.IdentityRole)
        //                     .OrderBy(x => x.IdentityRole.Name)
        //                     .ToList()
        //                     .Select(x => _mapper.MapToViewModel(x));

        //    return Json(roles.ToDataSourceResult(request));
        //}



        //public ActionResult Destroy([DataSourceRequest]DataSourceRequest request, UserRoleOptionViewModel viewModel)
        //{
        //    var mapper = new UserRoleOptionMapper();
        //    var roleRepo = new RoleRepository();
        //    var optionRepo = new UserRoleOptionRepository();

        //    var model = optionRepo.Read().Where(u => u.IdentityRoleID == viewModel.IdentityRoleID).First();
        //    optionRepo.Delete(model);

        //    var role = roleRepo.Read().Where(u => u.Id == viewModel.IdentityRoleID).First();
        //    roleRepo.Delete(role);

        //    return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        //}



        //// GET: /New Role
        //public ActionResult _NewRole()
        //{
        //    return PartialView(new UserRoleOptionViewModel 
        //    {  
        //        ViewQuotations = true,
        //        EditQuotations = true,
        //        DeleteQuotations = true,
        //        ExportQuotations = true,

        //        ViewJobs = true,
        //        EditJobs = true,
        //        DeleteJobs = true,
        //        ExportJobs = true,

        //        ViewInvoices = true,
        //        EditInvoices = true,
        //        DeleteInvoices = true,
        //        ExportInvoices = true,

        //        ViewPurchaseOrders = true,
        //        EditPurchaseOrders = true,
        //        DeletePurchaseOrders = true,
        //        ExportPurchaseOrders = true,

        //        ViewDeliveryNotes = true,
        //        EditDeliveryNotes = true,
        //        DeleteDeliveryNotes = true,
        //        ExportDeliveryNotes = true,

        //        ViewStockControl = true,
        //        EditStockControl = true,
        //        ExportStockControl = true,

        //        ViewCRM = true,
        //        EditCRM = true,
        //        DeleteCRM = true,
        //        ExportCRM = true,

        //        ViewProducts = true,
        //        EditProducts = true,
        //        DeleteProducts = true,
        //        ExportProducts = true, 
        //    });
        //}


        //// POST: /New Role
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        //public ActionResult _NewRole(UserRoleOptionViewModel viewModel)
        //{
        //    if (viewModel == null) return RedirectToAction("Index");

        //    //Check to see if model is valid
        //    if (!ModelState.IsValid)
        //    {
        //        return PartialView(viewModel);
        //    }

        //    var newRole = new IdentityRole { Id = Guid.NewGuid().ToString("N"), Name = viewModel.IdentityRoleName };
        //    roleRepo.Create(newRole);

        //    var model = _mapper.MapToModel(viewModel);
        //    model.IdentityRoleID = newRole.Id;
        //    viewModel.IdentityRoleID = newRole.Id;
        //    model.CompanyID = CurrentUser.CompanyID;
        //    viewModel.CompanyID = CurrentUser.CompanyID.ToString();

        //    optionRepo.Create(model);
        //    //_mapper.MapToViewModel(model, viewModel);

        //    return Json(new { ID = newRole.Id });

        //}



        //// GET: /Edit Role
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        //public ActionResult _EditRole(string ID)
        //{
        //    try
        //    {                
        //        var clt = optionRepo.Read().Where(r => r.IdentityRoleID == ID).First();
        //        UserRoleOptionViewModel model = _mapper.MapToViewModel(clt);

        //        return PartialView(model);
        //    }
        //    catch
        //    {
        //        return PartialView("_NewRole", new UserRoleOptionViewModel());
        //    }

        //}


        //// POST: /Edit Role
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        //public ActionResult _EditRole(UserRoleOptionViewModel viewModel)
        //{
        //    if (viewModel == null) return RedirectToAction("Index");

        //    var role = roleRepo.Read().Where(r => r.Id == viewModel.IdentityRoleID).First();
        //    role.Name = viewModel.IdentityRoleName;
        //    roleRepo.Update(role, new string[] { "Name" });

        //    var model = optionRepo.Read().Where(r => r.IdentityRoleID == viewModel.IdentityRoleID).First();
        //    _mapper.MapToModel(viewModel, model);
        //    optionRepo.Update(model);
        //    _mapper.MapToViewModel(model, viewModel);

        //    ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
        //    return PartialView(viewModel);

        //}


        //public JsonResult DropDownRoles()
        //{
        //    var roles = optionRepo.Read()
        //            .Where(r => !r.IdentityRole.Name.Contains("Super") && (r.CompanyID == null))
        //            .OrderBy(r => r.IdentityRole.Name).ToArray()
        //            .Select(r => new RoleViewModel { ID = r.IdentityRoleID, Name = r.IdentityRole.Name })
        //            .ToList();

        //    return Json(roles, JsonRequestBehavior.AllowGet);
        //}
        
    }
}