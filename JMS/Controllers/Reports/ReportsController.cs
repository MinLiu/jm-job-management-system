﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class ReportsController : BaseController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        public ActionResult Departments()
        {
            return View();
        }

        public ActionResult Suppliers()
        {
            return View();
        }

        public ActionResult Jobs()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadDepartmentReports([DataSourceRequest]DataSourceRequest request, string filterText, int dateType, string fromD, string toD, bool includeAll)
        {
            //User passed parameters oo filter results
            DateTime fromDate;
            DateTime toDate;
            //Set the Date range for the query
            //When set the 'To Date' set the time to "23:59" by adding a day then removing 1 second. This is because the new date instances times start at midnight.
            //Example: 00:00 31st March -> 00:00 1st April -> 23:59 31st March.
            switch (dateType)
            {
                case 1: //Current Year
                    fromDate = new DateTime(DateTime.Today.Year, 1, 1);
                    toDate = new DateTime(DateTime.Today.Year, 12, 31);
                    break;
                case 2: //Last Year
                    fromDate = new DateTime(DateTime.Today.Year -1, 1, 1);
                    toDate = new DateTime(DateTime.Today.Year -1, 12, 31);
                    break;
                case 3: //Custom Date
                    fromDate = DateTime.Parse(fromD);
                    toDate = DateTime.Parse(toD).AddDays(1).AddSeconds(-1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            using (var db = new SnapDbContext())
            {
                var list = new DepartmentRepository().Read()
                                                     .Where(x => filterText.Equals("") || filterText.Equals(null) || x.Code.ToLower().Contains(filterText.ToLower())|| x.Name.ToLower().Contains(filterText.ToLower()))
                                                     .Where(x => includeAll || x.DepartmentPurchases.Any(p => p.Purchase.PurchaseStatusID == PurchaseStatusValues.Ordered && p.Purchase.DateOrdered >= fromDate && p.Purchase.DateOrdered <= toDate))
                                                     .Include(x => x.DepartmentPurchases)
                                                     .OrderBy(x => x.Code)
                                                     .Select(x => new ReportDepartmentGridViewModel()
                                                     {
                                                         DepartmentID = x.ID.ToString(),
                                                         DepartmentCode = x.Code,
                                                         DepartmentName = x.Name,
                                                         TotalCost = x.DepartmentPurchases.Any(p => p.Purchase.PurchaseStatusID == PurchaseStatusValues.Ordered 
                                                                                                 && p.Purchase.DateOrdered >= fromDate 
                                                                                                 && p.Purchase.DateOrdered <= toDate) ? x.DepartmentPurchases.Where(p => p.Purchase.PurchaseStatusID == PurchaseStatusValues.Ordered 
                                                                                                                                                                      && p.Purchase.DateOrdered >= fromDate 
                                                                                                                                                                      && p.Purchase.DateOrdered <= toDate)
                                                                                                                                                              .Sum(p => p.Purchase.PurchaseTotal) : 0
                                                     });

                //Sorting
                if (request.Sorts.Any())
                {
                    foreach (var sortItem in request.Sorts)
                    {
                        if (sortItem.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        {
                            switch (sortItem.Member)
                            {
                                case "DepartmentCode":
                                    list = list.OrderBy(x => x.DepartmentCode);
                                    break;
                                case "DepartmentName":
                                    list = list.OrderBy(x => x.DepartmentName);
                                    break;
                                case "TotalCost":
                                    list = list.OrderBy(x => x.TotalCost);
                                    break;
                                default:
                                    list = list.OrderBy(x => x.DepartmentCode);
                                    break;
                            }
                        }
                        else if (sortItem.SortDirection == System.ComponentModel.ListSortDirection.Descending)
                        {
                            switch (sortItem.Member)
                            {
                                case "DepartmentCode":
                                    list = list.OrderByDescending(x => x.DepartmentCode);
                                    break;
                                case "DepartmentName":
                                    list = list.OrderByDescending(x => x.DepartmentName);
                                    break;
                                case "TotalCost":
                                    list = list.OrderByDescending(x => x.TotalCost);
                                    break;
                                default:
                                    list = list.OrderByDescending(x => x.DepartmentCode);
                                    break;
                            }
                        }
                    }
                }

                var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
                DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
                return Json(result);
            }

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadSupplierReports([DataSourceRequest]DataSourceRequest request, string filterText, int dateType, string fromD, string toD, bool includeAll)
        {
            //User passed parameters oo filter results
            DateTime fromDate;
            DateTime toDate;
            //Set the Date range for the query
            //When set the 'To Date' set the time to "23:59" by adding a day then removing 1 second. This is because the new date instances times start at midnight.
            //Example: 00:00 31st March -> 00:00 1st April -> 23:59 31st March.
            switch (dateType)
            {
                case 1: //Current Year
                    fromDate = new DateTime(DateTime.Today.Year, 1, 1);
                    toDate = new DateTime(DateTime.Today.Year, 12, 31);
                    break;
                case 2: //Last Year
                    fromDate = new DateTime(DateTime.Today.Year - 1, 1, 1);
                    toDate = new DateTime(DateTime.Today.Year - 1, 12, 31);
                    break;
                case 3: //Custom Date
                    fromDate = DateTime.Parse(fromD);
                    toDate = DateTime.Parse(toD).AddDays(1).AddSeconds(-1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            using (var db = new SnapDbContext())
            {
                var list = new SupplierRepository().Read()
                                                    .Where(x => filterText.Equals("") || filterText.Equals(null) || x.Name.ToLower().Contains(filterText.ToLower()) || (x.SupplierType != null && x.SupplierType.Name.ToLower().Contains(filterText.ToLower())) || x.AccountDetail.ToLower().Contains(filterText.ToLower()))
                                                    .Where(x => includeAll || x.Purchases.Any(p => p.PurchaseStatusID == PurchaseStatusValues.Ordered && p.DateOrdered >= fromDate && p.DateOrdered <= toDate))
                                                    .Include(x => x.Purchases)
                                                    .OrderBy(x => x.Name)
                                                    .Select(x => new ReportSupplierGridViewModel()
                                                    {
                                                        SupplierID = x.ID.ToString(),
                                                        SupplierName = x.Name,
                                                        SupplierType = x.SupplierType != null ? x.SupplierType.Name : "",
                                                        SupplierAccountDetail = x.AccountDetail,
                                                        TotalCost = x.Purchases.Any(p => p.PurchaseStatusID == PurchaseStatusValues.Ordered
                                                                                      && p.DateOrdered >= fromDate
                                                                                      && p.DateOrdered <= toDate) ? x.Purchases.Where(p => p.PurchaseStatusID == PurchaseStatusValues.Ordered
                                                                                                                                        && p.DateOrdered >= fromDate
                                                                                                                                        && p.DateOrdered <= toDate)
                                                                                                                               .Sum(p => p.PurchaseTotal) : 0
                                                    });
                //Sorting
                if (request.Sorts.Any())
                {
                    foreach (var sortItem in request.Sorts)
                    {
                        if (sortItem.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        {
                            switch (sortItem.Member)
                            {
                                case "SupplierName":
                                    list = list.OrderBy(x => x.SupplierName);
                                    break;
                                case "SupplierType":
                                    list = list.OrderBy(x => x.SupplierType);
                                    break;
                                case "SupplierAccountDetail":
                                    list = list.OrderBy(x => x.SupplierAccountDetail);
                                    break;
                                case "TotalCost":
                                    list = list.OrderBy(x => x.TotalCost);
                                    break;
                                default:
                                    list = list.OrderBy(x => x.SupplierName);
                                    break;
                            }
                        }
                        else if (sortItem.SortDirection == System.ComponentModel.ListSortDirection.Descending)
                        {
                            switch (sortItem.Member)
                            {
                                case "SupplierName":
                                    list = list.OrderByDescending(x => x.SupplierName);
                                    break;
                                case "SupplierType":
                                    list = list.OrderByDescending(x => x.SupplierType);
                                    break;
                                case "SupplierAccountDetail":
                                    list = list.OrderByDescending(x => x.SupplierAccountDetail);
                                    break;
                                case "TotalCost":
                                    list = list.OrderByDescending(x => x.TotalCost);
                                    break;
                                default:
                                    list = list.OrderByDescending(x => x.SupplierName);
                                    break;
                            }
                        }
                    }
                }

                var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
                DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
                return Json(result);
            }

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadJobReports([DataSourceRequest]DataSourceRequest request, string filterText, string typeFilterText, int dateType, string fromD, string toD, bool includeAll)
        {
            //User passed parameters oo filter results
            DateTime fromDate;
            DateTime toDate;
            //Set the Date range for the query
            //When set the 'To Date' set the time to "23:59" by adding a day then removing 1 second. This is because the new date instances times start at midnight.
            //Example: 00:00 31st March -> 00:00 1st April -> 23:59 31st March.
            switch (dateType)
            {
                case 1: //Current Year
                    fromDate = new DateTime(DateTime.Today.Year, 1, 1);
                    toDate = new DateTime(DateTime.Today.Year, 12, 31);
                    break;
                case 2: //Last Year
                    fromDate = new DateTime(DateTime.Today.Year - 1, 1, 1);
                    toDate = new DateTime(DateTime.Today.Year - 1, 12, 31);
                    break;
                case 3: //Custom Date
                    fromDate = DateTime.Parse(fromD);
                    toDate = DateTime.Parse(toD).AddDays(1).AddSeconds(-1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }
            using (var db = new SnapDbContext())
            {
                var list = new JobRepository().Read()
                                               .Where(x => filterText.Equals("") || filterText.Equals(null) || x.Title.ToLower().Contains(filterText.ToLower()))
                                               .Where(x => typeFilterText.Equals("") || typeFilterText.Equals(null) || x.JobType != null && x.JobType.Name.ToLower().Contains(typeFilterText.ToLower()))
                                               .Where(x => includeAll || x.JobPurchases.Any(p => p.PurchaseStatusID == PurchaseStatusValues.Ordered && p.DateOrdered >= fromDate && p.DateOrdered <= toDate))
                                               .Include(x => x.JobPurchases)
                                               .OrderByDescending(x => x.JobID)
                                               .Select(x => new ReportJobGridViewModel()
                                               {
                                                   JobID = x.ID.ToString(),
                                                   JobNo = x.JobID,
                                                   JobTitle = x.Title,
                                                   JobType = x.JobType != null ? x.JobType.Name : "",
                                                   TotalCost = x.JobPurchases.Any(p => p.PurchaseStatusID == PurchaseStatusValues.Ordered
                                                                                    && p.DateOrdered >= fromDate
                                                                                    && p.DateOrdered <= toDate) ? x.JobPurchases.Where(p => p.PurchaseStatusID == PurchaseStatusValues.Ordered
                                                                                                                                         && p.DateOrdered >= fromDate
                                                                                                                                         && p.DateOrdered <= toDate)
                                                                                                                                .Sum(p => p.PurchaseTotal) : 0
                                               });
                //Sorting
                if (request.Sorts.Any())
                {
                    foreach (var sortItem in request.Sorts)
                    {
                        if (sortItem.SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                        {
                            switch (sortItem.Member)
                            {
                                case "JobNo":
                                    list = list.OrderBy(x => x.JobNo);
                                    break;
                                case "JobTitle":
                                    list = list.OrderBy(x => x.JobTitle);
                                    break;
                                case "JobType":
                                    list = list.OrderBy(x => x.JobType);
                                    break;
                                case "TotalCost":
                                    list = list.OrderBy(x => x.TotalCost);
                                    break;
                                default:
                                    list = list.OrderBy(x => x.JobNo);
                                    break;
                            }
                        }
                        else if (sortItem.SortDirection == System.ComponentModel.ListSortDirection.Descending)
                        {
                            switch (sortItem.Member)
                            {
                                case "JobNo":
                                    list = list.OrderByDescending(x => x.JobNo);
                                    break;
                                case "JobTitle":
                                    list = list.OrderByDescending(x => x.JobTitle);
                                    break;
                                case "JobType":
                                    list = list.OrderByDescending(x => x.JobType);
                                    break;
                                case "TotalCost":
                                    list = list.OrderByDescending(x => x.TotalCost);
                                    break;
                                default:
                                    list = list.OrderByDescending(x => x.JobNo);
                                    break;
                            }
                        }
                    }
                }

                var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
                DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
                return Json(result);
            }

        }
    }
    
}