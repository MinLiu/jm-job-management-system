﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class DepartmentsController : EntityController<Department, DepartmentViewModel>
    {
      
        public DepartmentsController()
            : base(new DepartmentRepository(), new DepartmentMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.DepartmentID = ID;

            return View();
        }

        public ActionResult Goto(string ID)
        {
            return RedirectToAction("Index", new { ID = ID });
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }



        #region Edit Department Panel

        // GET: /New Department
        public ActionResult _NewDepartment()
        {
            var department = new DepartmentRepository().Read().OrderByDescending(x => x.RefID).FirstOrDefault();
            var refID = department != null ? department.RefID + 1 : 1;
            return PartialView(new DepartmentViewModel() { RefID = refID });             
        }


        // POST: /New Department
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewDepartment(DepartmentViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);


            //Log Activity
            //AddLog(model.ID.ToString(), string.Format("{{{0}}} created.", 0), CurrentUser);
                
            return Json(new { ID = model.ID });                
           
        }



        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditDepartment(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Department clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                DepartmentViewModel model = _mapper.MapToViewModel(clt);

                ViewBag.CurrentUserID = CurrentUser.Id;
                ViewBag.CurrentUserEmail = CurrentUser.Email;
                return PartialView(model);
            }
            catch {
                return PartialView("_NewSupplier", new DepartmentViewModel());
            }

        }


        // POST: /Edit Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditDepartmentDetails(DepartmentViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);

            //Log Activity
            AddLog(viewModel.ID, string.Format("{{{0}}} details updated.", 0), CurrentUser); 

            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;
            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion

        /*
        public ActionResult _AddDepartmentPopup(string OrderID, string Type)
        {
            ViewBag.OrderID = OrderID;
            ViewBag.Type = Type;
            return PartialView(new DepartmentViewModel { });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddDepartmentPopup(DepartmentViewModel viewModel, string OrderID, string Type)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OrderID = OrderID;
                ViewBag.Type = Type;
                return PartialView(viewModel);
            }

            var supplier = _mapper.MapToModel(viewModel);
            supplier.CompanyID = CurrentUser.CompanyID;
            _repo.Create(supplier);

            if (Type == "PurchaseOrder")
            {
                var oRepo = new PurchaseOrderRepository();
                var model = oRepo.Find(Guid.Parse(OrderID));
                model.SupplierID = supplier.ID;
                model.LastUpdated = DateTime.Now;
                oRepo.Update(model, new string[] { "SupplierID", "LastUpdated" });
            }
           

            //Log Activity
            AddLog(viewModel.ID, string.Format("{{{0}}} details updated.", 0), CurrentUser);


            return JavaScript("OnEditSupplierContactAddress();");
        }


        public ActionResult _EditDepartmentPopup(string OrderID, string Type)
        {
            ViewBag.OrderID = OrderID;
            ViewBag.Type = Type;

            try
            {
                Department supplier = null;

                if (Type == "PurchaseOrder")
                {
                    var oRepo = new PurchaseOrderRepository();
                    var model = oRepo.Find(Guid.Parse(OrderID));
                    supplier = _repo.Find(model.SupplierID);
                }

                var viewModel = _mapper.MapToViewModel(supplier);
                return PartialView(viewModel);
            }
            catch
            {
                return RedirectToAction("_AddSupplierPopup", new { OrderID = OrderID, Type = Type });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditDepartmentPopup(DepartmentViewModel viewModel, string OrderID, string Type)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OrderID = OrderID;
                ViewBag.Type = Type;
                return PartialView(viewModel);
            }

            var customer = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, customer);
            _repo.Update(customer);

            //Log Activity
            AddLog(OrderID, string.Format("{{{0}}} supplier details updated.", 0), CurrentUser);


            return JavaScript("OnEditSupplierContactAddress();");
        }

        */

        public JsonResult DropDownSuppliers(string text, string supID, string pdtCode = null, string category = null, bool onlyAssociated = false)
        {
            if (text == null) text = "";

            var list = _repo.Read()
                            .Where(s => s.Deleted == false                                
                                && (s.Name.ToLower().Contains(text) || text == "" || s.ID.ToString() == supID)
                                && ((string.IsNullOrEmpty(pdtCode) || onlyAssociated == false)
                                )
                            )
                            .OrderByDescending(t => t.ID.ToString() == supID)
                            .ThenBy(c => c.Name)
                            .Take(500)
                            .ToArray()
                            .Select(t => _mapper.MapToViewModel(t)
            ).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult DropDownSuppliers_Virtualised([DataSourceRequest] DataSourceRequest request)
        {
            var suppliers = GetSuppliers();//.Where(s => s.SupplierProducts.Select(p=> p.ID.ToString() == pdtID).Count() > 0 || (pdtID == null || pdtID == "" || onlyAssociated == false));

            // Apply filtering.
            if (request.Filters.Count > 0)
            {
                // For simplicity, assume that only one filter will be specified at a time and that
                // the filter type will be "contains".
                var filter = request.Filters[0].GetAllFilterDescriptors().First().Value.ToString();
                if (!String.IsNullOrEmpty(filter))
                {
                    suppliers = suppliers.Where(c => c.Name.ToLower().Contains(filter.ToLower()));
                }
            }

            // Store the total record count post filtering but pre paging.
            var supplierCount = suppliers.Count();

            // Apply paging.
            suppliers = suppliers.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
            
            // Convert to view models.
            var supplierVMs = suppliers.ToList().Select(c => _mapper.MapToViewModel(c)).ToList();

            return Json(new DataSourceResult { Data = supplierVMs, Total = supplierCount });
        }

        public ActionResult DropDownSuppliers_ValueMapper(string[] values)
        {
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;
                var suppliers = GetSuppliers().ToList();

                foreach (var supplier in suppliers)
                {
                    if (values.Contains(supplier.ID.ToString()))
                    {
                        indices.Add(index);
                    }

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<Department> GetSuppliers()
        {
            var suppliers = _repo.Read()
                               .Where(c => c.Deleted == false)
                               .OrderBy(c => c.Name)
                               .AsQueryable();

            return suppliers;
        }


        public static bool AddLog(string ID, string Message, User CurrentUser)
        {
            try
            {
                var model = new SupplierRepository().Read().Where(p => p.ID.ToString().Equals(ID))
                    .First();

                var title = string.Format("Supplier '{0}'", model.Name ?? "");

            }
            catch { }
            return true;
        }

    }
    
}