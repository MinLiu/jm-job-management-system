﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class DepartmentGridController : GridController<Department, DepartmentViewModel>
    {

        public DepartmentGridController()
            : base(new DepartmentRepository(), new DepartmentMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            //if (ViewBag.ViewCRM == false)
            //   Response.Redirect("/");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {
            //User passed parameters oo filter results
            var departments = _repo.Read().Where(
                         x => 
                         (
                             (x.Name.ToLower().Contains(filterText.ToLower())
                               || x.Code.ToLower().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null)
                         )
                         //&& (cls.SupplierProducts.Where(t => t.SupplierTag.ID.ToString() == tag).Count() > 0 || tag == "" || tag == null)
                         && x.Deleted == false  //Make sure only non-deleted suppliers are returned
                     );

            departments = ApplyQueryFiltersSort(request, departments, "RefID");

            var supplierList = departments.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = supplierList.ToList(), Total = departments.Count() };
            return Json(result);

        }


        public virtual JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request)
        {
            var suppliers = _repo.Read().Where(p => p.Deleted == true);
            var viewModels = suppliers.OrderBy(c => c.Name).ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Delete([DataSourceRequest] DataSourceRequest request, ProductViewModel viewModel)
        {
            
            var model = _repo.Find(Guid.Parse(viewModel.ID));
            model.Deleted = true;
            _repo.Update(model, new string[] { "Deleted" });

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

    }
    
}