﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize]
    public class SupplierTypesController : GridController<SupplierType, SupplierTypeViewModel>
    {

        public SupplierTypesController()
            : base(new SupplierTypeRepository(), new SupplierTypeMapper())
        {

        }

        public override JsonResult Create(DataSourceRequest request, SupplierTypeViewModel viewModel)
        {
            return base.Create(request, viewModel);
        }
                

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = _repo.Read()
                            .OrderBy(o => o.Name)
                            .ToList()
                            .Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public JsonResult DropDownSupplierTypes()
        {
            var tags = _repo.Read()
                            .OrderBy(t => t.Name).ToArray()
                            .Select(t => _mapper.MapToViewModel(t))
                            .ToList();

            return Json(tags, JsonRequestBehavior.AllowGet);
        }

    }

}