﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using Fruitful.Import;
using System.Text.RegularExpressions;

namespace JMS.Controllers
{
    [Authorize]
    public class SupplierImportController : BaseController
    {

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.EditCRM == false)
                Response.Redirect("/");
        }

        // GET: ProductImport
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Suppliers");
        }

        public ActionResult UploadSuppliers()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadSuppliers(HttpPostedFileBase upload)
        {
            const string relativePath = "/Imports/";

            if (upload == null)
            {
                ViewBag.ErrorMessage = "No file has been uploaded.";
                return View("UploadSuppliers");
            }

            string extension = Path.GetExtension(upload.FileName);
            if (extension != ".xls")
            {
                ViewBag.ErrorMessage = "Uploaded file is the wrong file format.";
                return View("UploadSuppliers");
            }

            var physicalSavePath = SaveFile(upload, relativePath);

            var data = ImportService.CopyExcelFileToTable(Server.MapPath(physicalSavePath), true);

            string error = "";
            string[] columns = new string[] { "Name", "Address1", "Address2", "Address3", "Town", "Country", "Postcode", "Email", "Telephone", "Mobile", "Website", "AccountReference" };

            if (!ImportService.IsTableValid(data, columns, out error))
            {
                ViewBag.ErrorMessage = error;
                return View("UploadSuppliers");
            }

            List<Supplier> listToAdd = new List<Supplier>();

            using (var context = new SnapDbContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                //Load existing products
                var existing = context.Suppliers.ToList();
                foreach (DataRow row in data.Rows)
                {
                    if (row["Name"].ToString() == String.Empty) continue;

                    var exist = existing.Where(p => p.Name == row["Name"].ToString()).ToList();

                    if (exist.Count > 0)
                    {
                        foreach (var o in exist)
                        {
                            o.Deleted = false;
                            context.Suppliers.Attach(o);
                            context.Entry(o).State = EntityState.Modified;
                        }
                    }
                    else if(listToAdd.Where(s => s.Name == row["Name"].ToString()).Count() <= 0)
                    {
                        var newOrg = new Supplier
                        {
                            Name = row["Name"].ToString(),
                        };

                        //Add clients later in different context to prevent DbUpdateConcurrencyException
                        listToAdd.Add(newOrg);
                    }
                }
                context.SaveChanges();

            }


            //Add clients in a different context to prevent DbUpdateConcurrencyException
            using (var context = new SnapDbContext())
            {
                context.Suppliers.AddRange(listToAdd);
                context.SaveChanges();
            }


            ViewBag.SuccessMessage = string.Format("{0} Suppliers have been successfully imported.", data.Rows.Count);
            return View("UploadSuppliers");

        }

    }
}