﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class SupplierGridController : GridController<Supplier, SupplierViewModel>
    {

        public SupplierGridController()
            : base(new SupplierRepository(), new SupplierMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            //if (ViewBag.ViewCRM == false)
            //   Response.Redirect("/");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, string type, string typeFilterText)
        {            
            //User passed parameters oo filter results
            var suppliers = _repo.Read().Where(
                         cls =>
                         (
                             (cls.Name.ToLower().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null)
                         )
                         &&(typeFilterText.Equals("") || typeFilterText.Equals(null) || (cls.SupplierType != null && cls.SupplierType.Name.ToLower().Contains(typeFilterText.ToLower())))
                         //&& (cls.SupplierProducts.Where(t => t.SupplierTag.ID.ToString() == tag).Count() > 0 || tag == "" || tag == null)
                         && (cls.SupplierTypeID.ToString() == type || type == "" || type == null)
                         && cls.Deleted == false  //Make sure only non-deleted suppliers are returned
                     )                     
                     .Include(cls => cls.SupplierType);

            suppliers = ApplyQueryFiltersSort(request, suppliers, "Name");

            var supplierList = suppliers.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = supplierList.ToList(), Total = suppliers.Count() };
            return Json(result);

        }


        public virtual JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request)
        {
            var suppliers = _repo.Read().Where(p => p.Deleted == true);
            var viewModels = suppliers.OrderBy(c => c.Name).ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<SupplierViewModel>().ToDataSourceResult(request));
        }

    }
    
}