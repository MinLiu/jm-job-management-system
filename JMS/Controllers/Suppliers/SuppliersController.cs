﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class SuppliersController : EntityController<Supplier, SupplierViewModel>
    {
      
        public SuppliersController()
            : base(new SupplierRepository(), new SupplierMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.SupplierID = ID;

            return View();
        }

        public ActionResult GoTo(string ID)
        {
            return RedirectToAction("Index", new { ID = ID });
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }

        

        #region Edit Supplier Panel

        // GET: /New Supplier
        public ActionResult _NewSupplier()
        {
            return PartialView(new SupplierViewModel() { AccountDetail = "Account"});             
        }


        // POST: /New Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewSupplier(SupplierViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            return Json(new { ID = model.ID });                
           
        }



        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditSupplier(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Supplier clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                SupplierViewModel model = _mapper.MapToViewModel(clt);

                ViewBag.CurrentUserID = CurrentUser.Id;
                ViewBag.CurrentUserEmail = CurrentUser.Email;
                return PartialView(model);
            }
            catch {
                return PartialView("_NewSupplier", new SupplierViewModel());
            }

        }


        // POST: /Edit Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditSupplierDetails(SupplierViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion

        public ActionResult _ContactsTab(SupplierViewModel model)
        {
            return View(model);
        }

    }
    
}