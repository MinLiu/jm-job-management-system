﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize]
    public class SupplierContactsController : GridController<SupplierContact, SupplierContactViewModel>
    {

        public SupplierContactsController()
            : base(new SupplierContactRepository(), new SupplierContactMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string supID)
        {
            var list = _repo.Read().Where(p => (p.SupplierID.ToString() == supID || supID == null))
                .Include(p => p.Supplier)
                .OrderBy(o => o.Name)
                .ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));           
        }


        public JsonResult ReadSearch([DataSourceRequest]DataSourceRequest request, string filterText)
        {

            var list = _repo.Read().Where(p =>
                    (
                            (p.Name.ToLower().Contains(filterText.ToLower())
                                || p.PositionHeld.ToLower().Contains(filterText.ToLower())
                                || p.Email.ToLower().Contains(filterText.ToLower())
                                || p.Telephone.ToLower().Contains(filterText.ToLower())
                            ) || filterText.Equals("")
                        )
                     && p.Supplier.Deleted == false
                    )
                    .Include(p => p.Supplier)
                    .OrderBy(o => o.Name);


                var conList = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(c => _mapper.MapToViewModel(c));
                DataSourceResult result = new DataSourceResult { Data = conList.ToList(), Total = list.Count() };
                return Json(result);
        }

        public JsonResult DropDownSupplierContacts(string supID)
        {
            var list = _repo.Read().Where(t => t.SupplierID.ToString() == supID)
                .OrderBy(t => t.Name)
                .ToArray()
                .Select(c =>
                    new SupplierContactItemViewModel
                    {
                        ID = c.ID.ToString(),
                        SupplierID = c.SupplierID.ToString(),
                        Name = c.Name
                    }
            ).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }

}