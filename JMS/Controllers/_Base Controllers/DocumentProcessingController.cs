﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JMS.Controllers
{
    public class DocumentProcessingController : Controller
    {

        public byte[] GeneratePdfFromDocx(HttpServerUtilityBase server, string templateFilePath, Dictionary<string, string> replacements, Dictionary<string, string> images)
        {

            byte[] byteArray = null;

            //Load the word documnet template
            using (FileStream stream = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (BinaryReader binaryReader = new BinaryReader(stream))
                {
                    byteArray = binaryReader.ReadBytes((int)stream.Length);
                }
            }

            //Editing the document is done in memory to prevent the tempalte from being changed
            using (MemoryStream ms = new MemoryStream())
            {
                //Convert the word document into a WordprocessingDocument
                ms.Write(byteArray, 0, (int)byteArray.Length);
                DocumentFormat.OpenXml.Packaging.WordprocessingDocument wordprocessingDocument = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(ms, true);
                Body body = wordprocessingDocument.MainDocumentPart.Document.Body;

                //Get all the paragraphs from the body, headers and footers.
                List<DocumentFormat.OpenXml.Wordprocessing.Paragraph> paragraphs = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().ToList();
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    foreach (var dicItem in images)
                    {
                        if (paragraphs[x].InnerText.Contains(dicItem.Key))
                        {
                            try
                            {
                                paragraphs[x].RemoveAllChildren<DocumentFormat.OpenXml.Wordprocessing.Run>();
                                Drawing pic = Miscellaneous.CreateImageElement(dicItem.Value, wordprocessingDocument, server);
                                paragraphs[x].AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run(pic));
                            }
                            catch { }
                        }
                    }
                }


                foreach (var header in wordprocessingDocument.MainDocumentPart.HeaderParts)
                {
                    var headerParas = header.Header.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().ToList();
                    for (int x = headerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in images)
                        {
                            if (headerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    headerParas[x].RemoveAllChildren<DocumentFormat.OpenXml.Wordprocessing.Run>();
                                    Drawing pic = Miscellaneous.CreateHeaderImageElement(dicItem.Value, header, server);
                                    headerParas[x].AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(headerParas);
                }

                foreach (var footer in wordprocessingDocument.MainDocumentPart.FooterParts)
                {
                    var footerParas = footer.Footer.Descendants<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().ToList();
                    for (int x = footerParas.Count - 1; x >= 0; x--)
                    {
                        foreach (var dicItem in images)
                        {
                            if (footerParas[x].InnerText.Contains(dicItem.Key))
                            {
                                try
                                {
                                    footerParas[x].RemoveAllChildren<DocumentFormat.OpenXml.Wordprocessing.Run>();
                                    Drawing pic = Miscellaneous.CreateFooterImageElement(dicItem.Value, footer, server);
                                    footerParas[x].AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run(pic));
                                }
                                catch { }
                            }
                        }
                    }
                    paragraphs.AddRange(footerParas);
                }


                //Second replace paragrph texts, if text is empty then remove the whole paragraph (Prevent empty newlines).
                //Because a paragraph can be made of multiple Runs, which can sometimes split the paragraph text. 
                //We get the copy the paragraph text into a new paragraph with a single run that has the styling properties of the first run in the paragraph.
                //var splitArray = new char[] { '{', '}' };
                RunProperties runPr = null;
                for (int x = paragraphs.Count - 1; x >= 0; x--)
                {
                    foreach (var dicItem in replacements)
                    {
                        if (paragraphs[x].InnerText.Contains(dicItem.Key))
                        {
                            //Get the stying properties of the first run in the paragraph. 
                            //If  it fails to get the style then it should use the previous one found. This fixes the multiple tags in the some paragraph style remove issue. Example: {Tag} {Tag}
                            try { runPr = paragraphs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Run>().First().RunProperties.CloneNode(true) as DocumentFormat.OpenXml.Wordprocessing.RunProperties; }
                            catch { }

                            //Replace the tag in paragraph
                            Miscellaneous.InsertParagraphText2(paragraphs[x], dicItem.Key ?? "", dicItem.Value ?? "");
                        }
                    }
                }

                //Fourth, save document and convert to PDF.
                wordprocessingDocument.Save();
                wordprocessingDocument.Close();

                using (MemoryStream exportMs = new MemoryStream())
                {
                    Spire.Doc.Document document = new Spire.Doc.Document();

                    ms.Position = 0;
                    document.LoadFromStream(ms, Spire.Doc.FileFormat.Docx);

                    Spire.Doc.ToPdfParameterList toPdf = new Spire.Doc.ToPdfParameterList();
                    toPdf.PdfConformanceLevel = Spire.Pdf.PdfConformanceLevel.None;
                    document.JPEGQuality = 100;
                    document.SaveToStream(exportMs, toPdf);
                    return exportMs.ToArray();
                }

            }

        }
    }
}