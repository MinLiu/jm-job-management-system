﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using Kendo.Mvc;
using System.Linq.Expressions;


namespace JMS.Controllers
{
    public class GridController<TModel, TViewModel> : BaseController
        where TModel : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        protected readonly EntityRespository<TModel> _repo;
        protected readonly ModelMapper<TModel, TViewModel> _mapper;

        public GridController(EntityRespository<TModel> repo, ModelMapper<TModel, TViewModel> mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public virtual JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var models = _repo.Read();
            var viewModels = models.ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Create([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Map the view model to the model.
                var model = _mapper.MapToModel(viewModel);

                // Add the new entity to the database.
                _repo.Create(model);

                // Update the ID of the view model to match the ID of the newly-created entity.
                viewModel.ID = model.ID.ToString();
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Update([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Find the existing entity.
                var model = _repo.Find(Guid.Parse(viewModel.ID));

                // Map the view model to the model and update the database.
                _mapper.MapToModel(viewModel, model);
                _repo.Update(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Destroy([DataSourceRequest] DataSourceRequest request, TViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // Only the ID is required to delete a booking so create a dummy booking with the ID to delete.
                var model = new TModel { ID = Guid.Parse(viewModel.ID) };
                _repo.Delete(model);
            }

            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        public IQueryable<TModel> ApplyQueryFiltersSort([DataSourceRequest]DataSourceRequest request, IQueryable<TModel> query, string defaultSortColumn = "ID")
        {
            //Filtering            
            if (request.Filters.Any())
            {
                var filter = request.Filters.GetFilter<TModel>();
                query = query.Where(filter);
            }

            //Sorting
            if (request.Sorts.Any())
            {
                foreach (var x in request.Sorts)
                {
                    query = query.OrderBy(x.Member + ((x.SortDirection == ListSortDirection.Ascending) ? " ASC" : " DESC")) as IQueryable<TModel>;
                }
            }
            else
            {
                query = query.OrderBy(defaultSortColumn);
            }

            return query;
        }

        protected override void Dispose(bool disposing)
        {
            if (_repo != null)
                _repo.Dispose();

            base.Dispose(disposing);
        }


    }


    public static class GridCommandExtensions
    {
        public static Expression<Func<TGridModel, bool>> GetFilter<TGridModel>(this IList<IFilterDescriptor> filterDescriptors)
        {
            var filters = filterDescriptors.SelectMany(GetAllFilterDescriptors).ToArray();
            var parameter = Expression.Parameter(typeof(TGridModel), "c");
            if (filters.Length == 1)
                return Expression.Lambda<Func<TGridModel, bool>>(GetExpression(parameter, filters[0]), parameter);

            Expression exp = null;
            for (int index = 0; index < filters.Length; index += 2)   // условие И
            {
                var filter1 = filters[index];

                if (index == filters.Length - 1)
                {
                    exp = Expression.AndAlso(exp, GetExpression(parameter, filter1));
                    break;
                }
                var filter2 = filters[index + 1];
                var left = GetExpression(parameter, filter1);
                var right = GetExpression(parameter, filter2);
                exp = exp == null
                    ? Expression.AndAlso(left, right)
                    : Expression.AndAlso(exp, Expression.AndAlso(left, right));
            }

            return Expression.Lambda<Func<TGridModel, bool>>(exp, parameter);
        }

        private static Expression GetExpression(ParameterExpression parameter, FilterDescriptor filter)
        {
            var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            var startsWithMethod = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
            var endsWithMethod = typeof(string).GetMethod("EndsWith", new[] { typeof(string) });

            var property = filter.Member.Contains(".") ?
                filter.Member.Split('.').Aggregate((Expression)parameter, Expression.Property)  // (x => x.Property.FieldName)
                : Expression.Property(parameter, filter.Member);                                // (x => x.FieldName)
            var constant = Expression.Constant(filter.Value);               // значение для выражения

            switch (filter.Operator)
            {
                case FilterOperator.IsEqualTo:
                    return Expression.Equal(property, constant);
                case FilterOperator.IsNotEqualTo:
                    return Expression.NotEqual(property, constant);

                case FilterOperator.Contains:
                    return Expression.Call(property, containsMethod, constant);
                case FilterOperator.StartsWith:
                    return Expression.Call(property, startsWithMethod, constant);
                case FilterOperator.EndsWith:
                    return Expression.Call(property, endsWithMethod, constant);

                case FilterOperator.IsGreaterThan:
                    return Expression.GreaterThan(property, constant);
                case FilterOperator.IsGreaterThanOrEqualTo:
                    return Expression.GreaterThanOrEqual(property, constant);
                case FilterOperator.IsLessThan:
                    return Expression.LessThan(property, constant);
                case FilterOperator.IsLessThanOrEqualTo:
                    return Expression.LessThanOrEqual(property, constant);
                default:
                    throw new InvalidOperationException(string.Format("Неподдерживаемая операция {0} для колонки {1}", filter.Operator, filter.Member));
            }
        }

        public static IEnumerable<FilterDescriptor> GetAllFilterDescriptors(this IFilterDescriptor descriptor)
        {
            var filterDescriptor = descriptor as FilterDescriptor;
            if (filterDescriptor != null)
            {
                yield return filterDescriptor;
                yield break;
            }

            var compositeFilterDescriptor = descriptor as CompositeFilterDescriptor;
            if (compositeFilterDescriptor != null)
            {
                if (compositeFilterDescriptor.LogicalOperator == FilterCompositionLogicalOperator.Or)
                    throw new ArgumentOutOfRangeException("descriptor", "В фильтрах не поддерживается OR");

                foreach (var childDescriptor in compositeFilterDescriptor.FilterDescriptors.SelectMany(GetAllFilterDescriptors))
                    yield return childDescriptor;
            }
        }


        public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (!seenKeys.Contains(keySelector(element)))
                {
                    seenKeys.Add(keySelector(element));
                    yield return element;
                }
            }
        }

    }
}