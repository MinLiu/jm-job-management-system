﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using System.Web.Routing;
using Kendo.Mvc.UI;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Data.Entity;
using System;
using System.IO;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using System.Drawing;
using System.Threading.Tasks;

namespace JMS.Controllers
{
    public class BaseController : Controller
    {
        public UserManager UserManager;
        public User CurrentUser;  //This is the current user logged in.
        public UserRoleOption CurrentRoleOption; //This is the current user role.

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            Task.Run(() => new JMS.Schedules.ScheduleStatistics().GenerateData());

            try
            {
                if (!User.Identity.IsAuthenticated) return;

                UserManager = HttpContext.GetOwinContext().GetUserManager<UserManager>();

                using (var context = new SnapDbContext())
                {
                    var userId = User.Identity.GetUserId();
                    CurrentUser = context.Users.Where(u => u.Id == userId).First();
                    var roleId = CurrentUser.Roles.First().RoleId;
                    CurrentRoleOption = context.UserRoleOptions.Where(u => u.IdentityRoleID.ToString() == roleId).First();
                }

                //Check Login Misuse by looking at the cookies
                if (CurrentUser.Token != (Request.Cookies["JMS"] ?? new HttpCookie("JMS")).Value)
                {
                    ControllerContext.HttpContext.GetOwinContext().Authentication.SignOut();
                    Response.Redirect("/Home/LoginMisuse");
                }

                SetSubscriptionViewBag();
                SetRoleViewBag(CurrentRoleOption);
                SetCustomTheme();

                //Used for navbar settings
                int viewCount = 0;
                viewCount += (ViewBag.ViewQuotations) ? 1 : 0;
                viewCount += (ViewBag.ViewJobs) ? 1 : 0;
                viewCount += (ViewBag.ViewDeliveryNotes) ? 1 : 0;
                viewCount += (ViewBag.ViewInvoices) ? 1 : 0;
                viewCount += (ViewBag.ViewPurchaseOrders) ? 1 : 0;
                viewCount += (ViewBag.ViewStockControl) ? 1 : 0;

                ViewBag.ViewCount = viewCount;
                ViewBag.ColSize = (viewCount >= 4) ? 3 : 4;

            }
            catch {
                //Response.Redirect("/");
                ViewBag.ViewCRM = false;
                ViewBag.ViewProducts = false;
                ViewBag.ViewQuotations = false;
                ViewBag.ViewJobs = false;
                ViewBag.ViewInvoices = false;
                ViewBag.ViewDeliveryNotes = false;
                ViewBag.ViewPurchaseOrders = false;
                ViewBag.ViewStockControl = false;

                ViewBag.ColSize = 4;
                ViewBag.ViewCount = 0;
            }

        }


        public void SetSubscriptionViewBag()
        {
            ViewBag.NumberCRM = 9999;
            ViewBag.NumberProducts = 9999;
            ViewBag.NumberQuotations = 9999;
            ViewBag.NumberJobs = 9999;
            ViewBag.NumberInvoices = 9999;
            ViewBag.NumberDeliveryNotes = 9999;
            ViewBag.NumberPurchaseOrders = 9999;
            ViewBag.NumberStockControl = 9999;
        }


        public void SetRoleViewBag(UserRoleOption role)
        {
            ViewBag.ViewQuotations = true;
            ViewBag.EditQuotations = true;
            ViewBag.DeleteQuotations = true;
            ViewBag.ExportQuotations = true;

            ViewBag.ViewJobs = true;
            ViewBag.EditJobs = true;
            ViewBag.DeleteJobs = true;
            ViewBag.ExportJobs = true;

            ViewBag.ViewInvoices = true;
            ViewBag.EditInvoices = true;
            ViewBag.DeleteInvoices = true;
            ViewBag.ExportInvoices = true;

            ViewBag.ViewPurchaseOrders = true;
            ViewBag.EditPurchaseOrders = true;
            ViewBag.DeletePurchaseOrders = true;
            ViewBag.ExportPurchaseOrders = true;

            ViewBag.ViewDeliveryNotes = true;
            ViewBag.EditDeliveryNotes = true;
            ViewBag.DeleteDeliveryNotes = true;
            ViewBag.ExportDeliveryNotes = true;

            ViewBag.ViewStockControl = true;
            ViewBag.EditStockControl = true;
            ViewBag.ExportStockControl = true;

            ViewBag.ViewCRM = true;
            ViewBag.EditCRM = true;
            ViewBag.DeleteCRM = true;
            ViewBag.ExportCRM = true;

            ViewBag.ViewProducts = true;
            ViewBag.EditProducts = true;
            ViewBag.DeleteProducts = true;
            ViewBag.ExportProducts = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
            }

            base.Dispose(disposing);
        }


        public void CheckSubscription() {

        }



        public string SaveFile(HttpPostedFileBase file, string subFolder = "/", string filename = "", string companyID = null, HttpServerUtilityBase Server = null)
        {
            if(file == null) return null;

            if (Server == null) Server = this.Server;

            const string contentFolderRoot = "/Uploads/";
            var virtualPath = string.Format("{0}{1}{2}", contentFolderRoot, companyID ?? "0000", subFolder);
            

            var physicalPath = Server.MapPath(virtualPath);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var extension = Path.GetExtension(file.FileName);
            var str = (string.IsNullOrEmpty(filename)) ? Guid.NewGuid().ToString() + extension : filename + extension;
            var physicalSavePath = Server.MapPath(virtualPath) + str;
            file.SaveAs(physicalSavePath);
            return virtualPath + str; 
        }


        public bool DeleteFile(string virtualpath, HttpServerUtilityBase Server = null)
        {
            if (Server == null) Server = this.Server;

            try
            {
                var physicalPath = Server.MapPath(virtualpath);
                if (System.IO.File.Exists(physicalPath)) System.IO.File.Delete(physicalPath);
                return true;
            }
            catch { return false; }
        }


        public string CopyFile(string sourceVirtualPath, string subFolder = "/", string filename = "", string companyID = null, HttpServerUtilityBase Server = null)
        {
            const string contentFolderRoot = "/Uploads/";

            if (Server == null) Server = this.Server;

            var physicalPath = Server.MapPath(sourceVirtualPath);
            var copyVirtualPath = string.Format("{0}{1}{2}", contentFolderRoot, companyID ?? "0000", subFolder);
            var copyPhysicalPath = Server.MapPath(copyVirtualPath);

            if (!Directory.Exists(copyPhysicalPath))
            {
                Directory.CreateDirectory(copyPhysicalPath);
            }

            var extension = Path.GetExtension(sourceVirtualPath);
            var str = (string.IsNullOrEmpty(filename)) ? Guid.NewGuid().ToString() + extension : filename + extension;
            copyPhysicalPath += str;

            System.IO.File.Copy(physicalPath, copyPhysicalPath, true);

            return copyVirtualPath + str;
        }

        public bool DoesFileExists(string subFolder = "/", string filename = "")
        {
            const string contentFolderRoot = "/Uploads/";            

            var virtualPath = string.Format("{0}{1}{2}{3}", contentFolderRoot, "0000", subFolder, filename);

            return (System.IO.File.Exists(virtualPath));
        }

        //public string GetUploadVirtualFilePath(string subFolder = "/", string filename = "", string companyID = null)
        //{
        //    const string contentFolderRoot = "/Uploads/";
        //    return string.Format("{0}{1}{2}{3}", contentFolderRoot, companyID ?? CurrentUser.CompanyID.ToString(), subFolder, filename);
        //}

        //public string GetUploadPhysicalFilePath(string subFolder = "/", string filename = "", string companyID = null, HttpServerUtilityBase Server = null)
        //{
        //    if (Server == null) Server = this.Server;

        //    return Server.MapPath(GetUploadVirtualFilePath(subFolder, filename, companyID));
        //}

        protected void SetCustomTheme(Company company)
        {
            if (!string.IsNullOrWhiteSpace(company.ThemeColor))
            {
                ViewBag.ThemeColor = company.ThemeColor;
                try
                {
                    Color color = ColorTranslator.FromHtml(company.ThemeColor);
                    try { ViewBag.ThemeColorDark = ColorTranslator.ToHtml(Color.FromArgb(color.A, (int)(color.R * 0.9), (int)(color.G * 0.9), (int)(color.B * 0.9))); } catch { ViewBag.ThemeColorDark = "#000000"; }

                    //Get Perceived Brightness using formula "brightness = sqrt( .241 R2 + .691 G2 + .068 B2 )". ref link:http://alienryderflex.com/hsp.html
                    try { ViewBag.ThemeBrightness = Math.Sqrt(color.R * color.R * .241 + color.G * color.G * .691 + color.B * color.B * .068); } catch { }
                }
                catch { }
            }
            
            ViewBag.ScreenLogoImageURL = company.ScreenLogoImageURL;
        }

        protected void SetCustomTheme()
        {
            //SetCustomTheme(CurrentUser.Company);
        }

        protected void SetNoNavBar()
        {
            if (!User.Identity.IsAuthenticated)
                ViewBag.NoNavBar = true;
        }
    }


    public class MemoryPostedFile : HttpPostedFileBase
    {
        private readonly byte[] fileBytes;

        public MemoryPostedFile(byte[] fileBytes, string fileName = null)
        {
            this.fileBytes = fileBytes;
            this.FileName = fileName;
            this.InputStream = new MemoryStream(fileBytes);
        }

        public override int ContentLength => fileBytes.Length;

        public override string FileName { get; }

        public override Stream InputStream { get; }

        public override void SaveAs(string filename)
        {
            using (FileStream file = new FileStream(filename, FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[InputStream.Length];
                InputStream.Read(bytes, 0, (int)InputStream.Length);
                file.Write(bytes, 0, bytes.Length);
                InputStream.Close();
            }
        }
    }
}