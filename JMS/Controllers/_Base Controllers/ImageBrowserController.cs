﻿using JMS.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace JMS.Controllers
{

    public class ImageBrowserController : EditorImageBrowserController
    {
        public User CurrentUser;  //This is the current user logged in.
        private const string contentFolderRoot = "/Uploads/";
        private const string prettyName = "ImageBrowser/";
        private static readonly string[] foldersToCopy = new[] { "/Uploads/shared/" };


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            try
            {
                if (!User.Identity.IsAuthenticated) return;
                var userId = User.Identity.GetUserId().ToString();
                var userRepo = new UserRepository();
                CurrentUser = userRepo.Read().Where(u => u.Id == userId).First();

            }
            catch { }
        }
        /// <summary>
        /// Gets the base paths from which content will be served.
        /// </summary>
        public override string ContentPath
        {
            get
            {
                return CreateUserFolder();
            }
        }

        private string CreateUserFolder()
        {
            var virtualPath = Path.Combine(contentFolderRoot, "0000", prettyName);

            var path = Server.MapPath(virtualPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                foreach (var sourceFolder in foldersToCopy)
                {
                    CopyFolder(Server.MapPath(sourceFolder), path);
                }
            }
            return virtualPath;
        }

        private void CopyFolder(string source, string destination)
        {
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            foreach (var file in Directory.EnumerateFiles(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(file));
                System.IO.File.Copy(file, dest);
            }

            foreach (var folder in Directory.EnumerateDirectories(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(folder));
                CopyFolder(folder, dest);
            }
        }
    }
}
