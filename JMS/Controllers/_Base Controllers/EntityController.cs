﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;
using System.Linq.Dynamic;
using System.ComponentModel;
using Kendo.Mvc;
using System.Linq.Expressions;


namespace JMS.Controllers
{
    public class EntityController<TModel, TViewModel> : BaseController
        where TModel : class, IEntity, new()
        where TViewModel : class, IEntityViewModel, new()
    {
        protected readonly EntityRespository<TModel> _repo;
        protected readonly ModelMapper<TModel, TViewModel> _mapper;

        public EntityController(EntityRespository<TModel> repo, ModelMapper<TModel, TViewModel> mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        protected override void Dispose(bool disposing)
        {
            if (_repo != null)
                _repo.Dispose();

            base.Dispose(disposing);
        }
    }
}