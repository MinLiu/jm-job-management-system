﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class EngineersController : EntityController<Engineer, EngineerGridViewModel>
    {
      
        public EngineersController()
            : base(new EngineerRepository(), new EngineerGridMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Engineers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.SupplierID = ID;

            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }
    }
    
}