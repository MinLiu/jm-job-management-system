﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class PurchasesController : EntityController<Purchase, PurchaseViewModel>
    {
      
        public PurchasesController()
            : base(new PurchaseRepository(), new PurchaseMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.PurchaseID = ID;

            ViewBag.StatusOptions = new PurchaseStatusRepository().Read().OrderBy(x => x.SortPos).ToList().Select(x => new PurchaseStatusMapper().MapToViewModel(x)).ToList();

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }

        public ActionResult GoTo(string ID)
        {
            return RedirectToAction("Index", new { ID = ID });
        }

        #region Edit Purchase Panel

        // GET: /New Purchase
        public ActionResult _NewPurchase()
        {
            var defaultRequestorID = new RequestorRepository().Read().Where(x => x.FirstName == "Kate" && x.Surname == "Penneck").FirstOrDefault();
            return PartialView(new PurchaseViewModel()
            {
                DateOrdered = DateTime.Today,
                RequestorIDs = new List<string>() { defaultRequestorID?.ID.ToString() },
                QtyOrdered = 1
            });       
        }


        // POST: /New Purchase
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewPurchase(PurchaseViewModel viewModel, string submit)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            viewModel.ID = model.ID.ToString();

            UpdateRequestors(viewModel);
            UpdateOnBehalfOf(viewModel);
            UpdateDepartments(viewModel);

            if (submit == "Add & Create New")
            {
                viewModel.ID = null;
                viewModel.Title = "";
                viewModel.UnitPrice = 0;
                viewModel.QtyOrdered = 0;
                viewModel.PurchaseTotal = 0;
                viewModel.QtyReceived = 0;
                return PartialView(viewModel);
            }

            return Json(new { ID = model.ID });                
           
        }

        // POST: /CreateClone Purchase
        [HttpPost]
        public ActionResult _CreateClone(Guid id)
        {
            var viewModel = _mapper.MapToViewModel(_repo.Find(id));

            viewModel.ID = null;
            viewModel.Title = "";
            viewModel.UnitPrice = 0;
            viewModel.QtyOrdered = 0;
            viewModel.PurchaseTotal = 0;
            viewModel.QtyReceived = 0;

            return PartialView("_NewPUrchase", viewModel);
        }

        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditPurchase(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Purchase clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                PurchaseViewModel model = _mapper.MapToViewModel(clt);

                return PartialView(model);
            }
            catch {
                return PartialView("_NewPurchase", new PurchaseViewModel());
            }

        }


        // POST: /Edit Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditPurchaseDetails(PurchaseViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);

            UpdateRequestors(viewModel);
            UpdateOnBehalfOf(viewModel);
            UpdateDepartments(viewModel);

            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion


        private void UpdateRequestors(PurchaseViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.RequestorPurchases)
                             .FirstOrDefault();

            var oldList = model != null ? model.RequestorPurchases.Select(x => x.RequestorID.ToString()) : new List<string>();
            var newList = viewModel.RequestorIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new RequestorPurchaseRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.RequestorID.ToString() == id && x.PurchaseID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new RequestorPurchase() { RequestorID = Guid.Parse(id), PurchaseID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateOnBehalfOf(PurchaseViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.OnBehalfOfPurchases)
                             .FirstOrDefault();

            var oldList = model != null ? model.OnBehalfOfPurchases.Select(x => x.RequestorID.ToString()) : new List<string>();
            var newList = viewModel.OnBehalfOfIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new OnBehalfOfPurchaseRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.RequestorID.ToString() == id && x.PurchaseID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new OnBehalfOfPurchase() { RequestorID = Guid.Parse(id), PurchaseID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateDepartments(PurchaseViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.DepartmentPurchases)
                             .FirstOrDefault();

            var oldList = model != null ? model.DepartmentPurchases.Select(x => x.DepartmentID.ToString()) : new List<string>();
            var newList = viewModel.DepartmentIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new DepartmentPurchaseRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.DepartmentID.ToString() == id && x.PurchaseID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new DepartmentPurchase() { DepartmentID = Guid.Parse(id), PurchaseID = Guid.Parse(viewModel.ID) });
            }
        }
    }
    
}