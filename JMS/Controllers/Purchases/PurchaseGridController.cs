﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class PurchaseGridController : GridController<Purchase, PurchaseGridViewModel>
    {

        public PurchaseGridController()
            : base(new PurchaseRepository(), new PurchaseGridMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            //if (ViewBag.ViewCRM == false)
            //   Response.Redirect("/");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, string jobID, string supplierID, string orderOnBefalfID, string departmentID, List<int> statuses = null, List<Guid?> relatedJobIDs = null, List<Guid?> relatedSupplierIDs = null, List<Guid> relatedDepartmentIDs = null, List<Guid> relatedOnBehalfOfIDs = null, List<Guid?> relatedMaterialTypeIDs = null, List<Guid?> relatedProjectCodeIDs = null, int dateType = 0, string fromD = "", string toD = "", bool ShowOnlyYetToReceive = false)
        {
            //User passed parameters oo filter results

            DateTime fromDate;
            DateTime toDate;
            statuses = statuses ?? new List<int>();
            relatedJobIDs = relatedJobIDs ?? new List<Guid?>();
            relatedSupplierIDs = relatedSupplierIDs ?? new List<Guid?>();
            relatedDepartmentIDs = relatedDepartmentIDs ?? new List<Guid>();
            relatedOnBehalfOfIDs = relatedOnBehalfOfIDs ?? new List<Guid>();
            relatedMaterialTypeIDs = relatedMaterialTypeIDs ?? new List<Guid?>();
            relatedProjectCodeIDs = relatedProjectCodeIDs ?? new List<Guid?>();

            //Set the Date range for the query
            //When set the 'To Date' set the time to "23:59" by adding a day then removing 1 second. This is because the new date instances times start at midnight.
            //Example: 00:00 31st March -> 00:00 1st April -> 23:59 31st March.
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.AddMonths(1).AddSeconds(-1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.AddMonths(1).AddSeconds(-1);
                    break;
                case 3: //Custom Date
                    fromDate = DateTime.Parse(fromD);
                    toDate = DateTime.Parse(toD).AddDays(1).AddSeconds(-1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            var list = _repo.Read().Where(x =>
                                    (
                                        (x.Title.ToLower().Contains(filterText.ToLower())
                                        || x.PONo.ToLower().Contains(filterText.ToLower())
                                        || x.PartNo.ToLower().Contains(filterText.ToLower())
                                        || x.SupplierRef.ToLower().Contains(filterText.ToLower())
                                        ) || filterText.Equals("") || filterText.Equals(null)
                                    ))
                                    .Where(x => dateType == 0 || (x.DateOrdered >= fromDate && x.DateOrdered <= toDate))
                                    .Where(x => jobID.Equals("") || jobID.Equals(null) || x.RelatedJobID.ToString() == jobID)
                                    .Where(x => orderOnBefalfID.Equals("") || orderOnBefalfID.Equals(null) || x.OnBehalfOfPurchases.Select(r => r.RequestorID.ToString()).Contains(orderOnBefalfID))
                                    .Where(x => departmentID.Equals("") || departmentID.Equals(null) || x.DepartmentPurchases.Select(d => d.DepartmentID.ToString()).Contains(departmentID))
                                    .Where(x => supplierID.Equals("") || supplierID.Equals(null) || x.SupplierID.ToString() == supplierID)
                                    .Where(x => !relatedJobIDs.Any() || relatedJobIDs.Contains(x.RelatedJobID))
                                    .Where(x => !relatedSupplierIDs.Any() || relatedSupplierIDs.Contains(x.SupplierID))
                                    .Where(x => !relatedDepartmentIDs.Any() || relatedDepartmentIDs.Any(id => x.DepartmentPurchases.Select(d => d.DepartmentID).Contains(id)))
                                    .Where(x => !relatedOnBehalfOfIDs.Any() || relatedOnBehalfOfIDs.Any(id => x.OnBehalfOfPurchases.Select(d => d.RequestorID).Contains(id)))
                                    .Where(x => !relatedMaterialTypeIDs.Any() || relatedMaterialTypeIDs.Contains(x.MaterialTypeID))
                                    .Where(x => !relatedProjectCodeIDs.Any() || relatedProjectCodeIDs.Contains(x.ProjectCodeID))
                                    .Where(x => statuses.Contains(x.PurchaseStatusID))
                                    .Where(x => !ShowOnlyYetToReceive || (x.QtyOrdered != x.QtyReceived));

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "SupplierID") request.Sorts[x].Member = "Supplier.Name";
                if (request.Sorts[x].Member == "MaterialTypeID") request.Sorts[x].Member = "MaterialType.Name";
            }

            list = ApplyQueryFiltersSort(request, list, "DateOrdered desc");
            var purchaseTotal = list.Any() ? list.Sum(x => x.PurchaseTotal) : 0;

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            PurchaseDataSourceResult result = new PurchaseDataSourceResult { Data = results.ToList(), Total = list.Count(), PurchaseTotal = purchaseTotal };
            return Json(result);

        }

    }
    
}