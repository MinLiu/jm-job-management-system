﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class ManufacturerGridController : GridController<Manufacturer, ManufacturerViewModel>
    {

        public ManufacturerGridController()
            : base(new ManufacturerRepository(), new ManufacturerMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            //if (ViewBag.ViewCRM == false)
            //   Response.Redirect("/");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {
            //User passed parameters oo filter results
            var list = _repo.Read().Where(
                         cls => 
                         (
                             (cls.Name.ToLower().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null)
                         )
                         //&& (cls.SupplierProducts.Where(t => t.SupplierTag.ID.ToString() == tag).Count() > 0 || tag == "" || tag == null)
                         && cls.Deleted == false  //Make sure only non-deleted suppliers are returned
                     );

            list = ApplyQueryFiltersSort(request, list, "RefID");

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
            return Json(result);

        }


        public virtual JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request)
        {
            var list = _repo.Read().Where(p => p.Deleted == true);
            var viewModels = list.OrderBy(c => c.Name).ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }


        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(new List<SupplierViewModel>().ToDataSourceResult(request));
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult Delete([DataSourceRequest] DataSourceRequest request, ProductViewModel viewModel)
        {
            
            var model = _repo.Find(Guid.Parse(viewModel.ID));
            model.Deleted = true;
            _repo.Update(model, new string[] { "Deleted" });

           
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }


        // POST: /Delete Suppliers
        [HttpPost]
        public ActionResult _DeleteManufacturers(string IDs)
        {
            //IDs are passed as a string e.g.  1,2,4,5,6,7,8            
            foreach (string i in IDs.Split(',').ToArray())
            {
                try
                {
                    var model = _repo.Find(Guid.Parse(i));
                    model.Deleted = true;
                    _repo.Update(model, new string[] { "Deleted" });

                }
                catch { }
            }
            return Json("Success");
        }

    }
    
}