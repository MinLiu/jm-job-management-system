﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class ManufacturersController : EntityController<Manufacturer, ManufacturerViewModel>
    {
      
        public ManufacturersController()
            : base(new ManufacturerRepository(), new ManufacturerMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.SupplierID = ID;

            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }



        #region Edit Manufacturer Panel

        // GET: /New Manufacturer
        public ActionResult _NewManufacturer()
        {
            var manufacturer = new ManufacturerRepository().Read().OrderByDescending(x => x.RefID).FirstOrDefault();
            var refID = manufacturer != null ? manufacturer.RefID + 1 : 1;

            return PartialView(new ManufacturerViewModel() { RefID = refID });             
        }


        // POST: /New Manufacturer
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewManufacturer(ManufacturerViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);


            //Log Activity
            //AddLog(model.ID.ToString(), string.Format("{{{0}}} created.", 0), CurrentUser);
                
            return Json(new { ID = model.ID });                
           
        }



        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditManufacturer(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Manufacturer clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                ManufacturerViewModel model = _mapper.MapToViewModel(clt);

                ViewBag.CurrentUserID = CurrentUser.Id;
                ViewBag.CurrentUserEmail = CurrentUser.Email;
                return PartialView(model);
            }
            catch {
                return PartialView("_NewSupplier", new ManufacturerViewModel());
            }

        }


        // POST: /Edit Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditManufacturerDetails(ManufacturerViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);


            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;
            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion

        /*
        public ActionResult _AddManufacturerPopup(string OrderID, string Type)
        {
            ViewBag.OrderID = OrderID;
            ViewBag.Type = Type;
            return PartialView(new ManufacturerViewModel { });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _AddManufacturerPopup(ManufacturerViewModel viewModel, string OrderID, string Type)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OrderID = OrderID;
                ViewBag.Type = Type;
                return PartialView(viewModel);
            }

            var supplier = _mapper.MapToModel(viewModel);
            supplier.CompanyID = CurrentUser.CompanyID;
            _repo.Create(supplier);

            if (Type == "PurchaseOrder")
            {
                var oRepo = new PurchaseOrderRepository();
                var model = oRepo.Find(Guid.Parse(OrderID));
                model.SupplierID = supplier.ID;
                model.LastUpdated = DateTime.Now;
                oRepo.Update(model, new string[] { "SupplierID", "LastUpdated" });
            }
           

            //Log Activity
            AddLog(viewModel.ID, string.Format("{{{0}}} details updated.", 0), CurrentUser);


            return JavaScript("OnEditSupplierContactAddress();");
        }


        public ActionResult _EditManufacturerPopup(string OrderID, string Type)
        {
            ViewBag.OrderID = OrderID;
            ViewBag.Type = Type;

            try
            {
                Manufacturer supplier = null;

                if (Type == "PurchaseOrder")
                {
                    var oRepo = new PurchaseOrderRepository();
                    var model = oRepo.Find(Guid.Parse(OrderID));
                    supplier = _repo.Find(model.SupplierID);
                }

                var viewModel = _mapper.MapToViewModel(supplier);
                return PartialView(viewModel);
            }
            catch
            {
                return RedirectToAction("_AddSupplierPopup", new { OrderID = OrderID, Type = Type });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditManufacturerPopup(ManufacturerViewModel viewModel, string OrderID, string Type)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OrderID = OrderID;
                ViewBag.Type = Type;
                return PartialView(viewModel);
            }

            var customer = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, customer);
            _repo.Update(customer);

            //Log Activity
            AddLog(OrderID, string.Format("{{{0}}} supplier details updated.", 0), CurrentUser);


            return JavaScript("OnEditSupplierContactAddress();");
        }

        */

    }
    
}