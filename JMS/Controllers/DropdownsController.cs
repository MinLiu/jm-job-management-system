﻿using JMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JMS.Controllers
{
    public class DropdownsController : BaseController
    {
      
        public JsonResult DropDownUsers()
        {
            //Get the teams where the user is in     
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersWithNotAssigned()
        {
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();

            users.Add(new UserItemViewModel { ID = "-1", Email = "Not Assigned" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownEventUsers()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)
                                    .Where(u => u.Id != CurrentUser.Id)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersSearch()
        {
            //Get the teams where the user is in                
            var users = SnapDbContext.GetCompanyUsers(CurrentUser)                                    
                                    .Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    })
                                    .GroupBy(p => p.ID)
                                    .Select(g => g.First())
                                    .ToList();


            //users.Insert(0, new UserItemViewModel { ID = "", Email = "All Users" });
            //users.Insert(0, new UserItemViewModel { ID = CurrentUser.Id, Email = CurrentUser.Email });
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DropDownUsersAll()
        {
            //Get the teams where the user is in  
            var userRepo = new UserRepository();

            var users = userRepo.Read().Select(
                                    u => new UserItemViewModel
                                    {
                                        ID = u.Id,
                                        Email = u.Email
                                    }).ToList();

            users.Insert(0, new UserItemViewModel { ID = "", Email = "" });
            return Json(users, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DropDownCurrencies()
        {
            using (var context = new SnapDbContext())
            {
                var currencies = context.Currencies.Where(c => c.UseInCommerce == true)
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Code
                    }).ToList();
                return Json(currencies, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult DropDownModuleTypes()
        {
            using (var context = new SnapDbContext())
            {
                
                bool accessProducts = ViewBag.ViewProducts;
                bool accessCRM = ViewBag.ViewCRM;
                bool accessQuotations = ViewBag.ViewQuotations;
                bool accessJobs = ViewBag.ViewJobs;
                bool accessInvoices = ViewBag.ViewInvoices;
                bool accessDeliveryNotes = ViewBag.ViewDeliveryNotes;
                bool accessPurchaseOrders = ViewBag.ViewPurchaseOrders;

                var items = context.ModuleTypes
                    .Where(m => 
                        (m.ID == ModuleTypeValues.PRODUCTS && accessProducts)
                        || (m.ID == ModuleTypeValues.CLIENTS && accessCRM)
                        || (m.ID == ModuleTypeValues.SUPPLIERS && accessPurchaseOrders)
                        || (m.ID == ModuleTypeValues.QUOTATIONS && accessQuotations)
                        || (m.ID == ModuleTypeValues.JOBS && accessJobs)
                        || (m.ID == ModuleTypeValues.DELIVERY_NOTES && accessDeliveryNotes)
                        || (m.ID == ModuleTypeValues.INVOICES && accessInvoices)
                        || (m.ID == ModuleTypeValues.PURCHASE_ORDERS && accessPurchaseOrders)
                    )
                    .Select(
                    c => new SelectItemViewModel
                    {
                        ID = c.ID.ToString(),
                        Name = c.Name
                    }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownLocations()
        {
            using (var context = new SnapDbContext())
            {
                var currencies = context.Locations.Where(x => x.Deleted == false)
                    .OrderBy(x => x.Name)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(currencies, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownProjectCodes()
        {
            using (var context = new SnapDbContext())
            {
                var currencies = context.ProjectCodes.Where(x => x.Deleted == false)
                    .OrderBy(x => x.Code)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Code
                    }).ToList();
                return Json(currencies, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownEquipStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var statuses = context.EquipStatuses
                                      .OrderBy(x => x.SortPos)
                                      .Select(x =>
                                      new SelectItemViewModel()
                                      {
                                          ID = x.ID.ToString(),
                                          Name = x.Name
                                      })
                                      .ToList();
                return Json(statuses, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownPurchaseStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var statuses = context.PurchaseStatuses
                                      .OrderBy(x => x.SortPos)
                                      .Select(x =>
                                      new SelectItemViewModel()
                                      {
                                          ID = x.ID.ToString(),
                                          Name = x.Name
                                      })
                                      .ToList();
                return Json(statuses, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownManufacturers()
        {
            using (var context = new SnapDbContext())
            {
                var manufacturers = context.Manufacturers.Where(x => x.Deleted == false)
                    .OrderBy(x => x.Name)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(manufacturers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownSuppliers()
        {
            using (var context = new SnapDbContext())
            {
                var manufacturers = context.Suppliers.Where(x => x.Deleted == false)
                    .OrderBy(x => x.Name)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(manufacturers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownDepartments()
        {
            using (var context = new SnapDbContext())
            {
                var departments = context.Departments.Where(x => x.Deleted == false)
                    .OrderBy(x => x.Code)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Code + ": " + x.Name
                    }).ToList();
                return Json(departments, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownMaterialTypes()
        {
            using (var context = new SnapDbContext())
            {
                var materialTypes = context.MaterialTypes
                    .OrderBy(x => x.Name)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(materialTypes, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobTypes()
        {
            using (var context = new SnapDbContext())
            {
                var jobTypes = context.JobTypes
                    .OrderBy(x => x.Name)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(jobTypes, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobUrgencyLevels()
        {
            using (var context = new SnapDbContext())
            {
                var jobUrgencyLevels = context.JobUrgencyLevels
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(jobUrgencyLevels, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownEngineers()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.Engineers
                    .OrderBy(x => x.Name)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownRequestors()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.Requestors
                    .OrderBy(x => x.FirstName)
                    .ToList()
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobs()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.Jobs
                    .OrderBy(x => x.JobID)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.JobID + ": " + x.Title
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownPaymentMethods()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.PaymentMethods
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownEquipments()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.Equipment
                    .OrderBy(x => x.EquipNo)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.EquipNo
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobDesignStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.JobDesignStatuses
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobElectricalStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.JobElectricalStatuses
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobSoftwareStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.JobSoftwareStatuses
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobWorkshopStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.JobWorkshopStatuses
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownJobStatuses()
        {
            using (var context = new SnapDbContext())
            {
                var engineers = context.JobStatuses
                    .OrderBy(x => x.SortPos)
                    .Select(
                    x => new SelectItemViewModel
                    {
                        ID = x.ID.ToString(),
                        Name = x.Name
                    }).ToList();
                return Json(engineers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DropDownRoles()
        {
            using (var context = new SnapDbContext())
            {
                var roles = context.Roles
                               .Where(x => x.Name != "Super Admin")
                               .OrderBy(x => x.Name)
                               .Select(x => new SelectItemViewModel
                               {
                                   ID = x.Id,
                                   Name = x.Name
                               }).ToList();

                return Json(roles, JsonRequestBehavior.AllowGet);
            }
        }
    }



}