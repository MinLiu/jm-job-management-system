﻿using JMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;

namespace JMS.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard");

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
                        
            //return View();
        }


        public ActionResult Dashboard()
        {           
            return View();
        }

        public ActionResult _StockLogs()
        {
            return PartialView();
        }

        public ActionResult _ActivityLogs()
        {
            return PartialView();
        }
        
        public ActionResult SubscriptionInvalid()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult _404()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Oops()
        {
            return View();
        }


        public ActionResult LoginMisuse()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            return View();
        }

        public ActionResult Inactive()
        {
            return View();
        }

        public ActionResult ImportRequestors()
        {
            var listToAdd = new List<Requestor>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Requestors.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach(System.Data.DataRow row in data.Rows)
            {
                listToAdd.Add(new Requestor()
                {
                    ID = Guid.NewGuid(),
                    RefID = Int64.Parse(row[1].ToString()),
                    FirstName = row[2].ToString().Trim(),
                    Surname = row[3].ToString().Trim()
                });
            }

            var db = new SnapDbContext();
            db.Requestors.AddRange(listToAdd);
            db.SaveChanges();

            return Json("Success",JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportDepartments()
        {
            var listToAdd = new List<Department>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Departments.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                listToAdd.Add(new Department()
                {
                    ID = Guid.NewGuid(),
                    RefID = Int64.Parse(row[0].ToString()),
                    Name = row[1].ToString(),
                    Code = row[2].ToString(),
                    Contact = row[3].ToString()
                });
            }

            var db = new SnapDbContext();
            db.Departments.AddRange(listToAdd);
            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportEngineers()
        {
            var listToAdd = new List<Engineer>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Engineers.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                listToAdd.Add(new Engineer()
                {
                    ID = Guid.NewGuid(),
                    ShortName = row[0].ToString(),
                    Name = row[1].ToString(),
                    EngineerID = int.Parse(row[2].ToString()),
                    Active = row[3].ToString() == "True"
                });
            }

            var db = new SnapDbContext();
            db.Engineers.AddRange(listToAdd);
            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportManufaturers()
        {
            var listToAdd = new List<Manufacturer>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Manufacturers.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                listToAdd.Add(new Manufacturer()
                {
                    ID = Guid.NewGuid(),
                    RefID = int.Parse(row[0].ToString()),
                    Name = row[1].ToString()
                });
            }

            var db = new SnapDbContext();
            db.Manufacturers.AddRange(listToAdd);
            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportSuppliers()
        {
            var typesToAdd = new List<SupplierType>();
            var suppliersToAdd = new List<Supplier>();
            var contactsToAdd = new List<SupplierContact>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Suppliers.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                // type
                var typeName = row[2].ToString();
                SupplierType type = null;
                if (!string.IsNullOrWhiteSpace(typeName))
                {
                    type = typesToAdd.Where(x => x.Name == typeName).FirstOrDefault();
                    if (type == null)
                    {
                        type = new SupplierType()
                        {
                            ID = Guid.NewGuid(),
                            Name = typeName
                        };
                        typesToAdd.Add(type);
                    }
                }

                // supplier
                var supplierName = row[1].ToString();
                var supplier = suppliersToAdd.Where(x => x.Name == supplierName).FirstOrDefault();
                if (supplier == null)
                {
                    supplier = new Supplier()
                    {
                        ID = Guid.NewGuid(),
                        Name = supplierName,
                        SupplierTypeID = type != null ? type.ID : null as Guid?,
                        AccountDetail = row[3].ToString(),
                        RefID = int.Parse(row[0].ToString()),
                        Deleted = false
                    };
                    suppliersToAdd.Add(supplier);
                }

                // contact
                var contactName = row[4].ToString();
                if (!string.IsNullOrWhiteSpace(contactName))
                {
                    var contact = contactsToAdd.Where(x => x.Name == contactName && x.SupplierID == supplier.ID).FirstOrDefault();
                    if (contact == null)
                    {
                        contact = new SupplierContact()
                        {
                            ID = Guid.NewGuid(),
                            Name = contactName,
                            PositionHeld = row[5].ToString(),
                            Telephone = row[6].ToString(),
                            Email = row[7].ToString(),
                            SupplierID = supplier.ID
                        };
                        contactsToAdd.Add(contact);
                    }
                }
            }

            var db = new SnapDbContext();
            db.SupplierTypes.AddRange(typesToAdd);
            db.Suppliers.AddRange(suppliersToAdd);
            db.SupplierContacts.AddRange(contactsToAdd);
            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportEquipments()
        {
            var db = new SnapDbContext();

            var locationsToAdd = new List<Location>();
            var projectCodesToAdd = new List<ProjectCode>();
            var equipmentsToAdd = new List<Equipment>();
            var equipmentAssetToAdd = new List<EquipmentAsset>();
            var requestorEquipmentsToAdd = new List<RequestorEquipment>();
            var engineerEquipmentsToAdd = new List<EngineerEquipment>();
            var departmentEquipmentsToAdd = new List<DepartmentEquipment>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Equipment.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                var departmentCode = row[0].ToString();
                var refID = int.Parse(row[1].ToString());
                var equipmentNumber = row[2].ToString();
                var assetNo = row[3].ToString();
                var title = row[4].ToString();
                var locationName = row[5].ToString();
                var equipType = row[6].ToString();
                var manufacturerName = row[7].ToString();
                var drawingNo = row[8].ToString();
                var serialNo = row[9].ToString();
                var pmNo = row[10].ToString();
                var purchaseDate = row[11].ToString();
                var projectCodeName = row[12].ToString().Trim();
                var riskAssessmentRef = row[13].ToString();
                var equipStatus = row[14].ToString();
                var pcControlled = row[15].ToString();
                var pcNetworked = row[16].ToString();
                var pcReference = row[17].ToString();
                var softwareAndVersion = row[18].ToString();
                var extractInterlock = row[19].ToString();
                var alarmInterlock = row[20].ToString();
                var visualDate = row[21].ToString();
                var fullDate = row[22].ToString();
                var electricalTestNumber = row[23].ToString();
                var requestorName1 = row[24].ToString();
                var requestorName2 = row[25].ToString();
                var requestorName3 = row[26].ToString();
                var engineerName1 = row[27].ToString();
                var engineerName2 = row[28].ToString();
                var engineerName3 = row[29].ToString();
                var notes = row[30].ToString();

                var manufacturerID = !string.IsNullOrWhiteSpace(manufacturerName) ? db.Manufacturers.Where(x => x.Name == manufacturerName).First().ID : null as Guid?;

                // location
                Location location = null;
                if (!string.IsNullOrWhiteSpace(locationName))
                {
                    location = db.Locations.Where(x => x.Name == locationName).FirstOrDefault();
                    if (location == null)
                    {
                        location = locationsToAdd.Where(x => x.Name == locationName).FirstOrDefault();
                        if (location == null)
                        {
                            location = new Location()
                            {
                                ID = Guid.NewGuid(),
                                Name = locationName
                            };
                            locationsToAdd.Add(location);
                        }
                    }
                }

                // project codes
                ProjectCode projectCode = null;
                if (!string.IsNullOrWhiteSpace(projectCodeName))
                {
                    projectCode = db.ProjectCodes.Where(x => x.Code == projectCodeName).FirstOrDefault();
                    if (projectCode == null)
                    {
                        projectCode = projectCodesToAdd.Where(x => x.Code == projectCodeName).FirstOrDefault();
                        if (projectCode == null)
                        {
                            projectCode = new ProjectCode()
                            {
                                ID = Guid.NewGuid(),
                                Code = projectCodeName
                            };
                            projectCodesToAdd.Add(projectCode);
                        }
                    }
                }

                // equipment

                var equipment = equipmentsToAdd.Where(x => x.EquipNo == equipmentNumber).FirstOrDefault();
                if (equipment == null)
                {
                    equipment = new Equipment()
                    {
                        ID = Guid.NewGuid(),
                        RefID = refID,
                        EquipNo = equipmentNumber,
                        AssetNo = assetNo,
                        Title = title,
                        LocationID = location != null ? location.ID : null as Guid?,
                        EquipType = equipType,
                        ManufacturerID = manufacturerID,
                        DrawingNo = drawingNo,
                        SerialNo = serialNo,
                        PMNo = pmNo,
                        DOMorPurchase = !string.IsNullOrWhiteSpace(purchaseDate) ? DateTime.Parse(purchaseDate) : null as DateTime?,
                        ProjectCodeID = projectCode != null ? projectCode.ID : null as Guid?,
                        RiskAssessmentRef = riskAssessmentRef,
                        EquipStatusID = equipStatus == "Live" ? EquipStatusValues.Live : equipStatus == "Decommissioned" ? EquipStatusValues.Decommissioned : EquipStatusValues.Construction,
                        PCControlled = pcControlled == "True",
                        PCNetworked = pcNetworked == "True",
                        PCReference = pcReference,
                        SoftwareAndVersion = softwareAndVersion,
                        ExtractInterlock = extractInterlock == "True",
                        AlarmInterlock = alarmInterlock == "True",
                        ElectricalVisualInspectionDate = !string.IsNullOrWhiteSpace(visualDate) ? DateTime.Parse(visualDate) : null as DateTime?,
                        ElectricalFullInspectionDate = !string.IsNullOrWhiteSpace(fullDate) ? DateTime.Parse(fullDate) : null as DateTime?,
                        ElectricalTestNumber = electricalTestNumber,
                        Note = notes
                    };
                    equipmentsToAdd.Add(equipment);
                }
                else
                {
                    // Different Asset No
                    if (drawingNo != equipment.DrawingNo || manufacturerID != equipment.ManufacturerID || pmNo != equipment.PMNo || title != equipment.Title || serialNo != equipment.SerialNo)
                    {
                        if (equipmentAssetToAdd.Where(x => x.EquipmentID == equipment.ID && x.DrawingNo == drawingNo && x.SerialNo == serialNo && x.ManufacturerID == manufacturerID && x.PMNo == pmNo && x.Title == title).Count() == 0)
                        {
                            var equipmentAsset = new EquipmentAsset()
                            {
                                ID = Guid.NewGuid(),
                                EquipmentID = equipment.ID,
                                DrawingNo = drawingNo,
                                ManufacturerID = manufacturerID,
                                PMNo = pmNo,
                                Title = title,
                                SerialNo = serialNo
                            };
                            equipmentAssetToAdd.Add(equipmentAsset);
                        }
                    }
                }

                // requestorEquipment
                if (!string.IsNullOrWhiteSpace(requestorName1))
                {
                    requestorName1 = requestorName1 == "Stephen Bennett (JMTC)" ? "Stephen Bennett" : requestorName1;
                    if (requestorEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.Requestor.Name == requestorName1).Count() == 0)
                    {
                        requestorEquipmentsToAdd.Add(new RequestorEquipment()
                        {
                            EquipmentID = equipment.ID,
                            Requestor = db.Requestors.Where(x => x.FirstName + " " + x.Surname == requestorName1).First()
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(requestorName2))
                {
                    requestorName2 = requestorName2 == "Stephen Bennett (JMTC)" ? "Stephen Bennett" : requestorName2;
                    if (requestorEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.Requestor.Name == requestorName2).Count() == 0)
                    {
                        requestorEquipmentsToAdd.Add(new RequestorEquipment()
                        {
                            EquipmentID = equipment.ID,
                            Requestor = db.Requestors.Where(x => x.FirstName + " " + x.Surname == requestorName2).First()
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(requestorName3))
                {
                    requestorName3 = requestorName3 == "Stephen Bennett (JMTC)" ? "Stephen Bennett" : requestorName3;
                    if (requestorEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.Requestor.Name == requestorName3).Count() == 0)
                    {
                        requestorEquipmentsToAdd.Add(new RequestorEquipment()
                        {
                            EquipmentID = equipment.ID,
                            Requestor = db.Requestors.Where(x => x.FirstName + " " + x.Surname == requestorName3).First()
                        });
                    }
                }

                // engineerEquipments
                if (!string.IsNullOrWhiteSpace(engineerName1))
                {
                    var engineerID = db.Engineers.Where(x => x.Name == engineerName1).Single().ID;
                    if (engineerEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.EngineerID == engineerID).Count() == 0)
                    {
                        engineerEquipmentsToAdd.Add(new EngineerEquipment()
                        {
                            EquipmentID = equipment.ID,
                            EngineerID = engineerID
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(engineerName2))
                {
                    var engineerID = db.Engineers.Where(x => x.Name == engineerName2).Single().ID;
                    if (engineerEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.EngineerID == engineerID).Count() == 0)
                    {
                        engineerEquipmentsToAdd.Add(new EngineerEquipment()
                        {
                            EquipmentID = equipment.ID,
                            EngineerID = engineerID
                        });
                    }
                }
                if (!string.IsNullOrWhiteSpace(engineerName3))
                {
                    var engineerID = db.Engineers.Where(x => x.Name == engineerName3).Single().ID;
                    if (engineerEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.EngineerID == engineerID).Count() == 0)
                    {
                        engineerEquipmentsToAdd.Add(new EngineerEquipment()
                        {
                            EquipmentID = equipment.ID,
                            EngineerID = engineerID
                        });
                    }
                }

                // departmentEquipments
                if (!string.IsNullOrWhiteSpace(departmentCode))
                {
                    if (departmentEquipmentsToAdd.Where(x => x.EquipmentID == equipment.ID && x.Department.Code == departmentCode).Count() == 0)
                    {
                        departmentEquipmentsToAdd.Add(new DepartmentEquipment()
                        {
                            EquipmentID = equipment.ID,
                            Department = db.Departments.Where(x => x.Code == departmentCode).First()
                        });
                    }
                }
            }

            db.Locations.AddRange(locationsToAdd);
            db.ProjectCodes.AddRange(projectCodesToAdd);
            db.Equipment.AddRange(equipmentsToAdd);
            db.EquipmentAssets.AddRange(equipmentAssetToAdd);
            db.RequestorEquipments.AddRange(requestorEquipmentsToAdd);
            db.EngineerEquipments.AddRange(engineerEquipmentsToAdd);
            db.DepartmentEquipments.AddRange(departmentEquipmentsToAdd);
            db.SaveChanges();


            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RevertEquipments()
        {
            var db = new SnapDbContext();

            db.DepartmentEquipments.RemoveRange(db.DepartmentEquipments);
            db.EngineerEquipments.RemoveRange(db.EngineerEquipments);
            db.RequestorEquipments.RemoveRange(db.RequestorEquipments);
            db.EquipmentAssets.RemoveRange(db.EquipmentAssets);
            db.Equipment.RemoveRange(db.Equipment);

            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportJobs()
        {
            var db = new SnapDbContext();

            var jobTypesToAdd = new List<JobType>();
            var locationsToAdd = new List<Location>();
            var projectCodesToAdd = new List<ProjectCode>();
            var equipmentsToAdd = new List<Equipment>();
            var jobsToAdd = new List<Job>();
            var jobIssuesToAdd = new List<JobIssue>();
            var jobEngineersToAdd = new List<EngineerJob>();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\Jobs.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                var jobID = row[0].ToString();
                var historicJobID = row[1].ToString();
                var departmentCode = row[2].ToString();
                var locationName = row[3].ToString();
                var projectCodeName = row[4].ToString().Trim();
                var submittedDate = row[8].ToString();
                var requiredByDate = row[9].ToString();
                var plannedStartDate = row[10].ToString();
                var estCompletionDate = row[11].ToString();
                var percentComplete = row[12].ToString();
                var workshopEstHrs = row[13].ToString();
                var urgency = row[14].ToString().ToLower().Trim();
                var designStatus = row[16].ToString().ToLower().Trim();
                var workshopStatus = row[17].ToString().ToLower().Trim();
                var electricalStatus = row[18].ToString().ToLower().Trim();
                var softwareStatus = row[19].ToString().ToLower().Trim();
                var equipNo = row[21].ToString().ToLower().Trim();
                var jobStatus = row[22].ToString().ToLower().Trim();
                var currentIssue = row[33].ToString();
                var expectedTaskCompletedBy = row[34].ToString();
                var engineer1 = row[35].ToString().Trim();
                var engineer2 = row[36].ToString().Trim();
                var engineer3 = row[37].ToString().Trim();
                var engineer4 = row[38].ToString().Trim();
                var closedDate = row[39].ToString();

                // Job type
                var jobTypeName = row[20].ToString().ToLower().Trim();
                JobType jobType = null;
                if (!string.IsNullOrWhiteSpace(jobTypeName))
                {
                    jobType = db.JobTypes.Where(x => x.Name == jobTypeName).FirstOrDefault();
                    if (jobType == null)
                    {
                        jobType = jobTypesToAdd.Where(x => x.Name == jobTypeName).FirstOrDefault();
                        if (jobType == null)
                        {
                            jobType = new JobType()
                            {
                                ID = Guid.NewGuid(),
                                Name = jobTypeName
                            };
                            jobTypesToAdd.Add(jobType);
                        }
                    }
                }

                // location
                Location location = null;
                if (!string.IsNullOrWhiteSpace(locationName))
                {
                    location = db.Locations.Where(x => x.Name == locationName).FirstOrDefault();
                    if (location == null)
                    {
                        location = locationsToAdd.Where(x => x.Name == locationName).FirstOrDefault();
                        if (location == null)
                        {
                            location = new Location()
                            {
                                ID = Guid.NewGuid(),
                                Name = locationName
                            };
                            locationsToAdd.Add(location);
                        }
                    }
                }

                // project codes
                ProjectCode projectCode = null;
                if (!string.IsNullOrWhiteSpace(projectCodeName))
                {
                    projectCode = db.ProjectCodes.Where(x => x.Code == projectCodeName).FirstOrDefault();
                    if (projectCode == null)
                    {
                        projectCode = projectCodesToAdd.Where(x => x.Code == projectCodeName).FirstOrDefault();
                        if (projectCode == null)
                        {
                            projectCode = new ProjectCode()
                            {
                                ID = Guid.NewGuid(),
                                Code = projectCodeName
                            };
                            projectCodesToAdd.Add(projectCode);
                        }
                    }
                }

                // Equipment
                Equipment equipment = null;
                if (!string.IsNullOrWhiteSpace(equipNo))
                {
                    equipment = db.Equipment.Where(x => x.EquipNo == equipNo.ToUpper()).FirstOrDefault();
                    //if (equipment == null)
                    //{
                    //    equipment = equipmentsToAdd.Where(x => x.EquipNo == equipNo.ToUpper()).FirstOrDefault();
                    //    if (equipment == null)
                    //    {
                    //        equipment = new Equipment()
                    //        {
                    //            ID = Guid.NewGuid(),
                    //            EquipStatusID = EquipStatusValues.Decommissioned,
                    //            Deleted = false,
                    //            RefID = 335,
                    //            EquipNo = equipNo.ToUpper(),
                    //            PCControlled = false,
                    //            PCNetworked = false,
                    //            AlarmInterlock = false,
                    //            ExtractInterlock = false
                    //        };
                    //        equipmentsToAdd.Add(equipment);
                    //    }
                    //}
                }

                // job
                var job = new Job();
                job.ID = Guid.NewGuid();
                job.JobID = long.Parse(jobID);
                job.HistoricJID = historicJobID;
                job.DepartmentID = !string.IsNullOrWhiteSpace(departmentCode) ? db.Departments.Where(x => x.Code == departmentCode).First().ID : null as Guid?;
                job.LocationID = location != null ? location.ID : null as Guid?;
                job.ProjectCodeID = projectCode != null ? projectCode.ID : null as Guid?;
                job.ExternalProjectID = row[5].ToString();
                job.Title = row[6].ToString();
                job.OnHarvest = row[7].ToString().ToLower().Trim() == "true";
                job.SubmittedDate = !string.IsNullOrWhiteSpace(submittedDate) ? DateTime.Parse(submittedDate) : null as DateTime?;
                job.RequiredByDate = !string.IsNullOrWhiteSpace(requiredByDate) ? DateTime.Parse(requiredByDate) : null as DateTime?;
                job.PlannedStartDate = !string.IsNullOrWhiteSpace(plannedStartDate) ? DateTime.Parse(plannedStartDate) : null as DateTime?;
                job.EstCompleteionDate = !string.IsNullOrWhiteSpace(estCompletionDate) ? DateTime.Parse(estCompletionDate) : null as DateTime?;
                job.PercentComplete = !string.IsNullOrWhiteSpace(percentComplete) ? int.Parse(percentComplete) : 0;
                job.StatusWorkshopEstHrs = !string.IsNullOrWhiteSpace(workshopEstHrs) ? decimal.Parse(workshopEstHrs) : null as decimal?;
                job.UrgencyLevelID = urgency == "high" ? JobUrgencyLevelValues.High : urgency == "medium" ? JobUrgencyLevelValues.Medium : urgency == "low" ? JobUrgencyLevelValues.Low : urgency == "safety" ? JobUrgencyLevelValues.Safety : null as int?;
                job.DrawingNo = row[15].ToString();
                job.StatusDesignID = designStatus == "awaiting" ? JobDesignStatusValues.Awaiting : designStatus == "in progress" ? JobDesignStatusValues.InProgress : designStatus == "fin" ? JobDesignStatusValues.Fin : designStatus == "n/a" ? JobDesignStatusValues.NA : 999;
                job.StatusWorkshopID = workshopStatus == "awaiting" ? JobWorkshopStatusValues.Awaiting : workshopStatus == "in progress" ? JobWorkshopStatusValues.InProgress : workshopStatus == "fin" ? JobWorkshopStatusValues.Fin : workshopStatus == "n/a" ? JobWorkshopStatusValues.NA : 999;
                job.StatusElectricalID = electricalStatus == "awaiting" ? JobElectricalStatusValues.Awaiting : electricalStatus == "in progress" ? JobElectricalStatusValues.InProgress : electricalStatus == "fin" ? JobElectricalStatusValues.Fin : electricalStatus == "n/a" ? JobElectricalStatusValues.NA : 999;
                job.StatusSoftwareID = softwareStatus == "awaiting" ? JobSoftwareStatusValues.Awaiting : softwareStatus == "in progress" ? JobSoftwareStatusValues.InProgress : softwareStatus == "fin" ? JobSoftwareStatusValues.Fin : softwareStatus == "n/a" ? JobSoftwareStatusValues.NA : 999;
                job.JobTypeID = jobType != null ? jobType.ID : null as Guid?;
                job.EquipmentNoID = equipment != null ? equipment.ID : null as Guid?;
                job.StatusJobID = jobStatus == "closed" ? JobStatusValues.Closed : jobStatus == "closed - cancelled" ? JobStatusValues.ClosedCancelled : jobStatus == "open" ? JobStatusValues.Open : jobStatus == "open - on hold" ? JobStatusValues.OpenOnHold : 999;
                job.StatusInfoRequired = row[23].ToString().ToLower().Trim() == "true";
                job.StageLaunch = row[24].ToString().ToLower().Trim() == "true";
                job.StageConcept = row[25].ToString().ToLower().Trim() == "true";
                job.StagePrelimDesign = row[26].ToString().ToLower().Trim() == "true";
                job.StageFinalDesign = row[27].ToString().ToLower().Trim() == "true";
                job.StageConstruction = row[28].ToString().ToLower().Trim() == "true";
                job.StageDelivery = row[29].ToString().ToLower().Trim() == "true";
                job.TrackingStarted = row[30].ToString().ToLower().Trim() == "true";
                job.TrackingDone = row[31].ToString().ToLower().Trim() == "true";
                job.Note = row[32].ToString();
                job.EstIssuesCompletionDate = !string.IsNullOrWhiteSpace(expectedTaskCompletedBy) ? DateTime.Parse(expectedTaskCompletedBy) : null as DateTime?;
                job.ClosedDate = !string.IsNullOrWhiteSpace(closedDate) ? DateTime.Parse(closedDate) : null as DateTime?;
                job.FileLocation = row[40].ToString();

                jobsToAdd.Add(job);

                // issue
                if (!string.IsNullOrWhiteSpace(currentIssue))
                {
                    jobIssuesToAdd.Add(new JobIssue()
                    {
                        ID = Guid.NewGuid(),
                        JobID = job.ID,
                        Details = currentIssue,
                        Complete = false,
                        EstCompleteDate = DateTime.Today,
                    });
                }

                // engineer
                if (!string.IsNullOrWhiteSpace(engineer1))
                {
                    jobEngineersToAdd.Add(new EngineerJob()
                    {
                        EngineerID = db.Engineers.Where(x => x.Name == engineer1).First().ID,
                        JobID = job.ID,
                    });
                }
                if (!string.IsNullOrWhiteSpace(engineer2))
                {
                    jobEngineersToAdd.Add(new EngineerJob()
                    {
                        EngineerID = db.Engineers.Where(x => x.Name == engineer2).First().ID,
                        JobID = job.ID,
                    });
                }
                if (!string.IsNullOrWhiteSpace(engineer3))
                {
                    jobEngineersToAdd.Add(new EngineerJob()
                    {
                        EngineerID = db.Engineers.Where(x => x.Name == engineer3).First().ID,
                        JobID = job.ID,
                    });
                }
                if (!string.IsNullOrWhiteSpace(engineer4))
                {
                    jobEngineersToAdd.Add(new EngineerJob()
                    {
                        EngineerID = db.Engineers.Where(x => x.Name == engineer4).First().ID,
                        JobID = job.ID,
                    });
                }
            }

            db.JobTypes.AddRange(jobTypesToAdd);
            db.Locations.AddRange(locationsToAdd);
            db.ProjectCodes.AddRange(projectCodesToAdd);
            //db.Equipment.AddRange(equipmentsToAdd);
            db.Jobs.AddRange(jobsToAdd);
            db.JobIssues.AddRange(jobIssuesToAdd);
            db.EngineerJobs.AddRange(jobEngineersToAdd);
            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportJobRequestors()
        {
            var db = new SnapDbContext();

            var jobRequestorsToAdd = new List<RequestorJob>();

            var requestorList = db.Requestors.ToList();

            var path = @"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\JobRequestors.xlsx";
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                var requestorName = row[0].ToString();
                var jobRefID = long.Parse(row[2].ToString());

                var requestor = requestorList.Where(x => x.Name.Trim() == requestorName.Trim()).Single();
                var job = db.Jobs.Where(x => x.JobID == jobRefID).Single();

                jobRequestorsToAdd.Add(new RequestorJob()
                {
                    JobID = job.ID,
                    RequestorID = requestor.ID
                });
            }
            db.RequestorJobs.AddRange(jobRequestorsToAdd);
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RevertJobs()
        {
            var db = new SnapDbContext();

            db.EngineerJobs.RemoveRange(db.EngineerJobs);
            db.JobIssues.RemoveRange(db.JobIssues);
            db.Jobs.RemoveRange(db.Jobs);

            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportPurchases(string filename)
        {
            var db = new SnapDbContext();

            var materialTypesToAdd = new List<MaterialType>();
            var suppliersToAdd = new List<Supplier>();
            var supplierRefID = 500;
            var purchasesToAdd = new List<Purchase>();
            var projectCodesToAdd = new List<ProjectCode>();
            var requestorPurchasesToAdd = new List<RequestorPurchase>();
            var onBefalfPurchasesToAdd = new List<OnBehalfOfPurchase>();
            //var requestorJobsToAdd = new List<RequestorJob>();
            var departmentPurchasesToAdd = new List<DepartmentPurchase>();

            var path = String.Format(@"C:\Users\Fruitful\Desktop\Note\Johnson Matthey\Data\{0}.xlsx", filename);
            var data = Fruitful.Import.ImportService.CopyExcelFileToTable(path, false);

            foreach (System.Data.DataRow row in data.Rows)
            {
                var id = row[0].ToString();
                var title = row[1].ToString();
                var supplierName = row[2].ToString();
                var partNo = row[3].ToString();
                var materialTypeName = row[4].ToString();
                var materialSpec = row[5].ToString();
                var qtyOrdered = row[6].ToString();
                var qtyRecd = row[7].ToString();
                var unitPrice = row[8].ToString();
                var totalPrice = row[9].ToString();
                var poCurrency = row[10].ToString();
                var purchaseStatus = row[11].ToString();
                var dateOrdered = row[12].ToString();
                var poNo = row[13].ToString();
                var orderBy = row[14].ToString();
                var paymentMethod = row[15].ToString();
                var dateReceived = row[16].ToString();
                var workshopNo = row[17].ToString();
                var projectCodeName = row[18].ToString().Trim();
                var dateDue = row[19].ToString();
                var leadTimeApprox = row[20].ToString();
                var supplierRef = row[21].ToString();
                var notes = row[22].ToString();
                var jobHistoricID = row[23].ToString();
                var jobID = row[24].ToString();
                var orderOnBehalf1 = row[25].ToString();
                var orderOnBehalf2 = row[26].ToString();
                var departmentName1 = row[27].ToString();
                var departmentName2 = row[28].ToString();

                // Material Type
                MaterialType materialType = null;
                if (!string.IsNullOrWhiteSpace(materialTypeName))
                {
                    materialType = db.MaterialTypes.Where(x => x.Name.Trim() == materialTypeName.Trim()).FirstOrDefault();
                    if (materialType == null)
                    {
                        materialType = materialTypesToAdd.Where(x => x.Name.Trim() == materialTypeName.Trim()).FirstOrDefault();
                        if (materialType == null)
                        {
                            materialType = new MaterialType()
                            {
                                ID = Guid.NewGuid(),
                                Name = materialTypeName.Trim()
                            };
                            materialTypesToAdd.Add(materialType);
                        }
                    }
                }

                // Supplier
                Supplier supplier = null;
                if (!string.IsNullOrEmpty(supplierName))
                {
                    supplier = db.Suppliers.Where(x => x.Name == supplierName).FirstOrDefault();
                    if (supplier == null)
                    {
                        supplier = suppliersToAdd.Where(x => x.Name == supplierName).FirstOrDefault();
                        if (supplier == null)
                        {
                            suppliersToAdd.Add(new Supplier()
                            {
                                ID = Guid.NewGuid(),
                                Name = supplierName,
                                RefID = supplierRefID++,
                                Deleted = false,
                                SupplierTypeID = null
                            });
                        }
                    }
                }

                // project codes
                ProjectCode projectCode = null;
                if (!string.IsNullOrWhiteSpace(projectCodeName))
                {
                    projectCode = db.ProjectCodes.Where(x => x.Code == projectCodeName).FirstOrDefault();
                    if (projectCode == null)
                    {
                        projectCode = projectCodesToAdd.Where(x => x.Code == projectCodeName).FirstOrDefault();
                        if (projectCode == null)
                        {
                            projectCode = new ProjectCode()
                            {
                                ID = Guid.NewGuid(),
                                Code = projectCodeName
                            };
                            projectCodesToAdd.Add(projectCode);
                        }
                    }
                }

                var purchase = new Purchase();
                purchase.RefID = Int64.Parse(id);
                purchase.Title = title;
                purchase.SupplierID = supplier != null ? supplier.ID : null as Guid?;
                purchase.PartNo = partNo;
                purchase.ProjectCodeID = projectCode != null ? projectCode.ID : null as Guid?;
                purchase.MaterialTypeID = materialType != null ? materialType.ID : null as Guid?;
                purchase.MaterialSpec = materialSpec;
                purchase.QtyOrdered = !string.IsNullOrWhiteSpace(qtyOrdered) ? decimal.Parse(qtyOrdered) : 0;
                purchase.QtyReceived = !string.IsNullOrWhiteSpace(qtyRecd) ? decimal.Parse(qtyRecd) : 0;
                purchase.UnitPrice = !string.IsNullOrWhiteSpace(unitPrice) ? decimal.Parse(unitPrice) : 0;
                purchase.PurchaseTotal = !string.IsNullOrWhiteSpace(totalPrice) ? decimal.Parse(totalPrice) : 0;
                purchase.POCurrency = poCurrency;
                purchase.PurchaseStatusID = purchaseStatus == "Ordered" ? PurchaseStatusValues.Ordered : purchaseStatus == "Enquiry" ? PurchaseStatusValues.Enquiry : purchaseStatus == "Quoted" ? PurchaseStatusValues.Quoted : 999;
                purchase.DateOrdered = !string.IsNullOrEmpty(dateOrdered) ? DateTime.Parse(dateOrdered) : null as DateTime?;
                purchase.PONo = poNo;
                purchase.PaymentMethodID = !string.IsNullOrEmpty(paymentMethod) ? (paymentMethod == "Account" ? PaymentMethodValues.Account : paymentMethod == "Credit Card" ? PaymentMethodValues.CreditCard : paymentMethod == "FOC" ? PaymentMethodValues.FOC : paymentMethod == "Pro forma" ? PaymentMethodValues.ProForma : 999) : null as int?;
                purchase.DateReceived = !string.IsNullOrEmpty(dateReceived) ? DateTime.Parse(dateReceived) : null as DateTime?;
                purchase.WorkshopNo = workshopNo;
                purchase.DateDue = !string.IsNullOrEmpty(dateDue) ? DateTime.Parse(dateDue) : null as DateTime?;
                purchase.LeadTimeApprx = leadTimeApprox;
                purchase.SupplierRef = supplierRef;
                purchase.Note = notes;
                purchase.HistoricJobID = jobHistoricID;
                purchase.RelatedJobID = !string.IsNullOrEmpty(jobID)? db.Jobs.Where(x => x.JobID.ToString() == jobID).Single().ID : null as Guid?;
                purchasesToAdd.Add(purchase);

                // requestor
                var requestorsInDB = db.Requestors.ToList();
                if (!string.IsNullOrEmpty(orderBy))
                {
                    orderBy = orderBy == "Stephen Bennett (JMTC)" ? "Stephen Bennett" : orderBy;
                    requestorPurchasesToAdd.Add(new RequestorPurchase()
                    {
                        PurchaseID = purchase.ID,
                        RequestorID = requestorsInDB.Where(x => x.Name == orderBy).Single().ID
                    });
                }

                // order on behalf
                if (!string.IsNullOrEmpty(orderOnBehalf1))
                {
                    orderOnBehalf1 = orderOnBehalf1 == "Stephen Bennett (JMTC)" ? "Stephen Bennett" : orderOnBehalf1;
                    var requestorID = requestorsInDB.Where(x => x.Name == orderOnBehalf1).Single().ID;
                    onBefalfPurchasesToAdd.Add(new OnBehalfOfPurchase()
                    {
                        PurchaseID = purchase.ID,
                        RequestorID = requestorID
                    });

                    //if (purchase.RelatedJobID != null)
                    //{
                    //    if (db.RequestorJobs.Where(x => x.JobID == purchase.RelatedJobID && x.RequestorID == requestorID).Count() == 0)
                    //    {
                    //        if (requestorJobsToAdd.Where(x => x.JobID == purchase.RelatedJobID && x.RequestorID == requestorID).Count() == 0)
                    //        {
                    //            requestorJobsToAdd.Add(new RequestorJob()
                    //            {
                    //                JobID = purchase.RelatedJobID.Value,
                    //                RequestorID = requestorID
                    //            });
                    //        }
                    //    }
                    //}
                }

                if (!string.IsNullOrEmpty(orderOnBehalf2))
                {
                    orderOnBehalf2 = orderOnBehalf2 == "Stephen Bennett (JMTC)" ? "Stephen Bennett" : orderOnBehalf2;
                    var requestorID = requestorsInDB.Where(x => x.Name == orderOnBehalf2).Single().ID;
                    onBefalfPurchasesToAdd.Add(new OnBehalfOfPurchase()
                    {
                        PurchaseID = purchase.ID,
                        RequestorID = requestorID
                    });

                    //if (purchase.RelatedJobID != null)
                    //{
                    //    if (db.RequestorJobs.Where(x => x.JobID == purchase.RelatedJobID && x.RequestorID == requestorID).Count() == 0)
                    //    {
                    //        if (requestorJobsToAdd.Where(x => x.JobID == purchase.RelatedJobID && x.RequestorID == requestorID).Count() == 0)
                    //        {
                    //            requestorJobsToAdd.Add(new RequestorJob()
                    //            {
                    //                JobID = purchase.RelatedJobID.Value,
                    //                RequestorID = requestorID
                    //            });
                    //        }
                    //    }
                    //}
                }

                // department

                if (!string.IsNullOrEmpty(departmentName1))
                {
                    var departmentID = db.Departments.Where(x => x.Code == departmentName1).Single().ID;
                    departmentPurchasesToAdd.Add(new DepartmentPurchase()
                    {
                        DepartmentID = departmentID,
                        PurchaseID = purchase.ID
                    });
                }

                if (!string.IsNullOrEmpty(departmentName2))
                {
                    var departmentID = db.Departments.Where(x => x.Code == departmentName2).Single().ID;
                    departmentPurchasesToAdd.Add(new DepartmentPurchase()
                    {
                        DepartmentID = departmentID,
                        PurchaseID = purchase.ID
                    });
                }
            }

            db.MaterialTypes.AddRange(materialTypesToAdd);
            db.ProjectCodes.AddRange(projectCodesToAdd);
            db.Suppliers.AddRange(suppliersToAdd);
            db.Purchases.AddRange(purchasesToAdd);
            db.RequestorPurchases.AddRange(requestorPurchasesToAdd);
            db.OnBehalfOfPurchase.AddRange(onBefalfPurchasesToAdd);
            //db.RequestorJobs.AddRange(requestorJobsToAdd);
            db.DepartmentPurchases.AddRange(departmentPurchasesToAdd);
            db.SaveChanges();


            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult RevertPurchases()
        {
            var db = new SnapDbContext();

            db.DepartmentPurchases.RemoveRange(db.DepartmentPurchases);
            db.RequestorJobs.RemoveRange(db.RequestorJobs);
            db.OnBehalfOfPurchase.RemoveRange(db.OnBehalfOfPurchase);
            db.RequestorPurchases.RemoveRange(db.RequestorPurchases);
            db.Purchases.RemoveRange(db.Purchases);

            db.SaveChanges();

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateJobsMonthly(string y, string m)
        {
            var timestamp = new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);
            if (!string.IsNullOrEmpty(y) && !string.IsNullOrEmpty(m))
            {
                try
                {
                    timestamp = new DateTime(int.Parse(y), int.Parse(m), 1);
                }
                catch { }
            }

            using (var dbcontext = new SnapDbContext())
            {
                var countJobsOnHold = dbcontext.Jobs.Where(x => x.StatusJobID == JobStatusValues.OpenOnHold).Count();
                var countJobsOpen = dbcontext.Jobs.Where(x => x.StatusJobID == JobStatusValues.Open).Count();

                if (!dbcontext.MonthlyJobsOnHold.Any(x => x.Month == timestamp))
                {
                    dbcontext.MonthlyJobsOnHold.Add(new MonthlyJobsOnHold()
                    {
                        Month = timestamp,
                        Count = countJobsOnHold
                    });
                }
                if (!dbcontext.MonthlyJobsOpened.Any(x => x.Month == timestamp))
                {
                    dbcontext.MonthlyJobsOpened.Add(new MonthlyJobsOpened()
                    {
                        Month = timestamp,
                        Count = countJobsOpen
                    });
                }
                dbcontext.SaveChanges();
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}