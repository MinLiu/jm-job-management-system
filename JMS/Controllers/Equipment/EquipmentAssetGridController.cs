﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class EquipmentAssetGridController : GridController<EquipmentAsset, EquipmentAssetGridViewModel>
    {

        public EquipmentAssetGridController()
            : base(new EquipmentAssetRepository(), new EquipmentAssetGridMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string equipmentID)
        {
            //User passed parameters oo filter results
            var list = _repo.Read().Where(x => equipmentID.Equals("") || equipmentID.Equals(null) || x.EquipmentID.ToString() == equipmentID);

            list = ApplyQueryFiltersSort(request, list, "PMNo");

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(x => _mapper.MapToViewModel(x));
            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
            return Json(result);

        }
    }
    
}