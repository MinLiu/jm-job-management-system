﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class EquipmentController : EntityController<Equipment, EquipmentViewModel>
    {
      
        public EquipmentController()
            : base(new EquipmentRepository(), new EquipmentMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.EquipmentID = ID;

            ViewBag.StatusOptions = new EquipStatusRepository().Read().OrderBy(x => x.SortPos).ToList().Select(x => new EquipStatusMapper().MapToViewModel(x)).ToList();

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }

        public ActionResult Goto(string ID)
        {
            return RedirectToAction("Index", new { ID = ID });
        }


        #region Edit Equipment Panel

        // GET: /New Equipment
        public ActionResult _NewEquipment()
        {
            var equipment = new EquipmentRepository().Read().OrderByDescending(x => x.EquipNo).FirstOrDefault();

            int equipNo;
            try
            {
                equipNo = equipment != null ? int.Parse(equipment.EquipNo.Substring(4)) + 1 : 1;
            }
            catch
            {
                equipNo = 1;
            }
            return PartialView(new EquipmentViewModel() { EquipNo = String.Format("JMTC{0}", equipNo.ToString("").PadLeft(4, '0')) });
        }


        // POST: /New Equipment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewEquipment(EquipmentViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            viewModel.ID = model.ID.ToString();
            UpdateRequestors(viewModel);
            UpdateEngineers(viewModel);
            UpdateDepartments(viewModel);

            return Json(new { ID = model.ID });                
           
        }



        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditEquipment(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Equipment clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                EquipmentViewModel model = _mapper.MapToViewModel(clt);

                ViewBag.CurrentUserID = CurrentUser.Id;
                ViewBag.CurrentUserEmail = CurrentUser.Email;
                return PartialView(model);
            }
            catch {
                return PartialView("_NewSupplier", new EquipmentViewModel());
            }

        }


        // POST: /Edit Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditEquipmentDetails(EquipmentViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);

            UpdateRequestors(viewModel);
            UpdateEngineers(viewModel);
            UpdateDepartments(viewModel);


            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion

        private void UpdateRequestors(EquipmentViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.RequestorEquipments)
                             .FirstOrDefault();

            var oldList = model != null ? model.RequestorEquipments.Select(x => x.RequestorID.ToString()) : new List<string>();
            var newList = viewModel.RequestorIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new RequestorEquipmentRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.RequestorID.ToString() == id && x.EquipmentID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new RequestorEquipment() { RequestorID = Guid.Parse(id), EquipmentID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateEngineers(EquipmentViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.EngineerEquipments)
                             .FirstOrDefault();

            var oldList = model != null ? model.EngineerEquipments.Select(x => x.EngineerID.ToString()) : new List<string>();
            var newList = viewModel.EngineerIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new EngineerEquipmentRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.EngineerID.ToString() == id && x.EquipmentID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new EngineerEquipment() { EngineerID = Guid.Parse(id), EquipmentID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateDepartments(EquipmentViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.DepartmentEquipments)
                             .FirstOrDefault();

            var oldList = model != null ? model.DepartmentEquipments.Select(x => x.DepartmentID.ToString()) : new List<string>();
            var newList = viewModel.DepartmentIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new DepartmentEquipmentRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.DepartmentID.ToString() == id && x.EquipmentID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new DepartmentEquipment() { DepartmentID = Guid.Parse(id), EquipmentID = Guid.Parse(viewModel.ID) });
            }
        }
    }
    
}