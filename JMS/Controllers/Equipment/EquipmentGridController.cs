﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class EquipmentGridController : GridController<Equipment, EquipmentGridViewModel>
    {

        public EquipmentGridController()
            : base(new EquipmentRepository(), new EquipmentGridMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, string requestorID, List<int> statuses = null, List<Guid> relatedDepartmentIDs = null, List<Guid?> relatedLocationIDs = null, List<Guid> relatedEngineerIDs = null, List<Guid?> relatedManufacturerIDs = null, bool pcControlled = false, bool pcNetworked = false, bool alarmInterlock = false, bool extractInterlock = false)
        {
            //User passed parameters oo filter results

            statuses = statuses ?? new List<int>();
            relatedDepartmentIDs = relatedDepartmentIDs ?? new List<Guid>();
            relatedLocationIDs = relatedLocationIDs ?? new List<Guid?>();
            relatedManufacturerIDs = relatedManufacturerIDs ?? new List<Guid?>();
            relatedEngineerIDs = relatedEngineerIDs ?? new List<Guid>();

            var list = _repo.Read().Where(
                         x => 
                         (
                             (x.EquipNo.ToLower().Contains(filterText.ToLower())
                               || x.Title.ToLower().Contains(filterText.ToLower())
                               || x.PMNo.ToLower().Contains(filterText.ToLower())
                               || x.EquipmentAssets.Any(asset => asset.Title.ToLower().Contains(filterText.ToLower()) || asset.PMNo.ToLower().Contains(filterText.ToLower()))
                             ) || filterText.Equals("") || filterText.Equals(null)
                         ))
                         .Where(x => x.Deleted == false)
                         .Where(x => requestorID.Equals("") || requestorID.Equals(null) || x.RequestorEquipments.Select(r => r.RequestorID.ToString()).Contains(requestorID))
                         .Where(x => !relatedDepartmentIDs.Any() || relatedDepartmentIDs.Any(id => x.DepartmentEquipments.Select(d => d.DepartmentID).Contains(id)))
                         .Where(x => !relatedEngineerIDs.Any() || relatedEngineerIDs.Any(id => x.EngineerEquipments.Select(e => e.EngineerID).Contains(id)))
                         .Where(x => !relatedLocationIDs.Any() || relatedLocationIDs.Contains(x.LocationID))
                         .Where(x => !relatedManufacturerIDs.Any() || relatedManufacturerIDs.Contains(x.ManufacturerID) || relatedManufacturerIDs.Any(id => x.EquipmentAssets.Select(asset => asset.ManufacturerID).Contains(id)))
                         .Where(x => !pcControlled || x.PCControlled)
                         .Where(x => !pcNetworked || x.PCNetworked)
                         .Where(x => !alarmInterlock || x.AlarmInterlock)
                         .Where(x => !extractInterlock || x.ExtractInterlock)
                         .Where(x => statuses.Contains(x.EquipStatusID));

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "LocationID") request.Sorts[x].Member = "Location.Name";
                if (request.Sorts[x].Member == "ManufacturerID") request.Sorts[x].Member = "Manufacturer.Name";
            }

            list = ApplyQueryFiltersSort(request, list, "EquipNo");

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(x => _mapper.MapToViewModel(x));
            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
            return Json(result);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReadByRequestor([DataSourceRequest]DataSourceRequest request, Guid requestorID)
        {
            //User passed parameters oo filter results
            var list = _repo.Read().Where(x => x.RequestorEquipments.Where(r => r.RequestorID == requestorID).Count() > 0);

            list = ApplyQueryFiltersSort(request, list, "EquipNo");

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(x => _mapper.MapToViewModel(x));
            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
            return Json(result);

        }

        public FileResult Export(string filterText, string requestorID, List<int> statuses = null, List<Guid> relatedDepartmentIDs = null, List<Guid?> relatedLocationIDs = null, List<Guid> relatedEngineerIDs = null, List<Guid?> relatedManufacturerIDs = null, bool pcControlled = false, bool pcNetworked = false, bool alarmInterlock = false, bool extractInterlock = false)
        {
            statuses = statuses ?? new List<int>();
            relatedDepartmentIDs = relatedDepartmentIDs ?? new List<Guid>();
            relatedLocationIDs = relatedLocationIDs ?? new List<Guid?>();
            relatedManufacturerIDs = relatedManufacturerIDs ?? new List<Guid?>();
            relatedEngineerIDs = relatedEngineerIDs ?? new List<Guid>();

            var list = _repo.Read().Where(
                         x =>
                         (
                             (x.EquipNo.ToLower().Contains(filterText.ToLower())
                               || x.Title.ToLower().Contains(filterText.ToLower())
                               || x.PMNo.ToLower().Contains(filterText.ToLower())
                               || x.EquipmentAssets.Any(asset => asset.Title.ToLower().Contains(filterText.ToLower()) || asset.PMNo.ToLower().Contains(filterText.ToLower()))
                             ) || filterText.Equals("") || filterText.Equals(null)
                         ))
                         .Where(x => x.Deleted == false)
                         .Where(x => requestorID.Equals("") || requestorID.Equals(null) || x.RequestorEquipments.Select(r => r.RequestorID.ToString()).Contains(requestorID))
                         .Where(x => !relatedDepartmentIDs.Any() || relatedDepartmentIDs.Any(id => x.DepartmentEquipments.Select(d => d.DepartmentID).Contains(id)))
                         .Where(x => !relatedEngineerIDs.Any() || relatedEngineerIDs.Any(id => x.EngineerEquipments.Select(e => e.EngineerID).Contains(id)))
                         .Where(x => !relatedLocationIDs.Any() || relatedLocationIDs.Contains(x.LocationID))
                         .Where(x => !relatedManufacturerIDs.Any() || relatedManufacturerIDs.Contains(x.ManufacturerID) || relatedManufacturerIDs.Any(id => x.EquipmentAssets.Select(asset => asset.ManufacturerID).Contains(id)))
                         .Where(x => !pcControlled || x.PCControlled)
                         .Where(x => !pcNetworked || x.PCNetworked)
                         .Where(x => !alarmInterlock || x.AlarmInterlock)
                         .Where(x => !extractInterlock || x.ExtractInterlock)
                         .Where(x => statuses.Contains(x.EquipStatusID))
                         .OrderBy(x => x.EquipNo)
                         .ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            var newDataFormat = workbook.CreateDataFormat();
            var dateTimeStyle = workbook.CreateCellStyle(); dateTimeStyle.DataFormat = newDataFormat.GetFormat("dd/MM/yyyy");
            var numberStyle = workbook.CreateCellStyle(); numberStyle.DataFormat = newDataFormat.GetFormat("0.00");

            var maxRequestors = list.Select(e => e.RequestorEquipments.Count).OrderByDescending(e => e).FirstOrDefault();
            var maxDesigners = list.Select(e => e.EngineerEquipments.Count).OrderByDescending(e => e).FirstOrDefault();

            int index = 0;

            int rowNumber = 1;

            foreach (var item in list)
            {
                index = 0;
                var totalAsset = item.EquipmentAssets.Count;
                var assetNo = 1;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(index).SetCellValue(String.Join(", ", item.DepartmentEquipments.Select(d => d.Department.Code))); index++;
                row.CreateCell(index).SetCellValue(item.EquipNo); index++;
                row.CreateCell(index).SetCellValue(totalAsset != 0 ? assetNo++.ToString() : ""); index++;
                row.CreateCell(index).SetCellValue(item.Title); index++;
                row.CreateCell(index).SetCellValue(item.Location != null ? item.Location.Name : ""); index++;
                row.CreateCell(index).SetCellValue(item.EquipType); index++;
                row.CreateCell(index).SetCellValue(item.Manufacturer != null ? item.Manufacturer.Name : ""); index++;
                row.CreateCell(index).SetCellValue(item.DrawingNo); index++;
                row.CreateCell(index).SetCellValue(item.SerialNo); index++;
                row.CreateCell(index).SetCellValue(item.PMNo); index++;
                row.CreateCell(index).SetCellValue(item.DOMorPurchase != null ? item.DOMorPurchase.Value.ToString("dd/MM/yyyy") : ""); row.GetCell(index).CellStyle = dateTimeStyle; index++;
                row.CreateCell(index).SetCellValue(item.ProjectCode != null ? item.ProjectCode.Code : ""); index++;
                row.CreateCell(index).SetCellValue(item.RiskAssessmentRef); index++;
                row.CreateCell(index).SetCellValue(item.EquipStatus.Name); index++;
                row.CreateCell(index).SetCellValue(item.PCControlled); index++;
                row.CreateCell(index).SetCellValue(item.PCNetworked); index++;
                row.CreateCell(index).SetCellValue(item.PCReference); index++;
                row.CreateCell(index).SetCellValue(item.SoftwareAndVersion); index++;
                row.CreateCell(index).SetCellValue(item.ExtractInterlock); index++;
                row.CreateCell(index).SetCellValue(item.AlarmInterlock); index++;
                row.CreateCell(index).SetCellValue(item.ElectricalVisualInspectionDate != null ? item.ElectricalVisualInspectionDate.Value.ToString("dd/MM/yyyy") : ""); row.GetCell(index).CellStyle = dateTimeStyle; index++;
                row.CreateCell(index).SetCellValue(item.ElectricalFullInspectionDate != null ? item.ElectricalFullInspectionDate.Value.ToString("dd/MM/yyyy") : ""); row.GetCell(index).CellStyle = dateTimeStyle; index++;
                row.CreateCell(index).SetCellValue(item.ElectricalTestNumber); index++;
                for (var i = 0; i < maxRequestors; i++)
                {
                    if (item.RequestorEquipments.Count > i)
                        row.CreateCell(index).SetCellValue(item.RequestorEquipments.OrderBy(r => r.Requestor.Name).ElementAt(i).Requestor.Name); 
                    else
                        row.CreateCell(index).SetCellValue("");
                    index++;
                }
                for (var i = 0; i < maxDesigners; i++)
                {
                    if (item.EngineerEquipments.Count > i)
                        row.CreateCell(index).SetCellValue(item.EngineerEquipments.OrderBy(r => r.Engineer.Name).ElementAt(i).Engineer.Name);
                    else
                        row.CreateCell(index).SetCellValue("");
                    index++;
                }

                foreach (var asset in item.EquipmentAssets.OrderBy(a => a.PMNo))
                {
                    var subrow = sheet.CreateRow(rowNumber++);
                    index = 0;

                    subrow.CreateCell(index).SetCellValue(String.Join(", ", item.DepartmentEquipments.Select(d => d.Department.Code))); index++;
                    subrow.CreateCell(index).SetCellValue(item.EquipNo); index++;
                    subrow.CreateCell(index).SetCellValue(assetNo++.ToString()); index++;
                    subrow.CreateCell(index).SetCellValue(asset.Title); index++;
                    subrow.CreateCell(index).SetCellValue(item.Location != null ? item.Location.Name : ""); index++;
                    subrow.CreateCell(index).SetCellValue(item.EquipType); index++;
                    subrow.CreateCell(index).SetCellValue(asset.Manufacturer != null ? item.Manufacturer.Name : ""); index++;
                    subrow.CreateCell(index).SetCellValue(asset.DrawingNo); index++;
                    subrow.CreateCell(index).SetCellValue(asset.SerialNo); index++;
                    subrow.CreateCell(index).SetCellValue(asset.PMNo); index++;
                    subrow.CreateCell(index).SetCellValue(item.DOMorPurchase != null ? item.DOMorPurchase.Value.ToString("dd/MM/yyyy") : ""); subrow.GetCell(index).CellStyle = dateTimeStyle; index++;
                    subrow.CreateCell(index).SetCellValue(item.ProjectCode != null ? item.ProjectCode.Code : ""); index++;
                    subrow.CreateCell(index).SetCellValue(item.RiskAssessmentRef); index++;
                    subrow.CreateCell(index).SetCellValue(item.EquipStatus.Name); index++;
                    subrow.CreateCell(index).SetCellValue(item.PCControlled); index++;
                    subrow.CreateCell(index).SetCellValue(item.PCNetworked); index++;
                    subrow.CreateCell(index).SetCellValue(item.PCReference); index++;
                    subrow.CreateCell(index).SetCellValue(item.SoftwareAndVersion); index++;
                    subrow.CreateCell(index).SetCellValue(item.ExtractInterlock); index++;
                    subrow.CreateCell(index).SetCellValue(item.AlarmInterlock); index++;
                    subrow.CreateCell(index).SetCellValue(item.ElectricalVisualInspectionDate != null ? item.ElectricalVisualInspectionDate.Value.ToString("dd/MM/yyyy") : ""); subrow.GetCell(index).CellStyle = dateTimeStyle; index++;
                    subrow.CreateCell(index).SetCellValue(item.ElectricalFullInspectionDate != null ? item.ElectricalFullInspectionDate.Value.ToString("dd/MM/yyyy") : ""); subrow.GetCell(index).CellStyle = dateTimeStyle; index++;
                    subrow.CreateCell(index).SetCellValue(item.ElectricalTestNumber); index++;
                    for (var i = 0; i < maxRequestors; i++)
                    {
                        if (item.RequestorEquipments.Count > i)
                            row.CreateCell(index).SetCellValue(item.RequestorEquipments.OrderBy(r => r.Requestor.Name).ElementAt(i).Requestor.Name);
                        else
                            row.CreateCell(index).SetCellValue("");
                        index++;
                    }
                    for (var i = 0; i < maxDesigners; i++)
                    {
                        if (item.EngineerEquipments.Count > i)
                            row.CreateCell(index).SetCellValue(item.EngineerEquipments.OrderBy(r => r.Engineer.Name).ElementAt(i).Engineer.Name);
                        else
                            row.CreateCell(index).SetCellValue("");
                        index++;
                    }
                }

            }

            index = 0;
            //Set the column names in the header row
            //Set headers after to make auto resizing easier
            headerRow.CreateCell(index).SetCellValue("Department"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Equipment Number"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Asset No"); index++;
            headerRow.CreateCell(index).SetCellValue("Title"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Location"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Equipment Type"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Outside Manufacturer"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Drawing No"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Serial No"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("PM No"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("DOM or Purchase"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Project Code"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Risk Assessment Ref"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Equipment Status"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("PC Controlled"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("PC Networked"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("PC Reference"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Software and Version"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("extract interlock"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("alarm interlock"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Electrical Visual Inspection Date"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Electrical Full Inspection Date"); sheet.AutoSizeColumn(index); index++;
            headerRow.CreateCell(index).SetCellValue("Electrical Test Number  - Q"); sheet.AutoSizeColumn(index); index++;
            for (var i = 0; i < maxRequestors; i++)
            {
                if (i == 0)
                {
                    headerRow.CreateCell(index).SetCellValue("Requestor");
                    sheet.AutoSizeColumn(index);
                }
                else
                {
                    headerRow.CreateCell(index).SetCellValue("Requestor" + (i + 1));
                    sheet.AutoSizeColumn(index);
                }
                index++;
            }
            for (var i = 0; i < maxDesigners; i++)
            {
                if (i == 0)
                {
                    headerRow.CreateCell(index).SetCellValue("Designer");
                    sheet.AutoSizeColumn(index);
                }
                else
                {
                    headerRow.CreateCell(index).SetCellValue("Designer" + (i + 1));
                    sheet.AutoSizeColumn(index);
                }
                index++;
            }
            headerRow.CreateCell(index).SetCellValue("Notes"); sheet.AutoSizeColumn(index); index++;

            //Write the workbook to a memory stream
            System.IO.MemoryStream output = new System.IO.MemoryStream();
            workbook.Write(output);

            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("Equipments Export {0}.xls", DateTime.Today.ToString("dd-MM-yyyy")));

        }

    }
    
}