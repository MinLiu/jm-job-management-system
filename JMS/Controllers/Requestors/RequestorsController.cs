﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class RequestorsController : EntityController<Requestor, RequestorViewModel>
    {
      
        public RequestorsController()
            : base(new RequestorRepository(), new RequestorMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (ID != null) ViewBag.SupplierID = ID;

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }



        #region Edit Requestor Panel

        // GET: /New Requestor
        public ActionResult _NewRequestor()
        {
            var requestor = new RequestorRepository().Read().OrderByDescending(x => x.RefID).FirstOrDefault();
            var refID = requestor != null ? requestor.RefID + 1 : 1;
            return PartialView(new RequestorViewModel() { RefID = refID });             
        }


        // POST: /New Requestor
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewRequestor(RequestorViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);


            //Log Activity
            //AddLog(model.ID.ToString(), string.Format("{{{0}}} created.", 0), CurrentUser);
                
            return Json(new { ID = model.ID });                
           
        }



        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditRequestor(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Requestor clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                RequestorViewModel model = _mapper.MapToViewModel(clt);

                ViewBag.CurrentUserID = CurrentUser.Id;
                ViewBag.CurrentUserEmail = CurrentUser.Email;
                return PartialView(model);
            }
            catch {
                return PartialView("_NewSupplier", new RequestorViewModel());
            }

        }


        // POST: /Edit Supplier
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditRequestorDetails(RequestorViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);


            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;
            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion

    }
    
}