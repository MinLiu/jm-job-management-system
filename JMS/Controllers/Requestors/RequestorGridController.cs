﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class RequestorGridController : GridController<Requestor, RequestorViewModel>
    {

        public RequestorGridController()
            : base(new RequestorRepository(), new RequestorMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            //if (ViewBag.ViewCRM == false)
            //   Response.Redirect("/");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText)
        {
            //User passed parameters oo filter results
            var list = _repo.Read().Where(
                         cls => 
                         (
                             ((cls.FirstName.ToLower() + " " + cls.Surname.ToLower()).Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null)
                         )
                     );

            list = ApplyQueryFiltersSort(request, list, "RefID");

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
            return Json(result);

        }


        public virtual JsonResult ReadDeleted([DataSourceRequest] DataSourceRequest request)
        {
            var list = _repo.Read();
            var viewModels = list.OrderBy(c => c.Name).ToList().Select(s => _mapper.MapToViewModel(s));

            return Json(viewModels.ToDataSourceResult(request));
        }


        public override JsonResult Destroy(DataSourceRequest request, RequestorViewModel viewModel)
        {
            return base.Destroy(request, viewModel);
        }

    }
    
}