﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize(Roles = "Super Admin,Super User,Owner,Admin")]
    public class SettingsController : BaseController
    {
        CompanyRepository _repo = new CompanyRepository();
        CompanyMapper _mapper = new CompanyMapper();
        
        // GET: Settings
        public ActionResult Index()
        {
            return RedirectToAction("Business", "Settings");
        }

        public ActionResult Business()
        {
            //var viewModel = _mapper.MapToViewModel(CurrentUser.Company);
            //return View(viewModel);
            return View();
        }

        public ActionResult Theme()
        {
            return RedirectToAction("Business", "Settings");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Business(CompanyViewModel viewModel, HttpPostedFileBase logoImage, string remove)
        {
            const string relativePath = "/Images/";

            if (viewModel == null) return RedirectToAction("Business", "Settings");

            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (!string.IsNullOrEmpty(remove))
            {
                try
                {
                    var physicalPath = Server.MapPath(viewModel.LogoImageURL);
                    if (System.IO.File.Exists(physicalPath))
                        System.IO.File.Delete(physicalPath);
                }
                catch { }
                viewModel.LogoImageURL = null;
            }
            //Save Logo if avaliable
            else if (logoImage != null)
            {
                if (!logoImage.ContentType.Contains("image")){ ViewBag.ErrorMessage = "Uploaded logo is not an Image."; return View(viewModel); }
                if (logoImage.ContentLength > 1 * 1024 * 1024) { ViewBag.ErrorMessage = "Uploaded logo is too big."; return View(viewModel); }
                viewModel.LogoImageURL = SaveFile(logoImage, relativePath);
            }
                      

            var model = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, model);
            model.LogoImageURL = viewModel.LogoImageURL;
            _repo.Update(model);

            SetCustomTheme(model);

            ViewBag.SuccessMessage = "Business Details Saved.";
            return View(viewModel);
           
        }

    }
}