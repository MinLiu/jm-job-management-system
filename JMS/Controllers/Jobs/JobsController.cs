﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class JobsController : EntityController<Job, JobViewModel>
    {
      
        public JobsController()
            : base(new JobRepository(), new JobMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            if (ViewBag.ViewCRM == false)
               Response.Redirect("/");
        }

        

        // GET: Suppliers
        public ActionResult Index(string ID)
        {
            if (!string.IsNullOrWhiteSpace(ID))
            {
                ViewBag.JobID = ID;
            }

            ViewBag.StatusOptions = new JobStatusRepository().Read().OrderBy(x => x.SortPos).ToList().Select(x => new JobStatusMapper().MapToViewModel(x)).ToList();
            SetJobViewBag();

            return View();
        }

        public ActionResult Deleted()
        {
            if (ViewBag.DeleteCRM == false)
                Response.Redirect("/");

            return View();
        }

        public ActionResult Goto(string ID)
        {
            return RedirectToAction("Index", new { ID = ID });
        }

        #region Edit Job Panel

        // GET: /New Job
        public ActionResult _NewJob()
        {
            var job = new JobRepository().Read().OrderByDescending(x => x.JobID).FirstOrDefault();
            var jobID = job != null ? job.JobID + 1 : 1;
            return PartialView(new JobViewModel()
            {
                JobID = jobID,
                SubmittedDate = DateTime.Today,
                UrgencyLevelID = JobUrgencyLevelValues.Medium,
                StatusJobID = JobStatusValues.Open,
                StatusSoftwareID = JobSoftwareStatusValues.NA,
                StatusDesignID = JobDesignStatusValues.NA,
                StatusElectricalID = JobElectricalStatusValues.NA,
                StatusWorkshopID = JobWorkshopStatusValues.NA
            });             
        }


        // POST: /New Job
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _NewJob(JobViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            if (viewModel.StatusJobID == JobStatusValues.Closed)
            {
                viewModel.EstimatedCompletion = 100;
            }

            // Auto populate close date when job is closed
            if (viewModel.StatusJobID == JobStatusValues.Closed || viewModel.StatusJobID == JobStatusValues.ClosedCancelled)
            {
                if (viewModel.ClosedDate == null)
                {
                    viewModel.ClosedDate = DateTime.Today;
                }
            }

            var model = _mapper.MapToModel(viewModel);
            _repo.Create(model);

            viewModel.ID = model.ID.ToString();

            UpdateEngineers(viewModel);
            UpdateRequestors(viewModel);

            //var engineerJobList = new List<EngineerJob>();
            //foreach (var id in viewModel.EngineerIDs)
            //{
            //    var engineerJob = new EngineerJob()
            //    {
            //        JobID = model.ID,
            //        EngineerID = Guid.Parse(id)
            //    };
            //    engineerJobList.Add(engineerJob);
            //}
            //new EngineerJobRepository().Create(engineerJobList);

            //var requestorJobList = new List<RequestorJob>();
            //foreach (var id in viewModel.RequestorIDs)
            //{
            //    var requestorJob = new RequestorJob()
            //    {
            //        JobID = model.ID,
            //        RequestorID = Guid.Parse(id)
            //    };
            //    requestorJobList.Add(requestorJob);
            //}
            //new RequestorJobRepository().Create(requestorJobList);

            return Json(new { ID = model.ID });                
           
        }



        // GET: /Edit Supplier
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditJob(string ID)
        {
            try
            {
                //Make sure the supplier being returned belongs to the user of the user
                Job clt =  _repo.Read().Where(o => o.ID.ToString() == ID).First();
                JobViewModel model = _mapper.MapToViewModel(clt);

                return PartialView(model);
            }
            catch(Exception e) {
                return PartialView("_NewJob", new JobViewModel());
            }

        }


        // POST: /Edit 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] 
        public ActionResult _EditJobDetails(JobViewModel viewModel)
        {
            if (viewModel == null) return RedirectToAction("Index");

            //Check to see if model is valid
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            if (viewModel.StatusJobID == JobStatusValues.Closed)
            {
                viewModel.EstimatedCompletion = 100;
            }

            // Auto populate close date when job is closed
            if (viewModel.StatusJobID == JobStatusValues.Closed || viewModel.StatusJobID == JobStatusValues.ClosedCancelled)
            {
                if (viewModel.ClosedDate == null)
                {
                    viewModel.ClosedDate = DateTime.Today;
                }
            }

            var edit = _repo.Find(Guid.Parse(viewModel.ID));
            _mapper.MapToModel(viewModel, edit);
            _repo.Update(edit);

            UpdateEngineers(viewModel);
            UpdateRequestors(viewModel);

            ViewBag.CurrentUserID = CurrentUser.Id;
            ViewBag.CurrentUserEmail = CurrentUser.Email;
            ViewBag.Saved = true; //Return a viewbag variable. Used to show message in front-end html.
            return PartialView(viewModel);
            
        }

        #endregion

        private void SetJobViewBag()
        {
            using (var context = new SnapDbContext())
            {
                var mapper = new JobStatusMapper();
                ViewBag.StatusOptions = context.JobStatuses.OrderBy(x => x.SortPos).ToList().Select(x => mapper.MapToViewModel(x)).ToList();
            }
            ViewBag.statuses = new List<string>();
            ViewBag.page = 1;
            ViewBag.pageSize = 20;
        }

        private void UpdateEngineers(JobViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.EngineerJobs)
                             .FirstOrDefault();

            var oldList = model != null ? model.EngineerJobs.Select(x => x.EngineerID.ToString()) : new List<string>();
            var newList = viewModel.EngineerIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd = newList.Except(oldList).ToList();

            var repo = new EngineerJobRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.EngineerID.ToString() == id && x.JobID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new EngineerJob() { EngineerID = Guid.Parse(id), JobID = Guid.Parse(viewModel.ID) });
            }
        }

        private void UpdateRequestors(JobViewModel viewModel)
        {
            var model = _repo.Read()
                             .Where(x => x.ID.ToString() == viewModel.ID)
                             .Include(x => x.RequestorJobs)
                             .FirstOrDefault();

            var oldList = model != null ? model.RequestorJobs.Select(x => x.RequestorID.ToString()) : new List<string>();
            var newList = viewModel.RequestorIDs ?? new List<string>();

            var listToDelete = oldList.Except(newList).ToList();
            var listToAdd= newList.Except(oldList).ToList();

            var repo = new RequestorJobRepository();
            foreach (var id in listToDelete)
            {
                repo.Delete(repo.Read().Where(x => x.RequestorID.ToString() == id && x.JobID.ToString() == viewModel.ID));
            }
            foreach (var id in listToAdd)
            {
                repo.Create(new RequestorJob() { RequestorID = Guid.Parse(id), JobID = Guid.Parse(viewModel.ID) });
            }
        }
    }
    
}