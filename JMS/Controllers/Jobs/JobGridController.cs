﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JMS.Controllers
{
    [Authorize]
    public class JobGridController : GridController<Job, JobGridViewModel>
    {

        public JobGridController()
            : base(new JobRepository(), new JobGridMapper())
        {

        }


        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.CheckSubscription();

            //if (ViewBag.ViewCRM == false)
            //   Response.Redirect("/");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string filterText, int dateType = 0, string fromD = "", string toD = "", string requestorID = "", string departmentID = "", List<int> statuses = null, List<Guid?> relatedDepartmentIDs = null, List<Guid> relatedRequestorIDs = null, List<Guid> relatedEngineerIDs = null, List<Guid?> jobTypes = null, List<Guid?> relatedEquipmentIDs = null, List<Guid?> relatedLocationIDs = null, List<Guid?> relatedProjectCodeIDs = null)
        {
            //User passed parameters oo filter results

            DateTime fromDate;
            DateTime toDate;
            statuses = statuses ?? new List<int>();
            relatedDepartmentIDs = relatedDepartmentIDs ?? new List<Guid?>();
            relatedRequestorIDs = relatedRequestorIDs ?? new List<Guid>();
            relatedEngineerIDs = relatedEngineerIDs ?? new List<Guid>();
            jobTypes = jobTypes ?? new List<Guid?>();
            relatedEquipmentIDs = relatedEquipmentIDs ?? new List<Guid?>();
            relatedLocationIDs = relatedLocationIDs ?? new List<Guid?>();
            relatedProjectCodeIDs = relatedProjectCodeIDs ?? new List<Guid?>();
            //Set the Date range for the query
            //When set the 'To Date' set the time to "23:59" by adding a day then removing 1 second. This is because the new date instances times start at midnight.
            //Example: 00:00 31st March -> 00:00 1st April -> 23:59 31st March.
            switch (dateType)
            {
                case 1: //Current Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day);
                    toDate = fromDate.AddMonths(1).AddSeconds(-1);
                    break;
                case 2: //Last Month
                    fromDate = DateTime.Today.AddDays(1 - (int)DateTime.Today.Day).AddMonths(-1);
                    toDate = fromDate.AddMonths(1).AddSeconds(-1);
                    break;
                case 3: //Custom Date
                    fromDate = DateTime.Parse(fromD);
                    toDate = DateTime.Parse(toD).AddDays(1).AddSeconds(-1);
                    break;
                default:
                    toDate = DateTime.Today.AddYears(20);
                    fromDate = DateTime.Today.AddYears(-20);
                    break;
            }

            var list = _repo.Read().Where(
                         x =>
                         (
                             (x.Title.ToLower().Contains(filterText.ToLower())
                               || x.JobID.ToString().Contains(filterText.ToLower())
                             ) || filterText.Equals("") || filterText.Equals(null)
                         ))
                         .Where(x => dateType == 0 || (x.SubmittedDate >= fromDate && x.SubmittedDate <= toDate))
                         .Where(x => departmentID.Equals("") || departmentID.Equals(null) || x.DepartmentID.ToString() == departmentID)
                         .Where(x => requestorID.Equals("") || requestorID.Equals(null) || x.RequestorJobs.Select(r => r.RequestorID.ToString()).Contains(requestorID))
                         .Where(x => !relatedDepartmentIDs.Any() || relatedDepartmentIDs.Contains(x.DepartmentID))
                         .Where(x => !relatedRequestorIDs.Any() || relatedRequestorIDs.Any(id => x.RequestorJobs.Select(d => d.RequestorID).Contains(id)))
                         .Where(x => !relatedEngineerIDs.Any() || relatedEngineerIDs.Any(id => x.EngineerJobs.Select(d => d.EngineerID).Contains(id)))
                         .Where(x => !jobTypes.Any() || jobTypes.Contains(x.JobTypeID))
                         .Where(x => !relatedEquipmentIDs.Any() || relatedEquipmentIDs.Contains(x.EquipmentNoID))
                         .Where(x => !relatedLocationIDs.Any() || relatedLocationIDs.Contains(x.LocationID))
                         .Where(x => !relatedProjectCodeIDs.Any() || relatedProjectCodeIDs.Contains(x.ProjectCodeID))
                         .Where(x => statuses.Contains(x.StatusJobID));

            //Correct sorts
            for (int x = request.Sorts.Count() - 1; x >= 0; x--)
            {
                if (request.Sorts[x].Member == "JobTypeID") request.Sorts[x].Member = "JobType.Name";
                if (request.Sorts[x].Member == "ProjectCodeID") request.Sorts[x].Member = "ProjectCode.Code";
                if (request.Sorts[x].Member == "LocationID") request.Sorts[x].Member = "Location.Name";
                if (request.Sorts[x].Member == "DepartmentID") request.Sorts[x].Member = "Department.Name";
            }

            list = ApplyQueryFiltersSort(request, list, "JobID desc");

            var results = list.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize).ToList().Select(clt => _mapper.MapToViewModel(clt));
            DataSourceResult result = new DataSourceResult { Data = results.ToList(), Total = list.Count() };
            return Json(result);
        }

        public FileResult Export()
        {
            var jobs = _repo.Read().Where(j => j.StatusJobID == JobStatusValues.Open || j.StatusJobID == JobStatusValues.OpenOnHold)
                                   .Where(j => j.JobType == null || j.JobType.Name != "purchase")
                                   .ToList();

            var workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            var sheet = workbook.CreateSheet();
            var headerRow = sheet.CreateRow(0);

            int x = 0;

            //Set the column names in the header row
            headerRow.CreateCell(x).SetCellValue("JID"); x++;
            headerRow.CreateCell(x).SetCellValue("Title"); x++;
            headerRow.CreateCell(x).SetCellValue("Department"); x++;
            headerRow.CreateCell(x).SetCellValue("Planned Start"); x++;
            headerRow.CreateCell(x).SetCellValue("Est Complete"); x++;
            headerRow.CreateCell(x).SetCellValue("Percent Complete"); x++;
            headerRow.CreateCell(x).SetCellValue("Job Status"); x++;
            headerRow.CreateCell(x).SetCellValue("Job Type"); x++;
            headerRow.CreateCell(x).SetCellValue("Urgency"); x++;
            headerRow.CreateCell(x).SetCellValue("Job On Harvest"); x++;
            headerRow.CreateCell(x).SetCellValue("Eng1"); x++;
            headerRow.CreateCell(x).SetCellValue("Eng2"); x++;
            headerRow.CreateCell(x).SetCellValue("Eng3"); x++;
            headerRow.CreateCell(x).SetCellValue("Eng4"); x++;

            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            NPOI.SS.UserModel.IDataFormat dataFormatCustom = workbook.CreateDataFormat();
            var dateStyle = workbook.CreateCellStyle();
            dateStyle.DataFormat = dataFormatCustom.GetFormat("dd-MMM-yy");
            foreach (var job in jobs)
            {
                x = 0;

                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(x).SetCellValue(job.JobID); x++;
                row.CreateCell(x).SetCellValue(job.Title); x++;
                row.CreateCell(x).SetCellValue(job.Department != null ? job.Department.Code : ""); x++;
                
                var cellStartDate = row.CreateCell(x);
                cellStartDate.CellStyle = dateStyle;
                if (job.PlannedStartDate != null)
                {
                    cellStartDate.SetCellValue(job.PlannedStartDate.Value); x++;
                }
                else
                {
                    cellStartDate.SetCellValue(""); x++;
                }

                var cellCompleteDate = row.CreateCell(x);
                cellCompleteDate.CellStyle = dateStyle;
                if (job.EstCompleteionDate != null)
                {
                    cellCompleteDate.SetCellValue(job.EstCompleteionDate.Value); x++;

                }
                else
                {
                    cellCompleteDate.SetCellValue(""); x++;
                }
                row.CreateCell(x).SetCellValue(job.PercentComplete); x++;
                row.CreateCell(x).SetCellValue(job.JobStatus.Name); x++;
                row.CreateCell(x).SetCellValue(job.JobType.Name); x++;
                row.CreateCell(x).SetCellValue(job.UrgencyLevel.Name); x++;
                row.CreateCell(x).SetCellValue(job.OnHarvest == true ? "True" : "False"); x++;
                foreach (var eng in job.EngineerJobs)
                {
                    row.CreateCell(x).SetCellValue(eng.Engineer.ShortName); x++;
                }
            }

            for (var i = 0; i < 14; i++)
                sheet.AutoSizeColumn(i);

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel", //MIME type of Excel files
                string.Format("Jobs In Progress {0}.xls", DateTime.Today.ToString("dd-MM-yyyy")));     //Suggested file name in the "Save as" dialog which will be displayed to the end user
        }

    }
    
}