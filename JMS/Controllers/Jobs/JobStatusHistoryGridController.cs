﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize]
    public class JobStatusHistoryGridController : GridController<JobStatusHistory, JobStatusHistoryGridViewModel>
    {

        public JobStatusHistoryGridController()
            : base(new JobStatusHistoryRepository(), new JobStatusHistoryMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string jobID)
        {
            var list = _repo.Read().Where(p => p.JobID.ToString() == jobID)
                                   .OrderByDescending(o => o.Timestamp)
                                   .ToList().Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

        public override JsonResult Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json( new List<JobStatusHistoryGridViewModel>().ToDataSourceResult(request));
        }


        public override JsonResult Create(DataSourceRequest request, JobStatusHistoryGridViewModel viewModel)
        {
            var result = base.Create(request, viewModel);

            return result;
        }

    }

}