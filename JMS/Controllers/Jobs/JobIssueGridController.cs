﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using JMS.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Routing;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;

namespace JMS.Controllers
{
    [Authorize]
    public class JobIssueGridController : GridController<JobIssue, JobIssueGridViewModel>
    {

        public JobIssueGridController()
            : base(new JobIssueRepository(), new JobIssueMapper())
        {

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Read([DataSourceRequest]DataSourceRequest request, string jobID)
        {
            var list = _repo.Read().Where(p => p.JobID.ToString() == jobID)
                                   .OrderByDescending(o => o.EstCompleteDate)
                                   .ToList()
                                   .Select(p => _mapper.MapToViewModel(p));
            return Json(list.ToDataSourceResult(request));
        }

    }

}