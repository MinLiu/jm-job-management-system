﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kendo.Mvc.UI
{
    public class PurchaseDataSourceResult : Kendo.Mvc.UI.DataSourceResult
    {
        public decimal PurchaseTotal { get; set; }
    }
}