﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SnapSuite.Startup))]
namespace SnapSuite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            JMS.Schedules.Schedule.ScheduleStart();
        }
    }
}
