﻿using JMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Used in the generation of Word Document images
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Drawing;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Drawing.Imaging;


public static class Miscellaneous
{

    public static void CheckLoginMisuse(User User, ControllerContext Context, HttpRequestBase Request, HttpResponseBase Response)
    {
        if (User.Token != (Request.Cookies["JMS"] ?? new HttpCookie("JMS")).Value) {
            Context.HttpContext.GetOwinContext().Authentication.SignOut();
            //Controller.RedirectToAction("Index", "Home");
            Response.Redirect("/");
        }
    }


    public static string Truncate(string source, int length)
    {
        if (source.Length > length)
        {
            source = source.Substring(0, length);
        }
        return source;
    }



    public static Drawing CreateHeaderImageElement(string filename, HeaderPart headerPart, HttpServerUtilityBase server, double maxWidthCm = 4.0, bool autoResize = false, bool matchHeight = false)
    {
        //ImagePart imagePart = headerPart.AddImagePart(ImagePartType.Jpeg);
        ImagePart imagePart = headerPart.AddImagePart(GetImagePartType(filename));
        string relationshipId = headerPart.GetIdOfPart(imagePart);

        return _CreateImageElement(filename, server, imagePart, relationshipId, maxWidthCm, autoResize, matchHeight);
    }

    public static Drawing CreateFooterImageElement(string filename, FooterPart footerPart, HttpServerUtilityBase server, double maxWidthCm = 4.0, bool autoResize = false, bool matchHeight = false)
    {
        //ImagePart imagePart = footerPart.AddImagePart(ImagePartType.Jpeg);
        ImagePart imagePart = footerPart.AddImagePart(GetImagePartType(filename));
        string relationshipId = footerPart.GetIdOfPart(imagePart);

        return _CreateImageElement(filename, server, imagePart, relationshipId, maxWidthCm, autoResize, matchHeight);
    }

    public static Drawing CreateImageElement(string filename, WordprocessingDocument wordDoc, HttpServerUtilityBase server, double maxWidthCm = 4.0, bool autoResize = false, bool matchHeight = false)
    {        
        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
        //ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
        ImagePart imagePart = mainPart.AddImagePart(GetImagePartType(filename));
        string relationshipId = mainPart.GetIdOfPart(imagePart);

        return _CreateImageElement(filename, server, imagePart, relationshipId, maxWidthCm, autoResize, matchHeight);
    }


    private static Drawing _CreateImageElement(string filename, HttpServerUtilityBase server, ImagePart imagePart, string relationshipId,  double maxWidthCm = 4.0, bool autoResize = false, bool matchHeight = false)
    {
        
        Bitmap image = null;
        var imgName = filename.Split('\\').Last();

        using (FileStream stream = new FileStream(server.MapPath(filename), FileMode.Open, FileAccess.Read, FileShare.Read))
        {
            imagePart.FeedData(stream);

            //Bitmap imageOriginal = new Bitmap(stream);
            //image = new Bitmap(imageOriginal.Width, imageOriginal.Height);
            //using (Graphics g = Graphics.FromImage(image))
            //{
            //    g.Clear(System.Drawing.Color.White);
            //    g.DrawImage(imageOriginal, 0, 0, imageOriginal.Width, imageOriginal.Height);
                
            //    using (MemoryStream ms = new MemoryStream())
            //    {
            //        image.Save(ms, ImageFormat.Jpeg);
            //        ms.Position = 0;
            //        imagePart.FeedData(ms);
            //    }
            //}
        }

        image = new Bitmap(imagePart.GetStream());
        long widthPx = (long)image.Width;
        long heightPx = (long)image.Height;
        const long emusPerInch = 914400L;
        const long emusPerCm = 360000L;
        const double dpi = 96.0;
        var widthEmus = (long)(widthPx / dpi * emusPerInch);
        var heightEmus = (long)(heightPx / dpi * emusPerInch);
        var maxWidthEmus = (long)(maxWidthCm * emusPerCm);

        //Scale the image to make sure it does not exceed max width
        if (widthEmus > maxWidthEmus || autoResize)
        {
            var ratio = (heightEmus * 1.0m) / widthEmus;
            widthEmus = maxWidthEmus;
            heightEmus = (long)(widthEmus * ratio);
        }

        if ((heightEmus > maxWidthEmus || autoResize) && heightEmus > widthEmus && matchHeight)
        {
            var ratio = (widthEmus * 1.0m) / heightEmus;
            heightEmus = maxWidthEmus;
            widthEmus = (long)(heightEmus * ratio);
        }

        

        // Define the reference of the image.
        // Code borrowed from https://msdn.microsoft.com/en-us/library/office/bb497430.aspx. How no idea how it works
        Drawing element =
             new Drawing(
                 new DW.Inline(

                     new DW.Extent() { Cx = widthEmus, Cy = heightEmus },
                     new DW.EffectExtent() { LeftEdge = 0L, TopEdge = 0L, RightEdge = 0L, BottomEdge = 0L },
                     new DW.DocProperties() { Id = (UInt32Value)1U, Name = imgName },
                     new DW.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoChangeAspect = true }),
                     new A.Graphic(
                         new A.GraphicData(
                             new PIC.Picture(
                                 new PIC.NonVisualPictureProperties(
                                     new PIC.NonVisualDrawingProperties() { Id = (UInt32Value)0U, Name = imgName },
                                     new PIC.NonVisualPictureDrawingProperties()),
                                 new PIC.BlipFill(
                                     new A.Blip(new A.BlipExtensionList(new A.BlipExtension( new DocumentFormat.OpenXml.Office2010.Drawing.UseLocalDpi() ) { Uri = Guid.NewGuid().ToString("B") }))
                                     {
                                         Embed = relationshipId,
                                         CompressionState = A.BlipCompressionValues.HighQualityPrint
                                     },
                                     new A.Stretch(new A.FillRectangle())),
                                 new PIC.ShapeProperties(
                                     new A.Transform2D(new A.Offset() { X = 0L, Y = 0L }, new A.Extents() { Cx = widthEmus, Cy = heightEmus }),
                                     new A.PresetGeometry(new A.AdjustValueList()) { Preset = A.ShapeTypeValues.Rectangle }))
                         ) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                 )
                 {
                     DistanceFromTop = (UInt32Value)0U,
                     DistanceFromBottom = (UInt32Value)0U,
                     DistanceFromLeft = (UInt32Value)0U,
                     DistanceFromRight = (UInt32Value)0U//,
                     //EditId = new Random().Next().ToString("X")
                 });

        //element.
        return element;

    }


    public static ImagePartType GetImagePartType(string filename)
    {
        var fileExtension = filename.Split('.').DefaultIfEmpty("").Last().ToLower();

        switch (fileExtension) {
            case "bmp": return ImagePartType.Bmp;
            case "emf": return ImagePartType.Emf;
            case "gif": return ImagePartType.Gif;
            case "icon": return ImagePartType.Icon;
            case "jpg": return ImagePartType.Jpeg;
            case "jpeg": return ImagePartType.Jpeg;
            case "pcx": return ImagePartType.Pcx;
            case "png": return ImagePartType.Png;
            case "tiff": return ImagePartType.Tiff;
            case "wmf": return ImagePartType.Wmf;
            default: return ImagePartType.Bmp;
        }
    }

    public enum WordDocumentPart { Header, Body, Footer }

    public static ImageCodecInfo GetEncoderInfo(ImageFormat format)
    {

        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

        foreach (ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }

    public static void ReplaceParagraphTexts(List<DocumentFormat.OpenXml.Wordprocessing.Paragraph> paragraphs, Dictionary<string, string> dictionary, bool remove = true)
    {
        //Replace paragrph texts, if text is empty then remove the whole paragraph (Prevent empty newlines).
        //Because a paragraph can be made of multiple Runs, which can sometimes split the paragraph text. 
        //We get the copy the paragraph text into a new paragraph with a single run that has the styling properties of the first run in the paragraph.
        var splitArray = new char[] { '{', '}' };
        RunProperties runPr = null;
        for (int x = paragraphs.Count - 1; x >= 0; x--)
        {

            foreach (var dicItem in dictionary)
            {
                if (paragraphs[x].InnerText.Contains(dicItem.Key))
                {
                    if (string.IsNullOrEmpty(dicItem.Value) && paragraphs[x].InnerText.Split(splitArray, StringSplitOptions.RemoveEmptyEntries).Count() <= 1 && remove)
                    {
                        try { paragraphs[x].Remove(); }
                        catch { }
                    }
                    else
                    {
                        //Get the stying properties of the first run in the paragraph. 
                        //If  it fails to get the style then it should use the previous one found. This fixes the multiple tags in the some paragraph style remove issue. Example: {Tag} {Tag}
                        try
                        {
                            runPr = paragraphs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Run>().Where(r => r.RunProperties != null).First().RunProperties.CloneNode(true) as DocumentFormat.OpenXml.Wordprocessing.RunProperties;
                        }
                        catch { }

                        //Replace tag with dictionary value in the passed paragraph
                        InsertParagraphText2(paragraphs[x], dicItem.Key ?? "", dicItem.Value ?? "");
                    }
                }
            }
        }
    }

    public static void InsertParagraphText2(DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph, string tag, string text)
    {
        /*
         * IMPORTANT:
         * ------------------------------------------------------
         * Replacing the text of a paragraph is NOT simple.
         * Parapraghs are made up of Runs. 
         * The issues is these Runs are in a weried style that splits to paragraph text.
         * 
         * Example:
         * Hello Worl could be <p> <r>Hell<r> <r>o W<r> <r>orl<r> <r>d!<r> <p>
         * 
         * And because each run can have its own formatting and styling a blank text replace will not do.
         * So the text is replaced as follows:
         * 1. Combine the tag Runs.
         * 2. Remove the un-needed runs.
         * 3. Replace the tag text.

        */

        //Get all the runs in the paragraph
        List<DocumentFormat.OpenXml.Wordprocessing.Run> runs = paragraph.Descendants<DocumentFormat.OpenXml.Wordprocessing.Run>().ToList();

        if (runs.Count > 1)
        {
            for (int x = 0; x < runs.Count; x++) //Loop through each run
            {
                //If the run has an opening of a tag '{' but not that same number of closing tags '{' start combining
                if (runs[x].InnerText.Contains("{") && runs[x].InnerText.Count(s => s == '{') != runs[x].InnerText.Count(s => s == '}') && x < runs.Count)
                {
                    for (int y = x + 1; y < runs.Count; y++) //Scan through the runs after.
                    {
                        if (runs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().Count() <= 0) //If the run doesn't have a text, then add one.
                            runs[x].Append(new DocumentFormat.OpenXml.Wordprocessing.Text());

                        //Get the text of the next run
                        var txt = runs[y].Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().DefaultIfEmpty(new DocumentFormat.OpenXml.Wordprocessing.Text()).FirstOrDefault();

                        runs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().First().Text += txt.Text; //Combine the string in current run with next run
                        txt.Text = "<del>"; //Blank the next run text. 

                        //if the run text the same number of opening and closing tags, '{' or '}', then move to the next run in the list.
                        if (runs[x].InnerText.Count(s => s == '{') == runs[x].InnerText.Count(s => s == '}')) break;
                    }
                }
            }

            //Remove the blank runs
            for (int x = 0; x < runs.Count; x++)
            {
                var txt = runs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().DefaultIfEmpty(new DocumentFormat.OpenXml.Wordprocessing.Text()).FirstOrDefault();
                if (txt.Text == "<del>") runs[x].Remove();
            }

            runs = paragraph.Descendants<DocumentFormat.OpenXml.Wordprocessing.Run>().ToList();
        }

        //Go through each run and replace the the tag text.
        for (int x = 0; x < runs.Count; x++)
        {
            var txt = runs[x].Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().First();
            if (!txt.Text.Contains(tag)) continue;
            txt.Text = txt.Text.Replace(tag, text);
        }
    }
    
}



