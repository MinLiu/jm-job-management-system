﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace JMS.Models
{
    public static class EmailService
    {
        public static bool SendEmail(string email, string subject, string message, HttpServerUtilityBase server)
        {
            try
            {
                using (var context = new SnapDbContext())
                {
                    var settings = context.EmailSettings.First();

                    //Create Email
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.To.Add(email);
                    mailMessage.From = new MailAddress(settings.Email);
                    mailMessage.Subject = subject;

                    mailMessage.Body = String.Format("<div class=\"row\"> <div class=\"col-sm-12\"> <p><img src=\"cid:email_logo.png\" style=\"max-height:30px;\"/></p> <hr /><br />{0} </div> </div>",
                                                       message.Replace("\n", "<br />"));
                    mailMessage.IsBodyHtml = true;

                    try { Attachment img1 = new Attachment(server.MapPath("/Content/email_logo.png")); img1.ContentId = "email_logo.png"; mailMessage.Attachments.Add(img1); }
                    catch { }

                    var emailer = new Fruitful.Email.Emailer(context);
                    emailer.SendEmail(mailMessage);

                    //Fruitful.Email.SendGridEmailer.SendEmail(mailMessage);


                    return true;
                }
            }
            catch
            {
                return false;
            }

        }

        public static bool SendEmails(string[] emails, string subject, string message, HttpServerUtilityBase server)
        {
            try
            {
                using (var context = new SnapDbContext())
                {
                    var settings = context.EmailSettings.First();

                    MailMessage mailMessage = new MailMessage();
                    foreach (string e in emails)
                    {
                        mailMessage.To.Add(e);
                    }

                    mailMessage.From = new MailAddress(settings.Email);
                    mailMessage.Subject = subject;

                    mailMessage.Body = String.Format("<div class=\"row\"> <div class=\"col-sm-12\"> <p><img src=\"cid:email_logo.png\" style=\"max-height:30px;\"/></p> <hr /><br />{0} </div> </div>",
                                                       message.Replace("\n", "<br />"));
                    mailMessage.IsBodyHtml = true;

                    try { Attachment img1 = new Attachment(server.MapPath("/Content/email_logo.png")); img1.ContentId = "email_logo.png"; mailMessage.Attachments.Add(img1); }
                    catch { }

                    var emailer = new Fruitful.Email.Emailer(context);
                    emailer.SendEmail(mailMessage);

                    //Fruitful.Email.SendGridEmailer.SendEmail(mailMessage);

                    return true;
                }
            }
            catch
            {
                return false;
            }

        }

        public static bool SendEmail(MailMessage mailMessage, HttpServerUtilityBase server)
        {
            try
            {

                using (var context = new SnapDbContext())
                {
                    var emailer = new Fruitful.Email.Emailer(context);
                    emailer.SendEmail(mailMessage);

                    //Fruitful.Email.SendGridEmailer.SendEmail(mailMessage);

                    return true;
                }
            }
            catch
            {
                return false;
            }

        }
    }
}
