﻿

//Important:
//---------------------
//The global variables are declared and set on the main edit page.
// - purchaseOrderID
// - sectionID

function Refresh() {
    RefreshHeader();
    RefreshItems();
    KeepScrollPosition();
}


function RefreshHeader() {
    setTimeout(function () {
        var url = '/PurchaseOrders/_Header?purchaseOrderID=' + purchaseOrderID;
        $("#div_Header").load(url);
    }, 500);
}


function RefreshSections() {
    setTimeout(function () {
        var url = '/PurchaseOrders/_ItemsTabURL?purchaseOrderID=' + purchaseOrderID;
        var ts = $("#Items_Tabstrip").data("kendoTabStrip");
        var item = ts.contentElement(0);
        $(item).load(url);
        KeepScrollPosition();
    }, 200);
}


function RefreshItems() {

    $('[id*="GridItems"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();

        $('[id*="divSectionTotal"]').each(function () {
            var url = '/PurchaseOrders/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}

function RefreshSectionItems(sectionID) {
    $('[id*="GridItems' + sectionID + '"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();

        $('[id*="divSectionTotal"]').each(function () {
            var url = '/PurchaseOrders/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}

function OnEditClientContactAddress() {
    HideViews();
    RefreshHeader();
}

function OnEditSupplierContactAddress() {
    HideViews();
    RefreshHeader();
}


function OnGridItemsCancel(e) {
    RefreshItems();
}


function AddProducts(newSecID) {
    $("#SlideContent_AddProducts").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddProducts").fadeIn();
    $("#Button_Close_Content").fadeIn();

    sectionID = newSecID;

    var supplier = $("#SupplierID").data('kendoDropDownList').value();
    if (supplier == "") supplier = null;
    $("#DD_Suppliers").data('kendoDropDownList').value(supplier);
    $("#GridAddProducts").data("kendoGrid").dataSource.read();
    KeepScrollPosition();
}


function AddProductItem(pdtID, qty) {

    //sectionID set in function approve
    $.ajax({
        url: '/PurchaseOrders/_AddProductItem',
        type: 'POST',
        data: {
            "id": pdtID,
            "purchaseOrderID": purchaseOrderID,
            "sectionID": sectionID,
            "qty": qty,
        },
        dataType: "json",
        success: function (data) {
            ProductAdded();  //function found on main Edit view
            RefreshSectionItems(sectionID);
        }
    });
}



function AddCustomProduct(sectionID) {
    var url = '/PurchaseOrders/_AddCustomProduct?purchaseOrderID=' + purchaseOrderID + '&sectionID=' + sectionID;
    $("#div_AddCustom").load(url);

    $("#SlideContent_AddCustom").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").fadeIn();
    $("#Button_Close_Content").fadeIn();
    KeepScrollPosition();
}


function AssociateQuotation() {
    var url = '/PurchaseOrders/_PickAssociateQuotation?purchaseOrderID=' + purchaseOrderID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AssociateJob() {
    var url = '/PurchaseOrders/_PickAssociateJob?purchaseOrderID=' + purchaseOrderID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function ShowApplyVAT() {
    var url = '/PurchaseOrders/_ApplyVAT?purchaseOrderID=' + purchaseOrderID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddSupplier() {
    var url = '/Suppliers/_AddSupplierPopup?OrderID=' + purchaseOrderID + "&Type=PurchaseOrder";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddClient() {
    var url = '/Clients/_AddClientPopup?OrderID=' + purchaseOrderID + "&Type=PurchaseOrder";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddClientAddress(delivery) {
    var url = '/Clients/_AddClientAddressPopup?OrderID=' + purchaseOrderID + "&Delivery=" + delivery + "&Type=PurchaseOrder";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditSupplier() {
    var url = '/Suppliers/_EditSupplierPopup?OrderID=' + purchaseOrderID + "&Type=PurchaseOrder";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClient() {
    var url = '/Clients/_EditClientPopup?OrderID=' + purchaseOrderID + "&Type=PurchaseOrder";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClientAddress(delivery) {
    var url = '/Clients/_EditClientAddressPopup?OrderID=' + purchaseOrderID + "&Delivery=" + delivery + "&Type=PurchaseOrder";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}


function NewSupplierDeliveryNote() {
    var url = '/SupplierDeliveryNote/_NewSupplierDeliveryNote?purchaseOrderID=' + purchaseOrderID;
    $("#Edit_Body").load(url);

    $("#SlideContent_SupplierContent").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_SupplierContent").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditSupplierDeliveryNote(id) {
    var url = '/SupplierDeliveryNote/_EditSupplierDeliveryNote?ID=' + id;
    $("#Edit_Body").load(url);

    $("#SlideContent_SupplierContent").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_SupplierContent").fadeIn();
    $("#Button_Close_Content").fadeIn();
}


function NewSupplierInvoice() {
    var url = '/SupplierInvoice/_NewSupplierInvoice?purchaseOrderID=' + purchaseOrderID;
    $("#Edit_Body").load(url);

    $("#SlideContent_SupplierContent").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_SupplierContent").fadeIn();
    $("#Button_Close_Content").fadeIn();
}


function EditSupplierInvoice(id) {
    var url = '/SupplierInvoice/_EditSupplierInvoice?ID=' + id;
    $("#Edit_Body").load(url);

    $("#SlideContent_SupplierContent").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_SupplierContent").fadeIn();
    $("#Button_Close_Content").fadeIn();
}



function HideViews() {
    $("#SlideContent_AddProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_SupplierContent").animate({ width: '0%' }, { queue: false, duration: 300 });
    setTimeout(function () {
        $("#SlideContent_AddProducts").fadeOut();
        $("#SlideContent_AddCustom").fadeOut();
        $("#Button_Close_Content").fadeOut();
        $("#Content_ItemPhoto").fadeOut();
        $("#SlideContent_SupplierContent").fadeOut();
        $("#div_itemPhoto").html("");
        $("#Edit_Body").html("");
        $("#div_AddCustom").empty();
    }, 300);
    KeepScrollPosition();
}

function ProductAdded() {
    $("#div_Added").fadeIn();
    setTimeout(function () { $("#div_Added").fadeOut(); }, 2000);

    RefreshHeader();
    RefreshItems();
}


function OpenItemImage(i) {
    var url = '/PurchaseOrders/_ItemImage' + '/?id=' + i;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function OnItemImageUploadSuccess(ee) {
    //HideViews();
    RefreshItems();
    OpenItemImage($('#input_ItemID').val());

    /*
    var response = ee.response.data();
    alert(response);

    if (ee.response.status == "error") {
        alert("Error: " + ee.response.message);
    }
    else if (ee.response.status == "success") {
        //OpenItemImage(e.response.message);
    }
    */
}

function OnItemImageUpload(e) {
    var files = e.files;
    $.each(files, function () {
        if (this.size / 1024 / 1024 > 1) {
            alert("Max 1Mb file size is allowed!")
            e.preventDefault();
        }
    });
}


function RemoveItemImage(id) {
    $.ajax({
        url: '/PurchaseOrderItems/RemoveItemImage',
        type: 'POST',
        data: { "id": id },
        dataType: "json",
        success: function (data) {
            //HideViews();
            OpenItemImage(id);
            RefreshItems();
        }
    });
}


function KeepScrollPosition() {
    $("html, body").animate({ scrollTop: window.pageYOffset }, 100);
}

function GoToBottomPage() {
    //Scroll to the bottom of page.
    //Delay the scroll to stop conflict with GridCreated scroll animation.
    setTimeout(function () {
        $("html, body").animate({ scrollTop: $(document).height() }, 100);
    }, 130);
}


function ShowSavedMessage() {
    $("#div_Saved").fadeIn();
    setTimeout(function () { $("#div_Saved").fadeOut(); }, 2000);
}



function OnGridItemsRequestEnd(e) {
    //RequestEnd handler code
    if (e.type != 'read') {
        Refresh();
    }
}


function AddBlankLine(sectionID) {
    $.ajax({
        url: '/PurchaseOrderItems/_AddBlankItem',
        type: 'POST',
        data: { purchaseOrderID: purchaseOrderID, sectionID: sectionID },
        dataType: "json",
        success: function (data) { RefreshItems(); KeepScrollPosition(); }
    });
}

function AddItemToProduct(id) {

    var url = '/PurchaseOrderItems/_AddItemToProduct/?id=' + id + "&purchaseOrderID=" + purchaseOrderID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
    //KeepScrollPosition();
    return false;
}


function ConvertItemToCustom(id) {
    $.ajax({
        url: '/PurchaseOrderItems/_ConvertItemToCustom',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function ConvertItemToKit(id) {
    $.ajax({
        url: '/PurchaseOrderItems/_ConvertItemToKit',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function ConvertService(id, isService) {
    $.ajax({
        url: '/PurchaseOrderItems/_ConvertService',
        type: 'POST',
        data: { id: id, isService: isService },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function SetUseKitPrice(id, useKitPrice) {
    $.ajax({
        url: '/PurchaseOrderItems/_SetUseKitPrice',
        type: 'POST',
        data: { id: id, useKitPrice: useKitPrice },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function AddSection(id) {
    $.ajax({
        url: '/PurchaseOrderSections/_AddSection',
        type: 'POST',
        data: { purchaseOrderID: purchaseOrderID },
        dataType: "json",
        success: function (data) { RefreshSections(); GoToBottomPage(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}


function CopySection(id) {
    $.ajax({
        url: '/PurchaseOrderSections/_CopySection',
        type: 'POST',
        data: { sectionID: id },
        dataType: "json",
        success: function (data) { RefreshSections(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}


function RemoveSection(id) {
    $.ajax({
        url: '/PurchaseOrderSections/_RemoveSection',
        type: 'POST',
        data: { sectionID: id },
        dataType: "json",
        success: function (data) { RefreshSections(); RefreshHeader(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}

function ReorderSection(sectionID, direction) {
    $.ajax({
        url: '/PurchaseOrderSections/_ReorderSection',
        type: 'POST',
        data: { sectionID: sectionID, direction: direction },
        dataType: "json",
        success: function (data) { RefreshSections(); },
    });

    //KeepScrollPosition();
    return false;
}


function GridItemsDatabound(e) {
    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() == 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();
        var msg = options.pageable.messages.empty;
        if (!msg) msg = "No Records Found."; // Default message

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row no-drag'><td colspan='" + colCount + "'><h1 style='color:lightgray; text-align:center; margin:40px;'>" + msg + "</h1></td></tr>");
    }

    //If the page number is less 1 then hide pager.
    var pages = grid.dataSource.total() / grid.dataSource.pageSize();
    if (pages < 1) {
        grid.pager.element.hide();
    }

    /*
    //Selects all delete buttons
    $("#GridItems tbody tr .k-grid-delete").each(function () {
        var currentDataItem = $("#GridItems").data("kendoGrid").dataItem($(this).closest("tr"));

        //Check in the current dataItem if the row is deletable
        if (currentDataItem.IsKitItem) {
            $(this).remove();
        }
    })
    */

    var rows = e.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        try {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);

            if (dataItem.get("IsKitItem") == true) {
                row.css('background-color', 'LightGrey');
                row.css('font-style', 'italic');
            }
        }
        catch (err) { }
    }

}


function OnGridItemsEdit(e) {
    //Hide input based on the type of line item
    if (e.model.IsComment || e.model.ID == 0) {

        e.container.find("input[name='ProductCode']").each(function () { $(this).hide() });
        e.container.find("input[name='Unit']").each(function () { $(this).hide() });
        e.container.find("input[name='VAT']").each(function () { $(this).hide() });
        //e.container.find("input[name='Markup']").each(function () { $(this).hide() });
        //e.container.find("input[name='ListPrice']").each(function () { $(this).hide() });
        //e.container.find("input[name='Cost']").each(function () { $(this).hide() });
        e.container.find("input[name='Price']").each(function () { $(this).hide() });
        e.container.find("input[name='Quantity']").each(function () { $(this).hide() });
        e.container.find("input[name='QuantityReceived']").each(function () { $(this).hide() });
        e.container.find("input[name='QuantityRemaining']").each(function () { $(this).hide() });
        //e.container.find("input[name='Margin']").each(function () { $(this).hide() });
        //e.container.find("input[name='MarginPrice']").each(function () { $(this).hide() });
        e.container.find("table[id='table_info_input']").each(function () { $(this).hide() });
    }

}
