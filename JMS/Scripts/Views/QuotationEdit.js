﻿

//Important:
//---------------------
//The global variables are declared and set on the main edit page.
// - quoteID
// - sectionID

function Refresh() {
    RefreshHeader();
    RefreshItems();
    RefreshCostItems();
    RefreshLabourItems();
    KeepScrollPosition();
}

function RefreshHeader() {
    setTimeout(function () {
        var url = '/Quotations/_Header?quoteID=' + quoteID;
        $("#div_Header").load(url);
    }, 500);
}


function RefreshSections() {
    setTimeout(function () {
        var url = '/Quotations/_ItemsTabURL?quoteID=' + quoteID;
        var ts = $("#Items_Tabstrip").data("kendoTabStrip");
        var item = ts.contentElement(0);
        $(item).load(url);
        KeepScrollPosition();
    }, 200);
}

function RefreshItems() {

    $('[id*="GridItems"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();

        $('[id*="divSectionTotal"]').each(function () {
            var url = '/Quotations/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}

function RefreshSectionItems(sectionID) {
    $('[id*="GridItems' + sectionID + '"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();

        $('[id*="divSectionTotal"]').each(function () {
            var url = '/Quotations/_SectionTotal?sectionID=' + $(this).attr("data-section");
            $(this).load(url);
        });
    });
}

function RefreshCostItems() {

    $('[id*="GridCostItems"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();
    });
}

function RefreshLabourItems() {

    $('[id*="GridLabourItems"]').each(function () {
        $(this).data('kendoGrid').dataSource.read();
    });
}


function OnEditClientContactAddress() {
    HideViews();
    RefreshHeader();
}


function OnGridItemsCancel(e) {
    RefreshItems();
}

function AddProducts(newSecID) {

    sectionID = newSecID;
    //var url = '/Quotations/_AddProducts?quoteID=' + quoteID;
    //$("#div_AddProducts").load(url);

    $("#SlideContent_AddProducts").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddProducts").fadeIn();
    $("#Button_Close_Content").fadeIn();
    //$("#GridAddProducts").data("kendoGrid").dataSource.read();
    KeepScrollPosition();
}

function AddCostProducts() {

    $("#SlideContent_AddCostProducts").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCostProducts").fadeIn();
    $("#Button_Close_Content").fadeIn();
    //$("#GridAddCostProducts").data("kendoGrid").dataSource.read();
    KeepScrollPosition();
}


function AddProductItem(pdtID, qty) {

    //sectionID set in function approve
    $.ajax({
        url: '/Quotations/_AddProductItem',
        type: 'POST',
        data: {
            "id": pdtID,
            "quoteID": quoteID,
            "sectionID": sectionID,
            "qty": qty,
        },
        dataType: "json",
        success: function (data) {
            ProductAdded();  //function found on main Edit view
            RefreshSectionItems(sectionID);
        }
    });
}

function AddProductCostItem(pdtID, qty) {

    $.ajax({
        url: '/Quotations/_AddProductCostItem',
        type: 'POST',
        data: {
            "id": pdtID,
            "quoteID": quoteID,
            "qty": qty,
        },
        dataType: "json",
        success: function (data) {
            ProductAdded();  //function found on main Edit view
            $('#GridCostItems').data('kendoGrid').dataSource.read();
        }
    });
}

function AddLabourItem(labID, qty) {

    $.ajax({
        url: '/Quotations/_AddLabourItem',
        type: 'POST',
        data: {
            "id": labID,
            "quoteID": quoteID,
            "qty": qty,
        },
        dataType: "json",
        success: function (data) {
            ProductAdded();  //function found on main Edit view
            $('#GridLabourItems').data('kendoGrid').dataSource.read();
        }
    });
}


function AddCustomProduct(sectionID) {
    var url = '/Quotations/_AddCustomProduct?quoteID=' + quoteID + '&sectionID=' + sectionID;
    $("#div_AddCustom").load(url);

    $("#SlideContent_AddCustom").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").fadeIn();
    $("#Button_Close_Content").fadeIn();
    KeepScrollPosition();
}

function AddCustomCostProduct() {
    var url = '/Quotations/_AddCustomCostProduct?quoteID=' + quoteID;
    $("#div_AddCustom").load(url);

    $("#SlideContent_AddCustom").animate({ width: '90%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").fadeIn();
    $("#Button_Close_Content").fadeIn();
    KeepScrollPosition();
}

function AddLabour() {
    var url = '/Quotations/_AddLabour';
    $("#div_AddCustom").load(url);

    $("#SlideContent_AddCustom").animate({ width: '50%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").fadeIn();
    $("#Button_Close_Content").fadeIn();
    KeepScrollPosition();
}

function _PickAssociateInvoice() {
    var url = '/Quotations/_PickAssociateInvoice?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AssociateInvoice(invoiceID) {
    if (!confirm('Are you sure you want to associate this invoice?')) return;
    $.ajax({
        url: '/Quotations/AssociateInvoice',
        type: 'POST',
        data: {
            quoteID: quoteID,
            invoiceID: invoiceID
        },
        success: function(response) {
            $('#GridAssociateInvoices').data('kendoGrid').dataSource.read();
            $('#GridInvoices').data('kendoGrid').dataSource.read();
            RefreshHeader();
        }
    })
}

function UnAssociateInvoice(invoiceID) {
    if (!confirm('Are you sure you want to unassociate this invoice?')) return;
    $.ajax({
        url: '/Quotations/AssociateInvoice',
        type: 'POST',
        data: {
            invoiceID: invoiceID
        },
        success: function(response) {
            $('#GridInvoices').data('kendoGrid').dataSource.read();
            RefreshHeader();
        }
    })
}

function _PickAssociatePurchaseOrder() {
    var url = '/Quotations/_PickAssociatePurchaseOrder?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AssociatePurchaseOrder(purchaseOrderID) {
    if (!confirm('Are you sure you want to associate this purchase order?')) return;
    $.ajax({
        url: '/Quotations/AssociatePurchaseOrder',
        type: 'POST',
        data: {
            quoteID: quoteID,
            purchaseOrderID: purchaseOrderID
        },
        success: function(response) {
            $('#GridAssociatePOs').data('kendoGrid').dataSource.read();
            $('#GridPurchaseOrders').data('kendoGrid').dataSource.read();
            RefreshHeader();
        }
    })
}

function UnAssociatePurchaseOrder(purchaseOrderID) {
    if (!confirm('Are you sure you want to unassociate this purchase order?')) return;
    $.ajax({
        url: '/Quotations/AssociatePurchaseOrder',
        type: 'POST',
        data: {
            purchaseOrderID: purchaseOrderID
        },
        success: function(response) {
            $('#GridPurchaseOrders').data('kendoGrid').dataSource.read();
            RefreshHeader();
        }
    })
}

function _PickAssociateJob() {
    var url = '/Quotations/_PickAssociateJob?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AssociateJob(jobID) {
    if (!confirm('Are you sure you want to associate this job?')) return;
    $.ajax({
        url: '/Quotations/AssociateJob',
        type: 'POST',
        data: {
            quoteID: quoteID,
            jobID: jobID
        },
        success: function(response) {
            $('#GridAssociateJobs').data('kendoGrid').dataSource.read();
            $('#GridJobs').data('kendoGrid').dataSource.read();
            RefreshHeader();
        }
    })
}

function UnAssociateJob(jobID) {
    if (!confirm('Are you sure you want to unassociate this purchase order?')) return;
    $.ajax({
        url: '/Quotations/AssociateJob',
        type: 'POST',
        data: {
            jobID: jobID
        },
        success: function(response) {
            $('#GridJobs').data('kendoGrid').dataSource.read();
            RefreshHeader();
        }
    })
}

function ShowApplyDiscount() {
    var url = '/Quotations/_ApplyDiscount?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function ShowApplyMarkup() {
    var url = '/Quotations/_ApplyMarkup?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function ShowApplyMargin() {
    var url = '/Quotations/_ApplyMargin?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function ShowApplyVAT() {
    var url = '/Quotations/_ApplyVAT?quoteID=' + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddClient() {
    var url = '/Clients/_AddClientPopup?OrderID=' + quoteID + "&Type=Quotation";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddClientAddress(delivery) {
    var url = '/Clients/_AddClientAddressPopup?OrderID=' + quoteID + "&Delivery=" + delivery + "&Type=Quotation";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function AddClientContact() {
    var url = '/Clients/_AddClientContactPopup?OrderID=' + quoteID + "&Type=Quotation";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClient() {
    var url = '/Clients/_EditClientPopup?OrderID=' + quoteID + "&Type=Quotation";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClientAddress(delivery) {
    var url = '/Clients/_EditClientAddressPopup?OrderID=' + quoteID + "&Delivery=" + delivery + "&Type=Quotation";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function EditClientContact() {
    var url = '/Clients/_EditClientContactPopup?OrderID=' + quoteID + "&Type=Quotation";
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}


function HideViews() {
    $("#SlideContent_AddProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCostProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustomProducts").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_AddCustom").animate({ width: '0%' }, { queue: false, duration: 300 });
    $("#SlideContent_EditQuoteItem").animate({ height: '0%' }, { queue: false, duration: 300 });
    setTimeout(function () {
        $("#SlideContent_AddProducts").fadeOut();
        $("#SlideContent_AddCostProducts").fadeOut();
        $("#SlideContent_AddCustom").fadeOut();
        $("#SlideContent_AddCustomProducts").fadeOut();
        $("#Button_Close_Content").fadeOut();
        $("#Content_ItemPhoto").fadeOut(); $("#Content_ItemPhoto").css("max-width", "600px");        
        $("#SlideContent_EditQuoteItem").fadeOut();
        $("#div_itemPhoto").html("");
        $("#div_AddCustom").empty();
    }, 300);
    KeepScrollPosition();
}

function EditPriceBreaks(itemID) {
    var url = '/QuotationItemPriceBreaks/_EditQuotationItem?quoteItemID=' + itemID;
    $("#div_EditQuoteItem").load(url);

    $("#SlideContent_EditQuoteItem").animate({ height: '75%' }, { queue: false, duration: 300 });
    $("#SlideContent_EditQuoteItem").fadeIn();
    $("#Button_Close_Content").fadeIn();
    KeepScrollPosition();
}

function ProductAdded() {
    $("#div_Added").fadeIn();
    setTimeout(function () { $("#div_Added").fadeOut(); }, 2000);

    RefreshHeader();
}

function CostProductAdded() {
    $("#div_Added").fadeIn();
    setTimeout(function () { $("#div_Added").fadeOut(); }, 2000);

    RefreshHeader();
}


function OpenItemImage(i) {
    var url = '/Quotations/_ItemImage/?id=' + i;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
}

function OnItemImageUploadSuccess(ee) {
    //HideViews();
    RefreshItems();
    OpenItemImage($('#input_ItemID').val());

    /*
    var response = ee.response.data();
    alert(response);

    if (ee.response.status === "error") {
        alert("Error: " + ee.response.message);
    }
    else if (ee.response.status === "success") {
        //OpenItemImage(e.response.message);
    }
    */
}

function OnItemImageUpload(e) {
    var files = e.files;
    $.each(files, function () {
        if (this.size / 1024 / 1024 > 1) {
            alert("Max 1Mb file size is allowed!")
            e.preventDefault();
        }
    });
}


function RemoveItemImage(id) {
    $.ajax({
        url: '/QuotationItems/RemoveItemImage',
        type: 'POST',
        data: { "id": id },
        dataType: "json",
        success: function (data) {
            //HideViews();
            OpenItemImage(id);
            RefreshItems();
        }
    });
}


function KeepScrollPosition() {
    $("html, body").animate({ scrollTop: window.pageYOffset }, 100);
}

function GoToBottomPage() {
    //Scroll to the bottom of page.
    //Delay the scroll to stop conflict with GridCreated scroll animation.
    setTimeout(function () {
        $("html, body").animate({ scrollTop: $(document).height() }, 100);
    }, 130);
}


function ShowSavedMessage() {
    $("#div_Saved").fadeIn();
    setTimeout(function () { $("#div_Saved").fadeOut(); }, 2000);
}


function OnGridItemsRequestEnd(e) {
    //RequestEnd handler code
    if (e.type !== 'read') {
        Refresh();
    }
}


function AddBlankLine(sectionID) {
    $.ajax({
        url: '/QuotationItems/_AddBlankItem',
        type: 'POST',
        data: { quoteID: quoteID, sectionID: sectionID },
        dataType: "json",
        success: function (data) { RefreshItems(); KeepScrollPosition(); }
    });
}

function AddBlankCostLine() {
    $.ajax({
        url: '/QuotationCostItems/_AddBlankItem',
        type: 'POST',
        data: { quoteID: quoteID },
        dataType: "json",
        success: function (data) { RefreshCostItems(); KeepScrollPosition(); }
    });
}


function AddItemToProduct(id) {

    var url = '/QuotationItems/_AddItemToProduct/?id=' + id + "&quoteID=" + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
    //KeepScrollPosition();
    return false;
}

function AddCostItemToProduct(id) {

    var url = '/QuotationCostItems/_AddItemToProduct/?id=' + id + "&quoteID=" + quoteID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Button_Close_Content").fadeIn();
    //KeepScrollPosition();
    return false;

}

function ConvertItemToCustom(id) {

    $.ajax({
        url: '/QuotationItems/_ConvertItemToCustom',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function ConvertCostItemToCustom(id) {

    $.ajax({
        url: '/QuotationCostItems/_ConvertItemToCustom',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshCostItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}


function ConvertItemToKit(id) {
    $.ajax({
        url: '/QuotationItems/_ConvertItemToKit',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function ConvertCostItemToKit(id) {
    $.ajax({
        url: '/QuotationCostItems/_ConvertItemToKit',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshCostItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function CopyItem(id) {
    $.ajax({
        url: '/QuotationItems/_CopyItem',
        type: 'POST',
        data: { id: id },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function SetUseKitPrice(id, useKitPrice) {
    $.ajax({
        url: '/QuotationItems/_SetUseKitPrice',
        type: 'POST',
        data: { id: id, useKitPrice: useKitPrice },
        dataType: "json",
        success: function (data) { RefreshItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}

function SetUseCostKitPrice(id, useKitPrice) {
    $.ajax({
        url: '/QuotationCostItems/_SetUseKitPrice',
        type: 'POST',
        data: { id: id, useKitPrice: useKitPrice },
        dataType: "json",
        success: function (data) { RefreshCostItems(); RefreshHeader(); },
    });

    //KeepScrollPosition();
    return false;
}



function AddSection(id) {
    $.ajax({
        url: '/QuotationSections/_AddSection',
        type: 'POST',
        data: { quoteID: quoteID },
        dataType: "json",
        success: function (data) { RefreshSections(); GoToBottomPage(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}

function CopySection(id) {
    $.ajax({
        url: '/QuotationSections/_CopySection',
        type: 'POST',
        data: { sectionID: id },
        dataType: "json",
        success: function (data) { RefreshSections(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}


function RemoveSection(id) {
    $.ajax({
        url: '/QuotationSections/_RemoveSection',
        type: 'POST',
        data: { sectionID: id },
        dataType: "json",
        success: function (data) { RefreshSections(); RefreshHeader(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}

function ReorderSection(sectionID, direction) {
    $.ajax({
        url: '/QuotationSections/_ReorderSection',
        type: 'POST',
        data: { sectionID: sectionID, direction: direction },
        dataType: "json",
        success: function (data) { RefreshSections(); },
    });

    //KeepScrollPosition();
    return false;
}


function AddAssociativeItems(itemID, secID) {
    sectionID = secID;

    var url = '/Quotations/_AssociativeItemPopup?itemID=' + itemID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Content_ItemPhoto").css("max-width", "1000px");
    $("#Button_Close_Content").fadeIn();
}

function SwitchAlternativeItems(itemID) {
    var url = '/Quotations/_AlternativeItemPopup?itemID=' + itemID;
    $("#div_itemPhoto").load(url);

    $("#Content_ItemPhoto").fadeIn();
    $("#Content_ItemPhoto").css("max-width", "1000px");
    $("#Button_Close_Content").fadeIn();
}

function SwitchItem(id, pdtID) {

    $.ajax({
        url: '/QuotationItems/_SwitchItem',
        type: 'POST',
        data: {
            id: id,
            productID: pdtID,
            quoteID: quoteID,
        },
        dataType: "json",
        success: function (data) { HideViews(); RefreshItems(); RefreshHeader(); ShowSavedMessage(); },
    });

    //KeepScrollPosition();
    return false;
}


function GridItemsDatabound(e) {
    var grid = e.sender;

    //If no records are returned then insert No Records Message
    if (grid.dataSource.total() === 0) {
        var colCount = grid.columns.length;
        var options = grid.getOptions();
        var msg = options.pageable.messages.empty;
        if (!msg) msg = "No Records Found."; // Default message

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class='kendo-data-row no-drag'><td colspan='" + colCount + "'><h1 style='color:lightgray; text-align:center; margin:40px;'>" + msg + "</h1></td></tr>");
    }

    //If the page number is less 1 then hide pager.
    var pages = grid.dataSource.total() / grid.dataSource.pageSize();
    if (pages < 1) {
        grid.pager.element.hide();
    }

    /*
    //Selects all delete buttons
    $("#GridItems tbody tr .k-grid-delete").each(function () {
        var currentDataItem = $("#GridItems").data("kendoGrid").dataItem($(this).closest("tr"));

        //Check in the current dataItem if the row is deletable
        if (currentDataItem.IsKitItem) {
            $(this).remove();
        }
    })
    */
    SetColours(grid);    
}

function SetColours(grid) {

    var rows = grid.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        try {
            var row = $(rows[j]);
            var dataItem = grid.dataItem(row);

            if (dataItem.get("IsKitItem") === true) {
                row.css('background-color', 'LightGrey');
                row.css('font-style', 'italic');
            }

            if (dataItem.get("IsPriceBreak") === true) {
                row.addClass("no-drag");
                row.css('background-color', 'LightBlue');
                row.css('font-style', 'italic');

                row.find(".k-grid-edit").each(function () { $(this).remove(); });
                row.find(".k-grid-delete").each(function () { $(this).remove(); });
            }
            
        }
        catch (err) { }
    }

}

function OnGridItemsEdit(e) {

    if (e.model.UsePriceBreaks && !e.model.IsComment)
    {
        $(".k-grid[id*='GridItems']").each(function () { $(this).data("kendoGrid").cancelChanges(); });
        EditPriceBreaks(e.model.ID);
        return;
    }
    
    
    //Hide input based on the type of line item
    if (e.model.IsComment || e.model.ID === 0) {

        e.container.find("input[name='ProductCode']").each(function () { $(this).hide() });
        e.container.find("input[name='Unit']").each(function () { $(this).hide() });
        e.container.find("input[name='VAT']").each(function () { $(this).hide() });
        e.container.find("input[name='Markup']").each(function () { $(this).hide() });
        e.container.find("input[name='Discount']").each(function () { $(this).hide() });
        e.container.find("input[name='ListPrice']").each(function () { $(this).hide() });
        e.container.find("input[name='Cost']").each(function () { $(this).hide() });
        e.container.find("input[name='Price']").each(function () { $(this).hide() });
        e.container.find("input[name='Quantity']").each(function () { $(this).hide() });
        e.container.find("input[name='Margin']").each(function () { $(this).hide() });
        e.container.find("input[name='MarginPrice']").each(function () { $(this).hide() });
        e.container.find("input[name='SetupCost']").each(function () { $(this).hide() });
                
        e.container.find("table[id='table_info_input']").each(function () { $(this).hide() });

    }
    if (e.model.HidePrices) {
        e.container.find("input[name='VAT']").each(function () { $(this).hide() });
        e.container.find("input[name='Markup']").each(function () { $(this).hide() });
        e.container.find("input[name='Discount']").each(function () { $(this).hide() });
        e.container.find("input[name='ListPrice']").each(function () { $(this).hide() });
        e.container.find("input[name='Cost']").each(function () { $(this).hide() });
        e.container.find("input[name='Price']").each(function () { $(this).hide() });
        //e.container.find("input[name='Quantity']").each(function () { $(this).hide() });
        e.container.find("input[name='Margin']").each(function () { $(this).hide() });
        e.container.find("input[name='MarginPrice']").each(function () { $(this).hide() });
        e.container.find("input[name='SetupCost']").each(function () { $(this).hide() });
    }
    
    setTimeout(function () {
        SetColours(e.sender);
    }, 40);
    

}


function EditLabourOptions()
{
    var url = '/QuotationSettings/Edit?QuotationID=' + quoteID;
    window.location.href = url
}

