﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JMS.Schedules
{
    public class Schedule
    {
        private static IScheduler sched;

        public static async void ScheduleStart()
        {
            // Construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // Get a scheduler
            sched = await schedFact.GetScheduler();
            await sched.Start();

            // Define the job and tie it to the class
            IJobDetail job_Statistics = JobBuilder.Create<ScheduleStatistics>()
                .WithIdentity("Job_Statistics")
                .Build();

            // Trigger the job to run now, and then every 15 seconds
            ITrigger trigger_Statistics = TriggerBuilder.Create()
                .WithIdentity("Trigger_Statistics")
                .StartNow()
                .WithCronSchedule("0 0 1 1 * ?")
                .Build();

            await sched.ScheduleJob(job_Statistics, trigger_Statistics);
        }
    }
}