﻿using JMS.Models;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JMS.Schedules
{
    [DisallowConcurrentExecution]
    public class ScheduleStatistics: Quartz.IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            GenerateData();
        }

        public void GenerateData()
        {
            try
            {
                using (var dbcontext = new SnapDbContext())
                {
                    DateTime timestamp = new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);

                    if (dbcontext.MonthlyJobsOnHold.Where(x => x.Month == timestamp).Any()) return;

                    var countJobsOnHold = dbcontext.Jobs.Where(x => x.StatusJobID == JobStatusValues.OpenOnHold).Count();
                    var countJobsOpen = dbcontext.Jobs.Where(x => x.StatusJobID == JobStatusValues.Open).Count();

                    dbcontext.MonthlyJobsOnHold.Add(new MonthlyJobsOnHold()
                    {
                        Month = timestamp,
                        Count = countJobsOnHold
                    });
                    dbcontext.MonthlyJobsOpened.Add(new MonthlyJobsOpened()
                    {
                        Month = timestamp,
                        Count = countJobsOpen
                    });
                    dbcontext.SaveChanges();
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}