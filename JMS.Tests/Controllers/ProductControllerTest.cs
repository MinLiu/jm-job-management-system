﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SnapSuite.Controllers;
using SnapSuite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Kendo.Mvc.UI;
using Kendo.Mvc;

namespace SnapSuite.Tests.Controllers
{
    [TestClass]
    public class ProductControllerTest
    {

        private ProductsController controller = new ProductsController { CurrentUser = new User { CompanyID = 1 } };
        private ProductGridController gridController = new ProductGridController { CurrentUser = new User { CompanyID = 1 } };
        private ProductRepository repo = new ProductRepository();
        private ProductMapper mapper = new ProductMapper();
        private ProductGridMapper gridMapper = new ProductGridMapper();

        private DataSourceRequest request = new DataSourceRequest { Sorts = new List<SortDescriptor>(), Aggregates = new List<AggregateDescriptor>(), Filters = new List<IFilterDescriptor>(), Groups = new List<GroupDescriptor>(), Page = 1, PageSize = 10 };

        protected void ClearProducts()
        {
            foreach (var p in repo.Read().ToList())
            {
                repo.Delete(p);
            }
        }

        [TestMethod]
        [Description("Clear database of all products")]
        public void _ClearProducts()
        {
            ClearProducts();
            Assert.AreEqual(0, repo.Read().Count());
        }

        [TestMethod]
        [Description("Does the Main Product (Index) Page Load")]
        public void DoesIndexPageLoad()
        {
            
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [Description("Does the Deleted Product Page Load")]
        public void DoesDeletePageLoad()
        {            
            ViewResult result = controller.Deleted() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [Description("Does the  Product Export Page Load")]
        public void DoesExportPageLoad()
        {            
            ViewResult result = controller.Export() as ViewResult;
            Assert.IsNotNull(result);
        }


        


        [TestMethod]
        [Description("Does main products grid output data")]
        public void DoesReadOutputData()
        {
            JsonResult result = gridController.Read(request, "", 0, false) as JsonResult;
            Assert.IsNotNull(result);
        }


        [TestMethod]
        [Description("Does main products grid output correct number of data entries")]
        public void DoesReadOutputCorrectDataTotal()
        {
            var initalCount = repo.Read().Where(p => p.CompanyID == 1 && p.Deleted == false).Count();

            JsonResult result = gridController.Read(request, "", 0, false) as JsonResult;

            var data = result.Data as Kendo.Mvc.UI.DataSourceResult;
            var outputCount = data.Total;

            Assert.AreEqual(initalCount, outputCount);
        }

        [TestMethod]
        [Description("Does read deleted return the correct number of data entries")]
        public void DoesReadDeletedOutputCorrectTotal()
        {

            for (int x = 0; x < 5; ++x)
            {
                var rand = Guid.NewGuid().ToString("N");
                repo.Create(new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = true });
            }            

            var initalCount = repo.Read().Where(p => p.CompanyID == 1 && p.Deleted == true).Count();

            JsonResult result = gridController.Read(request, "", 0, true) as JsonResult;
            var outputCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            Assert.AreEqual(initalCount, outputCount);
        }


        [TestMethod]
        [Description("Does products grid text fitler work")]
        public void DoesReadTextFilterWork()
        {
            
            var rand = Guid.NewGuid().ToString("N");
            repo.Create(new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1 });


            JsonResult firstResult = gridController.Read(request, "", 0, false) as JsonResult;
            var firstCount = (firstResult.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            JsonResult secondResult = gridController.Read(request, "fffffffffffffffffffffffff", 0, false) as JsonResult;
            var secondCount = (secondResult.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            Assert.AreNotSame(firstCount, secondCount);
        }

                

        [TestMethod]
        [Description("Does products grid deleted fitler work")]
        public void DoesReadDeletedFilterWork()
        {

            ClearProducts();

            //Add Live/Regular, not deleted
            for (int x = 0; x < 5; ++x)
            {
                var rand = Guid.NewGuid().ToString("N");
                repo.Create(new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = false });
            }

            //Add Delete
            for (int x = 0; x < 10; ++x)
            {
                var rand = Guid.NewGuid().ToString("N");
                repo.Create(new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = true });
            }
            
            //Should be 5
            JsonResult firstResult = gridController.Read(request, "", 0, false) as JsonResult;
            int firstCount = (firstResult.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            //Should be 10
            JsonResult secondResult = gridController.Read(request, "", 0, true) as JsonResult;
            int secondCount = (secondResult.Data as Kendo.Mvc.UI.DataSourceResult).Total;

           
            Assert.IsTrue((firstCount + 5) == secondCount);
        }



        [TestMethod]
        [Description("Does products grid delete work")]
        public void DoesGridDeleteWork()
        {

            ClearProducts();

            var rand = Guid.NewGuid().ToString("N");
            var model = new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = false};
            repo.Create(model);
            model = repo.Read().Where(x => x.ID == model.ID).Include(x => x.Currency).Include(x => x.Category).First();
            var viewModel = mapper.MapToViewModel(model);

            //Should be 1
            JsonResult result = gridController.Read(request, "", 0, false) as JsonResult;
            int initCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            //Delete Product
            gridController.Delete(request, viewModel);

            //Should be 0
            result = gridController.Read(request, "", 0, false) as JsonResult;
            int finalCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;


            Assert.IsTrue(initCount == 1 && finalCount == 0);
        }


        [TestMethod]
        [Description("Does delete products by IDs work")]
        public void DoesDeleteIDsWork()
        {

            ClearProducts();

            //Add products and make string of IDs
            string IDs = "";
            for (int x = 0; x < 10; x++)
            {
                var rand = Guid.NewGuid().ToString("N");
                var model = new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = false };                
                repo.Create(model);
                IDs += string.Format("{0},", model.ID);
            }


            //Should be 10
            JsonResult result = gridController.Read(request, "", 0, false) as JsonResult;
            int initCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            //Delete Product
            gridController._DeleteProducts(IDs);

            //Should be 0
            result = gridController.Read(request, "", 0, false) as JsonResult;
            int finalCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;


            Assert.IsTrue(initCount == 10 && finalCount == 0);
        }



        [TestMethod]
        [Description("Does restore products by IDs work")]
        public void DoesRestoreIDsWork()
        {
            ClearProducts();

            //Add products and make string of IDs
            var id = 0;
            for (int x = 0; x < 10; x++)
            {
                var rand = Guid.NewGuid().ToString("N");
                var model = new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = true };
                repo.Create(model);
                id = model.ID;
            }


            //Should be 0
            JsonResult result = gridController.Read(request, "", 0, false) as JsonResult;
            int initCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;

            //Delete Product
            gridController._RestoreDeletedProduct(id);

            //Should be 1
            result = gridController.Read(request, "", 0, false) as JsonResult;
            int finalCount = (result.Data as Kendo.Mvc.UI.DataSourceResult).Total;


            Assert.IsTrue(initCount == 0 && finalCount == 1);
        }



        [TestMethod]
        [Description("Does clear deleted products work")]
        public void DoesClearDeletedWork()
        {
            ClearProducts();

            //Add products
            for (int x = 0; x < 6; x++)
            {
                var rand = Guid.NewGuid().ToString("N");
                var model = new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = false };
                repo.Create(model);
            }

            //Add deleted products
            for (int x = 0; x < 4; x++)
            {
                var rand = Guid.NewGuid().ToString("N");
                var model = new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = true };
                repo.Create(model);
            }


            //Should be 10
            int initCount = repo.Read().Where(x => x.CompanyID == 1).Count();

            //Clear Deleted Products
            gridController._ClearDeletedProducts();

            //Should be 6            
            int finalCount = repo.Read().Where(x => x.CompanyID == 1).Count();


            Assert.IsTrue(initCount == 10 && finalCount == 6);
        }


        [TestMethod]
        [Description("Does products grid destroy work")]
        public void DoesGridDestroyWork()
        {
            ClearProducts();

            var rand = Guid.NewGuid().ToString("N");
            var model = new Product { CompanyID = 1, ProductCode = rand, Description = rand, CurrencyID = 1, Deleted = false };
            repo.Create(model);
            model = repo.Read().Where(x => x.ID == model.ID).Include(x => x.Currency).Include(x => x.Category).First();
            var viewModel = gridMapper.MapToViewModel(model);


            //Should be 1
            int initCount = repo.Read().Where(x => x.CompanyID == 1).Count();

            //Clear Deleted Products
            gridController.Destroy(request, viewModel);

            //Should be 0            
            int finalCount = repo.Read().Where(x => x.CompanyID == 1).Count();


            Assert.IsTrue(initCount == 1 && finalCount == 0);
        }



        [TestMethod]
        [Description("Does create new product view get returned")]
        public void CreateNewProductGet()
        {
            ActionResult result = controller._NewProduct() as ActionResult;
            Assert.IsNotNull(result);
        }


        [TestMethod]
        [Description("Does create blank product work")]
        public void CreateNewBlankProduct()
        {
            ClearProducts();

            var viewModel = new ProductViewModel
            {   
                //CurrencyID = 2,
                //CompanyID = 1                
            };

            controller._NewProduct(viewModel);

            var result = repo.Read().Count();

            Assert.IsTrue(result == 0);
        }


        [TestMethod]
        [Description("Does create new product work")]
        public void CreateNewProduct()
        {
            ClearProducts();

            var rand = Guid.NewGuid().ToString("N");
            var viewModel = new ProductViewModel
            {                
                ProductCode = rand,
                Description = rand,
                ImageURL = rand,
                Manufacturer = rand,
                VendorCode = rand,
                Unit = rand,
                ProductCategoryID = null,
                CurrencyID = 2,
                CompanyID = 1,
                Cost = 69,
                Price = 69,
                VAT = 69,
                IsBought = true,
                IsKit = false,
                IsService = true                
            };

            controller._NewProduct(viewModel);

            var model = repo.Read().Where(x => x.ProductCode == rand).First();

            bool result =
            (
                model.ProductCode == viewModel.ProductCode
                && model.Description == viewModel.Description
                && model.ImageURL == viewModel.ImageURL
                && model.Manufacturer == viewModel.Manufacturer
                && model.VendorCode == viewModel.VendorCode
                && model.Unit == viewModel.Unit
                && model.CurrencyID == viewModel.CurrencyID
                && model.CompanyID == viewModel.CompanyID
                && model.Cost == viewModel.Cost
                && model.Price == viewModel.Price
                && model.VAT == viewModel.VAT
                && model.IsBought == viewModel.IsBought
                && model.IsKit == viewModel.IsKit
                && model.IsService == viewModel.IsService
            );

            Assert.IsTrue(result);
        }


        [TestMethod]
        [Description("Does create new product when pass null fail")]
        public void CreateNewProductNull()
        {
            ClearProducts();

            ActionResult result = controller._NewProduct(null);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [Description("Does create new product when pass null not create product")]
        public void CreateNewProductNull2()
        {
            ClearProducts();

            controller._NewProduct(null);

            var result = repo.Read().Count();

            Assert.IsTrue(result == 0);
        }



        [TestMethod]
        [Description("Does edit product view get returned")]
        public void EditProductGet()
        {
            PartialViewResult result = controller._EditProduct(0) as PartialViewResult;
            Assert.IsNotNull(result);
        }


        [TestMethod]
        [Description("Does create new product work")]
        public void EditProduct()
        {
            ClearProducts();

            var model = new Product { ProductCode = "EEE", CompanyID = 1, CurrencyID = 1 };
            repo.Create(model);

            model = repo.Read().Where(x => x.ID == model.ID).Include(x => x.Category).Include(x => x.Currency).First();
            var viewModel = mapper.MapToViewModel(model);

            var rand = Guid.NewGuid().ToString("N");
            
            viewModel.ProductCode = rand;
            viewModel.Description = rand;
            viewModel.ImageURL = rand;
            viewModel.Manufacturer = rand;
            viewModel.VendorCode = rand;
            viewModel.Unit = rand;
            viewModel.ProductCategoryID = null;
            viewModel.CurrencyID = 2;
            viewModel.CompanyID = 1;
            viewModel.Cost = 69;
            viewModel.Price = 69;
            viewModel.VAT = 69;
            viewModel.IsBought = true;
            viewModel.IsKit = false;
            viewModel.IsService = true;
            

            controller._EditProduct(viewModel);

            var newRepo = new ProductRepository();
            var updatedModel = newRepo.Find(viewModel.ID);

            bool result =
            (
                updatedModel.ProductCode == viewModel.ProductCode
                && updatedModel.Description == viewModel.Description
                && updatedModel.ImageURL == viewModel.ImageURL
                && updatedModel.Manufacturer == viewModel.Manufacturer
                && updatedModel.VendorCode == viewModel.VendorCode
                && updatedModel.Unit == viewModel.Unit
                && updatedModel.CurrencyID == viewModel.CurrencyID
                && updatedModel.CompanyID == viewModel.CompanyID
                && updatedModel.Cost == viewModel.Cost
                && updatedModel.Price == viewModel.Price
                && updatedModel.VAT == viewModel.VAT
                && updatedModel.IsBought == viewModel.IsBought
                && updatedModel.IsKit == viewModel.IsKit
                && updatedModel.IsService == viewModel.IsService
            );

            Assert.IsTrue(result);
        }

    }
}
